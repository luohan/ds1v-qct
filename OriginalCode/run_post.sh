#!/bin/bash -e
# 
# syntax: ./run_post.sh (no argument is required)
#
# --------------------------------------------------------------

# set run parameters
IOUT=0    #initial output
FOUT=500  #final

ICOPY=1   #initial run/copy
FCOPY=1  #final

# --------------------------------------------------------------

# set run directories
RUN_DIR=`pwd`
CASE=`basename ${RUN_DIR}`
OUT_DIR=${RUN_DIR}/${CASE}_AVE

if [ -d "${OUT_DIR}" ]; then
rm -r ${OUT_DIR}
fi
mkdir ${OUT_DIR}
echo "Creating ${CASE}_AVE folder: done"

# --------------------------------------------------------------

# compile executable
gfortran -o ds1v_post.exe DS1_post.f90
echo "Compiling executable: done"

# --------------------------------------------------------------

# do for all outputs
for (( i=${IOUT}; i <= ${FOUT}; i++ )); do
ii=$(printf "%03d" $i)

# create input file for each output
cat <<EOF> ${RUN_DIR}/input_post.txt
${i}                      !output to be averaged
${RUN_DIR}/${CASE}_
${OUT_DIR}
${ICOPY}                  !initial copy
${FCOPY}                  !final copy
EOF

# local run
cd ${RUN_DIR}
ulimit -s unlimited
./ds1v_post.exe <input_post.txt >>post.log
echo "Post-processing output $ii: done"

done

rm *.exe *.mod
echo "Deleting post files: done"

# --------------------------------------------------------------

exit

