SUBROUTINE OXYGEN_DISSOCIATION
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
REAL(KIND=8) :: VDC=-1.d0
INTEGER :: J,IRATE=1,JRATE=1 
!--VDC>=0 for VDC model; <0 for tce model
!--IRATE=0 uncorrected rates; 1 corrected rates for recombination and exchange reactions
!--JRATE= same as IRATE but for dissociation reactions
!  
MSP=2  
MMRM=1  
MMVM=1  
!    
IF (JCD == 0) THEN  
  IF(IRM =150 ) MNRE=1   !testing only forward reaction
  MTBP=0  
  MEX=0  
  MMEX=0  
END IF  
!   
MNSR=0  
!  
CALL ALLOCATE_GAS 

!--species 1 is oxygen O2  
SP(1,1)=4.07d-10        !vss/vhs 4.01/4.07
SP(2,1)=273.D00  
SP(3,1)=0.77D00  
SP(4,1)=1.d0            !vss 1./1.4d0 !there is some bug with vss model  
SP(5,1)=5.312D-26       !vss correction in collision subroutine must be tested  
ISPR(1,1)=2  
ISPR(2,1)=0  
SPR(1,1)=5.d00   
ISPV(3)=1               !the number of vibrational modes  
SPVM(1,1,1)=2256.D00    !the characteristic vibrational temperature  
SPVM(2,1,1)=18000.D00   !constant Zv, or the reference Zv  
SPVM(3,1,1)=2256.d0     !-1 for a constant Zv, or the reference temperature  
SPVM(4,1,1)=59971.4D00  !the characteristic dissociation temperature
ISPVM(1,1,1)=2 
ISPVM(2,1,1)=2  
!--species 1 is atomic oxygen O  
SP(1,2)=3.0d-10         !--estimate  
SP(2,2)=273.D00  
SP(3,2)=0.75D00          !SMILE/estimate 0.75/0.80
SP(4,2)=1.D00  
SP(5,2)=2.656D-26  
ISPR(1,2)=0  
ISPV(2)=0


J=1
IF (JCD == 0 .AND. IRM == 150) THEN
!--REACTION 150 IS O2+O>O+O+O (SPARTA)  
  LE(J)=1   
  ME(J)=2   
  KP(J)=2   
  LP(J)=2   
  MP(J)=2   
  CI(J)=VDC 
  AE(J)=8.197d-19  
  AC(J)=1.660d-08
  BC(J)=-1.5d0
  !IF (JRATE == 1) THEN
  !  IF (CI(J) < 0.) THEN !TCE
  !    AC(J)=3.472299d+15/(AVOG*1000.d0)  
  !    BC(J)=4.256304d-01
  !  ELSE
  !    AC(J)=2.735897d22/(AVOG*1000.d0)
  !    BC(J)=-1.554205d0
  !  END IF
  !END IF
  ER(J)=-8.197d-19  
END IF
END SUBROUTINE OXYGEN_DISSOCIATION!  
