!******************************************************************************  
!****DSMC program for one-dimensional plane, cylindrical or spherical flows****  
!******************************************************************************  
!  
!--Version 1.17    
!  
!******************************************************************************  
!  
!--Development History   
!  
!--Version 1.1  writing commenced 16 November 2007, and was used for the RGD26 chemistry work  
!--Version 1.2  added the full coding for cylindrical and spherical flows (from April 2009)  
!--Version 1.3  added the moving wall and enhanced unsteady sampling options  
!--Version 1.4  added radial weighting factors  
!--Version 1.5  extended the new chemistry model to cope with O2-H2 sysyem   
!--Version 1.6  modified model for exothermic reactions  
!--Version 1.7  added velocity in y direction  
!--Version 1.8  removed diagnostic subroutines - first version for web (POSTED Nov 25, 2010)  
!--Version 1.9  revised volume for ternary collisions, only the relevant vibrational modes are considered in reactions,  
!               an improved procedure when there are multiple reactions possible for the collision pair  
!--Version 1.10 new Q-K procedure for exothermic exchange and chain reactions   
!--Version 1.11 includes the newer Q-K procedures as described in the Phys Fluids paper   
!--Version 1.12 gives priority to dissociation when considering reactions  
!--Version 1.13 option to base, ternary collision volume, activation energy adjustments, and  
!               vibrational relaxation numbers on either the macroscopic or collision temperatures  
!--Version 1.14 includes the sampling of distribution functions  
!--Version 1.15 TCE model is set as standard case, bug in reflected particles is fixed --isebasti (Nov 2013) 
!--Version 1.16 added thermophoretic calculations --isebasti (Nov 2013)
!--Version 1.17 openmp version + diffusion flame set up --isebasti (May 2015)
!******************************************************************************
!  
!--Base SI units are employed throughout the program  
!  
!--Variables in data file DS1VD.DAT  
!  
!--NVER the n in version n.m that generated the data file  
!--MVER the m in version n.m that generated the data file  
!  
!--m must change whenever the data file is altered  
!  
!--AMEG the approximate number of megabytes that are initially available to the calculation  
!  
!--IFX 0 for plane flows, 1 for cylindrical flows or 2 for spherical flows (the axis for cyl. flows and the center of spherical flows is at x=0)  
!  
!--the type of the boundaries of the flowfield is set by ITYPE, the number code is  
!    0 for a boundary with the stream  
!    1 plane of symmetry for IFX=0, or center for IFX>0 and XB(1)=0, or mandatory for outer boundary if IVB=1  
!    2 for solid surface  
!    3 for vacuum  
!      
!--XB(1) the minimum x coordinate of the flow (must be zero or positive if IFX>0)  
!--ITYPE(1) sets type of XB(1)  
!   
!--XB(2) the maximum x coordinate of the flow   
!--ITYPE(2) sets type of XB(2)  
!  
!--if IFX>0  
!----IWF 0 for no weighting factors, 1 for weighting factors based on radius  
!------the weighting factors are 1+WFM*(r/r_max)**IFX   
!----if (IWF=1) WFM the (maximum weighting factor -1)   
!--end if  
!  
!--GASCODE the code number of the gas  
!    1 for a hard sphere gas  
!    2 for argon  
!    3 for ideal gas  
!    4 for real oxygen  
!    5 for ideal air  
!    6 for real air @ 7.5 km/s  
!    7 for helium-xenon mixture with argon molecular weight  
!    8 for combustible hydrogen-oxygen  
!  
!--IGS 0 if the initial state is a vacuum, 1 if it is the stream or reference gas and (if present) the secondary stream
!      2 if it is the stream or reference gas with TDF temperature distribution   
!--ISECS=0 if there is no secondary stream, 1 if there is a secondary stream that applies when x>0. for IFX=0  
!          (XB(1) must then be negative and XB(2) must be positive),or x>XS for IFX>0.  
!--if (IFX>0 and ISECS=1)   
!----XS the initial boundary between the initial and secondary streams  
!----ISECS must be zero if there are radial weighting factors         
!--in a loop over K=1,2 (1 for XB(1), 2 for XB(2)  
!----if K=1 or (K=2 and ISECS=1)   
!------FND(K)  the number density of the stream or reference gas    
!------FTMP(K) temperature of stream or reference gas  
!------FRTMP(K) rotational temperature of stream or reference gas (if any species have rotational modes)  
!------FVTMP(K) vibrational temperature of stream or reference gas (if any species have vibrational modes)  
!------VFX(K) the velocity component in the x direction  
!------VFY(K) the velocity component in the y direction  
!------in a loop over the number of species (if there is more than one species)  
!--------FSP(N,K)) the fraction of species N in the stream or reference gas  
!------end if  
!----end if  
!----if ITYPE(K)=2  
!------TSURF(K) the surface temperature  
!------FSPEC(K) the fraction of specular reflection at this surface  
!------VSURF(K) the y velocity component  
!----end if  
!--end loop over ends  
!  
!--if ITYPE(1)=0 (stream) and IFX=0  
!----IREM 0 for no molecule removal (based on "stagnation streamline" theory)  
!         1 for steady removal (FREM=1) set as data (e.g. for shock wave problem)  
!         2 for removal set as a restart option  
!----if IREM=1  
!------XREM the coordinate at which the removal starts (uniform from XREM to XB(2))  
!----end if  
!--end if  
!  
!--if ITYPE(2)=1  
!----IVB 0 if the maximum x boundary is stationary, 1 if it is a moving specularly reflecting piston  
!------if IVB=1, ITYPE(2)=1   
!----if (IVB = 1) VELOB  the velocity of the boundary  
!--end if  
!  
!--MOLSC target number of molecules per sampling cell  
!--NCIS number of collision cells in a sampling cell   
!--CPDTM collisions in time step  
!--collisions appropriate to CPDTM*(local mean collision time)are calculated in a collision cell  
!  when its time falls half this time interval behind the flow time   
!--TPDTM maximum flow transits of a sampling cell in a time step  
!--a molecule is moved appropriate to the minimum of CPDTM*(local mean coll time) and TPDTM*(local transit time)  
!  when its time falls half this time interval behind the flow time   
!--NNC 0 to select collision partner randomly from collision cell, 1 for nearest neighbor collisions  
!--IMTS 0 for move time steps that are the same for all cells, 1 if they are allowed to vary over the cells  
!--SAMPRAT the number of time steps between samplings  
!--OUTRAT the number of sampling steps bewteen the output of results  
!--ISF 0 if the sample is reset only as a user restart option, 1 if it is reset at the end of each output interval (unsteady flow sampling)  
!--FRACSAM fraction (at the end) of the output interval in which samples are made in a flow with unsteady sampling;
!  sample for implicit boudary conditions is reset every FRACSAM*OUTRAT samples; FRACSAM possible values=0.5,0.25,0.2,0.1,0.05,0.02,0.025,0.01...   
!--JCD 0 if any reactions are based on rate equations (GASCODE=6 or 8 only), 1 if they are based on the Q-K reaction model  
!--IRECOM 0 if recombination reactions can be neglected, 1 if they must be included  
!  
!************************************************************************************  
!  
!--Restart options in Version 1.1  
!  
!--if IREM=2  
!----FREM fraction of molecule removal   
!----XREM the coordinate at which removal commences  
!--end if   
!  
!************************************************************************************  
!  
MODULE MOLECS  
!  
!--declares the variables associated with the molecules  
!  
IMPLICIT NONE  
!  
INTEGER, ALLOCATABLE, DIMENSION(:) :: IPCELL,IPSP,ICREF,IPCP  
INTEGER, ALLOCATABLE, DIMENSION(:,:) :: IPVIB  
INTEGER :: NM,MNM  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: PV  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: PX,PTIM,PROT  
!  
!--PX(N) position (x coordinate) of molecule N  
!--PTIM(N) molecule time  
!--PROT(N) rotational energy  
!--PV(1-3,N) u,v,w velocity components  
!--IPSP(N) the molecular species  
!--IPCELL(N) the collision cell number  
!--ICREF the cross-reference array (molecule numbers in order of collision cells)  
!--IPCP(N) the code number of the last collision partner of molecule   
!--IPVIB(K,N) level of vibrational mode K of molecule N  
!--NM number of molecules  
!--MNM the maximum number of molecules  
!  
END MODULE MOLECS  
!  
!*************************************************************************************  
!  
MODULE GEOM  
!  
!--declares the variables associated with the flowfield geometry and cell structure  
!  
IMPLICIT NONE  
!  
INTEGER :: NCELLS,NCCELLS,NCIS,NDIV,MDIV,ILEVEL,IFX,JFX,IVB,IWF      
INTEGER, DIMENSION(2) :: ITYPE              
INTEGER, ALLOCATABLE, DIMENSION(:) :: ICELL  
INTEGER, ALLOCATABLE, DIMENSION(:,:) :: ICCELL,JDIV  
REAL(KIND=8) :: DDIV,XS,VELOB,WFM,AWF  
REAL(KIND=8), DIMENSION(2) :: XB  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: CELL,CCELL  
!  
!--XB(1), XB(2) the minimum, maximum x coordinate  
!--DDIV the width of a division  
!--ITYPE(K) the tpe of boundary at the minimum x (K=1) and maximum x (K=2) boundaries  
!          0 for a stream boundary  
!          1 for a plane of symmetry  
!          2 for a solid surface  
!          3 for a vacuum  
!--NCELLS the number of sampling cells  
!--NCCELLS the number of collision cells  
!--NCIS the number of collision cells in a sampling cell  
!  MDIV the maximum number of sampling cell divisions at any level of subdivision  
!--IVB 0,1 for stationary, moving outer boundary  
!--IWF 0 for no radial weighting factors, 1 for radial weighting factors  
!--WFM, set in data as the maximum weighting factor -1, then divided by the maximum radius  
!--AWF overall ratio of real to weighted molecules   
!--VELOB the speed of the outer boundary  
!--JDIV(N,M) (-cell number) or (start address -1 in JDIV(N+1,M), where M is MDIV  
!--JFX  IFX+1  
!--CELL(M,N) information on sampling cell N  
!    M=1 x coordinate  
!    M=2 minimum x coordinate  
!    M=3 maximum x cooedinate   
!    M=4 volume  
!--ICELL(N) number of collision cells preceding those in sampling cell N  
!--CCELL(M,N) information on collision cell N  
!    M=1 volume  
!    M=2 remainder in collision counting  
!    M=3 half the allowed time step  
!    M=4 maximum value of product of cross-section and relative velocity   
!    M=5 collision cell time  
!--ICCELL(M,N) integer information on collision cell N  
!    M=1 the (start address -1) in ICREF of molecules in collision cell N  
!    M=2 the number of molecules in collision cell N  
!    M=3 the sampling cell in which the collision cell lies  
!  
END MODULE GEOM  
!  
!********************************************************************  
!  
MODULE GAS  
!  
!--declares the variables associated with the molecular species and the stream definition  
!  
IMPLICIT NONE  
!  
REAL(KIND=8) :: RMAS,CXSS,RGFS,VMPM,FDEN,FPR,FMA,FP,CTM  
REAL(KIND=8), DIMENSION(2) :: FND,FTMP,FRTMP,FVTMP,VFX,VFY,TSURF,FSPEC,VSURF,UVFX,UFND,UFTMP
REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: CI,AE,AC,BC,ER,ERS,CR,TNEX,PSF,DPER  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: FSP,SP,SPR,SPV,REA,THBP,VMP,UVMP,CPER  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: SPM,SPVM,ENTR 
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: SPEX,SPRC!,TNPVIB  
INTEGER :: MSP,MMVM,MMRM,MNRE,MNSR,MTBP,GASCODE,MMEX,MEX  
INTEGER, ALLOCATABLE, DIMENSION(:) :: ISP,ISPV,LE,ME,KP,LP,MP,IREV  
INTEGER, ALLOCATABLE, DIMENSION(:,:) :: ISPR,IREA,NREA,NRSP,LIS,LRS,ISRCD,ISPRC,TREACG,TREACL,NSPEX,IDL  
INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: ISPVM,NEX,IRCD,JREA  
INTEGER, ALLOCATABLE, DIMENSION(:,:,:,:) :: ISPEX
!------------- Following variables added by Han Luo to apply discret vibrational energy------
  INTEGER, SAVE :: AHVIB=0,MMDVIB 
  INTEGER, ALLOCATABLE, DIMENSION(:),SAVE   :: IDVIB
  INTEGER, ALLOCATABLE, DIMENSION(:,:),SAVE :: MDIVB
  REAL(KIND=8), ALLOCATABLE, DIMESION(:,:,:),SAVE :: EDVIB 
  !--AHVIB Use specific Evib model when AHVIB=1 
  !--MMDVIB maxium vibrational energy level
  !--IDVIB(M)  M=1,MSP IDVIB(M)=1 when uUse specific Evib model for specie M
  !--MDVIB(L,M)  L=1,ISPV(M)  M=1,MSP Maxium level of Evib for mode L
  !--EDVIB(N,L,M) M=1,MSP  L=1,ISPV(M)  N=1,MDVIB(L,M) Correspoding Evib(Kelvin) for different level
  !-------------EDVIB(1,L,M) is level one,ground state has zero Evib
!----------------------------------------------------------------------------------------------
!INTEGER, ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: NPVIB  !--isebasti: UVFX,UFND,UFTMP,UVMP,FRTMP,IDL,CPER,NPVIB,TNPVIB included      
!  
!--MSP the number of molecular species  
!--MMVM the maximum number of vibrational modes of any species  
!--MEX number of exchange or chain reactions  
!--MMEX the maximum number of exchange reactions involving the same precollision pair of molecules  
!--MMRM 0 if gass is completely monatomic, 1 if some species have rotation  
!--MNRE the number of gas phase chemical reactions (old scheme)  
!--MNSR the number oF surface reactions  
!--MTBP the number of third body tables  
!--SP(1,L) the reference diameter of species L         
!--SP(2,L) the reference temperature of species L  
!--SP(3,L) the viscosity-temperature power law of species L  
!--SP(4,L) the reciprocal of the VSS scattering parameter  
!--SP(5,L) molecular mass of species L  
!--ISPR(1,L) number of rotational degrees of freedom of species L  
!--ISPR(2,L) 0,1 for constant, polynomial rotational relaxation collision number  
!--SPR(1,L) constant rotational relaxation collision number of species L  
!           or the constant in a second order polynomial in temperature  
!--SPR(2,L) the coefficient of temperature in the polynomial  
!--SPR(3,L) the coefficient of temperature squared in the polynomial  
!--SPM(1,L,M) the reduced mass for species L,M  
!--SPM(2,L,M) the reference collision cross-section for species L,M  
!--SPM(3,L,M) the mean value of the viscosity-temperature power law 
!--SPM(4,L,M) the reference diameter for L,M collisions  
!--SPM(5,L,M) the reference temperature for species L,M  
!--SPM(6,L,M) reciprocal of the gamma function of (5/2-w) for species L,M  
!--SPM(7,L,M) rotational relaxation collision number for species L,M, or const in polynomial  
!--SPM(8,L,M) reciprocal of VSS scattering parameter  
!--ISPV(L) the number of vibrational modes  
!--SPVM(1,K,L) the characteristic vibrational temperature  
!--SPVM(2,K,L) constant Zv, or reference Zv for mode K  
!--SPVM(3,K,L) -1. for constant Zv, or reference temperature  
!--SPVM(4,K,L) the characteristic dissociation temperature
!--IDL(K,L) the dissociation vibrational level of species L for mode K
!--CPER(K,L) the numerator constant in post reaction energy redistribution rule for reaction K, molecule L
!--CPER(K,L) the denominator constant
!--NPVIB(J,NS,K,MS,KV,I) pre(J=1)/post(J=2) collisional vibrational distribution for sampling cell NS,reaction K,species MS,mode KV,level I
!--TNPVIB(J,NS,K,MS) total counter related to NPVIB
!--ISPVM(1,K,L) the species code of the first dissociation product  
!--ISPVM(2,K,L) the species code of the second dissociation product  
!--ISPRC(L,M) the species of the recombined molecule from species L and M  
!--SPRC(1,L,M) the constant a in the ternary collision volume  
!--SPRC(2,L,M) the temperature exponent b in the ternary collision volume  
!--NSPEX(L,M) the number of exchange reactios with L,M as the pre-collision species  
!--in the following variables, J is the reaction number (1 to NSPEX(L,M))  
!--ISPEX(J,0,L,M) the species that splits in an exchange reaction  
!--ISPEX(J,1,L,M) the post collision species of the molecule that splits in an L,M exchange reaction (all ISPEX are set to 0 if no exchange reaction)  
!--ISPEX(J,2,L,M) the post collision species of either the atom or the molecule that does not split in an L,M exchange reaction  
!--ISPEX(J,3,L,M) vibrational mode that is associated with this reaction  
!--ISPEX(J,4,L,M) degeneracy of this reaction  
!--SPEX(1,J,L,M) the constant a in the activation energy  
!--SPEX(2,J,L,M) the temperature exponent b in the activation energy  
!--SPEX(3,J,L,M)  for the heat of reaction  
!--TNEX(N) total number of exchange reaction N   
!--NEX(N,L,M) the number of exchange or chain reactions in L,M collisions  
!  
!--The reaction information for the Q-K reaction model (JCD=1) is in the above  
!--The reaction information for the traditional reaction model (JCD =0) follows   
!  
!--IRCD(N,M,L) the code number of the Nth reaction between species M,L   
!--IREA(1,N),IREA(2,N) the codes of the pre-reaction species  
!--JREA(1,N,K) the code of the Kth post reaction species from first mol.  
!--JREA(2,N,K) similarly for second, but -ve indicates 3rd body table  
!--NREA(1,N),NREA(2,N) the number of post-reaction molecules from each  
!--NRSP(L,M) the number of chemical reactions for L-M precollision  
!--REA(1,N) number of contributing internal degrees of freedom  
!--REA(2,N) the activation energy for reaction N  
!--REA(3,N) the constant in the reaction rate  
!--REA(4,N) temperature exponent in the rate equation  
!--REA(5,N) heat of reaction (+,- for exothermic,endothermic)  
!--REA(6,N) non-energy terms in the steric factor expressions (6.10,14)  
!--LE(N) the species code of the first molecule  
!--ME(N) the species code of the second molecule  
!--for three post-collision particles  
!----KP(N) the first post-reaction species code  
!----LP(N) the second post-reaction species code  
!----MP(N) the third post-reaction species code  
!--for one post-collision particle  
!----KP(N)=-1  
!----LP(N) the species code of the molecule  
!----MP(N) if positive the code of the third body in a recombination  
!          or, if negative, the code of the third-body table  
!--for two post-collision particles  
!----KP(N)=0  
!----LP(N) the first post-reaction species code  
!----MP(N) the second post-reaction species code  
!----CI(N) the number of internal degrees of freedom that contribute  
!----AE(N) the activation energy for the reaction  
!----AC(N) the pre-exponential parameter  
!----BC(N) the temperature exponent in the rate equation  
!----ER(N) the energy of the reaction
!--IREV(N) the index of the corresponding reverse reaction
!--THBP(N,L) third body efficiency in table N of species L   
!--RMAS reduced mass for single species case  
!--CXSS reference cross-section for single species case  
!--RGFS reciprocal of gamma function for single species case  
!--for the following, J=1 for the reference gas and/or the minimum x boundary, J=2 for the secondary sream at maximum x boundary  
!--FND(J) stream or reference gas number density  
!--FTMP(J) stream temperature  
!--FRTMP(J) the rotational temperature in the freestream  
!--FVTMP(J) the vibrational temperature in the freestream  
!--VFX(J)  the x velocity components of the stream  
!--VFY(J) the y velocity component in the stream  
!--FSP(N,J)) fraction of species N in the stream  
!--FMA stream Mach number  
!--VMP(N,J) most probable molecular velocity of species N at FTMP(J)  
!--VMPM the maximum value of VMP in stream 1  
!--ENTR(M,L,K) entry/removal information for species L at K=1 for 1, K=2 for XB(2)  
!    M=1 number per unit time  
!    M=2 remainder  
!    M=3 speed ratio  
!    M=4 first constant  
!    M=5 second constant  
!    M=6 the maxinum normal velocity component in the removal zone (> XREM)  
!--LIS(1,N) the species code of the first incident molecule  
!--LIS(2,N) the species code of the second incident molecule (0 if none)  
!--LRS(1,N) the species code of the first reflected molecule  
!--LRS(2,N) the species code of the second reflected molecule (0 if none)  
!--LRS(3,N) the species code of the third reflected molecule (0 if none)  
!--LRS(4,N) the species code of the fourth reflected molecule (0 if none)  
!--LRS(5,N) the species code of the fifth reflected molecule (0 if none)  
!--LRS(6,N) the species code of the sixth reflected molecule (0 if none)  
!--ERS(N) the energy of the reaction (+ve for recombination, -ve for dissociation)   
!--NSRSP(L) number of surface reactions that involve species L as incident molecule  
!--ISRCD(N,L) code number of Nth surface reaction with species L as incident molecule    
!--CTM approximate mean collision time in stream (exact for simple gas)  
!--FP approximate mean free path  
!--FDEN stream 1 density    
!--FPR stream 1 pressure  
!--FMA stream 1 Mach number  
!--RMAS reduced mass for single species case  
!--CXSS reference cross-section for single species case  
!--RGFS reciprocal of gamma function for single species case  
!--CR(L) collision rate of species L  
!--TREACG(N,L) the total number of species L gained from reaction type N=1 for dissociation, 2 for recombination, 3 for forward exchange, 4 for reverse exchange  
!--TREACL(N,L) the total number of species L lost from reaction type N=1 for dissociation, 2 for recombination, 3 for forward exchange, 4 for reverse exchange  
!         
END MODULE GAS  
!  
!********************************************************************  
!  
MODULE CALC  
!  
!--declares the variables associated with the calculation  
!  
IMPLICIT NONE  
!  
INTEGER :: NVER,MVER,MOLSC,JCD,ISF,ISECS,IGS,IREM,IRECOM,NNC,IMTS,ERROR,ITCV,IEAA,IZV,&
           IFI,IRM,IADAPT,IJSR,IPDF,ITHP,IUBC,IREAC,IPRS !--isebasti: IFI,IRM,IADAPT,IJSR,IPDF,ITHP,IUBC,IREAC,IPRS included  
REAL(KIND=8) :: FREM,XREM,FTIME,TLIM,PI,SPI,DPI,BOLTZ,FNUM,DTM,TREF,TSAMP,TOUT,AMEG,SAMPRAT,OUTRAT,TOTCOLI,TOTMOVI,&  !--isebasti: RANF removed
                DTSAMP,DTOUT,TPOUT,FRACSAM,TOTMOV,TOTCOL,PCOLLS,ENTMASS,CPDTM,TPDTM,TDISS,TRECOMB,TFOREX,TREVEX,TOTDUP,AVOG  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: VNMAX  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: TCOL
INTEGER, ALLOCATABLE, DIMENSION(:) :: ISEED !--isebasti: included  
!  
!--NVER.MVER the version number  
!--AMEG the initial number of megabytes to be used by the program  
!--MOLSC the target number of molecules per sampling cell  
!--FREM fraction of molecule removal   
!--XREM the coordinate at which removal commences   
!--FTIME the flow time  
!--TLIM the time at which the calculation stops  
!--FNUM the number of real molecules represented by each simulated molecule  
!--CPDTM the maximum number of collisions per time step (standard 0.2)  
!--TPDTM the maximum number of sampling cell transit times of the flow per time step   
!--TOTMOV total molecule moves  
!--TOTCOL total collisions
!--PCOLLS total collisions in NSPDF cell      
!--TOTDUP total duplicated collisions  
!--TDISS dissociations since sample reset  
!--TFOREX forward exchange reactions since sample reset  
!--TREVEX backward exchange reactions since sample reset  
!--TRECOMB recombinations since sample reset  
!--ENTMASS the current entry mass of which a fraction FREM is to be removed   
!--VNMAX(L) the maximum normal velocity component of species L  
!--JCD  0 if chemical reactions are based on rate equations (GASCODE=6 only), 1 if they based on the Q-K model   
!--TCOL species dependent collision counter  
!--ISF 0,1 for steady, unsteady flow sampling  
!--IRECOM 0 to neglect recombination reactions, 1 to include them  
!--ISECS 0,1 for no secondary stream,a secondary stream that applies for positive values of x  
!--IREM data item to set type of molecule removal  
!--NNC 0 for normal collisions, 1 for nearest neighbor collisions  
!--IMTS 0 for uniform move time steps, 1 for time steps that vary over the cells  
!--IGS 0 for initial gas, 1 for stream(s) or reference gas, 2 for stream(s) or reference gas with TDF temperature distribution
!--ITCV 0 to base ternary collision volume on macroscopic temperature, 1 on translational collision temperature  
!--IEAA 0 to base activation energy adjustments on macroscopic temperature, 1 on translational collision temperature  
!--N.B. the coefficent a for tern. coll. vols. and act. en. adj. requre a transformation if ITCV or IEAA = 1  
!--IZV  0 to base vibrational relaxation collision numbers on macroscopic temperature, 1 on quantized collision temperature
!--IFI 0 no forced ignition or >0 for forced ignition
!--IRM to select suboptions concerning GASCODE choice
!--IADAPT 0 for no cell adapting, 1 for adapting cells with VAR(3,N)/FND(1)>DCRIT,
!         2 for adapting cell with VAR(3,N)/MIN(VAR3,:)>DCRIT       
!--IDT thread id; =0 in serial calculation but =0,1,2,...,mnth (maximum number of threads) in parallel cases (LOCAL VARIABLE)
!--ISEED(0:mnth) vector containing current number generator seed correspoding to each thread
!--IJSR initial seed that produces ISEED values
!--IUBC 0 for no BCs updating, 1 for updating BCs based on target velocities
!       2 for updating BCs based on target inlet pressures
!
!  
END MODULE CALC  
!  
!********************************************************************  
!  
MODULE OUTPUT  
!  
!--declares the variables associated with the sampling and output  
!  
IMPLICIT NONE  
!  
INTEGER :: NSAMP,NMISAMP,NOUT,NDISSOC,NRECOMB,NBINS,NSNAP,NSPDF,IENERS  !--isebasti: included NBINS,NSNAP,NSPDF 
INTEGER, DIMENSION(0:100) :: NDISSL  
INTEGER(8), ALLOCATABLE :: NDROT(:,:),NDVIB(:,:,:)  
REAL(KIND=8):: TISAMP,XVELS,YVELS,AVDTM,DBINV(3),DBINC(3),ENERS,ENERS0 !--isebasti: DBINV,C,ENERS,ENERS0 included 
REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: COLLS,WCOLLS,CLSEP,REAC,SREAC  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: VAR,VARS,CSSS,CST,PDF,BIN !--isebasti:CST,PDF,BIN included  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: CS,VARSP,PDFS,BINS !--isebasti:PDFS,BINS included  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: CSS  
!  
!--NSAMP the number of samples  
!--TISAMP the time at which the sampling was last reset  
!--MNISAMP the number of molecules at the last reset  
!--AVDTM the average value of DTM in the cells  
!--NOUT the number of output intervals  
!--COLLS(N) total number of collisions in sampling cell N  
!--WCOLLS(N) total weighted collisins in N  
!--CLSEP(N) sum of collision pair separation in cell N  
!--CS(0,N,L) sampled number of species L in cell N  
!--CS(1,N,L) sampled weighted number of species L in cell N  
!----all the following CS are weighted sums   
!--CS(2,N,L), CS(3,N,L), CS(4,N,L) sampled sum of u, v, w  
!--CS(5,N,L), CS(6,N,L), CS(7,N,L) sampled sum of u*u, v*v, w*w  
!--CS(8,N,L) sampled sum of rotational energy of species L in cell N  
!--CS(8+K,N,L) sampled sum of vibrational energy of 
!              species L in cell N for mode K 
!  
!--in CSS, M=1 for incident molecules and M=2 for reflected molecules   
!--J=1 for surface at x=XB(1), 2 for surface at x=XB(2)  
!  
!--CSS(0,J,L,M) number sum of molecules of species L  
!--CSS(1,J,L,M) weighted number sum of molecules of species L  
!----all the following CSS are weighted    
!--CSS(2,J,L,M) normal momentum sum to surface   
!--CSS(3,J,L,M) y momentum sum to surface   
!--CSS(4,J,L,M) z momentum sum to surface   
!--CSS(5,J,L,M) tranlational energy sum to surface   
!--CSS(6,J,L,M) rotational energy sum to surface  
!--CSS(7,J,L,M) vibrational energy sum to the surface   
!--CSS(8,J,L,M) reaction energy sum to the surface   
!  
!--CSSS(1,J) weighted sum (over incident AND reflected molecules) of 1/normal vel. component  
!----all the following CSSS are weighted    
!--CSSS(2,J) similar sum of molecular mass / normal vel. component  
!--CSSS(3,J) similar sum of molecular mass * parallel vel. component / normal vel. component  
!--CSSS(4,J) similar sum of molecular mass * speed squared / normal vel. component  
!--CSSS(5,J) similar sum of rotational energy / normal vel. component  
!--CSSS(6,J) similar sum of rotational degrees of freedom /normal velocity component  
!  
!--REAC(N) the number of type N reactions  
!--SREAC(N) the number of type N surface reactions  
!  
!--VAR(M,N) the flowfield properties in cell N  
!--M=1 the x coordinate   
!--M=2 sample size  
!--M=3 number density  
!--M=4 density  
!--M=5 u velocity component  
!--M=6 v velocity component  
!--M=7 w velocity component  
!--M=8 translational temperature  
!--M=9 rotational temperature  
!--M=10 vibrational temperature  
!--M=11 temperature  
!--M=12 Mach number  
!--M=13 molecules per cell  
!--M=14 mean collision time / rate  
!--M=15 mean free path  
!--M=16 ratio (mean collisional separation) / (mean free path)   
!--M=17 flow speed  
!--M=18 scalar pressure nkT  
!--M=19 x component of translational temperature TTX  
!--M=20 y component of translational temperature TTY  
!--M=21 z component of translational temperature TTZ  
!  
!--VARSP(M,N,L) the flowfield properties for species L in cell N  
!--M=0 the sample size  
!--M=1 the fraction  
!--M=2 the temperature component in the x direction  
!--M=3 the temperature component in the y direction  
!--M=4 the temperature component in the z direction  
!--M=5 the translational temperature  
!--M=6 the rotational temperature  
!--M=7 the vibrational temperature  
!--M=8 the temperature  
!--M=9 the x component of the diffusion velocity  
!--M=10 the y component of the diffusion velocity  
!--M=11 the z component of the diffusion velocity  
!   
!--VARS(N,M) surface property N on interval L of surface M  
!  
!--N=1 the incident sample  
!--N=2 the reflected sample  
!--N=3 the incident number flux  
!--N=4 the reflected number flux  
!--N=5 the incident pressure  
!--N=6 the reflected pressure  
!--N=7 the incident parallel shear tress  
!--N=8 the reflected parallel shear stress  
!--N=9 the incident normal-to-plane shear stress  
!--N=10 the reflected normal shear stress  
!--N=11 the incident translational heat flux  
!--N=12 the reflected translational heat fluc  
!--N=13 the incident rotational heat flux  
!--N=14 the reflected rotational heat flux  
!--N=15 the incident vibrational heat flux  
!--N=16 the reflected vibrational heat flux  
!--N=17 the incident heat flux from surface reactions  
!--N=18 the reflected heat flux from surface reactions  
!--N=19 slip velocity      
!--N=20 temperature slip     
!--N=21 rotational temperature slip     
!--N=22 the net pressure  
!--N=23 the net parallel in-plane shear  
!--N=24 the net parallel normal-to-plane shear  
!--N=25 the net translational energy flux  
!--N=26 the net rotational heat flux  
!--N=27 the net vibrational heat flux  
!--N=28 the heat flux from reactions  
!--N=29 total incident heat transfer  
!--N=30 total reflected heat transfer  
!--N=31 net heat transfer   
!--N=32 surface temperature   --not implemented  
!--N=32+K the percentage of species K  
!  
!--COLLS(N) the number of collisions in sampling cell N  
!--WCOLLS(N) cumulative weighted number
!--CLSEP(N) the total collision partner separation distance in sampling cell N  
!  
!--The following variables apply in the sampling of distribution functions  
!--(some are especially for the dissociation of oxygen  
!  
!--IPDF 0 no pdf sampling, 1 with pdf sampling
!--NSPDF cell considered in pdf sampling (see SAMPLE_PDF subroutine)  
!--NDISSOC the number of dissociations  
!--NRECOMB the number of recombinations  
!--NDISSL(L) the number of dissociations from level   
!--NDROT(L,N) number of species L in rotational energy range N  
!--NDVIB(K,L,N) number of species L in vibrational level N and mode K
!--ENERS total energy sampling
!--ENERS0 previous sampled value
!--IPRS post-reaction vibrational levels are populated using 
!       0 LB, 1 pre-reaction sampled pdf, 2 for Bondar's approach
!       Hydrogen-Oxygen Detonation Study by DSMC method (2011) 
!  
END MODULE OUTPUT  
!  
!***********************************************************************  
!*****************************MAIN PROGRAM******************************  
!***********************************************************************  
!  
!--Version x.xx (must be set in NVER,MVER)  
!  
!--the version changes when there are major enhancements to the program  
!--the first subscript must change if there is any alteration to the data  
!--the second subscript is the release number   
!  
PROGRAM DS1  
!  
USE MOLECS  
USE GEOM  
USE GAS  
USE CALC  
USE OUTPUT
USE OMP_LIB  
!  
IMPLICIT NONE  
!  
INTEGER :: IRUN,NSEED,N,I,IRETREM,ISET  
REAL(KIND=8) :: A,WCLOCK(5)  
!  
NVER=1          !--for changes to basic architecture  
MVER=17         !--significant changes, but must change whenever the data in DS1VD.DAT changes
!
!$ WCLOCK(:)=omp_get_wtime()  
!
!--set constants  
PI=3.1415926535897932D00  
DPI=6.283185307179586D00  
SPI=1.772453850905516D00  
BOLTZ=1.380658D-23  
AVOG=6.022169D26
IJSR=123456789           !--isebasti: base seed for Ziggurate Random# Generator
!  
!--set internal switches
IRUN=0       !initializing (do not change)
IADAPT=0     !initializing (do not change)
ITCV=1       !0 ternary collision volume depends on the macroscopic temperature; 1 collision temperature (used only for QK)  
IEAA=1       !0 activation energy adjustments depend on the macroscopic temperature; 1 collision temperature (used only for QK) 
IZV=0        !0 vibrational relaxation collision number depends on the Ttrans (PREFERABLE); 1 collision temperature
IPDF=1       !>0 sample PDFs for one particular cell (NSPDF)
IENERS=0     !>0 sample total energy in domain (must be extended for GASCODE /= 8)
NSPDF=1      !value can be modified in SAMPLE_PDF
NBINS=60     !number of bins in velocity/speed PDF sampling
NSNAP=10000  !number of particles in snapshot files
ITHP=0       !>0 compute thermophoretic forces
IUBC=0       !>0 update boundary conditions
IPRS=0       !post-reaction vibrational levels are populated using 0 LB, 1 pre-reaction sampled pdf, 2 Bondar's approach  
IREAC=0      !0 reaction on and rate sampling off (standard case)
!             1 reaction on and rate sampling on (samples only NSPDF cell, requires ISF>0)
!             2 reaction off and rate sampling on (called by run_ireac_ds1v.sh, samples only NSPDF cell, requires ISF=0 & NSEED>= 0)
!  
OPEN (9,FILE='DIAG.TXT',ACCESS='APPEND')  
WRITE (9,*,IOSTAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'Stop the DS1.EXE that is already running and try again'  
  STOP  
ELSE  
  WRITE (9,*) 'File DIAG.TXT has been opened'  
END IF  
!  
DO WHILE ((IRUN < 1).OR.(IRUN > 3))  
  WRITE (*,*) 'DS1 Version',NVER,'.',MVER  
  WRITE (*,*) 'Enter 1 to continue the current sample or'  
  WRITE (*,*) 'enter 2 to continue with a new sample, or'  
  WRITE (*,*) 'enter 3 to start a new run :-'  
  READ (*,*)  IRUN  
END DO
!  
IF (IRUN == 1) WRITE (9,*) 'Continuing the current run and sample'  
IF (IRUN == 2) WRITE (9,*) 'Continuing the current run with a new sample'  
IF (IRUN == 3) WRITE (9,*) 'Starting a new run'  
!
IF (IRUN <= 2) THEN  
  WRITE (*,*) 'Enter a seed (integer) for Random# Generator (a negative value calls run_multiple_runs.sh)'
  READ (*,*) NSEED
  IJSR=IJSR+100*NSEED 
  CALL SET_ZIGGURAT   
  WRITE (*,*) 'Setting Ziggurat Random# Generator: done'
  CALL READ_RESTART  
  TSAMP=FTIME  
  TOUT=FTIME
END IF  
!  
IF ((IRUN == 2).AND.(IVB == 0)) THEN  
  WRITE (*,*) 'Enter 0 to continue with current cells;'
  WRITE (*,*) '      1 to adapt cells based on reference number density;'
  WRITE (*,*) '      2 to adapt cells based on mininum number density;'    
  READ(*,*) IADAPT
!
  IF (IADAPT > 0) THEN   
    WRITE (*,*) 'Adapting cells after beginning'  
    CALL ADAPT_CELLS    
    CALL INDEX_MOLS  
    CALL WRITE_RESTART
  ELSE  
    WRITE (*,*) 'Continuing with existing cells'  
  END IF  
!  
  IF (IREM == 2) THEN  
    WRITE (*,*) 'Enter 1 to reset the removal details, or 0 to continue with current details:-'  
    READ(*,*) IRETREM  
    IF (IRETREM == 1) THEN  
      FREM=-1.D00  
      DO WHILE ((FREM < -0.0001).OR.(FREM > 2.))  
        WRITE (*,*) 'Enter the fraction of entering molecules that are removed:-'  
        READ (*,*) FREM  
        WRITE (*,999) FREM  
      END DO  
      WRITE (9,999) FREM  
999   FORMAT (' The ratio of removed to entering molecules is ',G15.5)  
      IF (FREM > 1.D-10) THEN  
      XREM=XB(1)-1.  
      DO WHILE ((XREM < XB(1)-0.0001).OR.(XREM > XB(2)+0.0001))  
        WRITE (*,*) 'Enter x coordinate of the upstream removal limit:-'  
        READ (*,*) XREM  
        WRITE (*,998) XREM,XB(2)  
      END DO  
      WRITE (9,998) XREM,XB(2)  
998   FORMAT (' The molecules are removed from ',G15.5,' to',G15.5)  
      END IF  
    END IF  
  END IF  
!  
  CALL INITIALISE_SAMPLES      
!  
END IF  
!
IF (IRUN == 3) THEN
  WRITE (*,*) 'Enter a seed (integer) for Random# Generator (a negative value calls run_multiple_cases.sh)'
  READ (*,*) NSEED
  IJSR=IJSR+100*NSEED 
  CALL SET_ZIGGURAT   
  WRITE (*,*) 'Setting Ziggurat Random# Generator: done'
  WRITE (*,*) 'Enter 0 to continue with current cells;'
  WRITE (*,*) '      1 to adapt cells based on reference number density;'
  WRITE (*,*) '      2 to adapt cells based on mininum number density;'    
  READ(*,*) IADAPT
  CALL READ_DATA
  IF (IREAC == 2) THEN
    WRITE (*,*) 'Enter temperature value for reaction rate sampling'
    READ (*,*) FTMP(1)
    FTMP=FTMP(1); FRTMP=FTMP(1); FVTMP=FTMP(1)
    FND=101325.d0/(BOLTZ*FTMP(1))
    IF (AE(1) <= 0.) FND=2.5d25*(FTMP/300.d0)  !101325.d0/(BOLTZ*FTMP(1))
  END IF
  CALL SET_INITIAL_STATE
END IF
!
!$ write(*,*) 'Pre-processing wallclock time (s):  ', omp_get_wtime()-WCLOCK(1)
!
TOTCOLI=TOTCOL  
TOTMOVI=TOTMOV
!
IF (NOUT == -1) THEN
  DO I=1,1
    FTIME=FTIME+DTM
    CALL MOLECULES_MOVE
    IF (ITYPE(1)==0.OR.ITYPE(2)==0) CALL MOLECULES_ENTER
    CALL INDEX_MOLS
    IF (IPDF > 0) CALL SAMPLE_PDF  
    CALL COLLISIONS  
    CALL SAMPLE_FLOW
  END DO
  CALL OUTPUT_RESULTS
!
  IF ((IRUN == 3) .AND. (IADAPT > 0)) THEN
    WRITE(*,*) 'Adapting cells at beginning'
    CALL ADAPT_CELLS
    CALL INDEX_MOLS
    CALL INITIALISE_SAMPLES
    DO I=1,10
      FTIME=FTIME+DTM
      CALL MOLECULES_MOVE
      IF (ITYPE(1)==0.OR.ITYPE(2)==0) CALL MOLECULES_ENTER
      CALL INDEX_MOLS
      IF (IPDF > 0) CALL SAMPLE_PDF  
      CALL COLLISIONS 
      CALL SAMPLE_FLOW
    END DO
    NOUT=NOUT-1
    CALL OUTPUT_RESULTS
    WRITE(*,*) 'Adapting cells at beginning: done'
  END IF
!
  CALL WRITE_RESTART
  CALL INITIALISE_SAMPLES
  WRITE(*,*) 'Writing outputs for initial condition: done'
END IF
!
IF (NSEED < 0) THEN
  IF (IREAC == 2) THEN
    WRITE (*,*) 'Set up does not support multiple copies'
    WRITE (9,*) 'Set up does not support multiple copies'
    STOP
  END IF
  WRITE (*,*) 'Creating multiple cases (different seeds) from RESTART file'
  WRITE (9,*) 'Creating multiple cases (different seeds) from RESTART file'
  CALL SYSTEM ("./run_multiple_cases.sh")
  STOP
END IF
!
!------------------------------------------------------------------------------
!
DO WHILE (FTIME < TLIM)  
!
!$ WCLOCK(2)=omp_get_wtime() 
!  
  DO N=1,INT(TPOUT)
!
    DO I=1,INT(SAMPRAT)
      FTIME=FTIME+DTM
!      !$ WCLOCK(1)=omp_get_wtime()
      IF(IREAC <= 1) CALL MOLECULES_MOVE  
!      !$ WRITE(*,*) 'MOVE wallclock time (s):    ', omp_get_wtime()-WCLOCK(1)
!
!      !$ WCLOCK(1)=omp_get_wtime()
      IF ((ITYPE(1) == 0).OR.(ITYPE(2) == 0)) CALL MOLECULES_ENTER
!      !$ WRITE(*,*) 'ENTER wallclock time (s):   ', omp_get_wtime()-WCLOCK(1)  
!   
!      !$ WCLOCK(1)=omp_get_wtime()
      IF(IREAC <= 1) CALL INDEX_MOLS  
!      !$ WRITE(*,*) 'INDEX wallclock time (s):   ', omp_get_wtime()-WCLOCK(1)
!  
!      !$ WCLOCK(1)=omp_get_wtime()
      CALL COLLISIONS  
!      !$ WRITE(*,*) 'COLL wallclock time (s):    ', omp_get_wtime()-WCLOCK(1)
    END DO
!
!    !$ WCLOCK(1)=omp_get_wtime()
    CALL SAMPLE_FLOW  
!    !$ WRITE(*,*) 'SAMPLE wallclock time (s):   ', omp_get_wtime()-WCLOCK(1)
!
!    !$ WCLOCK(1)=omp_get_wtime()
    IF (IPDF > 0) THEN
      IF (ISF == 0) CALL SAMPLE_PDF
      IF ((ISF == 1).AND.(N/TPOUT >= (1.d0-FRACSAM))) CALL SAMPLE_PDF
      IF ((ISF == 2).AND.(N >= INT(TPOUT-10))) CALL SAMPLE_PDF
    END IF
!    !$ WRITE(*,*) 'PDF wallclock time (s):      ', omp_get_wtime()-WCLOCK(1)
!
!    !$ WCLOCK(1)=omp_get_wtime()
    IF (ITHP > 0) CALL THERMOPHORETIC
!    !$ WRITE(*,*) 'THERMO wallclock time (s):   ', omp_get_wtime()-WCLOCK(1)
!    I need to create a variable with updated thermoph forces so that there is no problem on reseting CST samples
!
!    !$ WCLOCK(1)=omp_get_wtime()
    IF (N == INT(TPOUT*FRACSAM)*(N/INT(TPOUT*FRACSAM))) THEN
!      write(*,*) 'TPOUT,N,N/TPOUT',TPOUT,N,N/TPOUT
      IF (ISF  > 0) CALL UPDATE_MP
      IF (IUBC > 0) CALL UPDATE_BC
      IF ((ISF > 0).AND.(N/TPOUT <= (1.d0-FRACSAM))) CALL INITIALISE_SAMPLES
    END IF
!    !$ WRITE(*,*) 'UPDATE_MP wallclock time (s):', omp_get_wtime()-WCLOCK(1)
!
    IF ((ISF == 2).AND.(N == INT(TPOUT-10))) CALL INITIALISE_SAMPLES
!
  END DO
!
  !$ WCLOCK(1)=omp_get_wtime()
  CALL OUTPUT_RESULTS
  CALL WRITE_RESTART
  IF (ISF > 0) CALL INITIALISE_SAMPLES
  IF (IREAC == 2) THEN
    IF ((NOUT < 2).OR.(REAC(1) == 0)) CALL INITIALISE_SAMPLES 
    IF ((NOUT > 100).OR.(REAC(1) > 1.d8)) STOP
  END IF 
  !$ WRITE(*,*) 'OUTPUT wallclock time (s):  ', omp_get_wtime()-WCLOCK(1)
  !$ WRITE(*,*) 'LOOP wallclock time (s):    ', omp_get_wtime()-WCLOCK(2)
  !$ WRITE(9,*) 'LOOP wallclock time (s):    ', omp_get_wtime()-WCLOCK(2)
  WRITE(*,*) 
  WRITE(9,*)
!  
END DO
!
STOP  
END PROGRAM DS1  
!  
!***************************************************************************  
!  
SUBROUTINE READ_DATA  
!  
USE MOLECS  
USE GEOM  
USE GAS  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
INTEGER :: NVERD,MVERD,N,K  
!  
WRITE (*,*) 'Reading the data file DS1VD.in'  
OPEN (4,FILE='DS1VD.in')  
OPEN (3,FILE='DS1VD.txt')  
!  
WRITE (3,*) 'Data summary for program DS1'  
!  
READ (4,*) NVERD  
WRITE (3,*) 'The n in version number n.m is',NVERD  
READ (4,*) MVERD  
WRITE (3,*) 'The m in version number n.m is',MVERD  
!  
READ (4,*) AMEG  
WRITE (3,*) 'The approximate number of megabytes for the calculation is',AMEG  
!  
READ (4,*) IFX  
IF (IFX == 0) WRITE (3,*) 'Plane flow'  
IF (IFX == 1) WRITE (3,*) 'Cylindrical flow'  
IF (IFX == 2) WRITE (3,*) 'Spherical flow'  
JFX=IFX+1  
!  
READ (4,*) XB(1)  
WRITE (3,*) 'The minimum x coordinate is',XB(1)  
!  
READ (4,*) ITYPE(1)  
IF (ITYPE(1) == 0) WRITE (3,*) 'The minimum x coordinate is a stream boundary'  
IF (ITYPE(1) == 1) WRITE (3,*) 'The minimum x coordinate is a plane of symmetry'  
IF (ITYPE(1) == 2) WRITE (3,*) 'The minimum x coordinate is a solid surface'  
IF (ITYPE(1) == 3) WRITE (3,*) 'The minimum x coordinate is a vacuum'  
!  
READ (4,*) XB(2)  
WRITE (3,*) 'The maximum x coordinate is',XB(2)  
!  
READ (4,*) ITYPE(2)  
IF (ITYPE(2) == 0) WRITE (3,*) 'The maximum x coordinate is a stream boundary'  
IF (ITYPE(2) == 1) WRITE (3,*) 'The maximum x coordinate is a plane of symmetry'  
IF (ITYPE(2) == 2) WRITE (3,*) 'The maximum x coordinate is a solid surface'  
IF (ITYPE(2) == 3) WRITE (3,*) 'The maximum x coordinate is a vacuum'  
!  
IF (IFX > 0) THEN  
  READ (4,*) IWF  
  IF (IWF == 0) WRITE (3,*) 'There are no radial weighting factors'  
  IF (IWF == 1) WRITE (3,*) 'There are radial weighting factors'  
  IF (IWF == 1) THEN  
    READ (4,*) WFM  
    WRITE (3,*) 'The maximum value of the weighting factor is ',WFM+1.D00  
    WFM=WFM/XB(2)  
  END IF  
END IF    
!
READ (4,*) JCD              !--isebasti: reading JCD before GASCODE  
IF (JCD == 0) WRITE (3,*) ' Any chemical reactions are based on the continuum rate equations'
IF (JCD == 1) WRITE (3,*) ' Any chemical reactions are based on quantum kinetic theory'
!  
READ (4,*) GASCODE  
WRITE (3,*) GASCODE   
IF (GASCODE == 1) THEN  
  WRITE (3,*) 'Hard sphere gas'  
  CALL HARD_SPHERE  
END IF    
IF (GASCODE == 2) THEN  
  WRITE (3,*) 'Argon'  
  CALL ARGON  
END IF    
IF (GASCODE == 3) THEN  
  WRITE (3,*) 'Ideal gas'  
  READ (4,*) IRM
  IF (IRM == 1) WRITE (3,*) ' N2 (Trans+Rot; Zr const)'
  IF (IRM == 2) WRITE (3,*) ' N2 (Trans+Rot+Vib; Zv const)'
  IF (IRM == 3) WRITE (3,*) ' N2 (Trans+Rot+Vib; Zv(T))'
  IF (IRM == 4) WRITE (3,*) ' H2O (Trans+Rot+Vib)'
  IF (IRM == 5) WRITE (3,*) ' H2O+H2O (Trans+Rot+Vib)'
  IF (IRM == 6) WRITE (3,*) ' H2O+H (Trans+Rot+Vib)'                          
  CALL IDEAL_GAS    
END IF    
IF (GASCODE == 4) THEN  
  WRITE (3,*) ' Real oxygen'   
  CALL REAL_OXYGEN  
END IF    
IF (GASCODE == 5) THEN  
  WRITE (3,*) 'Ideal air'  
  CALL IDEAL_AIR  
END IF    
IF (GASCODE == 6) THEN  
  WRITE (3,*) 'Real air @ 7.5 km'  
  CALL REAL_AIR  
END IF   
IF (GASCODE == 7) THEN  
  WRITE (3,*) 'Helium-xenon mixture with mol. wt. of argon'  
  CALL HELIUM_XENON  
END IF  
IF (GASCODE == 8) THEN  
  WRITE (3,*) 'Oxygen-hydrogen'  
  READ (4,*) IRM
  IF (IRM == 1)    WRITE (3,*) ' H2/O2 full mechanism by Shatalov (2009)'   
  IF (IRM == 2)    WRITE (3,*) ' H2/O2 full mechanism by Davidenko (2006)'
  IF (IRM >= 100)  WRITE (3,*) ' A single forward reaction'  
  IF (IRM >= 200)  WRITE (3,*) ' Both forward and reverse reactions'
  CALL OXYGEN_HYDROGEN  
END IF   
IF (GASCODE == 9)THEN
  WRITE(3,*) 'Oxygen-dissociation'
  READ(4,*) IRM
  IF( IRM == 150) WRITE(3,*) 'O2 dissociation forward only'
  CALL OXYGEN_DISSOCIATION
END IF
!   
READ (4,*) IGS  
IF (IGS == 0) WRITE (3,*) 'The flowfield is initially a vacuum'  
IF (IGS == 1) WRITE (3,*) 'The flowfield is initially the stream(s) or reference gas'
IF (IGS == 2) WRITE (3,*) 'The flowfield is initially the stream(s) or reference gas with Temp-Nden distributions' !--isebasti: included   
READ (4,*) ISECS  
IF (ISECS == 0) WRITE (3,*) 'There is no secondary stream initially at XS > 0'  
IF (ISECS == 1) WRITE (3,*) 'There is a secondary stream applied initially at XS > 0 (XB(2) must be > 0)'  
IF (ISECS == 1) THEN  !--isebasti: this and line below were modified
  IF ((IWF == 1).AND.(IFX > 0)) THEN  
    WRITE (3,*) 'There cannot be a secondary stream when weighting factors are present'  
    STOP  
  END IF    
END IF
READ (4,*) XS  
WRITE (3,*) 'The secondary stream boundary/auxiliar interface is at x=',XS    
!   
DO K=1,2  
  IF ((K == 1).OR.(ITYPE(2) == 0)) THEN  !--isebasti: read XB(2) stream properties here even when ISECS=0
    IF (K == 1) WRITE (3,*) 'The XB(1) stream and/or reference gas properties are:-'  
    IF (K == 2) WRITE (3,*) 'The XB(2) stream and/or secondary stream reference gas properties are:-'  
!
    READ (4,*) FND(K)  
    WRITE (3,*) '    The stream number density is',FND(K)  
!  
    READ (4,*) FTMP(K)  
    WRITE (3,*) '    The stream temperature is',FTMP(K) 
!  
    READ (4,*) FRTMP(K)  
    WRITE (3,*) '    The stream rotational temperature is',FRTMP(K) 
!  
    IF (MMVM > 0) THEN  
      READ (4,*) FVTMP(K)  
      WRITE (3,*) '    The stream vibrational temperature is',FVTMP(K)  
    END IF  
!  
    READ (4,*) VFX(K)  
    WRITE (3,*) '    The stream velocity in the x direction is',VFX(K)  
    IF ((NVERD > 1).OR.((NVERD == 1).AND.(MVERD > 6))) THEN  
      READ (4,*) VFY(K)  
      WRITE (3,*) '    The stream velocity in the y direction is',VFY(K)      
    END IF     
!  
    DO N=1,MSP  
      READ (4,*) FSP(N,K)  
      WRITE (3,*) '    The fraction of species',N,' is',FSP(N,K)  
    END DO  
  END IF
!
  IF (ITYPE(K) == 2) THEN  
    IF (K == 1) WRITE (3,*) 'The minimum x boundary is a surface with the following properties'  
    IF (K == 2) WRITE (3,*) 'The maximum x boundary is a surface with the following properties'  
!  
    READ (4,*) TSURF(K)  
    WRITE (3,*) '     The temperature of the surface is',TSURF(K)  
!  
    READ (4,*) FSPEC(K)  
    WRITE (3,*) '     The fraction of specular reflection at this surface is',FSPEC(K)  
!  
    READ (4,*) VSURF(K)  
    WRITE (3,*) '     The velocity in the y direction of this surface is',VSURF(K)  
  END IF  
END DO  
!   
IF (ITYPE(1) == 0) THEN  
  READ (4,*) IREM  
  IF (IREM == 0) THEN  
    WRITE (3,*) 'There is no molecule removal'  
    XREM=XB(1)-1.D00  
    FREM=0.D00  
  ELSE IF (IREM == 1) THEN  
    READ (4,*) XREM  
    WRITE (3,*) ' There is full removal of the entering (at XB(1)) molecules between',XREM,' and',XB(2)    
    FREM=1.D00  
  ELSE IF (IREM == 2) THEN  
    WRITE (3,*) ' Molecule removal is specified whenever the program is restarted'  
    XREM=XB(1)-1.D00  
    FREM=0.D00  
  END IF   
ELSE  
  XREM=XB(1)-1.D00  
  FREM=0.D00  
END IF  
!  
!--set the speed of the outer boundary  
!  
IVB=0  
VELOB=0.D00  
IF (ITYPE(2) == 1) THEN  
  READ (4,*) IVB  
  IF (IVB == 0) WRITE (3,*) ' The outer boundary is stationary'  
  IF (IVB == 1) THEN  
    WRITE (3,*) ' The outer boundary moves with a constant speed'  
    READ (4,*) VELOB  
    WRITE (3,*) ' The speed of the outer boundary is',VELOB  
  END IF  
END IF  
!  
WRITE (3,*) 'Computational Parameters'     
READ (4,*) MOLSC  
WRITE (3,*) ' The target number of molecules per sampling cell is',MOLSC  
READ (4,*) NCIS  
WRITE (3,*) ' The number of collision cells per sampling cell is ',NCIS  
READ (4,*) CPDTM  
WRITE (3,*) ' Maximum collisions in a time step is',CPDTM  
READ (4,*) TPDTM  
WRITE (3,*) ' Maximum sampling cell transits in a time step is',TPDTM  
READ (4,*) NNC  
IF (NNC == 0) WRITE (3,*) ' Collision partners are selected randomly from the collision cell  '  
IF (NNC == 1) WRITE (3,*) ' Nearest neighbor collisions are employed '  
READ (4,*) IMTS  
IF (IMTS == 0) WRITE (3,*) ' The move time step is uniform over the cells  '  
IF (IMTS == 1) WRITE (3,*) ' The move time step can vary over the cells '  
READ (4,*) SAMPRAT  
WRITE (3,*) ' The number of time steps in a sampling interval is ',SAMPRAT  
READ (4,*) OUTRAT  
WRITE (3,*) ' The number of sampling intervals in an output interval is ',OUTRAT  
READ (4,*) ISF  
IF (ISF == 0) WRITE (3,*) ' The sampling is for an eventual steady flow '  
IF (ISF == 1) WRITE (3,*) ' The sampling is for a continuing unsteady flow (sampling size is depends on FRACSAM*OUTRAT)'
IF (ISF == 2) WRITE (3,*) ' The sampling is for a continuing unsteady flow (sampling size is at most 10)'    
READ (4,*) FRACSAM  
WRITE (3,*) ' Any unsteady sample/update is over the final ',FRACSAM,' of the output interval'  
!
!READ (4,*) JCD !--isebasti: reading JCD before GASCODE (see above)
!IF (JCD == 0) WRITE (3,*) ' Any chemical reactions are based on the continuum rate equations'
!IF (JCD == 1) WRITE (3,*) ' Any chemical reactions are based on quantum kinetic theory'  
!  
!--isebasti: lines bellow are commented to force TCE model and avoid allocation bugs 
!IF ((GASCODE == 4).OR.(GASCODE == 6).OR.(GASCODE == 8)) THEN  
!--deallocate EARLIER ALLOCATION  
!DEALLOCATE (NEX,NSPEX,SPEX,ISPEX,TREACG,PSF,TREACL,TNEX,STAT=ERROR)  
!IF (ERROR /= 0) THEN  
!  WRITE (*,*)'PROGRAM COULD NOT DEALLOCATE INITIAL GAS VARIABLES',ERROR  
!END IF  
!!--set the reaction data  
!  IF (GASCODE == 4) CALL REAL_OXYGEN  
!  IF (GASCODE == 6) CALL REAL_AIR  
!  IF (GASCODE == 8) CALL OXYGEN_HYDROGEN  
!END IF    
!  
READ (4,*) IRECOM  
IF (IRECOM == 0) WRITE (3,*) ' Any recombination reactions are not included'  
IF (IRECOM == 1) WRITE (3,*) ' Any recombination reactions are included'
!
READ (4,*) IFI !--isebasti: included IFI option
IF (IFI == 0) WRITE (3,*) ' Forced ignition is not allowed',IFI   
IF (IFI >  0) WRITE (3,*) ' Forced ignition is allowed',IFI
!
CLOSE (3)  
CLOSE (4)  
!
RETURN  
END SUBROUTINE READ_DATA  
!  
!***************************************************************************  
!  
SUBROUTINE SET_INITIAL_STATE  
!  
USE MOLECS  
USE GEOM  
USE GAS  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
INTEGER :: I,J,L,K,KK,KN,NMI,II,III,INC,NSET,NSC,NC,NDES,IDES(10000),IDT=0 !--isebasti: included NC,IDT
INTEGER(KIND=8) :: N,M  
REAL(KIND=8) :: A,B,BB,SN,XMIN,XMAX,WFMIN,DENG,TDF,DNF,VMP2,RANF,EVIB,TVAR(10000) !--isebasti: included TDF,DNF,VMP2,RANF,EVIB  
REAL(KIND=8), DIMENSION(3) :: DMOM  
REAL(KIND=8), DIMENSION(3,2) :: VB  
REAL(KIND=8), DIMENSION(2) :: ROTE  
REAL :: GAM,AA,ERF  
!  
!--NSET the alternative set numbers in the setting of exact initial state  
!--DMOM(N) N=1,2,3 for x,y and z momentum sums of initial molecules  
!--DENG the energy sum of the initial molecules  
!--VB alternative sets of velocity components  
!--ROTE alternative sets of rotational energy  
!--NMI the initial number of molecules  
!--INC counting increment  
!  
DMOM=0.D00  
DENG=0.D00  
!--set the number of molecules, divisions etc. based on stream 1  
!  
NMI=10000*AMEG+0.999999999D00  
NDIV=NMI/MOLSC !--MOLSC molecules per division  
WRITE (9,*) 'The number of divisions is',NDIV  
!  
MDIV=NDIV  
ILEVEL=0  
!  
ALLOCATE (JDIV(0:ILEVEL,MDIV),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR JDIV ARRAY',ERROR  
ENDIF  
!  
DDIV=(XB(2)-XB(1))/DFLOAT(NDIV)  
NCELLS=NDIV  
WRITE (9,*) 'The number of sampling cells is',NCELLS  
NCCELLS=NCIS*NDIV  
WRITE (9,*) 'The number of collision cells is',NCCELLS  
!  
!IF (IFX == 0) XS=0.D00  !--isebasti: do not reset XS read from READ_DATA
!  
IF (ISECS == 0) THEN  
  IF (IFX == 0) FNUM=(XB(2)-XB(1))*FND(1)/DFLOAT(NMI)  
  IF (IFX == 1) FNUM=PI*(XB(2)**2-XB(1)**2)*FND(1)/DFLOAT(NMI)  
  IF (IFX == 2) FNUM=1.3333333333333333333333D00*PI*(XB(2)**3-XB(1)**3)*FND(1)/DFLOAT(NMI)  
ELSE  
  IF (IFX == 0) FNUM=((XS-XB(1))*FND(1)+(XB(2)-XS)*FND(2))/DFLOAT(NMI)  
  IF (IFX == 1) FNUM=PI*((XS**2-XB(1)**2)*FND(1)+(XB(2)**2-XS**2)*FND(2))/DFLOAT(NMI)  
  IF (IFX == 2) FNUM=1.3333333333333333333333D00*PI*((XS**3-XB(1)**3)*FND(1)-(XB(2)**3-XS**3)*FND(2))/DFLOAT(NMI)  
END IF  
!  
FTIME=0.D00  
TLIM=1.D20  
!  
TOTMOV=0.D00  
TOTCOL=0.D00
PCOLLS=0.D00
TOTDUP=0.D00  
TCOL=0.D00  
TDISS=0.D00  
TRECOMB=0.D00  
TFOREX=0.D00  
TREVEX=0.D00  
TREACG=0  
TREACL=0  
TNEX=0.D00  
!  
DO N=1,NDIV  
  JDIV(0,N)=-N  
END DO  
!  
ALLOCATE (CELL(4,NCELLS),ICELL(NCELLS),CCELL(5,NCCELLS),ICCELL(3,NCCELLS),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR CELL ARRAYS',ERROR  
ENDIF  
!  
ALLOCATE (COLLS(NCELLS),WCOLLS(NCELLS),CLSEP(NCELLS),REAC(MNRE),SREAC(MNSR),VAR(21,NCELLS), &
          VARSP(0:11,NCELLS,MSP),VARS(0:32+MSP,2),CS(0:8+MMVM,NCELLS,MSP),CSS(0:8,2,MSP,2), &  !-isebasti: correcting CS allocation
          CSSS(6,2),CST(0:4,NCELLS),BINS(0:NBINS,4,MSP),BIN(0:NBINS,4),PDFS(0:NBINS,4,MSP),PDF(0:NBINS,4), &
          NDROT(MSP,100),NDVIB(MMVM,MSP,0:100),STAT=ERROR) !--isebasti: CST,PDFS,PDF,BINS,BIN,NPVIB,TNPVIB included
!IF (MNRE > 0) ALLOCATE (NPVIB(2,NCELLS,MNRE,MSP,MMVM,0:50),TNPVIB(2,NCELLS,MNRE,MSP),STAT=ERROR)
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR SAMPLING ARRAYS',ERROR  
ENDIF
!
CALL INITIALISE_SAMPLES  
!  
!--Set the initial cells  
  
DO N=1,NCELLS  
  CELL(2,N)=XB(1)+DFLOAT(N-1)*DDIV  
  CELL(3,N)=CELL(2,N)+DDIV  
  CELL(1,N)=CELL(2,N)+0.5D00*DDIV  
  IF (IFX == 0) CELL(4,N)=CELL(3,N)-CELL(2,N)    !--calculation assumes unit cross-section  
  IF (IFX == 1) CELL(4,N)=PI*(CELL(3,N)**2-CELL(2,N)**2)  !--assumes unit length of full cylinder  
  IF (IFX == 2) CELL(4,N)=1.33333333333333333333D00*PI*(CELL(3,N)**3-CELL(2,N)**3)    !--flow is in the full sphere  
  ICELL(N)=NCIS*(N-1)  
  DO M=1,NCIS  
    L=ICELL(N)+M  
    XMIN=CELL(2,N)+DFLOAT(M-1)*DDIV/DFLOAT(NCIS)  
    XMAX=XMIN+DDIV/DFLOAT(NCIS)  
    IF (IFX == 0) CCELL(1,L)=XMAX-XMIN  
    IF (IFX == 1) CCELL(1,L)=PI*(XMAX**2-XMIN**2)  !--assumes unit length of full cylinder  
    IF (IFX == 2) CCELL(1,L)=1.33333333333333333333D00*PI*(XMAX**3-XMIN**3)    !--flow is in the full sphere  
    CCELL(2,L)=0.D00  
    ICCELL(3,L)=N  
  END DO  
  VAR(8,N)=FTMP(1)
  VAR(9,N)=FRTMP(1)
  VAR(10,N)=FVTMP(1)
  VAR(11,N)=FTMP(1)
END DO  
!  
IF (IWF == 0) AWF=1.D00  
IF (IWF == 1) THEN  
!--FNUM must be reduced to allow for the weighting factors  
  A=0.D00  
  B=0.D00   
  DO N=1,NCELLS  
    A=A+CELL(4,N)  
    B=B+CELL(4,N)/(1.+WFM*CELL(1,N)**IFX)  
  END DO  
  AWF=A/B  
  FNUM=FNUM*B/A       
END IF  
!  
WRITE (9,*) 'FNUM is',FNUM  
!  
!--set the information on the molecular species  
!  
A=0.D00  
B=0.D00  
DO L=1,MSP  
  A=A+SP(5,L)*FSP(L,1)  
  B=B+(3.+ISPR(1,L))*FSP(L,1)  
  VMP(L,1)=SQRT(2.D00*BOLTZ*FTMP(1)/SP(5,L))  
  IF ((ITYPE(2)== 0).OR.(ISECS == 1)) VMP(L,2)=SQRT(2.D00*BOLTZ*FTMP(2)/SP(5,L))  
  VNMAX(L)=3.*VMP(L,1)  
  IF (L == 1) THEN  
    VMPM=VMP(L,1)  
  ELSE  
    IF (VMP(L,1) > VMPM) VMPM=VMP(L,1)  
  END IF  
END DO  
WRITE (9,*) 'VMPM =',VMPM   
FDEN=A*FND(1)  
FPR=FND(1)*BOLTZ*FTMP(1)  
FMA=VFX(1)/DSQRT((B/(B+2.D00))*BOLTZ*FTMP(1)/A)   
DO L=1,MSP  
  DO M=1,MSP  
    SPM(4,L,M)=0.5D00*(SP(1,L)+SP(1,M))  
    SPM(3,L,M)=0.5D00*(SP(3,L)+SP(3,M))  
    SPM(5,L,M)=0.5D00*(SP(2,L)+SP(2,M))  
    SPM(1,L,M)=SP(5,L)*(SP(5,M)/(SP(5,L)+SP(5,M)))  
    SPM(2,L,M)=0.25D00*PI*(SP(1,L)+SP(1,M))**2  
    AA=2.5D00-SPM(3,L,M)  
    A=GAM(AA)  
    SPM(6,L,M)=1.D00/A  
    SPM(8,L,M)=0.5D00*(SP(4,L)+SP(4,M))  
    IF ((ISPR(1,L) > 0).AND.(ISPR(1,M) > 0)) THEN  
      SPM(7,L,M)=(SPR(1,L)+SPR(1,M))*0.5D00  
    END IF  
    IF ((ISPR(1,L) > 0).AND.(ISPR(1,M) == 0)) THEN  
      SPM(7,L,M)=SPR(1,L)  
    END IF     
    IF ((ISPR(1,M) > 0).AND.(ISPR(1,L) == 0)) THEN  
      SPM(7,L,M)=SPR(1,M)  
    END IF       
  END DO  
END DO  
!
IF (MNRE > 0) CALL INIT_REAC
!
IF (MMVM > 0) THEN  !--isebasti: included
  IDL=0
  DO L=1,MSP
    IF (ISPV(L) > 0) THEN
      DO K=1,ISPV(L)
        IDL(K,L)=SPVM(4,K,L)/SPVM(1,K,L)  !assuming SHO
      END DO
    END IF
  END DO
END IF
!
IF (MNRE > 0) THEN  
  DO L=1,MSP
    IF (ISPV(L) == 0) DPER(L)=0   !number of different vibrational energy levels
    IF (ISPV(L) == 1) DPER(L)=IDL(K,L)+1.d0
  END DO
!
  CPER(:,:)=1.d0  !CPER
  DO KK=1,MNRE
    IF (REA(5,KK) < 0.d0) THEN !consider only endothermic (e.g., dissocation) reactions
      DO KN=1,2
        L=IREA(KN,KK)
!
        IF (ISPV(L) == 1) THEN
          A=0.d0
          B=0.d0
          DO I=0,IDL(1,L)
            B=B+1.d0
            EVIB=DFLOAT(I)*BOLTZ*SPVM(1,1,L)
            IF (EVIB < DABS(REA(5,KK))) A=A+1.d0
          END DO
          CPER(KK,KN)= DPER(L)/A           
        END IF
!
        IF (ISPV(L) == 3) THEN
          II=0
          DO I=0,IDL(1,L)
            DO J=0,IDL(2,L)
             DO K=0,IDL(3,L)
                II=II+1.d0    !number of possible states
                !stores the energy level for vibrational state (I,J,K)
                TVAR(II)=DFLOAT(I)*BOLTZ*SPVM(1,1,L)+DFLOAT(J)*BOLTZ*SPVM(1,2,L)+DFLOAT(K)*BOLTZ*SPVM(1,3,L)
              END DO
            END DO
          END DO
!
          NDES=0
          DO I=1,II
            DO J=1,II
              A=0.999999d0*TVAR(J)
              B=1.000001d0*TVAR(J)
              IF ((TVAR(I) > A ).AND.(TVAR(I) < B )) THEN
                NDES=NDES+1    !number of different energy states
                IDES(NDES)=I   !a different energy state 
              END IF
            END DO
          END DO
          DPER(L)=NDES         !number of different vibrational energy levels
!
          A=0.d0
          DO I=1,NDES
            EVIB=TVAR(IDES(I))
            IF (EVIB < DABS(REA(5,KK))) A=A+1.d0  !number of different energy states bellow REA(5,K)
          END DO
          !this is the C constant in Bondar's paper required for the normalization condition (Sum[CPER/DPER]=A*CPER/DPER=1)
          !note that only states with Evib < REA(5,K) contribute to the summation; others states are prohibited       
          CPER(KK,KN)=DPER(L)/A    
        END IF
        !write(*,*) KK,L,IDL(:,L),II,DPER(L),A,CPER(KK,KN)
      END DO
    END IF
  END DO
!
END IF
!pause
!
IF (MSP == 1) THEN  
  RMAS=SPM(1,1,1)  
  CXSS=SPM(2,1,1)  
  RGFS=SPM(6,1,1)  
END IF  
!  
DO L=1,MSP  
  CR(L)=0.  
  DO M=1,MSP  
    CR(L)=CR(L)+2.D00*SPI*SPM(4,L,M)**2*FND(1)*FSP(M,1)*(FTMP(1)/SPM(5,L,M))** &  
        (1.-SPM(3,L,M))*DSQRT(2.*BOLTZ*SPM(5,L,M)/SPM(1,L,M))  
  END DO  
END DO  
A=0.D00  
FP=0.D00  
DO L=1,MSP  
  A=A+FSP(L,1)*CR(L)  
  FP=FP+(2./SPI)*FSP(L,1)*VMP(L,1)/CR(L)  
END DO  
CTM=1.D00/A  
WRITE (9,*) 'Approximate collision time in the stream is',CTM  
!  
WRITE (9,*) 'Approximate mean free path in the stream is',FP   
!  
!--set the initial time step  
DTM=CTM*CPDTM  
IF (DABS(VFX(1)) > 1.D-6) THEN  
  A=(0.5D00*DDIV/VFX(1))*TPDTM  
ELSE  
  A=0.5D00*DDIV/VMPM    
END IF  
IF (IVB == 1) THEN  
  B=0.25D00*DDIV/(ABS(VELOB)+VMPM)  
  IF (B < A) A=B  
END IF  
IF (DTM > A) DTM=A    
DTSAMP=SAMPRAT*DTM  
DTOUT=OUTRAT*DTSAMP  
TSAMP=0.D00
TOUT=0.D00
TPOUT=OUTRAT
NOUT=-1
ENTMASS=0.D00
!  
WRITE (9,*) 'The initial value of the overall time step is',DTM  
!  
!--initialise cell quantities associated with collisions  
!  
DO N=1,NCCELLS  
  CCELL(3,N)=DTM/2.D00  
  CCELL(4,N)=2.D00*VMPM*SPM(2,1,1)  
  CALL ZGF(RANF,IDT)  
  CCELL(2,N)=RANF  
  CCELL(5,N)=0.D00  
END DO  
!  
!--set the entry quantities  
!  
DO K=1,2  
  IF (ITYPE(K) == 0) THEN  
    DO L=1,MSP      
      IF (K == 1) SN=VFX(1)/VMP(L,1)  
      IF (K == 2) SN=-VFX(2)/VMP(L,2)  
      AA=SN  
      A=1.D00+ERF(AA)  
      BB=DEXP(-SN**2)  
      ENTR(3,L,K)=SN  
      ENTR(4,L,K)=SN+SQRT(SN**2+2.D00)  
      ENTR(5,L,K)=0.5D00*(1.D00+SN*(2.D00*SN-ENTR(4,L,K)))  
      ENTR(6,L,K)=3.D00*VMP(L,K)  
      B=BB+SPI*SN*A  
      ENTR(1,L,K)=(FND(K)*FSP(L,K)*VMP(L,K))*B/(FNUM*2.D00*SPI)  
      ENTR(2,L,K)=0.D00  
    END DO  
  END IF  
END DO  
!  
!--initialize variables for updating boundary conditions  
! 
UVFX(:)=VFX(:)  !--isebasti
UFND(:)=FND(:)
UFTMP(:)=FTMP(:)
UVMP(:,:)=VMP(:,:)
!  
!--Set the uniform stream  
!  
MNM=1.1D00*NMI  
!  
IF (MMVM > 0) THEN  
  ALLOCATE (PX(MNM),PTIM(MNM),PROT(MNM),IPCELL(MNM),IPSP(MNM),ICREF(MNM),IPCP(MNM),PV(3,MNM),     &  
       IPVIB(0:MMVM,MNM),STAT=ERROR)  
  IPVIB=0
ELSE  
  IF (MMRM > 0) THEN  
    ALLOCATE (PX(MNM),PTIM(MNM),PROT(MNM),IPCELL(MNM),IPSP(MNM),ICREF(MNM),IPCP(MNM),PV(3,MNM),STAT=ERROR)  
  ELSE  
    ALLOCATE (PX(MNM),PTIM(MNM),IPCELL(MNM),IPSP(MNM),ICREF(MNM),IPCP(MNM),PV(3,MNM),STAT=ERROR)  
  END IF    
END IF  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR MOLECULE ARRAYS',ERROR  
ENDIF  
!  
NM=0
!  
IF (IGS == 1) THEN  
  WRITE (*,*) 'Setting the initial gas'  
  DO L=1,MSP  
    ROTE=0.  
    DO K=1,ISECS+1  
      IF (ISECS == 0) THEN         !--no secondary stream  
        M=(DFLOAT(NMI)*FSP(L,1)*AWF)  
        XMIN=XB(1)  
        XMAX=XB(2)  
      ELSE   
        A=(XS**JFX-XB(1)**JFX)*FND(1)+(XB(2)**JFX-XS**JFX)*FND(2)  
        IF (K == 1) THEN  
          M=IDINT(DFLOAT(NMI)*((XS**JFX-XB(1)**JFX)*FND(1)/A)*FSP(L,1))  
          XMIN=XB(1)  
          XMAX=XS  
        ELSE  
          M=IDINT(DFLOAT(NMI)*((XB(2)**JFX-XS**JFX)*FND(2)/A)*FSP(L,2))  
          XMIN=XS  
          XMAX=XB(2)     
        END IF  
      END IF   
      IF ((K == 1).OR.(ISECS == 1)) THEN  
        III=0   
        WFMIN=1.D00+WFM*XB(1)**IFX  
        N=1  
        INC=1  
        DO WHILE (N < M)  
          A=(XMIN**JFX+((DFLOAT(N)-0.5D00)/DFLOAT(M))*(XMAX-XMIN)**JFX)**(1.D00/DFLOAT(JFX))  
          IF (IWF == 0) THEN  
            B=1.D00  
          ELSE    
            B=WFMIN/(1.D00+WFM*A**IFX)  
            IF ((B < 0.1D00).AND.(INC == 1)) INC=10  
            IF ((B < 0.01D00).AND.(INC == 10)) INC=100  
            IF ((B < 0.001D00).AND.(INC == 100)) INC=1000  
            IF ((B < 0.0001D00).AND.(INC == 1000)) INC=10000  
          END IF    
          CALL ZGF(RANF,IDT)  
          IF (B*DFLOAT(INC) > RANF) THEN  
            NM=NM+1  
            PX(NM)=A  
            IPSP(NM)=L  
            PTIM(NM)=0.  
            IF (IVB == 0) CALL FIND_CELL(PX(NM),IPCELL(NM),KK)  
            IF (IVB == 1) CALL FIND_CELL_MB(PX(NM),IPCELL(NM),KK,PTIM(NM))  
            DO KK=1,3
              CALL RVELC(PV(KK,NM),A,VMP(L,K),IDT)
            END DO
            PV(1,NM)=PV(1,NM)+VFX(K)  
            PV(2,NM)=PV(2,NM)+VFY(K)
            IF (ISPR(1,L) > 0) CALL SROT(L,FRTMP(K),PROT(NM),IDT)
            IF (MMVM > 0) THEN
              IPVIB(0,NM)=0  
              IF (ISPV(L) > 0) THEN  
                  DO J=1,ISPV(L) 
                    IF (AHVIB .EQ. 0)THEN
                      CALL SVIB(L,FVTMP(K),IPVIB(J,NM),J,IDT)  
                    ELSE IF (IDVIB(L) .EQ. 0)THEN
                      CALL SVIB(L,FVTMP(K),IPVIB(J,NM),J,IDT) 
                    ELSE
                      CALL SDVIB(L,FVTMP(K),IPVIB(J,NM),J,IDT) 
                    END IF
                END DO  
              END IF  
            END IF  
          END IF  
          N=N+INC    
        END DO     
      END IF  
    END DO  
  END DO  
!  
!  WRITE (9,*) 'DMOM',DMOM  
!  WRITE (9,*) 'DENG',DENG  
END IF  
!
!--isebasti: included IGS=2 option
IF (IGS == 2) THEN  
  WRITE (*,*) 'Setting the initial gas with given temp TDF(NC) and number density DNF(NC) distributions' 
  DO L=1,MSP  
    ROTE=0.
    DO NC=1,NCELLS  
   !DO K=1,ISECS+1
      XMIN=CELL(2,NC)  
      XMAX=CELL(3,NC)
      TDF=FTMP(1)+2.d3*DEXP(-((CELL(1,NC)-XS)/0.2d-3)**2.d0)
      DNF=FND(1)*(FTMP(1)/TDF)     !nden is Gaussian
      TDF=FTMP(1)                  !T is uniform
      IF (ISECS == 0) THEN         !--no secondary stream
        K=1  
        M=(DFLOAT(NMI)*FSP(L,1)*AWF)*(CELL(4,NC)/(XB(2)-XB(1)))*(DNF/FND(1))
      ELSE
        A=(XS**JFX-XB(1)**JFX)*FND(1)+(XB(2)**JFX-XS**JFX)*FND(2)  
        IF (CELL(1,NC) < XS) THEN
          K=1  
          M=IDINT(DFLOAT(NMI)*((XMAX**JFX-XMIN**JFX)*DNF/A)*FSP(L,1))
        ELSE
          K=2  
          M=IDINT(DFLOAT(NMI)*((XMAX**JFX-XMIN**JFX)*DNF/A)*FSP(L,2))
        END IF  
      END IF
      IF ((K == 1).OR.(ISECS == 1)) THEN  
        III=0   
        WFMIN=1.D00+WFM*XB(1)**IFX  
        N=1  
        INC=1  
        DO WHILE (N < M)  
          A=(XMIN**JFX+((DFLOAT(N)-0.5D00)/DFLOAT(M))*(XMAX-XMIN)**JFX)**(1.D00/DFLOAT(JFX))  
          IF (IWF == 0) THEN  
            B=1.D00  
          ELSE    
            B=WFMIN/(1.D00+WFM*A**IFX)  
            IF ((B < 0.1D00).AND.(INC == 1)) INC=10  
            IF ((B < 0.01D00).AND.(INC == 10)) INC=100  
            IF ((B < 0.001D00).AND.(INC == 100)) INC=1000  
            IF ((B < 0.0001D00).AND.(INC == 1000)) INC=10000  
          END IF    
          CALL ZGF(RANF,IDT)  
          IF (B*DFLOAT(INC) > RANF) THEN  
            NM=NM+1  
            PX(NM)=A  
            IPSP(NM)=L  
            PTIM(NM)=0.  
            IF (IVB == 0) CALL FIND_CELL(PX(NM),IPCELL(NM),KK)  
            IF (IVB == 1) CALL FIND_CELL_MB(PX(NM),IPCELL(NM),KK,PTIM(NM))  
            VMP2=DSQRT(2.D00*BOLTZ*TDF/SP(5,L))  !based on TDF 
            DO KK=1,3
              CALL RVELC(PV(KK,NM),A,VMP2,IDT)
            END DO
            PV(1,NM)=PV(1,NM)+VFX(K)  
            PV(2,NM)=PV(2,NM)+VFY(K)
            IF (ISPR(1,L) > 0) CALL SROT(L,TDF,PROT(NM),IDT)   !based on TDF 
            IF (MMVM > 0) THEN
              IPVIB(0,NM)=0    
              IF (ISPV(L) > 0) THEN  
                  DO J=1,ISPV(L) 
                    IF (AHVIB .EQ. 0)THEN
                      CALL SVIB(L,TDF,IPVIB(J,NM),J,IDT)  
                    ELSE IF (IDVIB(J) .EQ. 0)THEN
                      CALL SVIB(L,TDF,IPVIB(J,NM),J,IDT) 
                    ELSE
                      CALL SDVIB(L,TDF,IPVIB(J,NM),J,IDT) 
                    END IF
                END DO  
              END IF  
            END IF  
          END IF  
          N=N+INC    
        END DO     
      END IF  
   !END DO
    END DO  
  END DO  
!  
  WRITE (9,*) 'DMOM',DMOM  
  WRITE (9,*) 'DENG',DENG  
END IF
!
!--SPECIAL CODING FOR INITIATION OF EXPLOSION IN H2-02 MIXTURE (FORCED IGNITION CASES)  
IF ((GASCODE == 8).AND.(IFI > 0)) THEN !--isebasti: IFC>0 allow forced ignition
IF(IGS == 1) A=0.5D00 !0.5 for p=0.1 atm; 
IF(IGS == 2) A=1.0D00
!
!--set the vibrational levels of A% random molecules to 5
IF (IFI == 1) THEN
  M=0.01D00*A*NM  
  DO N=1,M  
    CALL ZGF(RANF,IDT)  
    K=INT(RANF*DFLOAT(NM))+1
    L=IPSP(K)       !--isebasti: modified to increase all vibrational degrees of freedom  
    DO KK=1,ISPV(L) !--isebasti
      IPVIB(KK,K)=5 !--isebasti 
    END DO          !--isebasti
  END DO
END IF
!
!--isebasti: set vibrational levels of molecules within XMIN and XMAX to 5
IF (IFI == 2) THEN
  XMIN=XS-0.005D00*A*(XB(2)-XB(1))
  XMAX=XS+0.005D00*A*(XB(2)-XB(1))
  DO N=1,NM
    IF ((PX(N) >= XMIN).AND.(PX(N) <= XMAX)) THEN
      L=IPSP(N)
      IF (ISPV(L) > 0) IPVIB(1,N)=IDL(1,L)  
    END IF
  END DO
END IF
!
!--isebasti: set molecules within XMIN and XMAX to energy states consistent with temperature TDF
IF (IFI == 3) THEN
  XMIN=XS-0.005D00*A*(XB(2)-XB(1))
  XMAX=XS+0.005D00*A*(XB(2)-XB(1))
  DO N=1,NM
    IF ((PX(N) >= XMIN).AND.(PX(N) <= XMAX)) THEN
      L=IPSP(N)
      TDF=5.D03
      B=SQRT(2.D00*BOLTZ*TDF/SP(5,L))  !B=VMP at temperature TDF
      DO K=1,3
        CALL RVELC(PV(K,N),BB,B,IDT)
      END DO
      IF (PX(N) < XS) PV(1,N)=PV(1,N)+VFX(1)
      IF (PX(N) > XS) PV(1,N)=PV(1,N)+VFX(2)
      IF (ISPR(1,L) > 0) CALL SROT(L,TDF,PROT(N),IDT)
      IF (MMVM > 0) THEN  
        DO K=1,ISPV(L)  
          CALL SVIB(L,TDF,IPVIB(K,N),K,IDT)  
        END DO  
      END IF  
    END IF
  END DO
END IF
!
END IF
!  
RETURN  
END SUBROUTINE SET_INITIAL_STATE  
!  
!***************************************************************************  
!******************************GAS DATABASE*********************************  
!***************************************************************************  
!  
SUBROUTINE HARD_SPHERE  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
MSP=1  
MMRM=0  
MMVM=0  
MNRE=0  
MTBP=0  
MNSR=0  
MEX=0  
MMEX=0  
!  
CALL ALLOCATE_GAS  
!  
SP(1,1)=4.0D-10    !reference diameter  
SP(2,1)=273.       !reference temperature  
SP(3,1)=0.5        !viscosity-temperature index  
SP(4,1)=1.         !reciprocal of VSS scattering parameter (1 for VHS)  
SP(5,1)=5.D-26     !mass  
ISPR(1,1)=0        !number of rotational degrees of freedom  
!  
RETURN  
END SUBROUTINE HARD_SPHERE  
!  
!***************************************************************************  
!  
SUBROUTINE ARGON  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
MSP=1  
MMRM=0  
MMVM=0  
MNRE=0  
MTBP=0  
MNSR=0  
MEX=0  
MMEX=0  
!  
CALL ALLOCATE_GAS  
SP(1,1)=4.11D-10   !--isebasti: use VSS; VHS value is 4.17D-10  
SP(2,1)=273.15  
SP(3,1)=0.81      
SP(4,1)=1.D0/1.4D0 !--isebasti: use VSS  
SP(5,1)=6.63D-26  
ISPR(1,1)=0  
ISPR(2,1)=0  
!  
RETURN  
END SUBROUTINE ARGON  
!  
!***************************************************************************  
!  
SUBROUTINE IDEAL_GAS  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
INTEGER :: J !--isebasti: J included 
!  
IF(IRM == 1) THEN
  MSP=1; MMRM=2; MMVM=0
END IF
!
IF(IRM >= 2) THEN
  MSP=1; MMRM=2; MMVM=1
END IF
!
IF(IRM == 4) THEN
  MSP=1; MMRM=3; MMVM=3
END IF
!
IF(IRM == 5) THEN
  MSP=2; MMRM=3; MMVM=3
END IF
!
IF(IRM == 6) THEN 
  MSP=2; MMRM=3; MMVM=3
END IF
!
MNRE=0  
MTBP=0  
MEX=0  
MMEX=0  
MNSR=0  
!  
CALL ALLOCATE_GAS  
!  
IF(IRM == 1) THEN !N2 (Trans+Rot; Zr const)
SP(1,1)=4.17D-10  
SP(2,1)=273.D00  
SP(3,1)=0.74D00  
SP(4,1)=1.D00  
SP(5,1)=4.65D-26  
ISPR(1,1)=2  
ISPR(2,1)=0  
SPR(1,1)=5.D00
END IF  
!
IF(IRM == 2) THEN !N2 (Trans+Rot+Vib; Zv const)
SP(1,1)=4.17D-10  
SP(2,1)=273.D00  
SP(3,1)=0.74D00  
SP(4,1)=1.D00  
SP(5,1)=4.65D-26  
ISPR(1,1)=2  
ISPR(2,1)=0  
SPR(1,1)=5.D00  
ISPV(1)=1  
SPVM(1,1,1)=3371.D00  
SPVM(2,1,1)=5.D00
SPVM(3,1,1)=-1.D00  
SPVM(4,1,1)=113500.D00    
END IF  
!
IF(IRM == 3) THEN !N2 (Trans+Rot+Vib; Zv(T))
SP(1,1)=4.17D-10  
SP(2,1)=273.D00  
SP(3,1)=0.74D00  
SP(4,1)=1.D00  
SP(5,1)=4.65D-26  
ISPR(1,1)=2  
ISPR(2,1)=0  
SPR(1,1)=5.D00  
ISPV(1)=1  
SPVM(1,1,1)=3371.D00  
SPVM(2,1,1)=52600.D00
SPVM(3,1,1)=3371.D00  
SPVM(4,1,1)=113500.D00    
END IF  
!
IF(IRM == 4) THEN !H2O (Trans+Rot+Vib; Zv(T))
J=1
SP(1,J)=4.5D-10       !--estimate  
SP(2,J)=273.D00  
SP(3,J)=0.75D00      !-estimate  
SP(4,J)=1.0D00  
SP(5,J)=2.99D-26  
ISPR(1,J)=3  
ISPR(2,J)=0  
SPR(1,J)=5.D00  
ISPV(J)=3  
SPVM(1,1,J)=5261.D00  !--symmetric stretch mode    
SPVM(2,1,J)=5.D00 !5.D00 !20000.D00   !--estimate  
SPVM(3,1,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,1,J)=60043.83D00  
SPVM(1,2,J)=2294.D00  !--bend mode    
SPVM(2,2,J)=5.D00 !5.D00 !20000.D00   !--estimate  
SPVM(3,2,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,2,J)=60043.83D00  
SPVM(1,3,J)=5432.D00  !--asymmetric stretch mode    
SPVM(2,3,J)=5.D00 !5.D00 !20000.D00   !--estimate  
SPVM(3,3,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,3,J)=60043.83D00  
END IF
!    
IF(IRM == 5) THEN !H2O+H2O (Trans+Rot+Vib; Zv const)
J=1
SP(1,J)=4.5D-10       !--estimate  
SP(2,J)=273.D00  
SP(3,J)=1.0D00      !-estimate  
SP(4,J)=1.0D00  
SP(5,J)=2.99D-26  
ISPR(1,J)=3  
ISPR(2,J)=0  
SPR(1,J)=5.D00  
ISPV(J)=3  
SPVM(1,1,J)=5261.D00  !--symmetric stretch mode    
SPVM(2,1,J)=5.D00 !20000.D00   !--estimate  
SPVM(3,1,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,1,J)=60043.83D00  
SPVM(1,2,J)=2294.D00  !--bend mode    
SPVM(2,2,J)=5.D00 !20000.D00   !--estimate  
SPVM(3,2,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,2,J)=60043.83D00  
SPVM(1,3,J)=5432.D00  !--asymmetric stretch mode    
SPVM(2,3,J)=5.D00 !20000.D00   !--estimate  
SPVM(3,3,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,3,J)=60043.83D00
!
J=2
SP(1,J)=4.5D-10       !--estimate  
SP(2,J)=273.D00  
SP(3,J)=1.0D00        !-estimate from SMILE  
SP(4,J)=1.0D00  
SP(5,J)=2.99D-26  
ISPR(1,J)=3  
ISPR(2,J)=0  
SPR(1,J)=5.D00  
ISPV(J)=3  
SPVM(1,1,J)=5261.D00  !--symmetric stretch mode    
SPVM(2,1,J)=5.D00 !20000.D00   !--estimate  
SPVM(3,1,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,1,J)=60043.83D00  
SPVM(1,2,J)=2294.D00  !--bend mode    
SPVM(2,2,J)=5.D00 !20000.D00   !--estimate  
SPVM(3,2,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,2,J)=60043.83D00  
SPVM(1,3,J)=5432.D00  !--asymmetric stretch mode    
SPVM(2,3,J)=5.D00 !20000.D00   !--estimate  
SPVM(3,3,J)=-1.D00 !2500.D00    !--estimate  
SPVM(4,3,J)=60043.83D00 
END IF
!
IF(IRM == 6) THEN !H2O+H (Trans+Rot+Vib)
J=1  !--species 1 is water vapor H2O
SP(1,J)=4.5D-10       !--estimate  
SP(2,J)=273.D00  
SP(3,J)=1.00D00      !-estimate  
SP(4,J)=1.0D00  
SP(5,J)=2.99D-26  
ISPR(1,J)=3  
ISPR(2,J)=0  
SPR(1,J)=1.D00  
ISPV(J)=3  
SPVM(1,1,J)=5261.D00  !--symmetric stretch mode    
SPVM(2,1,J)=10. !20000.D00   !--estimate  
SPVM(3,1,J)=-1. !2500.D00    !--estimate  
SPVM(4,1,J)=60043.83D00  
SPVM(1,2,J)=2294.D00  !--bend mode    
SPVM(2,2,J)=10. !20000.D00   !--estimate  
SPVM(3,2,J)=-1. !2500.D00    !--estimate  
SPVM(4,2,J)=60043.83D00  
SPVM(1,3,J)=5432.D00  !--asymmetric stretch mode    
SPVM(2,3,J)=10. !20000.D00   !--estimate  
SPVM(3,3,J)=-1. !2500.D00    !--estimate  
SPVM(4,3,J)=60043.83D00
!
J=2  !--species 2 is atomic hydrogen H
SP(1,J)=2.5D-10      !--estimate  
SP(2,J)=273.D00  
SP(3,J)=0.8D00  
SP(4,J)=1.D00  
SP(5,J)=1.67D-27  
ISPR(1,J)=0  
ISPV(J)=0
END IF
!
RETURN  
END SUBROUTINE IDEAL_GAS  
!  
!***************************************************************************  
!  
SUBROUTINE REAL_OXYGEN  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
MSP=2  
MMRM=1  
MMVM=1  
MNRE=0  
MTBP=0  
MEX=0  
MMEX=0 
MNSR=0 
!  
CALL ALLOCATE_GAS 
!
SP(1,1)=4.07D-10  
SP(2,1)=273.D00  
SP(3,1)=0.77D00  
SP(4,1)=1.D00  
SP(5,1)=5.312D-26  
ISPR(1,1)=2  
ISPR(2,1)=0             ! 0,1 for constant,polynomial rotational relaxation collision number   
SPR(1,1)=5.             ! the collision number or the coefficient of temperature in the polynomial (if a polynomial, the coeff. of T^2 is in spr_db(3  )  
ISPV(1)=1               ! the number of vibrational modes  
SPVM(1,1,1)=2256.D00          ! the characteristic vibrational temperature  
SPVM(2,1,1)=90000.D00        ! a constant Zv, or the reference Zv  
IF (IZV == 1) SPVM(2,1,1)=5.D00*SPVM(2,1,1)   !--to allow for the reduction in relaxation time when based on quantized collision temperature  
SPVM(3,1,1)=2256.D00        ! -1 for a constant Zv, or the reference temperature  
SPVM(4,1,1)=59500.D00       ! characteristic dissociation temperature  
ISPVM(1,1,1)=2  
ISPVM(2,1,1)=2  
!  
!--species 2 is atomic oxygen  
SP(1,2)=3.D-10  
SP(2,2)=273.D00  
SP(3,2)=0.8D00  
SP(4,2)=1.D00  
SP(5,2)=2.656D-26  
ISPR(1,2)=0  
ISPV(2)=0     !--must be set!
!  
!--set data needed for recombination  
ISPRC=0  
ISPRC(2,2)=1              !--O+O -> O2  recombined species code for an O+O recombination  
SPRC(1,2,2,1)=0.04D00     !--UNADJUSTED  
IF (ITCV == 1) SPRC(1,2,2,1)=SPRC(1,2,2,1)*0.23  
SPRC(2,2,2,1)=-1.3D00  
SPRC(1,2,2,2)=0.07D00     !--UNADJUSTED  
IF (ITCV == 1) SPRC(1,2,2,2)=SPRC(1,2,2,2)*0.28  
SPRC(2,2,2,2)=-1.2D00  
!  
NSPEX=0  
SPEX=0.D00  
ISPEX=0
!  
RETURN  
END SUBROUTINE REAL_OXYGEN  
!  
!***************************************************************************  
!  
SUBROUTINE IDEAL_AIR  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
MSP=2  
MMRM=1  
MMVM=0  
MNRE=0  
MTBP=0  
MNSR=0  
MEX=0  
MMEX=0  
!  
CALL ALLOCATE_GAS  
!  
SP(1,1)=4.07D-10  
SP(2,1)=273.  
SP(3,1)=0.77  
SP(4,1)=1.  
SP(5,1)=5.312D-26  
ISPR(1,1)=2  
ISPR(2,1)=0  
SPR(1,1)=5.  
SP(1,2)=4.17D-10  
SP(2,2)=273.  
SP(3,2)=0.74  
SP(4,2)=1.  
SP(5,2)=4.65D-26  
ISPR(1,2)=2  
ISPR(2,2)=0  
SPR(1,2)=5.  
RETURN  
END SUBROUTINE IDEAL_AIR  
!  
!***************************************************************************  
!  
SUBROUTINE REAL_AIR  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
MSP=5  
MMRM=1  
MMVM=1  
  
!  
IF (JCD == 0) THEN  
  MNRE=23   
  MTBP=4  
  MEX=0  
  MMEX=0  
END IF  
IF (JCD == 1) THEN  
  MNRE=0         
  MTBP=0  
  MEX=4  
  MMEX=1   
END IF    
!   
MNSR=0  
CALL ALLOCATE_GAS  
!--species 1 is oxygen  
SP(1,1)=4.07D-10  
SP(2,1)=273.D00  
SP(3,1)=0.77D00  
SP(4,1)=1.d00  
SP(5,1)=5.312D-26  
ISPR(1,1)=2  
ISPR(2,1)=0  
SPR(1,1)=5.D00  
ISPV(1)=1               ! the number of vibrational modes  
SPVM(1,1,1)=2256.D00          ! the characteristic vibrational temperature  
SPVM(2,1,1)=18000.D00  !90000.D00        ! a constant Zv, or the reference Zv  
SPVM(3,1,1)=2256.D00        ! -1 for a constant Zv, or the reference temperature  
SPVM(4,1,1)=59500.D00  
ISPVM(1,1,1)=3  
ISPVM(2,1,1)=3  
!--species 2 is nitrogen  
SP(1,2)=4.17D-10  
SP(2,2)=273.D00  
SP(3,2)=0.74D00  
SP(4,2)=1.D00  
SP(5,2)=4.65D-26  
ISPR(1,2)=2  
ISPR(2,2)=0  
SPR(1,2)=5.D00  
ISPV(2)=1  
SPVM(1,1,2)=3371.D00  
SPVM(2,1,2)=52000.D00     !260000.D00      
SPVM(3,1,2)=3371.D00  
SPVM(4,1,2)=113500.D00    
ISPVM(1,1,2)=4  
ISPVM(2,1,2)=4  
!--species 3 is atomic oxygen  
SP(1,3)=3.D-10  
SP(2,3)=273.D00  
SP(3,3)=0.8D00  
SP(4,3)=1.D00  
SP(5,3)=2.656D-26  
ISPR(1,3)=0  
ISPV(3)=0  
!--species 4 is atomic nitrogen  
SP(1,4)=3.D-10  
SP(2,4)=273.D00  
SP(3,4)=0.8D00  
SP(4,4)=1.0D00  
SP(5,4)=2.325D-26  
ISPR(1,4)=0  
ISPV(4)=0  
!--species 5 is NO  
SP(1,5)=4.2D-10  
SP(2,5)=273.D00  
SP(3,5)=0.79D00  
SP(4,5)=1.0D00  
SP(5,5)=4.98D-26  
ISPR(1,5)=2  
ISPR(2,5)=0  
SPR(1,5)=5.D00  
ISPV(5)=1  
SPVM(1,1,5)=2719.D00  
SPVM(2,1,5)=14000.D00   !70000.D00  
SPVM(3,1,5)=2719.D00  
SPVM(4,1,5)=75500.D00  
ISPVM(1,1,5)=3  
ISPVM(2,1,5)=4  
!--following data is required if JCD=1 (new reaction model)  
IF (JCD == 1) THEN  
!--set the recombination data for the molecule pairs  
  ISPRC=0    !--data os zero unless explicitly set  
  SPRC=0.D00  
  ISPRC(3,3)=1    !--O+O -> O2  recombined species code for an O+O recombination  
  SPRC(1,3,3,1)=0.04D00  
  SPRC(2,3,3,1)=-1.3D00  
  SPRC(1,3,3,2)=0.07D00  
  SPRC(2,3,3,2)=-1.2D00  
  SPRC(1,3,3,3)=0.08D00  
  SPRC(2,3,3,3)=-1.2D00  
  SPRC(1,3,3,4)=0.09D00  
  SPRC(2,3,3,4)=-1.2D00  
  SPRC(1,3,3,5)=0.065D00  
  SPRC(2,3,3,5)=-1.2D00  
  ISPRC(4,4)=2  
  SPRC(1,4,4,1)=0.15D00  
  SPRC(2,4,4,1)=-2.05D00  
  SPRC(1,4,4,2)=0.09D00  
  SPRC(2,4,4,2)=-2.1D00  
  SPRC(1,4,4,3)=0.16D00  
  SPRC(2,4,4,3)=-2.0D00  
  SPRC(1,4,4,4)=0.17D00  
  SPRC(2,4,4,4)=-2.0D00  
  SPRC(1,4,4,5)=0.17D00  
  SPRC(2,4,4,5)=-2.1D00  
  ISPRC(3,4)=5  
  SPRC(1,3,4,1)=0.3D00  
  SPRC(2,3,4,1)=-1.9D00  
  SPRC(1,3,4,2)=0.4D00  
  SPRC(2,3,4,2)=-2.0D00  
  SPRC(1,3,4,3)=0.3D00  
  SPRC(2,3,4,3)=-1.75D00  
  SPRC(1,3,4,4)=0.3D00  
  SPRC(2,3,4,4)=-1.75D00  
  SPRC(1,3,4,5)=0.15D00  
  SPRC(2,3,4,5)=-1.9D00  
  ISPRC(4,3)=5  
  SPRC(1,4,3,1)=0.3D00  
  SPRC(2,4,3,1)=-1.9D00  
  SPRC(1,4,3,2)=0.4D00  
  SPRC(2,4,3,2)=-2.0D00  
  SPRC(1,4,3,3)=0.3D00  
  SPRC(2,4,3,3)=-1.75D00  
  SPRC(1,4,3,4)=0.3D00  
  SPRC(2,4,3,4)=-1.75D00  
  SPRC(1,4,3,5)=0.15D00  
  SPRC(2,4,3,5)=-1.9D00  
!--set the exchange reaction data  
  SPEX=0.D00  
  ISPEX=0  
  NSPEX=0  
  NSPEX(2,3)=1  
  NSPEX(3,2)=1  
  NSPEX(5,4)=1  
  NSPEX(4,5)=1  
  NSPEX(5,3)=1  
  NSPEX(3,5)=1  
  NSPEX(1,4)=1  
  NSPEX(4,1)=1  
!--N2+O->NO+N  
  ISPEX(1,0,2,3)=2  
  ISPEX(1,1,2,3)=5  
  ISPEX(1,2,2,3)=4  
  ISPEX(1,3,2,3)=1  
  ISPEX(1,4,2,3)=1  
  SPEX(1,1,2,3)=0.15D00  
  SPEX(2,1,2,3)=0.D00  
  SPEX(3,1,2,3)=-5.175D-19  
  NEX(1,2,3)=1  
  ISPEX(1,0,3,2)=2  
  ISPEX(1,1,3,2)=5  
  ISPEX(1,2,3,2)=4  
  ISPEX(1,3,3,2)=1  
  ISPEX(1,4,3,2)=1  
  SPEX(1,1,3,2)=0.15D00  
  SPEX(2,1,3,2)=0.D00  
  SPEX(3,1,3,2)=-5.175D-19  
  NEX(1,3,2)=1  
!--NO+N->N2+0  
  ISPEX(1,0,5,4)=5  
  ISPEX(1,1,5,4)=2  
  ISPEX(1,2,5,4)=3  
  ISPEX(1,3,5,4)=1  
  ISPEX(1,4,5,4)=1  
  SPEX(1,1,5,4)=0.033D00  
  SPEX(2,1,5,4)=0.8D00  
  SPEX(3,1,5,4)=5.175D-19  
  NEX(1,5,4)=2  
  ISPEX(1,0,4,5)=5  
  ISPEX(1,1,4,5)=2  
  ISPEX(1,2,4,5)=3  
  ISPEX(1,3,4,5)=1  
  ISPEX(1,4,4,5)=1  
  SPEX(1,1,4,5)=0.033D00  
  SPEX(2,1,4,5)=0.8D00  
  SPEX(3,1,4,5)=5.175D-19  
  NEX(1,4,5)=2  
!--NO+0->O2+N  
  ISPEX(1,0,5,3)=5  
  ISPEX(1,1,5,3)=1  
  ISPEX(1,2,5,3)=4  
  ISPEX(1,3,5,3)=1  
  ISPEX(1,4,5,3)=1  
  SPEX(1,1,5,3)=0.05D00  
  SPEX(2,1,5,3)=0.7D00  
  SPEX(3,1,5,3)=-2.719D-19  
  NEX(1,5,3)=3  
  ISPEX(1,0,3,5)=5  
  ISPEX(1,1,3,5)=1  
  ISPEX(1,2,3,5)=4  
  ISPEX(1,3,3,5)=1  
  ISPEX(1,4,3,5)=1  
  SPEX(1,1,3,5)=0.05D00  
  SPEX(2,1,3,5)=0.7D00  
  SPEX(3,1,3,5)=-2.719D-19  
  NEX(1,3,5)=3  
!--O2+N->NO+O  
  ISPEX(1,0,1,4)=1  
  ISPEX(1,1,1,4)=5  
  ISPEX(1,2,1,4)=3  
  ISPEX(1,3,1,4)=1  
  ISPEX(1,4,1,4)=1  
  SPEX(1,1,1,4)=0.D00  
  SPEX(2,1,1,4)=0.D00  
  SPEX(3,1,1,4)=2.719D-19  
  NEX(1,1,4)=4  
  ISPEX(1,0,4,1)=1  
  ISPEX(1,1,4,1)=5  
  ISPEX(1,2,4,1)=3  
  ISPEX(1,3,4,1)=1  
  ISPEX(1,4,4,1)=1  
  SPEX(1,1,4,1)=0.D00  
  SPEX(2,1,4,1)=0.D00  
  SPEX(3,1,4,1)=2.719D-19  
  NEX(1,4,1)=4  
!  
END IF  
IF (JCD == 0) THEN  
!--the following data is required if JCD=0 (old reaction model)  
! REACTION 1 IS O2+N->2O+N  
  LE(1)=1   
  ME(1)=4   
  KP(1)=3   
  LP(1)=3   
  MP(1)=4   
  CI(1)=1.  
  AE(1)=8.197D-19  
  AC(1)=5.993D-12  
  BC(1)=-1.  
  ER(1)=-8.197D-19   
!--REACTION 2 IS O2+NO>2O+NO  
  LE(2)=1   
  ME(2)=5   
  KP(2)=3   
  LP(2)=3  
  MP(2)=5   
  CI(2)=1.   
  AE(2)=8.197D-19  
  AC(2)=5.993D-12  
  BC(2)=-1.  
  ER(2)=-8.197D-19   
!--REACTION 3 IS O2+N2>2O+N2  
LE(3)=1   
ME(3)=2   
KP(3)=3   
LP(3)=3   
MP(3)=2   
CI(3)=1.5  
AE(3)=8.197D-19  
AC(3)=1.198D-11  
BC(3)=-1.  
ER(3)=-8.197D-19   
!--REACTION 4 IS 2O2>2O+O2  
LE(4)=1   
ME(4)=1   
KP(4)=3  
LP(4)=3   
MP(4)=1   
CI(4)=1.5  
AE(4)=8.197D-19  
AC(4)=5.393D-11  
BC(4)=-1.  
ER(4)=-8.197D-19   
!--REACTION 5 IS O2+O>3O  
LE(5)=1   
ME(5)=3   
KP(5)=3   
LP(5)=3   
MP(5)=3   
CI(5)=1.   
AE(5)=8.197D-19  
AC(5)=1.498D-10  
BC(5)=-1.  
ER(5)=-8.197D-19   
!--REACTION 6 IS N2+O>2N+O  
LE(6)=2  
ME(6)=3   
KP(6)=4   
LP(6)=4   
MP(6)=3   
CI(6)=0.5  
AE(6)=1.561D-18  
AC(6)=3.187D-13  
BC(6)=-0.5   
ER(6)=-1.561D-18   
!--REACTION 7 IS N2+O2>2N+O2  
LE(7)=2   
ME(7)=1   
KP(7)=4   
LP(7)=4   
MP(7)=1   
CI(7)=0.5  
AE(7)=1.561D-18  
AC(7)=3.187D-13  
BC(7)=-0.5    
ER(7)=-1.561D-18   
!--REACTION 8 IS N2+NO>2N+NO  
LE(8)=2  
ME(8)=5   
KP(8)=4   
LP(8)=4   
MP(8)=5   
CI(8)=0.5  
AE(8)=1.561D-18  
AC(8)=3.187D-13  
BC(8)=-0.5   
ER(8)=-1.561D-18   
!--REACTION 9 IS 2N2>2N+N2  
LE(9)=2   
ME(9)=2   
KP(9)=4   
LP(9)=4   
MP(9)=2   
CI(9)=1.   
AE(9)=1.561D-18  
AC(9)=7.968D-13  
BC(9)=-0.5   
ER(9)=-1.561D-18   
!--REACTION 10 IS N2+N>3N   
LE(10)=2  
ME(10)=4   
KP(10)=4   
LP(10)=4   
MP(10)=4   
CI(10)=1.   
AE(10)=1.561D-18  
AC(10)=6.9E12  
BC(10)=-1.5   
ER(10)=-1.561D-18   
!--REACTION 11 IS NO+N2>N+O+N2  
LE(11)=5   
ME(11)=2   
KP(11)=4   
LP(11)=3   
MP(11)=2   
CI(11)=1.   
AE(11)=1.043D-18  
AC(11)=6.59D-10   
BC(11)=-1.5   
ER(11)=-1.043D-18   
!--REACTION 12 IS NO+O2>N+O+O2  
LE(12)=5   
ME(12)=1   
KP(12)=4   
LP(12)=3   
MP(12)=1   
CI(12)=1.   
AE(12)=1.043D-18  
AC(12)=6.59D-10   
BC(12)=-1.5   
ER(12)=-1.043D-18   
!--REACTION 13 IS NO+NO>N+O+NO  
LE(13)=5   
ME(13)=5   
KP(13)=4  
LP(13)=3   
MP(13)=5   
CI(13)=1.   
AE(13)=1.043D-18  
AC(13)=1.318D-8   
BC(13)=-1.5   
ER(13)=-1.043D-18   
!--REACTION 14 IS NO+O>N+O+O  
LE(14)=5   
ME(14)=3   
KP(14)=4   
LP(14)=3   
MP(14)=3   
CI(14)=1.   
AE(14)=1.043D-18  
AC(14)=1.318D-8   
BC(14)=-1.5   
ER(14)=-1.043D-18   
!--REACTION 15 IS NO+N>2N+O   
LE(15)=5   
ME(15)=4   
KP(15)=4   
LP(15)=3   
MP(15)=4   
CI(15)=1.   
AE(15)=1.043D-18  
AC(15)=1.318D-8   
BC(15)=-1.5   
ER(15)=-1.043D-18   
!--REACTION 16 IS NO+O>O2+N   
LE(16)=5   
ME(16)=3   
KP(16)=0   
LP(16)=1  
MP(16)=4   
CI(16)=0.   
AE(16)=2.719D-19  
AC(16)=5.279D-21  
BC(16)=1.   
ER(16)=-2.719D-19   
!--REACTION 17 IS N2+O>NO+N   
LE(17)=2   
ME(17)=3   
KP(17)=0   
LP(17)=5   
MP(17)=4   
CI(17)=0.   
AE(17)=5.175D-19  
AC(17)=1.120D-16  
BC(17)=0.   
ER(17)=-5.175D-19   
!--REACTION 18 IS O2+N>NO+O   !Exothermic Exchange
LE(18)=1   
ME(18)=4   
KP(18)=0   
LP(18)=5   
MP(18)=3   
CI(18)=0.   
AE(18)=4.968D-20  
AC(18)=1.598D-18  
BC(18)=0.5  
ER(18)=2.719D-19  
!--REACTION 19 IS NO+N>N2+O   !Exothermic Exchange
LE(19)=5   
ME(19)=4   
KP(19)=0  
LP(19)=2   
MP(19)=3   
CI(19)=0.  
AE(19)=0.   
AC(19)=2.49D-17   
BC(19)=0.   
ER(19)=5.175D-19  
!--REACTION 20 IS O+O+M1>O2+M1  
LE(20)=3   
ME(20)=3   
KP(20)=-1   
LP(20)=1   
MP(20)=-1  
CI(20)=0.   
AE(20)=0.   
AC(20)=8.297D-45  
BC(20)=-0.5   
ER(20)=8.197D-19  
!--REACTION 21 IS N+N+M2>N2+M2  
LE(21)=4   
ME(21)=4   
KP(21)=-1  
LP(21)=2   
MP(21)=-2  
CI(21)=0.   
AE(21)=0.   
AC(21)=3.0051D-44   
BC(21)=-0.5   
ER(21)=1.561D-18  
!--REACTION 22 IS N+N+N>N2+N  
LE(22)=4  
ME(22)=4   
KP(22)=-1   
LP(22)=2   
MP(22)=-3  
CI(22)=0.   
AE(22)=0.   
AC(22)=6.3962D-40   
BC(22)=-1.5   
ER(22)=1.5637D-18   
!--REACTION 23 IS N+O+M3>NO+M3  
LE(23)=4   
ME(23)=3    
KP(23)=-1   
LP(23)=5   
MP(23)=-4  
CI(23)=0.   
AE(23)=0.   
AC(23)=2.7846D-40   
BC(23)=-1.5   
ER(23)=1.043D-18  
!   
  THBP(1,1)=9.  
  THBP(1,2)=2.  
  THBP(1,3)=25.   
  THBP(1,4)=1.   
  THBP(1,5)=1.  
  THBP(2,1)=1.  
  THBP(2,2)=2.5   
  THBP(2,3)=1.  
  THBP(2,4)=0.  
  THBP(2,5)=1.  
  THBP(3,1)=0.  
  THBP(3,2)=0.  
  THBP(3,3)=0.  
  THBP(3,4)=1.  
  THBP(3,5)=0.  
  THBP(4,1)=1.  
  THBP(4,2)=1.  
  THBP(4,3)=20.   
  THBP(4,4)=20.   
  THBP(4,5)=20.  
END IF    
RETURN  
END SUBROUTINE REAL_AIR  
!  
!******************************************************************************************************************************************  
!  
SUBROUTINE HELIUM_XENON  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
MSP=2  
MMRM=0  
MMVM=0  
MNRE=0  
MTBP=0  
MNSR=0  
MEX=0  
MMEX=0  
!  
CALL ALLOCATE_GAS  
!  
SP(1,1)=2.33D-10  
SP(2,1)=273.  
SP(3,1)=0.66  
SP(4,1)=1.  
SP(5,1)=6.65D-27  
ISPR(1,1)=0  
ISPR(2,1)=0  
SP(1,2)=5.74D-10  
SP(2,2)=273.  
SP(3,2)=0.85  
SP(4,2)=1.  
SP(5,2)=21.8D-26  
ISPR(1,2)=0  
ISPR(2,2)=0  
RETURN  
END SUBROUTINE HELIUM_XENON  
!  
!***************************************************************************  
!  
SUBROUTINE OXYGEN_HYDROGEN  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
REAL(KIND=8) :: VDC=-1.d0
INTEGER :: J,IRATE=1,JRATE=1 
!--VDC>=0 for VDC model; <0 for tce model
!--IRATE=0 uncorrected rates; 1 corrected rates for recombination and exchange reactions
!--JRATE= same as IRATE but for dissociation reactions
!  
MSP=8  
MMRM=3  
MMVM=3  
!  
!------Following code is added by Han Luo to use specific Evib model for oxygen
  AHVIB=1
  MMDVIB=46
!---------------------------------------------------------------------- 
IF (JCD == 1) THEN   
  MNRE=0         
  MTBP=0  
  MEX=16  
  MMEX=3  
END IF  
IF (JCD == 0) THEN  
  IF(IRM == 1) MNRE=37    !Shatalov
  IF(IRM == 2) MNRE=34    !Davidenko
  IF(IRM >= 100) MNRE=1   !testing only forward reaction
  IF(IRM >= 200) MNRE=2   !testing forward and reverse reactions
  MTBP=0  
  MEX=0  
  MMEX=0  
END IF  
!   
MNSR=0  
!  
CALL ALLOCATE_GAS  
!  
!--species 1 is hydrogen H2  
SP(1,1)=2.92D-10        !reference diameter  !vss/vhs  2.88/2.92
SP(2,1)=273.D00         !reference temperature
SP(3,1)=0.67D00         !viscosity index
SP(4,1)=1.d00           !reciprocal of VSS scattering parameter; vss 1/1.35 (with BUG!)
SP(5,1)=3.34D-27        !molecular mass
ISPR(1,1)=2             !number of rotational degrees of freedom of species L
ISPR(2,1)=0             !0,1 for constant, polynomial rotational relaxations collision number (Zrot)
SPR(1,1)=5.d0           !=100 from Haas1995 !constant Zrot or constant in 2nd order polynomial in temperature   
ISPV(1)=1               !the number of vibrational modes  
SPVM(1,1,1)=6159.D00    !the characteristic vibrational temperature  
SPVM(2,1,1)=18000.D00   !constant Zv, or reference Zv for mode K --estimate  
SPVM(3,1,1)=6159.D00    !-1 for constant Zv, or reference temperature  
SPVM(4,1,1)=52438.76D00 !charactheristic dissociation temperature; based on heats of formation    
ISPVM(1,1,1)=2          !the species code of the 1st dissociation product
ISPVM(2,1,1)=2          !the species code of the 2nd dissociation product  
!--species 2 is atomic hydrogen H  
SP(1,2)=2.33D-10         !SMILE/estimate 2.33/2.5  
SP(2,2)=273.D00  
SP(3,2)=0.75D00          !SMILE/estimate 0.75/0.80
SP(4,2)=1.D00  
SP(5,2)=1.67D-27  
ISPR(1,2)=0  
ISPV(2)=0  
!--species 3 is oxygen O2  
SP(1,3)=4.07d-10        !vss/vhs 4.01/4.07
SP(2,3)=273.D00  
SP(3,3)=0.77D00  
SP(4,3)=1.d0            !vss 1./1.4d0 !there is some bug with vss model  
SP(5,3)=5.312D-26       !vss correction in collision subroutine must be tested  
ISPR(1,3)=2  
ISPR(2,3)=0  
SPR(1,3)=5.d00   
ISPV(3)=1               !the number of vibrational modes  
SPVM(1,1,3)=2256.D00    !the characteristic vibrational temperature  
SPVM(2,1,3)=18000.D00   !constant Zv, or the reference Zv  
SPVM(3,1,3)=2256.d0     !-1 for a constant Zv, or the reference temperature  
SPVM(4,1,3)=59971.4D00  !the characteristic dissociation temperature
ISPVM(1,1,3)=4  
ISPVM(2,1,3)=4  

!---Following code added by Han Luo for specific Evib -------------------
  IDVIB(3)=1
  MDVIB(1,3)=46
  IF (IDVIB(3) .EQ. 1)THEN
    SPVM(4,1,3)=59360.976D00  !dissociation temperature based on Esposito
  END IF
  EDVIB(0,1,3)=0.D00
  EDVIB(1,1,3)=2.242007D+03
  EDVIB(2,1,3)=4.453843D+03
  EDVIB(3,1,3)=6.637827D+03
  EDVIB(4,1,3)=8.790479D+03
  EDVIB(5,1,3)=1.091296D+04
  EDVIB(6,1,3)=1.300295D+04
  EDVIB(7,1,3)=1.506044D+04
  EDVIB(8,1,3)=1.708428D+04
  EDVIB(9,1,3)=1.907331D+04
  EDVIB(10,1,3)=2.102636D+04
  EDVIB(11,1,3)=2.294460D+04
  EDVIB(12,1,3)=2.482571D+04
  EDVIB(13,1,3)=2.666852D+04
  EDVIB(14,1,3)=2.847303D+04
  EDVIB(15,1,3)=3.023809D+04
  EDVIB(16,1,3)=3.196253D+04
  EDVIB(17,1,3)=3.364520D+04
  EDVIB(18,1,3)=3.528609D+04
  EDVIB(19,1,3)=3.688404D+04
  EDVIB(20,1,3)=3.843789D+04
  EDVIB(21,1,3)=3.994765D+04
  EDVIB(22,1,3)=4.140983D+04
  EDVIB(23,1,3)=4.282559D+04
  EDVIB(24,1,3)=4.419261D+04
  EDVIB(25,1,3)=4.551089D+04
  EDVIB(26,1,3)=4.677928D+04
  EDVIB(27,1,3)=4.799555D+04
  EDVIB(28,1,3)=4.915903D+04
  EDVIB(29,1,3)=5.026831D+04
  EDVIB(30,1,3)=5.132201D+04
  EDVIB(31,1,3)=5.231873D+04
  EDVIB(32,1,3)=5.325684D+04
  EDVIB(33,1,3)=5.413473D+04
  EDVIB(34,1,3)=5.495077D+04
  EDVIB(35,1,3)=5.570298D+04
  EDVIB(36,1,3)=5.638939D+04
  EDVIB(37,1,3)=5.700791D+04
  EDVIB(38,1,3)=5.755646D+04
  EDVIB(39,1,3)=5.803283D+04
  EDVIB(40,1,3)=5.843472D+04
  EDVIB(41,1,3)=5.876043D+04
  EDVIB(42,1,3)=5.900909D+04
  EDVIB(43,1,3)=5.918230D+04
  EDVIB(44,1,3)=5.928700D+04
  EDVIB(45,1,3)=5.933862D+04
  EDVIB(46,1,3)=5.935758D+04
!-------------------------------------------------  
!--species 4 is atomic oxygen O  
SP(1,4)=3.0d-10         !--estimate  
SP(2,4)=273.D00  
SP(3,4)=0.75D00          !SMILE/estimate 0.75/0.80
SP(4,4)=1.D00  
SP(5,4)=2.656D-26  
ISPR(1,4)=0  
ISPV(4)=0  
!--species 5 is hydroxy OH  
SP(1,5)=3.50d-10          !--SMILE/estimate 3.50/4.10 
SP(2,5)=273.D00  
SP(3,5)=0.75D00         !-estimate  
SP(4,5)=1.0D00  
SP(5,5)=2.823D-26  
ISPR(1,5)=2  
ISPR(2,5)=0  
SPR(1,5)=5.D00  
ISPV(5)=1  
SPVM(1,1,5)=5360.D00  
SPVM(2,1,5)=18000.D00   !--estimate
SPVM(3,1,5)=5360.D00    !--estimate  
SPVM(4,1,5)=51497.18D00  
ISPVM(1,1,5)=2  
ISPVM(2,1,5)=4  
!--species 6 is water vapor H2O  
SP(1,6)=4.5d-10         !--estimate  
SP(2,6)=273.D00  
SP(3,6)=1.0D00         !SMILE/estimate 1.0/0.75  
SP(4,6)=1.0D00  
SP(5,6)=2.99D-26  
ISPR(1,6)=3  
ISPR(2,6)=0  
SPR(1,6)=10.d0        !--estimate Alexeenko (2003)
ISPV(6)=3  
SPVM(1,1,6)=2294.D00  !--bend mode    
SPVM(2,1,6)=250.d0    !--estimate Alexeenko (2003); for different species collisions    
SPVM(3,1,6)=-2.       !-2 indicates that Zv is different for same species collisions
SPVM(4,1,6)=60043.83D00  
SPVM(1,2,6)=5261.D00  !--symmetric stretch mode    
SPVM(2,2,6)=10.d0     !--estimate Alexeenko (2003); for same species collisions   
SPVM(3,2,6)=-1.  
SPVM(4,2,6)=60043.83D00  
SPVM(1,3,6)=5432.D00  !--asymmetric stretch mode    
SPVM(2,3,6)=10.d0  
SPVM(3,3,6)=-1.  
SPVM(4,3,6)=60043.83D00  
ISPVM(1,1,6)=2  
ISPVM(2,1,6)=5  
ISPVM(1,2,6)=2  
ISPVM(2,2,6)=5  
ISPVM(1,3,6)=2  
ISPVM(2,3,6)=5  
!--species 7 is hydroperoxy HO2  
SP(1,7)=5.5d-10       !--estimate  
SP(2,7)=273.D00  
SP(3,7)=0.75D00      !-estimate  
SP(4,7)=1.0D00  
SP(5,7)=5.479D-26  
ISPR(1,7)=2    !--assumes that HO2 is linear  
ISPR(2,7)=0  
SPR(1,7)=10.D00   !estimate
ISPV(7)=3  
SPVM(1,1,7)=4950.D00      
SPVM(2,1,7)=250 !20000.D00   !--estimate  
SPVM(3,1,7)=-2. ! 2500.D00    !--estimate  
SPVM(4,1,7)=24988.08D00  
SPVM(1,2,7)=2000.D00   
SPVM(2,2,7)=250 !20000.D00   !--estimate  
SPVM(3,2,7)=-1. !2500.D00    !--estimate  
SPVM(4,2,7)=24988.08D00  
SPVM(1,3,7)=1580.D00   
SPVM(2,3,7)=250 !20000.D00   !--estimate  
SPVM(3,3,7)=-1. !2500.D00    !--estimate  
SPVM(4,3,7)=24988.08D00  
ISPVM(1,1,7)=2  
ISPVM(2,1,7)=3  
ISPVM(1,2,7)=2  
ISPVM(2,2,7)=3  
ISPVM(1,3,7)=2  
ISPVM(2,3,7)=3
!--species 8 is hydroperoxy CO2  
SP(1,8)=5.687766d-10       !SMILE  
SP(2,8)=273.d0  
SP(3,8)=0.93d0  
SP(4,8)=1.0d0  
SP(5,8)=7.309999d-26  
ISPR(1,8)=2    !--assumes that CO2 is linear  
ISPR(2,8)=0  
SPR(1,8)=10.d0 !estimate   
ISPV(8)=3  
SPVM(1,1,8)=945.d0      
SPVM(2,1,8)=250.d0    !--estimate; for different species collisions  
SPVM(3,1,8)=-2.       !-2 indicates that Zv is different for same species collisions
SPVM(4,1,8)=64015.d0    
SPVM(1,2,8)=1903.d0    
SPVM(2,2,8)=10.d0     !--for same species collisions 
SPVM(3,2,8)=-1.  
SPVM(4,2,8)=64015.d0 
SPVM(1,3,8)=3329.d0   
SPVM(2,3,8)=10.d0  
SPVM(3,3,8)=-1.  
SPVM(4,3,8)=64015.d0   
ISPVM(1,1,8)=2  
ISPVM(2,1,8)=3  
ISPVM(1,2,8)=2  
ISPVM(2,2,8)=3  
ISPVM(1,3,8)=2  
ISPVM(2,3,8)=3    
!--Species 8 is argon  
!SP(1,8)=4.17D-10  !vss/vhs 4.11/4.17  
!SP(2,8)=273.15  
!SP(3,8)=0.81      
!SP(4,8)=1.        !vss 1./1.4 
!SP(5,8)=6.63D-26  
!ISPR(1,8)=0  
!ISPV(8)=0  
!
!ISPV(:)=0 !test mechanism without vibrational modes  
!  
IF (JCD ==1) THEN    !--Q-K model  
ISPRC=0    !--data is zero unless explicitly set  
ISPRC(4,4)=3    !--O+O+M -> O2+M  recombined species code for an O+O recombination  
ISPRC(2,2)=1    !--H+H+M -> H2+M  
ISPRC(2,4)=5    !--H+0+M -> OH+M  
ISPRC(4,2)=5    !--0+H+M -> OH+M  
ISPRC(2,5)=6    !--H+OH+M -> H2O+M  
ISPRC(5,2)=6    !--OH+H+M -> H2O+M  
ISPRC(2,3)=7    !--H+O2+M -> H02+M  
ISPRC(3,2)=7    !--O2+H+M -> HO2+M  
!--set the exchange reaction data  
SPEX=0.D00    !--all activation energies and heats of reaction are zero unless set otherwise  
ISPEX=0        !-- ISPEX is also zero unless set otherwise  
NSPEX=0  
!--set the number of exchange reactions for each species pair  
NSPEX(1,3)=1  
NSPEX(3,1)=1  
NSPEX(2,7)=3  
NSPEX(7,2)=3  
NSPEX(3,2)=1  
NSPEX(2,3)=1  
NSPEX(5,4)=1  
NSPEX(4,5)=1  
NSPEX(1,4)=1  
NSPEX(4,1)=1  
NSPEX(5,2)=1  
NSPEX(2,5)=1  
NSPEX(5,1)=1  
NSPEX(1,5)=1  
NSPEX(6,2)=1  
NSPEX(2,6)=1  
NSPEX(6,4)=2  
NSPEX(4,6)=2  
NSPEX(5,5)=2  
NSPEX(7,4)=1  
NSPEX(4,7)=1  
NSPEX(5,3)=1  
NSPEX(3,5)=1  
!--set the information on the chain reactions  
!--H2+O2 -> HO2+H  
ISPEX(1,0,1,3)=1  
ISPEX(1,1,1,3)=7  
ISPEX(1,2,1,3)=2  
ISPEX(1,3,1,3)=1  
SPEX(1,1,1,3)=3.79D-19  
SPEX(3,1,1,3)=-3.79D-19  
NEX(1,1,3)=1  
ISPEX(1,0,3,1)=1  
ISPEX(1,1,3,1)=7  
ISPEX(1,2,3,1)=2  
ISPEX(1,3,3,1)=1  
SPEX(1,1,3,1)=3.79D-19  
SPEX(3,1,3,1)=-3.79D-19  
NEX(1,3,1)=1  
!--HO2+H -> H2+02  
ISPEX(1,0,2,7)=7  
ISPEX(1,1,2,7)=1  
ISPEX(1,2,2,7)=3  
ISPEX(1,3,2,7)=1  
ISPEX(1,4,2,7)=0  
ISPEX(1,5,2,7)=0  
!--H02 is H-O-O so that not all vibrational modes contribute to this reaction, but the numbers here are guesses!  
SPEX(1,1,2,7)=0.D00  
SPEX(3,1,2,7)=3.79D-19  
NEX(1,2,7)=2  
ISPEX(1,0,7,2)=7  
ISPEX(1,1,7,2)=1  
ISPEX(1,2,7,2)=3  
ISPEX(1,3,7,2)=0  
ISPEX(1,4,7,2)=0  
ISPEX(1,5,7,2)=1  
ISPEX(1,6,7,2)=1  
SPEX(1,1,7,2)=0.D00  
SPEX(3,1,7,2)=3.79D-19  
NEX(1,7,2)=2  
!  
!--O2+H -> OH+O  
ISPEX(1,0,3,2)=3  
ISPEX(1,1,3,2)=5  
ISPEX(1,2,3,2)=4  
ISPEX(1,3,3,2)=1  
SPEX(1,1,3,2)=1.17D-19  
SPEX(3,1,3,2)=-1.17D-19  
NEX(1,3,2)=3  
ISPEX(1,0,2,3)=3  
ISPEX(1,1,2,3)=5  
ISPEX(1,2,2,3)=4  
ISPEX(1,3,2,3)=1  
SPEX(1,1,2,3)=1.17D-19  
SPEX(3,1,2,3)=-1.17D-19  
NEX(1,2,3)=3  
!--OH+O -> O2+H  
ISPEX(1,0,5,4)=5  
ISPEX(1,1,5,4)=3  
ISPEX(1,2,5,4)=2  
ISPEX(1,3,5,4)=1  
SPEX(1,1,5,4)=0.D00  
SPEX(3,1,5,4)=1.17D-19  
NEX(1,5,4)=4  
ISPEX(1,0,4,5)=5  
ISPEX(1,1,4,5)=3  
ISPEX(1,2,4,5)=2  
ISPEX(1,3,4,5)=1  
SPEX(1,1,4,5)=0.D00  
SPEX(3,1,4,5)=1.17D-19  
NEX(1,4,5)=4  
!  
!--H2+O -> OH+H  
ISPEX(1,0,1,4)=1  
ISPEX(1,1,1,4)=5  
ISPEX(1,2,1,4)=2  
ISPEX(1,3,1,4)=1  
SPEX(1,1,1,4)=0.13D-19  
SPEX(3,1,1,4)=-0.13D-19  
NEX(1,1,4)=5  
ISPEX(1,0,4,1)=1  
ISPEX(1,1,4,1)=5  
ISPEX(1,2,4,1)=2  
ISPEX(1,3,4,1)=1  
SPEX(1,1,4,1)=0.13D-19  
SPEX(3,1,4,1)=-0.13D-19  
NEX(1,4,1)=5  
!--OH+H -> H2+O  
ISPEX(1,0,5,2)=5  
ISPEX(1,1,5,2)=1  
ISPEX(1,2,5,2)=4  
ISPEX(1,3,5,2)=1  
SPEX(1,1,5,2)=0.D00  
SPEX(3,1,5,2)=0.13D-19  
NEX(1,5,2)=6  
ISPEX(1,0,2,5)=5  
ISPEX(1,1,2,5)=1  
ISPEX(1,2,2,5)=4  
ISPEX(1,3,2,5)=1  
SPEX(1,1,2,5)=0.D00  
SPEX(3,1,2,5)=0.13D-19  
NEX(1,2,5)=6  
!  
!--OH+H2 -> H2O+H  
ISPEX(1,0,5,1)=5  
ISPEX(1,1,5,1)=6  
ISPEX(1,2,5,1)=2  
ISPEX(1,3,5,1)=1  
SPEX(1,1,5,1)=-1.D-19  !--a negative activation energy enables reactions from the ground state        
SPEX(3,1,5,1)=1.05D-19  
NEX(1,5,1)=7  
ISPEX(1,0,1,5)=5  
ISPEX(1,1,1,5)=6  
ISPEX(1,2,1,5)=2  
ISPEX(1,3,1,5)=1  
SPEX(1,1,1,5)=-1.D-19  
SPEX(3,1,1,5)=1.05D-19  
NEX(1,1,5)=7  
!  
!--H20+H -> OH+H2  
ISPEX(1,0,6,2)=6  
ISPEX(1,1,6,2)=5  
ISPEX(1,2,6,2)=1  
ISPEX(1,3,6,2)=1  
ISPEX(1,4,6,2)=1  
ISPEX(1,5,6,2)=1  
SPEX(1,1,6,2)=1.05D-19*2.D00    !--activation energy set above heat of reaction  
SPEX(3,1,6,2)=-1.05D-19   !--heat of reaction  
NEX(1,6,2)=8  
ISPEX(1,0,2,6)=6  
ISPEX(1,1,2,6)=5  
ISPEX(1,2,2,6)=1  
ISPEX(1,3,2,6)=1  
ISPEX(1,4,2,6)=1  
ISPEX(1,5,2,6)=1  
SPEX(1,1,2,6)=1.05D-19*2.D00  
SPEX(3,1,2,6)=-1.05D-19  
NEX(1,2,6)=8  
!  
!--H2O+O -> OH+OH  
ISPEX(1,0,6,4)=6  
ISPEX(1,1,6,4)=5  
ISPEX(1,2,6,4)=5  
ISPEX(1,3,6,4)=1  
ISPEX(1,4,6,4)=1  
ISPEX(1,5,6,4)=1  
SPEX(1,1,6,4)=1.18D-19       
SPEX(3,1,6,4)=-1.18D-19  
NEX(1,6,4)=9  
ISPEX(1,0,4,6)=6  
ISPEX(1,1,4,6)=5  
ISPEX(1,2,4,6)=5  
ISPEX(1,3,4,6)=1  
ISPEX(1,4,4,6)=1  
ISPEX(1,5,4,6)=1  
SPEX(1,1,4,6)=1.18D-19      
SPEX(3,1,4,6)=-1.18D-19  
NEX(1,4,6)=9  
!--0H+OH -> H2O+O  
ISPEX(1,0,5,5)=5  
ISPEX(1,1,5,5)=6  
ISPEX(1,2,5,5)=4  
ISPEX(1,3,5,5)=1  
SPEX(1,1,5,5)=-1.D-19      
SPEX(3,1,5,5)=1.18D-19  
NEX(1,5,5)=10  
!  
!--H02+H -> 0H+OH  
ISPEX(2,0,7,2)=7  
ISPEX(2,1,7,2)=5  
ISPEX(2,2,7,2)=5  
ISPEX(2,3,7,2)=1  
ISPEX(2,4,7,2)=1  
ISPEX(2,5,7,2)=0  
SPEX(1,2,7,2)=0.D00  
SPEX(3,2,7,2)=2.49D-19  
NEX(2,7,2)=11  
ISPEX(2,0,2,7)=7  
ISPEX(2,1,2,7)=5  
ISPEX(2,2,2,7)=5  
ISPEX(2,3,2,7)=1  
ISPEX(2,4,2,7)=0  
ISPEX(2,5,2,7)=1  
SPEX(1,2,2,7)=0.D00  
SPEX(3,2,2,7)=2.49D-19  
NEX(2,2,7)=11  
!--OH+OH  -> HO2+H  
ISPEX(2,0,5,5)=5  
ISPEX(2,1,5,5)=7  
ISPEX(2,2,5,5)=2  
ISPEX(2,3,5,5)=1  
SPEX(1,2,5,5)=2.49D-19  
SPEX(3,2,5,5)=-2.49D-19  
NEX(2,5,5)=12  
!  
!--H02+H -> H2O+O  
ISPEX(3,0,7,2)=7  
ISPEX(3,1,7,2)=6  
ISPEX(3,2,7,2)=4  
ISPEX(3,3,7,2)=1  
ISPEX(3,4,7,2)=1  
ISPEX(3,5,7,2)=0  
SPEX(1,3,7,2)=0.D00      
SPEX(3,3,7,2)=3.67D-19  
NEX(3,7,2)=13  
ISPEX(3,0,2,7)=7  
ISPEX(3,1,2,7)=6  
ISPEX(3,2,2,7)=4  
ISPEX(3,3,2,7)=1  
ISPEX(3,4,2,7)=0  
ISPEX(3,5,2,7)=1  
SPEX(1,3,2,7)=0.D00       
SPEX(3,3,2,7)=3.67D-19  
NEX(3,2,7)=13  
!--H2O+O -> HO2+H  
ISPEX(2,0,6,4)=6  
ISPEX(2,1,6,4)=7  
ISPEX(2,2,6,4)=2  
ISPEX(2,3,6,4)=1  
ISPEX(2,4,6,4)=1  
ISPEX(2,5,6,4)=1  
SPEX(1,2,6,4)=3.67D-19     
SPEX(3,2,6,4)=-3.67D-19  
NEX(2,6,4)=14  
ISPEX(2,0,4,6)=6  
ISPEX(2,1,4,6)=7  
ISPEX(2,2,4,6)=2  
ISPEX(2,3,4,6)=1  
ISPEX(2,4,4,6)=1  
ISPEX(2,5,4,6)=1  
SPEX(1,2,4,6)=3.67D-19        
SPEX(3,2,4,6)=-3.67D-19  
NEX(2,4,6)=14  
!  
!--H02+0 -> OH+O2  
ISPEX(1,0,7,4)=7  
ISPEX(1,1,7,4)=5  
ISPEX(1,2,7,4)=3  
ISPEX(1,3,7,4)=1  
ISPEX(1,4,7,4)=1  
ISPEX(1,5,7,4)=0  
SPEX(1,1,7,4)=0.D00  
SPEX(3,1,7,4)=3.66D-19  
NEX(1,7,4)=15  
ISPEX(1,0,4,7)=7  
ISPEX(1,1,4,7)=5  
ISPEX(1,2,4,7)=3  
ISPEX(1,3,4,7)=1  
ISPEX(1,4,4,7)=1  
ISPEX(1,5,4,7)=0  
SPEX(1,1,4,7)=0.D00  
SPEX(3,1,4,7)=3.66D-19  
NEX(1,4,7)=15  
!--OH+O2 -> HO2+O  
ISPEX(1,0,5,3)=5  
ISPEX(1,1,5,3)=7  
ISPEX(1,2,5,3)=4  
ISPEX(1,3,5,3)=1  
SPEX(1,1,5,3)=3.66D-19  
SPEX(3,1,5,3)=-3.66D-19  
NEX(1,5,3)=16  
ISPEX(1,0,3,5)=5  
ISPEX(1,1,3,5)=7  
ISPEX(1,2,3,5)=4  
ISPEX(1,3,3,5)=1  
SPEX(1,1,3,5)=3.66D-19  
SPEX(3,1,3,5)=-3.66D-19  
NEX(1,3,5)=16  
END IF  
!
!--the following data is required if JCD=0 (TCE reaction model)  
!  
IF (JCD == 0 .AND. IRM == 2) THEN
!--rate data is based on Davidenko2006_SystematicStudySupersonicCombustion
!--------------------------------------------------------------
!--REACTION 1 IS H+H+H2>2H2  
  J=1
  LE(J)=2
  ME(J)=2  
  KP(J)=-1   
  LP(J)=1   
  MP(J)=1   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=2.5d0*9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0    
  IF (IRATE == 1) AC(J)=1.180305d17/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5999194d0
  ER(J)=7.239220d-19
  IREV(J)=7
!--REACTION 2 IS H+H+H>H2+H  
  J=2
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=2   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.673070d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5987639d0
  ER(J)=7.239220d-19
  IREV(J)=8
!--REACTION 3 IS H+H+O2>H2+O2  
  J=3
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=3   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.763155d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.6015061d0
  ER(J)=7.239220d-19
  IREV(J)=9
!--REACTION 4 IS H+H+O>H2+O
  J=4
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=4   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.636160d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5978748d0
  ER(J)=7.239220d-19
  IREV(J)=10
!--REACTION 5 IS H+H+OH>H2+OH
  J=5
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=5   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.720037d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.6002319d0
  ER(J)=7.239220d-19
  IREV(J)=11
!--REACTION 6 IS H+H+H2O>H2+H2O
  J=6
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=6   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=12.d0*9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=5.648955d17/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5993142d0
  ER(J)=7.239220d-19
  IREV(J)=12
!--------------------------------------------------------------
! REACTION 7 IS H2+H2->2H+H2  
  J=7
  LE(J)=1   
  ME(J)=1   
  KP(J)=2   
  LP(J)=2   
  MP(J)=1   
  CI(J)=VDC  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=2.5d0*5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0  
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.258131d+12/(AVOG*1000.d0)  
      BC(J)=8.798179d-01
    ELSE 
      AC(J)=1.124184d19/(AVOG*1000.d0)  
      BC(J)=-1.097974d0  
    END IF
  END IF
  ER(J)=-7.239220d-19
  IREV(J)=1
!--REACTION 8 IS H2+H>2H+H  
  J=8
  LE(J)=1   
  ME(J)=2   
  KP(J)=2   
  LP(J)=2  
  MP(J)=2   
  CI(J)=VDC  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=4.897980d+13/(AVOG*1000.d0)  
      BC(J)=4.199751d-01
    ELSE 
      AC(J)=1.799959d20/(AVOG*1000.d0)  
      BC(J)=-1.571824d0
    END IF
  END IF
  ER(J)=-7.239220d-19
  IREV(J)=2
!--REACTION 9 IS H2+O2>2H+O2  
  J=9
  LE(J)=1   
  ME(J)=3   
  KP(J)=2   
  LP(J)=2  
  MP(J)=3   
  CI(J)=VDC  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=9.199534d+11/(AVOG*1000.d0)  
      BC(J)=8.911783d-01
    ELSE 
      AC(J)=1.115394d20/(AVOG*1000.d0)  
      BC(J)=-1.519741d0
    END IF
  END IF
  ER(J)=-7.239220d-19
  IREV(J)=3
!--REACTION 10 IS H2+O>2H+O  
  J=10
  LE(J)=1   
  ME(J)=4   
  KP(J)=2   
  LP(J)=2  
  MP(J)=4   
  CI(J)=VDC  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.282898d+14/(AVOG*1000.d0)  
      BC(J)=2.374446d-01
    ELSE 
      AC(J)=2.110156d20/(AVOG*1000.d0)  
      BC(J)=-1.565169d0
    END IF
  END IF
  ER(J)=-7.239220d-19
  IREV(J)=4
!--REACTION 11 IS H2+OH>2H+OH  
  J=11
  LE(J)=1   
  ME(J)=5   
  KP(J)=2   
  LP(J)=2  
  MP(J)=5   
  CI(J)=VDC  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.260180d+11/(AVOG*1000.d0)  
      BC(J)=1.045312d+00
    ELSE 
      AC(J)=6.346795d19/(AVOG*1000.d0)  
      BC(J)=-1.443632d0
    END IF
  END IF
  ER(J)=-7.239220d-19
  IREV(J)=5
!--REACTION 12 IS H2+H2O>2H+H2O  
  J=12
  LE(J)=1   
  ME(J)=6   
  KP(J)=2   
  LP(J)=2  
  MP(J)=6   
  CI(J)=VDC  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=12.d0*5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.378291d+11/(AVOG*1000.d0)  
      BC(J)=1.322471d+00
    ELSE 
      AC(J)=5.874078d14/(AVOG*1000.d0)  
      BC(J)=0.1337761d0
    END IF
  END IF
  ER(J)=-7.239220d-19
  IREV(J)=6
!--------------------------------------------------------------
!--REACTION 13 IS OH+H+H2>H2O+H2   
  J=13
  LE(J)=5
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=1   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.5d0*2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0  
  IF (IRATE == 1) AC(J)=2.670047d22/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.956666d0 
  ER(J)=8.252333d-19
  IREV(J)=19   
!--REACTION 14 IS OH+H+H>H2O+H   
  J=14
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=2   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.354999d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.932729d0 
  ER(J)=8.252333d-19
  IREV(J)=20   
!--REACTION 15 IS OH+H+O2>H2O+O2   
  J=15
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=3   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.314137d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.931931d0 
  ER(J)=8.252333d-19 
  IREV(J)=21  
!--REACTION 16 IS OH+H+O>H2O+O   
  J=16
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=4   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.347222d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.932318d0
  ER(J)=8.252333d-19 
  IREV(J)=22  
!--REACTION 17 IS OH+H+OH>H2O+OH   
  J=17
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=5   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.422456d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.933605d0
  ER(J)=8.252333d-19 
  IREV(J)=23  
!--REACTION 18 IS OH+H+H2O>H2O+H2O   
  J=18
  LE(J)=5   
  ME(J)=2  
  KP(J)=-1   
  LP(J)=6   
  MP(J)=6   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=12.d0*2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=2.412574d23/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-2.019927d0 
  ER(J)=8.252333d-19 
  IREV(J)=24  
!--------------------------------------------------------------
!--REACTION 19 IS H2O+H2>OH+H+H2   
  J=19
  LE(J)=6   
  ME(J)=1   
  KP(J)=5   
  LP(J)=2   
  MP(J)=1   
  CI(J)=VDC  
  AE(J)=59743.d0*BOLTZ  
  AC(J)=2.5d0*8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=6.027961d+15/(AVOG*1000.d0)  
      BC(J)=1.632129d-01
    ELSE
      AC(J)=3.292285d33/(AVOG*1000.d0)
      BC(J)=-4.719732d0
    END IF
  END IF
  ER(J)=-8.252333d-19 
  IREV(J)=13  
!--REACTION 20 IS H2O+H>OH+H+H   
  J=20
  LE(J)=6   
  ME(J)=2   
  KP(J)=5   
  LP(J)=2   
  MP(J)=2   
  CI(J)=VDC  
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=3.031394d+13/(AVOG*1000.d0)  
      BC(J)=6.817159d-01
    ELSE
      AC(J)=8.936d22/(AVOG*1000.d0)  !1.900933d33/(AVOG*1000.d0)
      BC(J)=-1.835d0  !-4.858611d0
    END IF
  END IF
  ER(J)=-8.252333d-19 
  IREV(J)=14  
!--REACTION 21 IS H2O+O2>OH+H+O2   
  J=21
  LE(J)=6   
  ME(J)=3   
  KP(J)=5   
  LP(J)=2   
  MP(J)=3   
  CI(J)=VDC  
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=9.889330d+15/(AVOG*1000.d0)  
      BC(J)=6.684782d-03
    ELSE
      AC(J)=9.336914d32/(AVOG*1000.d0)
      BC(J)=-4.683550d0
    END IF
  END IF
  ER(J)=-8.252333d-19 
  IREV(J)=15  
!--REACTION 22 IS H2O+O>OH+H+O
  J=22
  LE(J)=6   
  ME(J)=4   
  KP(J)=5   
  LP(J)=2   
  MP(J)=4   
  CI(J)=VDC  
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.637473d+18/(AVOG*1000.d0)  
      BC(J)=-6.616960d-01
    ELSE
      AC(J)=8.936d22/(AVOG*1000.d0)  !9.055880d33/(AVOG*1000.d0)
      BC(J)=-1.835d0  !-4.942958d0
    END IF
  END IF
  ER(J)=-8.252333d-19   
  IREV(J)=16
!--REACTION 23 IS H2O+OH>OH+H+OH
  J=23
  LE(J)=6   
  ME(J)=5   
  KP(J)=5   
  LP(J)=2   
  MP(J)=5   
  CI(J)=VDC  
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=1.430367d+14/(AVOG*1000.d0)  
      BC(J)=4.977865d-01
    ELSE
      AC(J)=1.457072d33/(AVOG*1000.d0)
      BC(J)=-4.735936d0
    END IF
  END IF
  ER(J)=-8.252333d-19   
  IREV(J)=17
!--REACTION 24 IS H2O+H2O>OH+H+H2O   
  J=24
  LE(J)=6   
  ME(J)=6   
  KP(J)=5   
  LP(J)=2   
  MP(J)=6   
  CI(J)=VDC  
  AE(J)=59743.d0*BOLTZ  
  AC(J)=12.d0*8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=3.472299d+15/(AVOG*1000.d0)  
      BC(J)=4.256304d-01
    ELSE
      AC(J)=2.735897d22/(AVOG*1000.d0)
      BC(J)=-1.554205d0
    END IF
  END IF
  ER(J)=-8.252333d-19 
  IREV(J)=18  
!--------------------------------------------------------------
!--REACTION 25 IS OH+OH>H2O+O
  J=25
  LE(J)=5  
  ME(J)=5   
  KP(J)=0   
  LP(J)=6  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=50.d0*BOLTZ  
  AC(J)=1.506d9/(AVOG*1000.d0)  
  BC(J)=1.14d0
  IF (IRATE == 1) AC(J)=1.499219d9/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=1.140777d0
  ER(J)=1.113715d-19
  IREV(J)=26
!--REACTION 26 IS H2O+O>OH+OH
  J=26
  LE(J)=6  
  ME(J)=4   
  KP(J)=0   
  LP(J)=5  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=8613.d0*BOLTZ  
  AC(J)=2.220d10/(AVOG*1000.D00)  
  BC(J)=1.089d0
  IF (IRATE == 1) AC(J)=9.868296d8/(AVOG*1000.D00)  
  IF (IRATE == 1) BC(J)=1.451492d0
  ER(J)=-1.113715d-19
  IREV(J)=25
!--------------------------------------------------------------
!--REACTION 27 IS H2+O>OH+H   
  J=27
  LE(J)=1  
  ME(J)=4   
  KP(J)=0   
  LP(J)=5  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=3163.d0*BOLTZ  
  AC(J)=5.119d4/(AVOG*1000.d0)  
  BC(J)=2.67d0
  IF (IRATE == 1) AC(J)=2.733975d4/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=2.760115d0
  ER(J)=-1.006026d-20   !Bird -0.13d-19
  IREV(J)=28  
!--REACTION 28 IS OH+H>H2+O   
  J=28
  LE(J)=5
  ME(J)=2   
  KP(J)=0   
  LP(J)=1  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=2240.d0*BOLTZ  
  AC(J)=2.701d4/(AVOG*1000.d0)  
  BC(J)=2.65d0
  IF (IRATE == 1) AC(J)=2.465206d4/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=2.659041d0
  ER(J)=1.006026d-20   !Bird 0.13d-19 
  IREV(J)=27 
!--------------------------------------------------------------
!--REACTION 29 IS H2+OH>H20+H 
  J=29
  LE(J)=1  
  ME(J)=5   
  KP(J)=0   
  LP(J)=6  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=1660.d0*BOLTZ  
  AC(J)=1.024d8/(AVOG*1000.d0)  
  BC(J)=1.60d0  
  IF (IRATE == 1) AC(J)=1.050693d8/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=1.594793d0  
  ER(J)=1.013113d-19  !Bird 1.05D-19   
  IREV(J)=30
!--REACTION 30 IS H2O+H>H2+OH   
  J=30
  LE(J)=6  
  ME(J)=2   
  KP(J)=0   
  LP(J)=1  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=9300.d0*BOLTZ  !Dav 9300.d0  !Shat 9030.d0  !Conaire 9252.d0
  AC(J)=7.964d8/(AVOG*1000.d0)  !Dav 7.964d8   !Shat 4.52d8   !Conaire 2.3d9
  BC(J)=1.528d0  !Dav 1.528d0  !Shat 1.6d0  !Conaire 1.4d0
  IF (IRATE == 1) AC(J)=2.265672d7/(AVOG*1000.d0)  !Dav 7.964d8   !Shat 4.52d8   !Conaire 2.3d9
  IF (IRATE == 1) BC(J)=1.939508d0  !Dav 1.528d0  !Shat 1.6d0  !Conaire 1.4d0
  ER(J)=-1.013113d-19  !Bird 1.05D-19   
  IREV(J)=29
!--------------------------------------------------------------
!--REACTION 31 IS O2+H>OH+O
  J=31
  LE(J)=3  
  ME(J)=2   
  KP(J)=0   
  LP(J)=5  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=8456.d0*BOLTZ  
  AC(J)=1.987d14/(AVOG*1000.D00)  
  BC(J)=0.d0  
  IF (IRATE == 1) AC(J)=4.243092d13/(AVOG*1000.D00)  
  IF (IRATE == 1) BC(J)=0.1832357d0  
  ER(J)=-1.137477d-19  !Bird -1.17D-19 
  IREV(J)=32  
!--REACTION 32 IS OH+O>O2+H   
  J=32
  LE(J)=5  
  ME(J)=4   
  KP(J)=0   
  LP(J)=3  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=0.  !--isebasti: paper provides -118.d0*BOLTZ  
  AC(J)=8.930d11/(AVOG*1000.d0)  
  BC(J)=0.338d0
  IF (IRATE == 1) AC(J)=8.889175d11/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=0.3388361d0
  ER(J)=1.137477d-19  !Bird 1.17D-19 
  IREV(J)=31  
!--------------------------------------------------------------
!--REACTION 33 IS H2+O2>OH+OH   
  J=33
  LE(J)=1  
  ME(J)=3   
  KP(J)=0   
  LP(J)=5  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=24044.d0*BOLTZ  
  AC(J)=1.70d13/(AVOG*1000.d0)  
  BC(J)=0.
  IF (IRATE == 1) AC(J)=5.357596d11/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=0.3949028d0  
  ER(J)=-1.238079d-19 !Bird -1.3D-19
  IREV(J)=34
!--REACTION 34 IS OH+OH>H2+O2   
  J=34
  LE(J)=5
  ME(J)=5   
  KP(J)=0   
  LP(J)=1  
  MP(J)=3   
  CI(J)=0.
  AE(J)=14554.d0*BOLTZ  
  AC(J)=4.032d10/(AVOG*1000.D00)  
  BC(J)=0.317d0
  IF (IRATE == 1) AC(J)=1.390438d9/(AVOG*1000.D00)  
  IF (IRATE == 1) BC(J)=0.7079989d0
  ER(J)=1.238079d-19 !Bird 1.3D-19
  IREV(J)=33
END IF
!
!--------------------------------------------------------------
!Testing individual reactions
J=1
!
IF (JCD == 0 .AND. IRM == 101) THEN
!--REACTION D1 IS H+H+H2>2H2  (Davidenko)
  LE(J)=2
  ME(J)=2  
  KP(J)=-1   
  LP(J)=1   
  MP(J)=1   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=2.5d0*9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0    
  IF (IRATE == 1) AC(J)=1.180305d17/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5999194d0 
  ER(J)=7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 102) THEN
!--REACTION D2 IS H+H+H>H2+H  
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=2   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.673070d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5987639d0
  ER(J)=7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 103) THEN
!--REACTION D3 IS H+H+O2>H2+O2  
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=3   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.763155d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.6015061d0
  ER(J)=7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 104) THEN
!--REACTION D4 IS H+H+O>H2+O
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=4   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.636160d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5978748d0
  ER(J)=7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 105) THEN
!--REACTION D5 IS H+H+OH>H2+OH
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=5   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=4.720037d16/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.6002319d0
  ER(J)=7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 106) THEN
!--REACTION D6 IS H+H+H2O>H2+H2O
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=6   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=12.d0*9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  IF (IRATE == 1) AC(J)=5.648955d17/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-0.5993142d0
  ER(J)=7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 107) THEN
! REACTION D7 IS H2+H2->2H+H2  
  LE(J)=1   
  ME(J)=1   
  KP(J)=2   
  LP(J)=2   
  MP(J)=1   
  CI(J)=VDC 
  AE(J)=52105.d0*BOLTZ  
  AC(J)=2.5d0*5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0  
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.258131d+12/(AVOG*1000.d0)  
      BC(J)=8.798179d-01
    ELSE 
      AC(J)=1.124184d19/(AVOG*1000.d0)  
      BC(J)=-1.097974d0  
    END IF
  END IF
  ER(J)=-7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 108) THEN  
!--REACTION D8 IS H2+H>2H+H  
  LE(J)=1   
  ME(J)=2   
  KP(J)=2   
  LP(J)=2  
  MP(J)=2   
  CI(J)=VDC 
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=4.897980d+13/(AVOG*1000.d0)  
      BC(J)=4.199751d-01
    ELSE 
      AC(J)=1.799959d20/(AVOG*1000.d0)  
      BC(J)=-1.571824d0
    END IF
  END IF
  ER(J)=-7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 109) THEN
!--REACTION D9 IS H2+O2>2H+O2  
  LE(J)=1   
  ME(J)=3   
  KP(J)=2   
  LP(J)=2  
  MP(J)=3   
  CI(J)=VDC 
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=9.199534d+11/(AVOG*1000.d0)  
      BC(J)=8.911783d-01
    ELSE 
      AC(J)=1.115394d20/(AVOG*1000.d0)  
      BC(J)=-1.519741d0
    END IF
  END IF
  ER(J)=-7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 110) THEN
!--REACTION D10 IS H2+O>2H+O  
  LE(J)=1   
  ME(J)=4   
  KP(J)=2   
  LP(J)=2  
  MP(J)=4   
  CI(J)=VDC 
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.282898d+14/(AVOG*1000.d0)  
      BC(J)=2.374446d-01
    ELSE 
      AC(J)=2.110156d20/(AVOG*1000.d0)  
      BC(J)=-1.565169d0
    END IF
  END IF
  ER(J)=-7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 111) THEN
!--REACTION D11 IS H2+OH>2H+OH  
  LE(J)=1   
  ME(J)=5   
  KP(J)=2   
  LP(J)=2  
  MP(J)=5   
  CI(J)=VDC 
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.260180d+11/(AVOG*1000.d0)  
      BC(J)=1.045312d+00
    ELSE 
      AC(J)=6.346795d19/(AVOG*1000.d0)  
      BC(J)=-1.443632d0
    END IF
  END IF
  ER(J)=-7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 112) THEN
!--REACTION D12 IS H2+H2O>2H+H2O  
  LE(J)=1   
  ME(J)=6   
  KP(J)=2   
  LP(J)=2  
  MP(J)=6   
  CI(J)=VDC 
  AE(J)=52105.d0*BOLTZ  
  AC(J)=12.d0*5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.378291d+11/(AVOG*1000.d0)  
      BC(J)=1.322471d+00
    ELSE 
      AC(J)=5.874078d14/(AVOG*1000.d0)  
      BC(J)=0.1337761d0
    END IF
  END IF
  ER(J)=-7.239220d-19
END IF
!
IF (JCD == 0 .AND. IRM == 113) THEN
!--REACTION D13 IS OH+H+H2>H2O+H2   
  LE(J)=5
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=1   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.5d0*2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0  
  IF (IRATE == 1) AC(J)=2.670047d22/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.956666d0  
  ER(J)=8.252333d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 114) THEN   
!--REACTION D14 IS OH+H+H>H2O+H   
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=2   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.354999d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.932729d0   
  ER(J)=8.252333d-19   
END IF
!
IF (JCD == 0 .AND. IRM == 115) THEN   
!--REACTION D15 IS OH+H+O2>H2O+O2   
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=3   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.314137d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.931931d0 
  ER(J)=8.252333d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 116) THEN
!--REACTION D16 IS OH+H+O>H2O+O   
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=4   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.347222d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.932318d0 
  ER(J)=8.252333d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 117) THEN
!--REACTION D17 IS OH+H+OH>H2O+OH   
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=5   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=8.422456d21/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-1.933605d0  
  ER(J)=8.252333d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 118) THEN
!--REACTION D18 IS OH+H+H2O>H2O+H2O   
  LE(J)=5   
  ME(J)=2  
  KP(J)=-1   
  LP(J)=6   
  MP(J)=6   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=12.d0*2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  IF (IRATE == 1) AC(J)=2.412574d23/(AVOG*1000.d0)**2  
  IF (IRATE == 1) BC(J)=-2.019927d0   
  ER(J)=8.252333d-19   
END IF
!
IF (JCD == 0 .AND. IRM == 119) THEN
!--REACTION D19 IS H2O+H2>OH+H+H2   
  LE(J)=6   
  ME(J)=1   
  KP(J)=5   
  LP(J)=2   
  MP(J)=1   
  CI(J)=VDC 
  AE(J)=59743.d0*BOLTZ  
  AC(J)=2.5d0*8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=6.027961d+15/(AVOG*1000.d0)  
      BC(J)=1.632129d-01
    ELSE
      AC(J)=3.292285d33/(AVOG*1000.d0)
      BC(J)=-4.719732d0
    END IF
  END IF
  ER(J)=-8.252333d-19   
END IF
!
IF (JCD == 0 .AND. IRM == 120) THEN
!--REACTION D20 IS H2O+H>OH+H+H   
  LE(J)=6   
  ME(J)=2   
  KP(J)=5   
  LP(J)=2   
  MP(J)=2   
  CI(J)=VDC 
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=3.031394d+13/(AVOG*1000.d0)  
      BC(J)=6.817159d-01
    ELSE
      AC(J)=8.936d22/(AVOG*1000.d0)  !1.900933d33/(AVOG*1000.d0)
      BC(J)=-1.835d0  !-4.858611d0
    END IF
  END IF
  ER(J)=-8.252333d-19    
END IF
!
IF (JCD == 0 .AND. IRM == 121) THEN
!--REACTION D21 IS H2O+O2>OH+H+O2   
  LE(J)=6   
  ME(J)=3   
  KP(J)=5   
  LP(J)=2   
  MP(J)=3   
  CI(J)=VDC 
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=9.889330d+15/(AVOG*1000.d0)  
      BC(J)=6.684782d-03
    ELSE
      AC(J)=9.336914d32/(AVOG*1000.d0)
      BC(J)=-4.683550d0
    END IF
  END IF
  ER(J)=-8.252333d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 122) THEN
!--REACTION D22 IS H2O+O>OH+H+O
  LE(J)=6   
  ME(J)=4   
  KP(J)=5   
  LP(J)=2   
  MP(J)=4   
  CI(J)=VDC 
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=2.637473d+18/(AVOG*1000.d0)  
      BC(J)=-6.616960d-01
    ELSE
      AC(J)=8.936d22/(AVOG*1000.d0)  !9.055880d33/(AVOG*1000.d0)
      BC(J)=-1.835d0  !-4.942958d0
    END IF
  END IF
  ER(J)=-8.252333d-19    
END IF
!
IF (JCD == 0 .AND. IRM == 123) THEN
!--REACTION D23 IS H2O+OH>OH+H+OH
  LE(J)=6   
  ME(J)=5   
  KP(J)=5   
  LP(J)=2   
  MP(J)=5   
  CI(J)=VDC 
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=1.430367d+14/(AVOG*1000.d0)  
      BC(J)=4.977865d-01
    ELSE
      AC(J)=1.457072d33/(AVOG*1000.d0)
      BC(J)=-4.735936d0
    END IF
  END IF
  ER(J)=-8.252333d-19   
END IF
!
IF (JCD == 0 .AND. IRM == 124) THEN
!--REACTION D24 IS H2O+H2O>OH+H+H2O   
  LE(J)=6   
  ME(J)=6   
  KP(J)=5   
  LP(J)=2   
  MP(J)=6   
  CI(J)=VDC 
  AE(J)=59743.d0*BOLTZ
  AC(J)=12.d0*8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  IF (JRATE == 1) THEN
    IF (CI(J) < 0.) THEN !TCE
      AC(J)=3.472299d+15/(AVOG*1000.d0)  
      BC(J)=4.256304d-01
    ELSE
      AC(J)=2.735897d22/(AVOG*1000.d0)
      BC(J)=-1.554205d0
    END IF
  END IF
  ER(J)=-8.252333d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 125) THEN
!--REACTION D25 IS OH+OH>H20+O
  LE(J)=5  
  ME(J)=5   
  KP(J)=0   
  LP(J)=6  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=50.d0*BOLTZ  
  AC(J)=1.506d9/(AVOG*1000.d0)  
  BC(J)=1.14d0
  IF (IRATE == 1) AC(J)=1.499219d9/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=1.140777d0
  ER(J)=1.113715d-19
END IF
!
IF (JCD == 0 .AND. IRM == 126) THEN
!--REACTION D26 IS H2O+O>OH+OH
  LE(J)=6  
  ME(J)=4   
  KP(J)=0   
  LP(J)=5  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=8613.d0*BOLTZ  
  AC(J)=2.220d10/(AVOG*1000.D00)  
  BC(J)=1.089d0
  IF (IRATE == 1) AC(J)=9.868296d8/(AVOG*1000.D00)  
  IF (IRATE == 1) BC(J)=1.451492d0
  ER(J)=-1.113715d-19
END IF
!
IF (JCD == 0 .AND. IRM == 127) THEN
!--REACTION D27 IS H2+O>OH+H   
  LE(J)=1  
  ME(J)=4   
  KP(J)=0   
  LP(J)=5  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=3163.d0*BOLTZ  
  AC(J)=5.119d4/(AVOG*1000.d0)  
  BC(J)=2.67d0
  IF (IRATE == 1) AC(J)=2.733975d4/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=2.760115d0
  ER(J)=-1.006026d-20   !Bird -0.13d-19  
END IF
!
IF (JCD == 0 .AND. IRM == 128) THEN
!--REACTION D28 IS OH+H>H2+O   
  LE(J)=5
  ME(J)=2   
  KP(J)=0   
  LP(J)=1  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=2240.d0*BOLTZ  
  AC(J)=2.701d4/(AVOG*1000.d0)  
  BC(J)=2.65d0
  IF (IRATE == 1) AC(J)=2.465206d4/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=2.659041d0
  ER(J)=1.006026d-20   !Bird 0.13d-19   
END IF
!
IF (JCD == 0 .AND. IRM == 129) THEN
!--REACTION D29 IS H2+OH>H20+H 
  LE(J)=1  
  ME(J)=5   
  KP(J)=0   
  LP(J)=6  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=1660.d0*BOLTZ  
  AC(J)=1.024d8/(AVOG*1000.d0)  
  BC(J)=1.60d0  
  IF (IRATE == 1) AC(J)=1.050693d8/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=1.594793d0 
  ER(J)=1.013113d-19  !Bird 1.05D-19 
END IF
!
IF (JCD == 0 .AND. IRM == 130) THEN   
!--REACTION D30 IS H2O+H>H2+OH   
  LE(J)=6  
  ME(J)=2   
  KP(J)=0   
  LP(J)=1  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=9300.d0*BOLTZ  
  AC(J)=7.964d8/(AVOG*1000.d0)  
  BC(J)=1.528d0  
  IF (IRATE == 1) AC(J)=2.265672d7/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=1.939508d0
  ER(J)=-1.013113d-19  !Bird 1.05D-19   
END IF
!
IF (JCD == 0 .AND. IRM == 131) THEN   
!--REACTION D31 IS O2+H>OH+O
  LE(J)=3  
  ME(J)=2   
  KP(J)=0   
  LP(J)=5  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=8456.d0*BOLTZ  
  AC(J)=1.987d14/(AVOG*1000.D00)  
  BC(J)=0.d0  
  IF (IRATE == 1) AC(J)=4.243092d13/(AVOG*1000.D00)  
  IF (IRATE == 1) BC(J)=0.1832357d0
  ER(J)=-1.137477d-19  !Bird -1.17D-19  
END IF
!
IF (JCD == 0 .AND. IRM == 132) THEN   
!--REACTION D32 IS OH+O>H+O2   
  LE(J)=5  
  ME(J)=4   
  KP(J)=0   
  LP(J)=2  
  MP(J)=3   
  CI(J)=0.   
  AE(J)=0.  !--isebasti: paper provides -118.d0*BOLTZ  
  AC(J)=8.930d11/(AVOG*1000.d0)  
  BC(J)=0.338d0
  IF (IRATE == 1) AC(J)=8.889175d11/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=0.3388361d0
  ER(J)=1.137477d-19  !Bird 1.17D-19 
END IF
!
IF (JCD == 0 .AND. IRM == 133) THEN 
!--REACTION D33 IS H2+O2>OH+OH   
  LE(J)=1  
  ME(J)=3   
  KP(J)=0   
  LP(J)=5  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=24044.d0*BOLTZ  
  AC(J)=1.70d13/(AVOG*1000.d0)  
  BC(J)=0.
  IF (IRATE == 1) AC(J)=5.357596d11/(AVOG*1000.d0)  
  IF (IRATE == 1) BC(J)=0.3949028d0 
  ER(J)=-1.238079d-19 !Bird -1.3D-19
END IF
!
IF (JCD == 0 .AND. IRM == 134) THEN
!--REACTION D34 IS OH+OH>H2+O2   
  LE(J)=5
  ME(J)=5   
  KP(J)=0   
  LP(J)=1  
  MP(J)=3   
  CI(J)=0.
  AE(J)=14554.d0*BOLTZ  
  AC(J)=4.032d10/(AVOG*1000.D00)  
  BC(J)=0.317d0
  IF (IRATE == 1) AC(J)=1.390438d9/(AVOG*1000.D00)  
  IF (IRATE == 1) BC(J)=0.7079989d0
  ER(J)=1.238079d-19 !Bird 1.3D-19
END IF
!
!--------------------------------------------------------------
!
IF (JCD == 0 .AND. IRM == 150) THEN
!--REACTION 150 IS O2+O>O+O+O (SPARTA)  
  LE(J)=3   
  ME(J)=4   
  KP(J)=4   
  LP(J)=4   
  MP(J)=4   
  CI(J)=VDC 
  AE(J)=8.197d-19  
  AC(J)=1.660d-08
  BC(J)=-1.5d0
  !IF (JRATE == 1) THEN
  !  IF (CI(J) < 0.) THEN !TCE
  !    AC(J)=3.472299d+15/(AVOG*1000.d0)  
  !    BC(J)=4.256304d-01
  !  ELSE
  !    AC(J)=2.735897d22/(AVOG*1000.d0)
  !    BC(J)=-1.554205d0
  !  END IF
  !END IF
  ER(J)=-8.197d-19  
END IF
!
!--------------------------------------------------------------
!Testing reaction pairs
!
IF (JCD == 0 .AND. IRM == 201) THEN
!--REACTION 1 IS H+H+H2>2H2  
  J=1
  LE(J)=2
  ME(J)=2  
  KP(J)=-1   
  LP(J)=1   
  MP(J)=1   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=2.5d0*9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0  
  ER(J)=7.239220d-19
! REACTION 7 IS H2+H2->2H+H2  
  J=2
  LE(J)=1   
  ME(J)=1   
  KP(J)=2   
  LP(J)=2   
  MP(J)=1   
  CI(J)=0. !1.  
  AE(J)=52105.d0*BOLTZ  
  AC(J)=2.5d0*5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0  
  ER(J)=-7.239220d-19
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 202) THEN
!--REACTION 2 IS H+H+H>H2+H  
  J=1
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=2   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  ER(J)=7.239220d-19
!--REACTION 8 IS H2+H>2H+H  
  J=2
  LE(J)=1   
  ME(J)=2   
  KP(J)=2   
  LP(J)=2  
  MP(J)=2   
  CI(J)=0. !1.   
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  ER(J)=-7.239220d-19
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 203) THEN
!--REACTION 3 IS H+H+O2>H2+O2  
  J=1
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=3   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  ER(J)=7.239220d-19
!--REACTION 9 IS H2+O2>2H+O2  
  J=2
  LE(J)=1   
  ME(J)=3   
  KP(J)=2   
  LP(J)=2  
  MP(J)=3   
  CI(J)=0. !1.   
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  ER(J)=-7.239220d-19
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 204) THEN
!--REACTION 4 IS H+H+O>H2+O
  J=1
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=4   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  ER(J)=7.239220d-19
!--REACTION 10 IS H2+O>2H+O  
  J=2
  LE(J)=1   
  ME(J)=4   
  KP(J)=2   
  LP(J)=2  
  MP(J)=4   
  CI(J)=0. !1.   
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  ER(J)=-7.239220d-19
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 205) THEN
!--REACTION 5 IS H+H+OH>H2+OH
  J=1
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=5   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0  
  ER(J)=7.239220d-19
!--REACTION 11 IS H2+OH>2H+OH  
  J=2
  LE(J)=1   
  ME(J)=5   
  KP(J)=2   
  LP(J)=2  
  MP(J)=5   
  CI(J)=0. !1.   
  AE(J)=52105.d0*BOLTZ  
  AC(J)=5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  ER(J)=-7.239220d-19
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 206) THEN
!--REACTION 6 IS H+H+H2O>H2+H2O
  J=1
  LE(J)=2   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=1   
  MP(J)=6   
  CI(J)=0.  
  AE(J)=0.d0  
  AC(J)=12.d0*9.791d16/(AVOG*1000.d0)**2  
  BC(J)=-0.6d0
  AC(J)=5.648955d17/(AVOG*1000.d0)**2  
  BC(J)=-0.5993142d0
  ER(J)=7.239220d-19
  IREV(J)=2
!--REACTION 12 IS H2+H2O>2H+H2O  
  J=2
  LE(J)=1   
  ME(J)=6   
  KP(J)=2   
  LP(J)=2  
  MP(J)=6   
  CI(J)=0. !1.   
  AE(J)=52105.d0*BOLTZ  
  AC(J)=12.d0*5.086d16/(AVOG*1000.d0)  
  BC(J)=-0.362d0
  AC(J)=5.874078d14/(AVOG*1000.d0)  
  BC(J)=0.1337761d0
  ER(J)=-7.239220d-19
  IREV(J)=1
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 213) THEN
!--REACTION 13 IS OH+H+H2>H2O+H2   
  J=1
  LE(J)=5
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=1   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.5d0*2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0   
  ER(J)=8.252333d-19
!--REACTION 19 IS H2O+H2>OH+H+H2   
  J=2
  LE(J)=6   
  ME(J)=1   
  KP(J)=5   
  LP(J)=2   
  MP(J)=1   
  CI(J)=0. !1.
  AE(J)=59743.d0*BOLTZ  
  AC(J)=2.5d0*8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  ER(J)=-8.252333d-19 
END IF   
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 214) THEN   
!--REACTION 14 IS OH+H+H>H2O+H   
  J=1
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=2   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0   
  ER(J)=8.252333d-19
!--REACTION 20 IS H2O+H>OH+H+H   
  J=2
  LE(J)=6   
  ME(J)=2   
  KP(J)=5   
  LP(J)=2   
  MP(J)=2   
  CI(J)=0. !1.
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  ER(J)=-8.252333d-19
END IF   
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 215) THEN  
!--REACTION 15 IS OH+H+O2>H2O+O2   
  J=1
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=3   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0   
  ER(J)=8.252333d-19
!--REACTION 21 IS H2O+O2>OH+H+O2   
  J=2
  LE(J)=6   
  ME(J)=3   
  KP(J)=5   
  LP(J)=2   
  MP(J)=3   
  CI(J)=0. !1.
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  ER(J)=-8.252333d-19 
END IF   
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 216) THEN   
!--REACTION 16 IS OH+H+O>H2O+O   
  J=1
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=4   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0   
  ER(J)=8.252333d-19
!--REACTION 22 IS H2O+O>OH+H+O
  J=2
  LE(J)=6   
  ME(J)=4   
  KP(J)=5   
  LP(J)=2   
  MP(J)=4   
  CI(J)=0. !1.
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  ER(J)=-8.252333d-19   
END IF   
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 217) THEN   
!--REACTION 17 IS OH+H+OH>H2O+OH   
  J=1
  LE(J)=5   
  ME(J)=2   
  KP(J)=-1   
  LP(J)=6   
  MP(J)=5   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0   
  ER(J)=8.252333d-19
!--REACTION 23 IS H2O+OH>OH+H+OH
  J=2
  LE(J)=6   
  ME(J)=5   
  KP(J)=5   
  LP(J)=2   
  MP(J)=5   
  CI(J)=0. !1.
  AE(J)=59743.d0*BOLTZ  
  AC(J)=8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  ER(J)=-8.252333d-19 
END IF   
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 218) THEN   
!--REACTION 18 IS OH+H+H2O>H2O+H2O   
  J=1
  LE(J)=5   
  ME(J)=2  
  KP(J)=-1   
  LP(J)=6   
  MP(J)=6   
  CI(J)=0.   
  AE(J)=0.d0  
  AC(J)=12.d0*2.212d22/(AVOG*1000.d0)**2  
  BC(J)=-2.d0
  AC(J)=2.412574d23/(AVOG*1000.d0)**2  
  BC(J)=-2.019927d0 
  ER(J)=8.252333d-19 
  IREV(J)=2
!--REACTION 24 IS H2O+H2O>OH+H+H2O   
  J=2
  LE(J)=6   
  ME(J)=6   
  KP(J)=5   
  LP(J)=2   
  MP(J)=6   
  CI(J)=0. !1.
  AE(J)=59743.d0*BOLTZ  
  AC(J)=12.d0*8.936d22/(AVOG*1000.d0)
  BC(J)=-1.835d0
  AC(J)=2.735897d22/(AVOG*1000.d0)
  BC(J)=-1.554205d0
  ER(J)=-8.252333d-19 
  IREV(J)=1  
END IF   
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 225) THEN
!--REACTION 25 IS OH+OH>H20+O
  J=1
  LE(J)=5  
  ME(J)=5   
  KP(J)=0   
  LP(J)=6  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=50.d0*BOLTZ  
  AC(J)=1.506d9/(AVOG*1000.d0)  
  BC(J)=1.14d0
  ER(J)=1.113715d-19
!--REACTION 26 IS H2O+O>OH+OH
  J=2
  LE(J)=6  
  ME(J)=4   
  KP(J)=0   
  LP(J)=5  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=8613.d0*BOLTZ  
  AC(J)=2.220d10/(AVOG*1000.D00)  
  BC(J)=1.089d0
  ER(J)=-1.113715d-19
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 227) THEN
!--REACTION 27 IS H2+O>OH+H   
  J=1
  LE(J)=1  
  ME(J)=4   
  KP(J)=0   
  LP(J)=5  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=3163.d0*BOLTZ  
  AC(J)=5.119d4/(AVOG*1000.d0)  
  BC(J)=2.67d0
  ER(J)=-1.006026d-20   !Bird -0.13d-19  
!--REACTION 28 IS OH+H>H2+O   
  J=2
  LE(J)=5
  ME(J)=2   
  KP(J)=0   
  LP(J)=1  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=2240.d0*BOLTZ  ! Dav 2240.d0 !Conaire 2455.d0
  AC(J)=2.701d4/(AVOG*1000.d0)  !Dav 2.701d4 !Conaire 2.67d4
  BC(J)=2.65d0
  ER(J)=1.006026d-20   !Bird 0.13d-19  
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 229) THEN
!--REACTION 29 IS H2+OH>H20+H 
  J=1
  LE(J)=1  
  ME(J)=5   
  KP(J)=0   
  LP(J)=6  
  MP(J)=2   
  CI(J)=0.   
  AE(J)=1660.d0*BOLTZ  !Dav 1660  !Shat 1740 !Conaire 1732
  AC(J)=1.024d8/(AVOG*1000.d0)  !Dav 1.024d8  !Shat 2.17d8  !Conaire 2.16d8
  BC(J)=1.60d0  !Dav 1.60d0  !Shat 1.52d0 !Conaire 1.51d0
  AC(J)=1.050693d8/(AVOG*1000.d0)  !Dav 1.024d8  !Shat 2.17d8  !Conaire 2.16d8
  BC(J)=1.594793d0  !Dav 1.60d0  !Shat 1.52d0 !Conaire 1.51d0
  ER(J)=1.013113d-19  !Bird 1.05D-19   
  IREV(J)=2
!--REACTION 30 IS H2O+H>H2+OH   
  J=2
  LE(J)=6  
  ME(J)=2   
  KP(J)=0   
  LP(J)=1  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=9300.d0*BOLTZ  !Dav 9300.d0  !Shat 9030.d0  !Conaire 9252.d0
  AC(J)=7.964d8/(AVOG*1000.d0)  !Dav 7.964d8   !Shat 4.52d8   !Conaire 2.3d9
  BC(J)=1.528d0  !Dav 1.528d0  !Shat 1.6d0  !Conaire 1.4d0
  AC(J)=2.265672d7/(AVOG*1000.d0)  !Dav 7.964d8   !Shat 4.52d8   !Conaire 2.3d9
  BC(J)=1.939508d0  !Dav 1.528d0  !Shat 1.6d0  !Conaire 1.4d0
  ER(J)=-1.013113d-19  !Bird 1.05D-19   
  IREV(J)=1
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 231) THEN
!--REACTION 31 IS H+O2>OH+O
  J=1
  LE(J)=2  
  ME(J)=3   
  KP(J)=0   
  LP(J)=5  
  MP(J)=4   
  CI(J)=0.   
  AE(J)=8456.d0*BOLTZ  
  AC(J)=1.987d14/(AVOG*1000.D00)  
  BC(J)=0.d0  
  ER(J)=-1.137477d-19  !Bird -1.17D-19   
!--REACTION 32 IS OH+O>H+O2   
  J=2
  LE(J)=5  
  ME(J)=4   
  KP(J)=0   
  LP(J)=2  
  MP(J)=3   
  CI(J)=0.   
  AE(J)=0.  !--isebasti: paper provides -118.d0*BOLTZ  
  AC(J)=8.930d11/(AVOG*1000.d0)  
  BC(J)=0.338d0
  ER(J)=1.137477d-19  !Bird 1.17D-19   
END IF
!--------------------------------------------------------------
IF (JCD == 0 .AND. IRM == 233) THEN
!--REACTION 33 IS H2+O2>OH+OH   
  J=1
  LE(J)=1  
  ME(J)=3   
  KP(J)=0   
  LP(J)=5  
  MP(J)=5   
  CI(J)=0.   
  AE(J)=24044.d0*BOLTZ  
  AC(J)=1.70d13/(AVOG*1000.d0)  
  BC(J)=0.   
  ER(J)=-1.238079d-19 !Bird -1.3D-19
!--REACTION 34 IS OH+OH>H2+O2   
  J=2
  LE(J)=5
  ME(J)=5   
  KP(J)=0   
  LP(J)=1  
  MP(J)=3   
  CI(J)=0.
  AE(J)=14554.d0*BOLTZ  
  AC(J)=4.032d10/(AVOG*1000.D00)  
  BC(J)=0.317d0
  ER(J)=1.238079d-19 !Bird 1.3D-19
END IF
!--------------------------------------------------------------
!
!
RETURN  
END SUBROUTINE OXYGEN_HYDROGEN  
!  
!***************************************************************************  
!*************************END OF GAS DATABASE*******************************  
!***************************************************************************  
!  
SUBROUTINE ALLOCATE_GAS  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
ALLOCATE (FSP(MSP,2),SP(5,MSP),SPR(3,MSP),SPM(8,MSP,MSP),ISPR(2,MSP),ISPV(MSP),ENTR(6,MSP,2),NRSP(MSP,MSP),      &  
          VMP(MSP,2),UVMP(MSP,2),VNMAX(MSP),CR(MSP),TCOL(MSP,MSP),ISPRC(MSP,MSP),SPRC(2,MSP,MSP,MSP),STAT=ERROR)  !--isebasti: UVMP included
!  
IF (ERROR /= 0) THEN  
  WRITE (*,*)'PROGRAM COULD NOT ALLOCATE SPECIES VARIABLES',ERROR  
END IF  
!  
ALLOCATE (NEX(MMEX,MSP,MSP),NSPEX(MSP,MSP),SPEX(3,MMEX,MSP,MSP),ISPEX(MMEX,0:4,MSP,MSP),TREACG(4,MSP),         &  
          PSF(MMEX),TREACL(4,MSP),TNEX(MEX),STAT=ERROR)  
!  
IF (ERROR /= 0) THEN  
  WRITE (*,*)'PROGRAM COULD NOT ALLOCATE Q-K REACTION VARIABLES',ERROR  
END IF  
!  
IF (MMVM > 0) THEN  !--isebasti: IDL,CPER,DPER,IREV included
  ALLOCATE (SPVM(4,MMVM,MSP),ISPVM(2,MMVM,MSP),IDL(MMVM,MSP),DPER(MSP),STAT=ERROR)  
    IF (AHVIB .EQ. 1)THEN
       ALLOCATE(IDVIB(MSP),MDVIB(MMVM,MSP),EDVIB(0:MMDVIB,MMVM,MSP))
       IDVIB=0
    END IF 
  IF (ERROR /= 0) THEN  
    WRITE (*,*)'PROGRAM COULD NOT ALLOCATE VIBRATION VARIABLES',ERROR  
  END IF  
END IF  
!  
IF (MNRE > 0) THEN  
  ALLOCATE (CI(MNRE),AE(MNRE),AC(MNRE),BC(MNRE),ER(MNRE),REA(6,MNRE),LE(MNRE),ME(MNRE),KP(MNRE),LP(MNRE),MP(MNRE),      &  
            IREA(2,MNRE),JREA(2,MNRE,2),IRCD(MNRE,MSP,MSP),NREA(2,MNRE),CPER(MNRE,2),IREV(MNRE),STAT=ERROR)  
  IF (ERROR /= 0) THEN  
    WRITE (*,*)'PROGRAM COULD NOT ALLOCATE REACTION VARIABLES',ERROR  
  END IF  
END IF   
!  
IF (MTBP > 0) THEN  
  ALLOCATE (THBP(MTBP,MSP),STAT=ERROR)  
  IF (ERROR /= 0) THEN  
    WRITE (*,*)'PROGRAM COULD NOT ALLOCATE THIRD BODY VARIABLES',ERROR  
  END IF  
END IF  
!  
IF (MNSR > 0) THEN  
  ALLOCATE (ERS(MNSR),LIS(2,MNSR),LRS(6,MNSR),ISRCD(MNSR,MSP),STAT=ERROR)  
  IF (ERROR /= 0) THEN  
    WRITE (*,*)'PROGRAM COULD NOT ALLOCATE SURFACE REACTION VARIABLES',ERROR  
  END IF  
END IF  
!  
RETURN  
END SUBROUTINE ALLOCATE_GAS  
!  
!*****************************************************************************  
!  
SUBROUTINE READ_RESTART  
!  
USE MOLECS  
USE GEOM  
USE GAS  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
INTEGER :: ZCHECK  
!  
101 CONTINUE  
OPEN (7,FILE='PARAMETERS.DAT',FORM='UNFORMATTED',ERR=101) !--isebasti: replace binary by unformatted  
READ (7) NCCELLS,NCELLS,MMRM,MMVM,MNM,MNRE,MNSR,MSP,MTBP,ILEVEL,MDIV,IRECOM,MMEX,MEX,ISF,NBINS,NSNAP !--isebasti: included ISF,NBINS,NSAP  
CLOSE(7)  
!  
IF (MMVM > 0) THEN  
  ALLOCATE (PX(MNM),PTIM(MNM),PROT(MNM),IPCELL(MNM),IPSP(MNM),ICREF(MNM),IPCP(MNM),PV(3,MNM),      &  
       IPVIB(0:MMVM,MNM),STAT=ERROR)  
ELSE  
  IF (MMRM > 0) THEN  
    ALLOCATE (PX(MNM),PTIM(MNM),PROT(MNM),IPCELL(MNM),IPSP(MNM),ICREF(MNM),IPCP(MNM),PV(3,MNM),STAT=ERROR)  
  ELSE  
    ALLOCATE (PX(MNM),PTIM(MNM),IPCELL(MNM),IPSP(MNM),ICREF(MNM),IPCP(MNM),PV(3,MNM),STAT=ERROR)  
  END IF    
END IF  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR MOLECULE ARRAYS',ERROR  
ENDIF  
!  
ALLOCATE (JDIV(0:ILEVEL,MDIV),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR JDIV ARRAY',ERROR  
ENDIF  
!  
ALLOCATE (CELL(4,NCELLS),ICELL(NCELLS),CCELL(5,NCCELLS),ICCELL(3,NCCELLS),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR CELL ARRAYS',ERROR  
ENDIF  
!  
ALLOCATE (COLLS(NCELLS),WCOLLS(NCELLS),CLSEP(NCELLS),REAC(MNRE),SREAC(MNSR),VAR(21,NCELLS), &  
          VARSP(0:11,NCELLS,MSP),VARS(0:32+MSP,2),CS(0:8+MMVM,NCELLS,MSP),CSS(0:8,2,MSP,2), &  !--isebasti: correcting CS allocation
          CSSS(6,2),CST(0:4,NCELLS),BINS(0:NBINS,4,MSP),BIN(0:NBINS,4),&
          PDFS(0:NBINS,4,MSP),PDF(0:NBINS,4),NDROT(MSP,100),NDVIB(MMVM,MSP,0:100),STAT=ERROR) !--isebasti: CST,BINS,BIN,PDFS,PDF included
!IF (MNRE > 0) ALLOCATE (NPVIB(2,NCELLS,MNRE,MSP,MMVM,0:50),TNPVIB(2,NCELLS,MNRE,MSP),STAT=ERROR) 
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR SAMPLING ARRAYS',ERROR  
ENDIF  
!  
CALL ALLOCATE_GAS  
!  
102 CONTINUE  
OPEN (7,FILE='RESTART.DAT',FORM='UNFORMATTED',ERR=102) !--isebasti: replace binary by unformatted  
READ (7) AC,AE,AVDTM,BC,BOLTZ,CCELL,CELL,CI,CLSEP,COLLS, &  
         CPDTM,CR,CS,CSS,CSSS,CTM,CXSS,CST,BINS,BIN,PDFS,PDF,NSPDF,NDROT,NDVIB,DDIV,DPI,DTM,DTSAMP,DTOUT, & 
         ENTMASS,ENTR,ER,ERROR,ERS,FDEN,FMA,FND,FNUM,FRACSAM,FSP,FP,FPR,FREM,FSPEC, &  
         FTMP,FTIME,FRTMP,FVTMP,GASCODE,ICCELL,ICELL,ICREF,IFX,IMTS,IPCELL,IPCP, &  
         IPSP,IPVIB,IRCD,IREA,IREM,ISECS,ISF,ISPEX,ISPR,ISPRC,ISPV,ISPVM,ISRCD,ITYPE,IVB,IWF,JCD, &  
         JDIV,JREA,KP,LE,LIS,LP,LRS,ME,MOLSC,MP,MVER,NCCELLS,NCELLS, &  
         NCIS,NDIV,NEX,NM,NMISAMP,NNC,NOUT,NREA,NRSP,NSAMP,NSPEX,NVER,OUTRAT,SAMPRAT,PI,PROT,PTIM,PV,PX, &  
         REAC,RGFS,RMAS,REA,SP,SPEX,SPI,SPM,SPR,SPRC,SPV,SPVM,IDL,CPER,DPER,ENERS,ENERS0,IREV,SREAC, &  !,NPVIB,TNPVIB
         TCOL,TDISS,TFOREX,TRECOMB,TREVEX,THBP,TISAMP,TPOUT,TREF,TLIM,TOTCOL,PCOLLS,TOTDUP,TOTMOV,     &  
         TREACG,TREACL,TOUT,TPDTM,TREF,TSAMP,TSURF,VAR,VARS,VARSP,VELOB,VFX,VFY,UVFX,UFND,UFTMP,UVMP,VMP, & 
         VMPM,VNMAX,VSURF,WCOLLS,WFM,XB,XREM,XVELS,YVELS,TNEX,ZCHECK 
!--isebasti: CST,BINS,BIN,PDFS,PDF,UVFX,UFND,UFTMP,UVMP,IDL,CPER,DPER,ENERS,ENERS0,IREV,NPVIB,TNPVIB included 
CLOSE(7)  
!  
IF (ZCHECK /= 1234567) THEN  
  WRITE (9,*) NM,' Molecules, Check integer =',ZCHECK  
  STOP  
ELSE  
  WRITE (9,*) 'Restart file read, Check integer=',ZCHECK    
END IF  
!  
RETURN  
END SUBROUTINE READ_RESTART  
!  
!*****************************************************************************  
!  
SUBROUTINE WRITE_RESTART  
!  
USE MOLECS  
USE GEOM  
USE GAS  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
INTEGER :: ZCHECK
!  
ZCHECK=1234567  
!  
101 CONTINUE  
OPEN (7,FILE='PARAMETERS.DAT',FORM='UNFORMATTED',ERR=101) !--isebasti: replace binary by unformatted  
WRITE (7) NCCELLS,NCELLS,MMRM,MMVM,MNM,MNRE,MNSR,MSP,MTBP,ILEVEL,MDIV,IRECOM,MMEX,MEX,ISF,NBINS,NSNAP !--isebasti: included ISF,NBINS,NSAP  
CLOSE(7)  
!  
102 CONTINUE  
OPEN (7,FILE='RESTART.DAT',FORM='UNFORMATTED',ERR=102) !--isebasti: replace binary by unformatted  
WRITE (7)AC,AE,AVDTM,BC,BOLTZ,CCELL,CELL,CI,CLSEP,COLLS, &  
         CPDTM,CR,CS,CSS,CSSS,CTM,CXSS,CST,BINS,BIN,PDFS,PDF,NSPDF,NDROT,NDVIB,DDIV,DPI,DTM,DTSAMP,DTOUT, & 
         ENTMASS,ENTR,ER,ERROR,ERS,FDEN,FMA,FND,FNUM,FRACSAM,FSP,FP,FPR,FREM,FSPEC, &  
         FTMP,FTIME,FRTMP,FVTMP,GASCODE,ICCELL,ICELL,ICREF,IFX,IMTS,IPCELL,IPCP, &  
         IPSP,IPVIB,IRCD,IREA,IREM,ISECS,ISF,ISPEX,ISPR,ISPRC,ISPV,ISPVM,ISRCD,ITYPE,IVB,IWF,JCD, &  
         JDIV,JREA,KP,LE,LIS,LP,LRS,ME,MOLSC,MP,MVER,NCCELLS,NCELLS, &  
         NCIS,NDIV,NEX,NM,NMISAMP,NNC,NOUT,NREA,NRSP,NSAMP,NSPEX,NVER,OUTRAT,SAMPRAT,PI,PROT,PTIM,PV,PX, &  
         REAC,RGFS,RMAS,REA,SP,SPEX,SPI,SPM,SPR,SPRC,SPV,SPVM,IDL,CPER,DPER,ENERS,ENERS0,IREV,SREAC, &  !,NPVIB,TNPVIB
         TCOL,TDISS,TFOREX,TRECOMB,TREVEX,THBP,TISAMP,TPOUT,TREF,TLIM,TOTCOL,PCOLLS,TOTDUP,TOTMOV,     &  
         TREACG,TREACL,TOUT,TPDTM,TREF,TSAMP,TSURF,VAR,VARS,VARSP,VELOB,VFX,VFY,UVFX,UFND,UFTMP,UVMP,VMP, & 
         VMPM,VNMAX,VSURF,WCOLLS,WFM,XB,XREM,XVELS,YVELS,TNEX,ZCHECK
!--isebasti: CST,BINS,BIN,PDFS,PDF,UVFX,IDL,CPER,DPER,ENERS,ENERS0,IREV,NPVIB,TNPVIB,UVFX,UFND,UFTMP,UVMP included    
CLOSE(7)  
!
WRITE (9,*) 'Restart files written'  
!  
RETURN  
END SUBROUTINE WRITE_RESTART  
!  
!*****************************************************************************  
!  
SUBROUTINE FIND_CELL(X,NCC,NSC)  
!  
USE MOLECS  
USE GEOM  
USE CALC  
!  
IMPLICIT NONE  
!  
INTEGER :: N,L,M,NSC,NCC,ND  
REAL(KIND=8) :: X,FRAC,DSC  
!  
!--NCC collision cell number  
!--NSC sampling cell number  
!--X location  
!--ND division number  
!--DSC the ratio of the sub-division width to the division width  
!  
ND=(X-XB(1))/DDIV+0.99999999999999D00  
!  
IF (JDIV(0,ND) < 0) THEN    !the division is a level 0 (no sub-division) sampling cell  
  NSC=-JDIV(0,ND)  
!  IF (IFX == 0)  
  NCC=NCIS*(X-CELL(2,NSC))/(CELL(3,NSC)-CELL(2,NSC))+0.9999999999999999D00  
  NCC=NCC+ICELL(NSC)  
!  IF (NCC == 0) NCC=1  
  RETURN  
ELSE  !--the molecule is in a subdivided division  
  FRAC=(X-XB(1))/DDIV-DFLOAT(ND-1)  
  M=ND  
  DO N=1,ILEVEL  
    DSC=1.D00/DFLOAT(N+1)  
    DO L=1,2  !over the two level 1 subdivisions  
      IF (((L == 1).AND.(FRAC < DSC)).OR.((L == 2).AND.(FRAC >= DSC))) THEN  
        M=JDIV(N-1,M)+L  !the address in JDIV  
        IF (JDIV(N,M) < 0) THEN  
          NSC=-JDIV(N,M)  
          NCC=NCIS*(X-CELL(2,NSC))/(CELL(3,NSC)-CELL(2,NSC))+0.999999999999999D00   
          IF (NCC == 0) NCC=1  
          NCC=NCC+ICELL(NSC)  
          RETURN  
        END IF  
      END IF  
    END DO  
    FRAC=FRAC-DSC  
  END DO  
END IF          
WRITE (9,*) 'No cell for molecule at x=',X          
STOP  
END SUBROUTINE FIND_CELL  
!  
!*****************************************************************************  
!  
SUBROUTINE FIND_CELL_MB(X,NCC,NSC,TIM)  
!  
USE MOLECS  
USE GEOM  
USE CALC  
!  
IMPLICIT NONE  
!  
INTEGER :: N,L,M,NSC,NCC,ND  
REAL(KIND=8) :: X,FRAC,DSC,A,B,C,TIM  
!  
!--NCC collision cell number  
!--NSC sampling cell number  
!--X location  
!--ND division number  
!--DSC the ratio of the sub-division width to the division width  
!--TIM the time  
!  
A=(XB(2)+VELOB*TIM-XB(1))/DFLOAT(NDIV)      !--new DDIV  
ND=(X-XB(1))/A+0.99999999999999D00  
B=XB(1)+DFLOAT(ND-1)*A  
!  
!the division is a level 0 sampling cell  
NSC=-JDIV(0,ND)  
NCC=NCIS*(X-B)/A+0.99999999999999D00  
NCC=NCC+ICELL(NSC)  
RETURN  
WRITE (9,*) 'No cell for molecule at x=',X          
STOP  
END SUBROUTINE FIND_CELL_MB  
!  
!******************************************************************************  
!  
FUNCTION GAM(X)  
!  
!--calculates the Gamma function of X.  
!  
A=1.  
Y=X  
IF (Y < 1.) THEN  
  A=A/Y  
ELSE  
  Y=Y-1.  
  DO WHILE (Y >= 1.)  
    A=A*Y  
    Y=Y-1.  
  END DO  
END IF  
GAM=A*(1.-0.5748646*Y+0.9512363*Y**2-0.6998588*Y**3+  &  
       0.4245549*Y**4-0.1010678*Y**5)  
!  
RETURN  
!  
END FUNCTION GAM  
!  
!*****************************************************************************  
!  
FUNCTION ERF(S)  
!  
!--evaluates the error function of S  
!  
B=ABS(S)  
IF (B < 4.) THEN  
  C=EXP(-B*B)  
  T=1./(1.+0.3275911*B)  
  D=1.-(0.254829592*T-0.284496736*T*T+1.421413741*T*T*T- &  
    1.453152027*T*T*T*T+1.061405429*T*T*T*T*T)*C  
ELSE  
  D=1.  
END IF  
IF (S < 0.) D=-D  
ERF=D  
RETURN  
END FUNCTION ERF  
!  
!*****************************************************************************  
!  
SUBROUTINE RVELC(U,V,VMP,IDT)  
!  
USE CALC  
!  
IMPLICIT NONE  
!  
!--generates two random velocity components U and V in an equilibrium  
!--gas with most probable speed VMP
INTEGER :: IDT !included IDT  
REAL(KIND=8) :: U,V,VMP,A,B,RANF !--isebasti: included RANF  
!  
CALL ZGF(RANF,IDT)  
A=DSQRT(-DLOG(RANF))  
CALL ZGF(RANF,IDT)  
B=DPI*RANF  
U=A*DSIN(B)*VMP  
V=A*DCOS(B)*VMP  
RETURN  
!  
END SUBROUTINE RVELC  
!  
!*****************************************************************************  
!  
SUBROUTINE SROT(L,TEMP,ROTE,IDT)  
!  
!--sets a typical rotational energy ROTE of species L  
!  
USE CALC  
USE GAS  
!  
IMPLICIT NONE  
!  
INTEGER :: I,L,IDT !included IDT
REAL(KIND=8) :: A,B,ROTE,ERM,TEMP,RANF !--isebasti: included RANF  
!      
IF (ISPR(1,L).EQ.2) THEN  
  CALL ZGF(RANF,IDT)  
  ROTE=-DLOG(RANF)*BOLTZ*TEMP  
ELSE  
  A=0.5D00*ISPR(1,L)-1.D00  
  I=0  
  DO WHILE (I == 0)  
    CALL ZGF(RANF,IDT)  
    ERM=RANF*10.D00  
!--there is an energy cut-off at 10 kT  
    B=((ERM/A)**A)*DEXP(A-ERM)  
    CALL ZGF(RANF,IDT)  
    IF (B > RANF) I=1  
  END DO  
  ROTE=ERM*BOLTZ*TEMP  
END IF  
!  
RETURN  
!  
END SUBROUTINE SROT  
!  
!*****************************************************************************  
!  
SUBROUTINE SVIB(L,TEMP,IVIB,K,IDT)  
!  
!--sets a typical vibrational state at temp. TEMP of mode K of species L  
!  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
INTEGER :: K,L,N,IDT !included IDT
REAL(KIND=8) :: TEMP,RANF !--isebasti: included RANF  
INTEGER :: IVIB  
!  
CALL ZGF(RANF,IDT)  
N=-DLOG(RANF)*TEMP/SPVM(1,K,L)                 !eqn(11.24)  
!--the state is truncated to an integer  
IVIB=N  
END  
!  
!*****************************************************************************  
!  
SUBROUTINE INIT_REAC  
!  
!--initialise the reaction variables (in the old reaction model)  
!  
USE MOLECS  
!  
USE GAS  
USE CALC  
USE GEOM  
USE OUTPUT  
!   
IMPLICIT NONE  
!  
INTEGER :: I,K,KK,L,M,N,LS,MS,NNRE,NTBP  
REAL(KIND=8) :: A,B,C,D,EPS,AL,X  
REAL :: GAM,AA,BB  
!  
!--I,K,L,M,N working integers  
!--A,B working variables  
!--LS,MS species  
!--EPS symmetry factor  
!  
!  
!--convert the reaction data into the required form  
!  
IF (MNRE == 0) THEN  
  NNRE=1  
ELSE   
  NNRE=MNRE  
END IF  
IF (MTBP == 0) THEN  
  NTBP=1  
ELSE  
  NTBP=MTBP  
END IF  
!  
IF (MNRE > 0) THEN   
  REA=0.; IREA=0; NREA=0; JREA=0; NRSP=0; IRCD=0 ; REAC=0.  
!  
  DO  N=1,MNRE  
    L=LE(N)  
    M=ME(N)  
!--the pre-reaction species codes are L and M   
    IF ((L > 0).AND.(M > 0)) THEN  
      IREA(1,N)=L  
      IREA(2,N)=M  
      IF (M < L) THEN  
        K = L  
        L = M  
        M = K  
      END IF  
      NRSP(L,M) = NRSP(L,M) + 1  
      IF (L /= M) NRSP(M,L) = NRSP(M,L) + 1  
      K = NRSP(L,M)  
!--this is the Kth reaction involving species L and M  
      IRCD(K,M,L)=N  
      IRCD(K,L,M)=N  
    END IF   
    K=KP(N)  
    L=LP(N)  
    M=MP(N)
    IF (K > 0) THEN  
!--three post-collision particles (dissociation)  
      NREA(1,N)=2  
      NREA(2,N)=1  
      JREA(1,N,1)=K  
      JREA(1,N,2)=L  
      JREA(2,N,1)=M  
    END IF    
    IF (K == 0) THEN  
!--two post-collision particles (exchange) 
      NREA(1,N)=1  
      NREA(2,N)=1  
      JREA(1,N,1)=L  
      JREA(2,N,1)=M  
    END IF  
    IF (K == -1) THEN  
!--one post collision particle (recombination)  
      NREA(1,N)=1  
      NREA(2,N)=0  
      JREA(1,N,1)=L  
      JREA(2,N,1)=M  
    END IF  
!  
    REA(1,N)=CI(N)  
    REA(2,N)=AE(N)  
    REA(3,N)=AC(N)  
    REA(4,N)=BC(N)  
    REA(5,N)=ER(N)  
  END DO  
!   
END IF  
!  
DO L=1,MNRE  
  DO M=1,MSP  
    DO N=1,MSP  
      IF (IRCD(L,N,M) == 0) IRCD(L,N,M)=IRCD(L,M,N)  
    END DO  
  END DO  
END DO
!
!--calculate the coefficients of the energy terms in steric factors (see SMILE documentation) 
!  
DO K=1,MNRE
  LS=IREA(1,K)  
  MS=IREA(2,K)  
  EPS=1.D00  
  IF (LS == MS) EPS=2.D00  
  I=NREA(1,K)+NREA(2,K)  
  AL=SPM(3,LS,MS)-0.5d0  !alpha
  X=REA(4,K)-0.5d0+AL
!
  IF (I == 1) THEN  
!--recombination reaction
    C=EPS*SPI*REA(3,K)*SPM(1,LS,MS)**0.5d0
    D=(2.d0**1.5d0)*(BOLTZ**REA(4,K))*SPM(2,LS,MS)*((2.d0-AL)*BOLTZ*SPM(5,LS,MS))**AL
    REA(6,K)=(C/D)
  END IF 
  IF (I == 2) THEN  
!--exchange reaction
    BB=2.d0-AL
    B=GAM(BB)
    C=EPS*SPI*REA(3,K)*(SPM(1,LS,MS)/(8.d0*BOLTZ))**0.5d0
!   D=SPM(2,LS,MS)*(BOLTZ**X)*(2.d0-AL*SPM(5,LS,MS))**AL  !Alexeenko-SMILE report with bug
!   REA(6,K)=(C/D)*(1.d0/B)                               !Alexeenko-SMILE report with bug
    D=SPM(2,LS,MS)*(BOLTZ**X)*(SPM(5,LS,MS))**AL          !Ivanov-SMILE report
    REA(6,K)=(C/D)                                        !Ivanov-report
  END IF
  IF (I == 3) THEN  
!--dissociation reaction
    IF (REA(1,K) < 0.) THEN !TCE
      BB=2.d0-AL
      B=GAM(BB)
      C=EPS*SPI*REA(3,K)*(SPM(1,LS,MS)/(8.d0*BOLTZ))**0.5d0
      D=SPM(2,LS,MS)*(BOLTZ**X)*(SPM(5,LS,MS))**AL        !Ivanov-SMILE report
      REA(6,K)=(C/D)                                      !Ivanov-SMILE report
    ELSE  !VDC
      BB=2.d0-AL
      B=GAM(BB)
      C=EPS*REA(3,K)*SPI*0.5d0*(SPM(1,LS,MS)/(2.d0*BOLTZ))**(0.5d0-AL)
      D=SPM(2,LS,MS)*B
      REA(6,K)=(C/D)
    END IF
  END IF
!
END DO  
!  
RETURN  
!  
END SUBROUTINE INIT_REAC  
!  
!*****************************************************************************  
!  
SUBROUTINE MOLECULES_ENTER  
!  
!--molecules enter boundary at XB(1) and XB(2) and may be removed behind a wave  
!  
USE MOLECS  
USE GAS  
USE CALC  
USE GEOM  
USE OUTPUT
!   
IMPLICIT NONE  
!  
INTEGER :: K,L,M,N,NENT,II,J,JJ,KK,NTRY,IDT=0 !included IDT  
REAL(KIND=8) :: A,B,AA,BB,U,VN,XI,X,DX,DY,DZ,RANF !--isebasti: included RANF  
!  
!--NENT number to enter in the time step  
!  
DO J=1,2       !--J is the end  
  IF (ITYPE(J) == 0) THEN  
    KK=1 !--the entry surface will normally use the reference gas (main stream) properties  
    IF ((J == 2).OR.((ISECS == 1).AND.(XB(2) > 0.D00))) KK=2  !--KK is 1 for reference gas 2 for the secondary stream  !--isebasti: AND replaced by OR
    DO L=1,MSP  
      A=ENTR(1,L,J)*DTM+ENTR(2,L,J)  
      NENT=A  
      ENTR(2,L,J)=A-NENT  
      IF (NENT > 0) THEN  
        DO M=1,NENT  
          IF (NM >= MNM) THEN  
            WRITE (*,*) 'EXTEND_MNM from MOLECULES_ENTER'                 
            CALL EXTEND_MNM(1.1)  
          END IF  
          NM=NM+1  
          AA=DMAX1(0.D00,ENTR(3,L,J)-3.D00)   
          BB=DMAX1(3.D00,ENTR(3,L,J)+3.D00)  
          II=0  
          DO WHILE (II == 0)  
            CALL ZGF(RANF,IDT)  
            B=AA+(BB-AA)*RANF  
            U=B-ENTR(3,L,J)  
            A=(2.D00*B/ENTR(4,L,J))*DEXP(ENTR(5,L,J)-U*U)  
            CALL ZGF(RANF,IDT)  
            IF (A > RANF) II=1  
          END DO  
          PV(1,NM)=B*UVMP(L,KK)  !--isebasti: VMP replaced by UVMP
          IF (J == 2) PV(1,NM)=-PV(1,NM)           
!             
          CALL RVELC(PV(2,NM),PV(3,NM),UVMP(L,KK),IDT)  !--isebasti: VMP replaced by UVMP
          PV(2,NM)=PV(2,NM)+VFY(J)  
!            
          IF (ISPR(1,L) > 0) CALL SROT(L,UFTMP(KK),PROT(NM),IDT)  !--isebasti: UFTMP replaced by UFTMP
!            
          IF (MMVM > 0) THEN
            IPVIB(0,NM)=0  
            DO K=1,ISPV(L)  
              IF (AHVIB .EQ. 0)THEN
                CALL SVIB(L,UFTMP(KK),IPVIB(K,NM),K,IDT)  
              ELSE IF (IDVIB(L) .EQ. 0)THEN
                CALL SVIB(L,UFTMP(KK),IPVIB(K,NM),K,IDT) 
              ELSE
                CALL SDVIB(L,UFTMP(KK),IPVIB(K,NM),K,IDT) 
              END IF
            END DO  
          END IF  
          IPSP(NM)=L  
!--advance the molecule into the flow  
          CALL ZGF(RANF,IDT)          
          XI=XB(J)  
          DX=DTM*RANF*PV(1,NM)  
          IF (IFX == 0) X=XI+DX  
          IF (IFX > 0) DY=DTM*RANF*PV(2,NM)  
          DZ=0.D00  
          IF (IFX == 2) DZ=DTM*RANF*PV(3,NM)  
          IF (IFX > 0) CALL AIFX(XI,DX,DY,DZ,X,PV(1,NM),PV(2,NM),PV(3,NM),IDT)  
          IF (IFX == 0) PX(NM)=X  
          PTIM(NM)=FTIME  
          IF (IVB == 0) CALL FIND_CELL(PX(NM),IPCELL(NM),JJ)  
          IF (IVB == 1) CALL FIND_CELL_MB(PX(NM),IPCELL(NM),JJ,PTIM(NM))  
          IPCP(NM)=0  
          IF (XREM > XB(1)) ENTMASS=ENTMASS+SP(5,L)  
        END DO  
      END IF  
    END DO  
  END IF  
END DO    
!  
!--stagnation streamline molecule removal  
IF (XREM > XB(1)) THEN  
  ENTMASS=FREM*ENTMASS  
  NTRY=0  
  DO WHILE ((ENTMASS > 0.D00).AND.(NTRY < 10000))  
    NTRY=NTRY+1  
    IF (NTRY == 10000) THEN  
      WRITE (*,*) 'Unable to find molecule for removal'  
      ENTMASS=0.D00  
      VNMAX=0.D00  
    END IF  
    CALL ZGF(RANF,IDT)  
    N=NM*RANF+0.9999999D00  
    IF (PX(N) > XREM) THEN  
      CALL ZGF(RANF,IDT)  
      !IF (RANF < ((PX(N)-XREM)/(XB(2)-XREM))**2) THEN  
      IF (DABS(VFY(1)) < 1.D-3) THEN  
        VN=DSQRT(PV(2,N)*PV(2,N)+PV(3,N)*PV(3,N))   !--AXIALLY SYMMETRIC STREAMLINE  
      ELSE  
        VN=DABS(PV(3,N))   !--TWO-DIMENSIONAL STREAMLINE  
      END IF    
      L=IPSP(N)  
      IF (VN > VNMAX(L)) VNMAX(L)=VN  
      CALL ZGF(RANF,IDT)  
      IF (RANF < VN/VNMAX(L)) THEN  
        CALL REMOVE_MOL(N)  
        ENTMASS=ENTMASS-SP(5,L)  
        NTRY=0  
      END IF  
      !END IF     
    END IF  
  END DO  
END IF  
  
!  
END SUBROUTINE MOLECULES_ENTER  
!  
!*****************************************************************************  
!  
SUBROUTINE EXTEND_MNM(FAC)  
!  
!--the maximum number of molecules is increased by a specified factor  
!--the existing molecules are copied TO disk storage  
!  
USE MOLECS  
USE CALC  
USE GAS  
!  
IMPLICIT NONE  
!  
INTEGER :: M,N,MNMN  
REAL :: FAC  
!  
!--M,N working integers  
!--MNMN extended value of MNM  
!--FAC the factor for the extension  
MNMN=FAC*MNM  
WRITE (*,*) 'Maximum number of molecules is to be extended from',MNM,' to',MNMN  
WRITE (*,*) '( if the additional memory is available !! )'  
OPEN (7,FILE='EXTMOLS.SCR',FORM='UNFORMATTED') !--isebasti: replace binary by unformatted  
WRITE (*,*) 'Start write to disk storage'  
DO N=1,MNM  
  IF (MMVM > 0) THEN  
    WRITE (7) PX(N),PTIM(N),PROT(N),(PV(M,N),M=1,3),IPSP(N),IPCELL(N),ICREF(N),IPCP(N),(IPVIB(M,N),M=0,MMVM)  
  ELSE  
    IF (MMRM > 0) THEN  
      WRITE (7) PX(N),PTIM(N),PROT(N),(PV(M,N),M=1,3),IPSP(N),IPCELL(N),ICREF(N),IPCP(N)  
    ELSE  
      WRITE (7) PX(N),PTIM(N),(PV(M,N),M=1,3),IPSP(N),IPCELL(N),ICREF(N),IPCP(N)  
    END IF    
  END IF    
END  DO  
WRITE (*,*) 'Disk write completed'  
CLOSE (7)  
IF (MMVM > 0) THEN  
  DEALLOCATE (PX,PTIM,PROT,PV,IPSP,IPCELL,ICREF,IPCP,IPVIB,STAT=ERROR)  
ELSE  
  IF (MMRM > 0) THEN  
    DEALLOCATE (PX,PTIM,PROT,PV,IPSP,IPCELL,ICREF,IPCP,STAT=ERROR)  
  ELSE  
    DEALLOCATE (PX,PTIM,PV,IPSP,IPCELL,ICREF,IPCP,STAT=ERROR)  
  END IF    
END IF  
IF (ERROR /= 0) THEN  
  WRITE (*,*)'PROGRAM COULD NOT DEALLOCATE MOLECULES',ERROR   
!  STOP  
END IF  
!  
WRITE (*,*) 'Molecule arrays have been deallocated'  
!  
IF (MMVM > 0) THEN  
  ALLOCATE (PX(MNMN),PTIM(MNMN),PROT(MNMN),PV(3,MNMN),IPSP(MNMN),IPCELL(MNMN),&
             ICREF(MNMN),IPCP(MNMN),IPVIB(0:MMVM,MNMN),STAT=ERROR)  
ELSE  
  IF (MMRM > 0) THEN  
    ALLOCATE (PX(MNMN),PTIM(MNMN),PROT(MNMN),PV(3,MNMN),IPSP(MNMN),IPCELL(MNMN),ICREF(MNMN),IPCP(MNMN),STAT=ERROR)  
  ELSE  
    ALLOCATE (PX(MNMN),PTIM(MNMN),PV(3,MNMN),IPSP(MNMN),IPCELL(MNMN),ICREF(MNMN),IPCP(MNMN),STAT=ERROR)  
  END IF    
END IF  
IF (ERROR /= 0) THEN  
  WRITE (*,*)'PROGRAM COULD NOT ALLOCATE SPACE FOR EXTEND_MNM',ERROR  
!  STOP  
END IF  
!  
PX=0.; PTIM=0.; PV=0.; IPSP=0; IPCELL=0; ICREF=0; IPCP=0  
IF (MMRM > 0) PROT=0.  
IF (MMVM > 0) IPVIB=0  
!--restore the original molecules  
OPEN (7,FILE='EXTMOLS.SCR',FORM='UNFORMATTED') !--isebasti: replace binary by unformatted  
WRITE (*,*) 'Start read back from disk storage'  
DO N=1,MNM  
  IF (MMVM > 0) THEN  
    READ (7) PX(N),PTIM(N),PROT(N),(PV(M,N),M=1,3),IPSP(N),IPCELL(N),ICREF(N),IPCP(N),(IPVIB(M,N),M=0,MMVM)  
  ELSE  
    IF (MMRM > 0) THEN  
      READ (7) PX(N),PTIM(N),PROT(N),(PV(M,N),M=1,3),IPSP(N),IPCELL(N),ICREF(N),IPCP(N)  
    ELSE  
      READ (7) PX(N),PTIM(N),(PV(M,N),M=1,3),IPSP(N),IPCELL(N),ICREF(N),IPCP(N)  
    END IF    
  END IF  
END DO  
WRITE (*,*) 'Disk read completed'  
CLOSE (7,STATUS='DELETE')  
!  
MNM=MNMN  
!  
RETURN  
END SUBROUTINE EXTEND_MNM  
!  
!*****************************************************************************  
!  
SUBROUTINE MOLECULES_MOVE  
!  
!--molecule moves appropriate to the time step  
!  
USE MOLECS  
USE GAS  
USE GEOM  
USE CALC  
USE OUTPUT
USE OMP_LIB
!  
IMPLICIT NONE  
!  
INTEGER :: N,L,M,K,NCI,J,II,JJ,IDT=0 !included IDT,iflag
REAL(KIND=8) :: A,B,X,XI,XC,DX,DY,DZ,DTIM,S1,XM,R,TI,DTC,POB,UR,WFI,WFR,WFRI,RANF !--isebasti: included RANF  
!  
!--N working integer  
!--NCI initial collision cell  
!--DTIM time interval for the move  
!--POB position of the outer boundary  
!--TI initial time  
!--DTC time interval to collision with surface  
!--UR radial velocity component  
!--WFI initial weighting factor  
!--WFR weighting factor radius  
!--WFRI initial weighting factor radius  
!
!$omp parallel &
!$omp private(idt,n,nci,dtim,wfi,ii,ti,xi,dx,x,dz,dy,r,j,l,dtc,xc,s1,wfr,wfri,k,m,jj) &
!$omp reduction(+:totmov,entmass)
!$    idt=omp_get_thread_num()  !thread id
!$omp do schedule (static)
!
DO N=1,NM
!  
  NCI=IPCELL(N)  
  IF (IMTS == 0) DTIM=DTM  
  IF (IMTS == 1) DTIM=2.D00*CCELL(3,NCI)  
  IF (FTIME-PTIM(N) > 0.5*DTIM) THEN  
    WFI=1.D00  
    IF (IWF == 1) WFI=1.D00+WFM*PX(N)**IFX  
    II=0 !--becomes 1 if a molecule is removed  
    TI=PTIM(N)  
    PTIM(N)=TI+DTIM  
    TOTMOV=TOTMOV+1  
!  
    XI=PX(N)  
    DX=DTIM*PV(1,N)  
    X=XI+DX  
!      
    IF (IFX > 0) THEN  
      DZ=0.D00  
      DY=DTIM*PV(2,N)  
      IF (IFX == 2) DZ=DTIM*PV(3,N)  
      R=DSQRT(X*X+DY*DY+DZ*DZ)  
    END IF    
!  
    IF (IFX == 0) THEN    
      DO J=1,2    ! 1 for minimum x boundary, 2 for maximum x boundary  
        IF (II == 0) THEN  
          IF (((J == 1).AND.(X < XB(1))).OR.((J == 2).AND.(X > (XB(2)+VELOB*PTIM(N))))) THEN  !--molecule crosses a boundary  
            IF ((ITYPE(J) == 0).OR.(ITYPE(J) == 3)) THEN            
              IF (XREM > XB(1)) THEN  
                L=IPSP(N)  
                ENTMASS=ENTMASS-SP(5,L)  
              END IF                  
              IPCELL(N)=-IPCELL(N) !molecule is marked for removel !--isebasti: original use CALL REMOVE_MOL(N); !N=N-1  
              II=1  
            END IF  
!            
            IF (ITYPE(J) == 1) THEN  
              IF ((IVB == 0).OR.(J == 1)) THEN  
                X=2.D00*XB(J)-X  
                PV(1,N)=-PV(1,N)  
              ELSE IF ((J == 2).AND.(IVB == 1)) THEN  
                DTC=(XB(2)+TI*VELOB-XI)/(PV(1,N)-VELOB)  
                XC=XI+PV(1,N)*DTC  
                PV(1,N)=-PV(1,N)+2.*VELOB  
                X=XC+PV(1,N)*(DTIM-DTC)              
              END IF    
            END IF              
!      
            IF (ITYPE(J) == 2) THEN  
              CALL REFLECT(N,J,X,IDT)  
                IF((J == 2).AND.(X < XB(1))) THEN !--isebasti: included to fix bug  
                  DO WHILE ((X<XB(1)).OR.(X>XB(2)))  
                   !WRITE(9,*) 'REFLECTED MOLEC CROSSING OPPOSITE BOUNDARY:',FTIME,N,X,PV(1,N)  
                    IF (X < XB(1)) CALL REFLECT(N,1,X,IDT) !molecules reflected at xb2 crossed xb1  
                    IF (X > XB(2)) CALL REFLECT(N,2,X,IDT) !molecules reflected at xb1 crossed xb2  
                   !WRITE(9,*) 'REFLECTED MOLEC CROSSING OPPOSITE BOUNDARY:',FTIME,N,X,PV(1,N)  
                  END DO  
                END IF  
            END IF    
          END IF  
        END IF  
      END DO  
    ELSE         !--cylindrical or spherical flow  
!--check boundaries      
      IF ((X < XB(1)).AND.(XB(1) > 0.D00)) THEN  
        CALL RBC(XI,DX,DY,DZ,XB(1),S1)  
        IF (S1 < 1.D00) THEN     !--intersection with inner boundary  
          IF (ITYPE(1) == 2) THEN !--solid surface  
            DX=S1*DX  
            DY=S1*DY  
            DZ=S1*DZ  
            CALL AIFX(XI,DX,DY,DZ,X,PV(1,N),PV(2,N),PV(3,N),IDT)  
            CALL REFLECT(N,1,X,IDT)       
          ELSE  
            IPCELL(N)=-IPCELL(N) !--isebasti: CALL REMOVE_MOL(N); !N=N-1 
            II=1  
          END IF  
        END IF         
      ELSE IF ((IVB == 0).AND.(R > XB(2))) THEN  
        CALL RBC(XI,DX,DY,DZ,XB(2),S1)  
        IF (S1 < 1.D00) THEN     !--intersection with outer boundary  
          IF (ITYPE(2) == 2) THEN !--solid surface  
            DX=S1*DX  
            DY=S1*DY  
            DZ=S1*DZ  
            CALL AIFX(XI,DX,DY,DZ,X,PV(1,N),PV(2,N),PV(3,N),IDT)  
            X=1.001D00*XB(2)  
            DO WHILE (X > XB(2))  
              CALL REFLECT(N,2,X,IDT)  
            END DO       
          ELSE  
            IPCELL(N)=-IPCELL(N) !--isebasti: CALL REMOVE_MOL(N); !N=N-1  
            II=1  
          END IF  
        END IF   
      ELSE IF ((IVB == 1).AND.(R > (XB(2)+PTIM(N)*VELOB))) THEN  
        IF (IFX == 1) UR=DSQRT(PV(1,N)**2+PV(2,N)**2)  
        IF (IFX == 2) UR=DSQRT(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
        DTC=(XB(2)+TI*VELOB-XI)/(UR-VELOB)  
        S1=DTC/DTIM  
        DX=S1*DX  
        DY=S1*DY  
        DZ=S1*DZ  
        CALL AIFX(XI,DX,DY,DZ,X,PV(1,N),PV(2,N),PV(3,N),IDT)  
        PV(1,N)=-PV(1,N)+2.*VELOB  
        X=X+PV(1,N)*(DTIM-DTC)       
      ELSE  
        CALL AIFX(XI,DX,DY,DZ,X,PV(1,N),PV(2,N),PV(3,N),IDT)  
      END IF  
!        
!--DIAGNOSTIC      
      IF (X > XB(2)+PTIM(N)*VELOB) THEN  
        WRITE (*,*) N,FTIME,X,XB(2)+PTIM(N)*VELOB  
      END IF  
!        
!--Take action on weighting factors  
      IF ((IWF == 1).AND.(II >= 0)) THEN  
        WFR=WFI/(1.D00+WFM*X**IFX)  
        L=0  
        WFRI=WFR  
        IF (WFR >= 1.D00) THEN  
          DO WHILE (WFR >= 1.D00)  
            L=L+1  
            WFR=WFR-1.D00  
          END DO  
        END IF  
        CALL ZGF(RANF,IDT)  
        IF (RANF <= WFR) L=L+1  
        IF (L == 0) THEN  
          IPCELL(N)=-IPCELL(N) !--isebasti: CALL REMOVE_MOL(N); !N=N-1  
          II=1  
        END IF  
        L=L-1          
        IF (L > 0) THEN  
          DO K=1,L
!$omp critical  
            IF (NM >= MNM) CALL EXTEND_MNM(1.1)   
            NM=NM+1  !--isebasti: for spherical and cylindrical geometries, it will cause a problem because NM value changes.
            PX(NM)=X  
            DO M=1,3  
              PV(M,NM)=PV(M,N)  
            END DO  
            IF (MMRM > 0) PROT(NM)=PROT(N)  
            IPCELL(NM)=ABS(IPCELL(N))  !--isebasti: I think we should remove ABS  
            IPSP(NM)=IPSP(N)  
            IPCP(NM)=IPCP(N)  
            IF (MMVM > 0) THEN  
              DO M=1,MMVM  
                IPVIB(M,NM)=IPVIB(M,N)  
              END DO  
            END IF  
            PTIM(NM)=PTIM(N)    !+5.D00*DFLOAT(K)*DTM  
!--note the possibility of a variable time advance that may take the place of the duplication buffer in earlier programs          
!  
            IF (PX(NM) > XB(2)+PTIM(NM)*VELOB) THEN  
              WRITE (*,*) 'DUP',NM,FTIME,PX(NM),XB(2)+PTIM(NM)*VELOB  
            END IF  
!$omp end critical
          END DO  
        END IF  
      END IF  
    END IF  
!      
    IF (II == 0) THEN    
      PX(N)=X  
        if ((px(n) > xb(1)).and.(px(n) < xb(2))) then  
          continue  
        else
!$omp critical    
          write (*,*) n,'Outside flowfield at',px(n)
!$omp end critical  
        end if  
      IF (IVB == 0) CALL FIND_CELL(PX(N),IPCELL(N),JJ)  
      IF (IVB == 1) CALL FIND_CELL_MB(PX(N),IPCELL(N),JJ,PTIM(N))  
    END IF  
!      
  END IF  
!      
END DO
!$omp end do
!$omp end parallel
!
!--isebasti: remove marked molecules  
N=0  
DO WHILE (N < NM)  
  N=N+1
  IF (IPCELL(N) < 0) THEN  
    CALL REMOVE_MOL(N)
    N=N-1 
  END IF
END DO 
!  
RETURN  
!  
END SUBROUTINE MOLECULES_MOVE  
!  
!*****************************************************************************  
!  
SUBROUTINE REFLECT(N,J,X,IDT)  
!  
!--reflects molecule N and samples the surface J properties  
!  
USE MOLECS  
USE GAS  
USE GEOM  
USE CALC  
USE OUTPUT
USE OMP_LIB  
!  
IMPLICIT NONE  
!  
INTEGER :: N,J,L,K,M,IDT !included IDT  
REAL(KIND=8) :: A,B,VMPS,DTR,X,XI,DX,DY,DZ,WF,RANF !--isebasti: included RANF 
REAL(KIND=8) :: VIBGEN !--han
!  
!--VMPS most probable velocity at the surface temperature  
!--DTR time remaining after molecule hits a surface  
!  
!iflag=0; if ((n == 66430).and.(ftime > 1.5255121136)) iflag=1 !--isebasti: lines for debugging  
!  
L=IPSP(N)  
WF=1.D00  
IF (IWF == 1) WF=1.D00+WFM*X**IFX
!$omp critical  
CSS(0,J,L,1)=CSS(0,J,L,1)+1.D00  
CSS(1,J,L,1)=CSS(1,J,L,1)+WF  
CSS(2,J,L,1)=CSS(2,J,L,1)+WF*PV(1,N)*SP(5,L)  
CSS(3,J,L,1)=CSS(3,J,L,1)+WF*(PV(2,N)-VSURF(J))*SP(5,L)  
CSS(4,J,L,1)=CSS(4,J,L,1)+WF*PV(3,N)*SP(5,L)  
A=(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
CSS(5,J,L,1)=CSS(5,J,L,1)+WF*0.5D00*SP(5,L)*A  
IF (ISPR(1,L) > 0) CSS(6,J,L,1)=CSS(6,J,L,1)+WF*PROT(N)  
IF (MMVM > 0) THEN  
  IF (ISPV(L).GT.0) THEN  
    DO K=1,ISPV(L)
      CSS(7,J,L,1)=CSS(7,J,L,1)+WF*VIBGEN(IPVIB(K,N),K,L) 
    END DO  
  END IF  
END IF   
B=DABS(PV(1,N))  
CSSS(1,J)=CSSS(1,J)+WF/B  
CSSS(2,J)=CSSS(2,J)+WF*SP(5,L)/B  
CSSS(3,J)=CSSS(3,J)+WF*SP(5,L)*PV(2,N)/B  
!--this assumes that any flow normal to the x direction is in the y direction  
CSSS(4,J)=CSSS(4,J)+WF*SP(5,L)*A/B          
IF (ISPR(1,L) > 0) THEN  
  CSSS(5,J)=CSSS(5,J)+WF*PROT(N)/B  
  CSSS(6,J)=CSSS(6,J)+WF*ISPR(1,L)/B  
END IF
!$omp end critical     
!  
CALL ZGF(RANF,IDT)  
IF (FSPEC(J) > RANF) THEN !--specular reflection  
  X=2.D00*XB(J)-X            
  PV(1,N)=-PV(1,N)  
  DTR=(X-XB(J))/PV(1,N)  
ELSE                      !--diffuse reflection  
  VMPS=SQRT(2.D00*BOLTZ*TSURF(J)/SP(5,L))  
  DTR=(X-XB(J))/PV(1,N)   !--isebasti: original line DTR=(XB(J)-PX(N))/PV(1,N) was corrected  
  !write(*,*) n,px(n),x-px(n),x,dtm,dtr  
  CALL ZGF(RANF,IDT)  
  PV(1,N)=SQRT(-LOG(RANF))*VMPS  
  IF (J == 2) PV(1,N)=-PV(1,N)  
  CALL RVELC(PV(2,N),PV(3,N),VMPS,IDT)  
  PV(2,N)=PV(2,N)+VSURF(J)  
  IF (ISPR(1,L) > 0) CALL SROT(L,TSURF(J),PROT(N),IDT)  
  IF (MMVM > 0) THEN  
    DO K=1,ISPV(L)  
      IF (AHVIB .EQ. 0)THEN
        CALL SVIB(L,TSURF(J),IPVIB(K,N),K,IDT)  
      ELSE IF (IDVIB(L) .EQ. 0)THEN
        CALL SVIB(L,TSURF(J),IPVIB(K,N),K,IDT) 
      ELSE
        CALL SDVIB(L,TSURF(J),IPVIB(K,N),K,IDT) 
      END IF
    END DO  
  END IF  
END IF    
!
!$omp critical    
CSS(2,J,L,2)=CSS(2,J,L,2)-WF*PV(1,N)*SP(5,L)  
CSS(3,J,L,2)=CSS(3,J,L,2)-WF*(PV(2,N)-VSURF(J))*SP(5,L)  
CSS(4,J,L,2)=CSS(4,J,L,2)-WF*PV(3,N)*SP(5,L)  
A=(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
CSS(5,J,L,2)=CSS(5,J,L,2)-WF*0.5D00*SP(5,L)*A  
IF (ISPR(1,L) > 0) CSS(6,J,L,2)=CSS(6,J,L,2)-WF*PROT(N)  
IF (MMVM > 0) THEN  
  IF (ISPV(L).GT.0) THEN  
    DO K=1,ISPV(L) 
      CSS(7,J,L,2)=CSS(7,J,L,2)-WF*VIBGEN(IPVIB(K,N),K,L)  
    END DO  
  END IF  
END IF  
B=DABS(PV(1,N))  
CSSS(1,J)=CSSS(1,J)+WF/B  
CSSS(2,J)=CSSS(2,J)+WF*SP(5,L)/B  
CSSS(3,J)=CSSS(3,J)+WF*SP(5,L)*PV(2,N)/B  
!--this assumes that any flow normal to the x direction is in the y direction  
CSSS(4,J)=CSSS(4,J)+WF*SP(5,L)*A/B          
IF (ISPR(1,L) > 0) THEN  
  CSSS(5,J)=WF*CSSS(5,J)+PROT(N)/B  
  CSSS(6,J)=CSSS(6,J)+WF*ISPR(1,L)/B  
END IF
!$omp end critical       
!  
XI=XB(J)  
DX=DTR*PV(1,N)  
DZ=0.D00  
IF (IFX > 0) DY=DTR*PV(2,N)  
IF (IFX == 2) DZ=DTR*PV(3,N)      
IF (IFX == 0) X=XI+DX  
IF (IFX > 0) CALL AIFX(XI,DX,DY,DZ,X,PV(1,N),PV(2,N),PV(3,N),IDT)  
!write(*,*) n,xi,x-xi,x,dtm,dtr  
!pause  
!  
RETURN  
!  
END SUBROUTINE REFLECT  
!  
!*****************************************************************************  
!  
SUBROUTINE RBC(XI,DX,DY,DZ,R,S)  
!  
!--calculates the trajectory fraction S from a point at radius XI with  
!----displacements DX, DY, and DZ to a possible intersection with a  
!----surface of radius R, IFX=1, 2 for cylindrical, spherical geometry  
!  
USE MOLECS  
USE GAS  
USE GEOM  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
REAL(KIND=8) :: A,B,C,XI,DX,DY,DZ,R,S,DD,S1,S2  
!  
DD=DX*DX+DY*DY  
IF (IFX == 2) DD=DD+DZ*DZ  
B=XI*DX/DD  
C=(XI*XI-R*R)/DD  
A=B*B-C  
IF (A >= 0.D00) THEN  
!--find the least positive solution to the quadratic  
  A=DSQRT(A)  
  S1=-B+A  
  S2=-B-A  
  IF (S2 < 0.D00) THEN  
    IF (S1 > 0.D00) THEN  
      S=S1  
    ELSE  
      S=2.D00  
    END IF  
  ELSE IF (S1 < S2) THEN  
    S=S1  
  ELSE  
    S=S2  
  END IF  
ELSE  
  S=2.D00  
!--setting S to 2 indicates that there is no intersection  
END IF  
!  
RETURN  
!  
END SUBROUTINE RBC  
!  
!*****************************************************************************  
!  
SUBROUTINE AIFX(XI,DX,DY,DZ,X,U,V,W,IDT)   
!  
!--calculates the new radius and realigns the velocity components in  
!----cylindrical and spherical flows  
!  
USE MOLECS  
USE GAS  
USE GEOM  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
INTEGER :: IDT !included IDT
REAL(KIND=8) :: A,B,C,XI,DX,DY,DZ,X,U,V,W,DR,VR,S,RANF !--isebasti: included RANF 
!  
IF (IFX == 1) THEN  
  DR=DY  
  VR=V  
ELSE IF (IFX == 2) THEN  
  DR=DSQRT(DY*DY+DZ*DZ)  
  VR=DSQRT(V*V+W*W)  
END IF  
A=XI+DX  
X=DSQRT(A*A+DR*DR)  
S=DR/X  
C=A/X  
B=U  
U=B*C+VR*S  
V=-B*S+VR*C  
IF (IFX == 2) THEN  
  VR=V  
  CALL ZGF(RANF,IDT)  
  A=DPI*RANF  
  V=VR*DSIN(A)  
  W=VR*DCOS(A)  
END IF  
!  
RETURN  
!  
END SUBROUTINE AIFX  
!  
!*****************************************************************************  
!  
SUBROUTINE REMOVE_MOL(N)  
!  
!--remove molecule N and replaces it by NM  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
!  
IMPLICIT NONE  
!  
INTEGER :: N,NC,M,K  
   
!--N the molecule number  
!--M,K working integer   
!  
IF (N /= NM) THEN  
  PX(N)=PX(NM)  
  PV(1:3,N)=PV(1:3,NM)  
  IF (MMRM > 0) PROT(N)=PROT(NM)  
  IF (MMVM > 0) IPVIB(:,N)=IPVIB(:,NM)  
  IPCELL(N)=IPCELL(NM)  !ABS(IPCELL(NM))  !--isebasti: removed ABS 
  IPSP(N)=IPSP(NM)  
  IPCP(N)=IPCP(NM)  
  PTIM(N)=PTIM(NM)  
END IF  
NM=NM-1  
!  
RETURN  
!  
END SUBROUTINE REMOVE_MOL   
!  
!*****************************************************************************  
!  
SUBROUTINE INDEX_MOLS  
!  
!--index the molecules to the collision cells  
!  
USE MOLECS  
USE CALC  
USE GEOM
USE OUTPUT    
!  
IMPLICIT NONE  
!  
INTEGER :: N,M,K  
!  
!--N,M,K working integer  
!  
ICCELL(2,:)=0    !--isebasti: replace original loop by this    
! 
!IF (NOUT==1) write(*,*) 'index_01'       
IF (NM /= 0) THEN  
  DO N=1,NM      !--isebasti: no openmp; tests show best efficiency for 1 thread
    M=IPCELL(N)  
    ICCELL(2,M)=ICCELL(2,M)+1  
  END DO
!
!IF (NOUT==1) write(*,*) 'index_02'         
  M=0  
  DO N=1,NCCELLS  
    ICCELL(1,N)=M  
    M=M+ICCELL(2,N)  
  END DO  
!
  ICCELL(2,:)=0  !--isebasti: included
!
!IF (NOUT==1) write(*,*) 'index_03'         
  DO N=1,NM  
    M=IPCELL(N)  
    ICCELL(2,M)=ICCELL(2,M)+1  
    K=ICCELL(1,M)+ICCELL(2,M)  
    ICREF(K)=N  
  END DO  
!IF (NOUT==1) write(*,*) 'index_04'       
!  
END IF
!  
RETURN  
!  
END SUBROUTINE INDEX_MOLS  
!  
!*****************************************************************************  
!  
SUBROUTINE LBS(XMA,XMB,ERM,IDT)  
!  
!--selects a Larsen-Borgnakke energy ratio using eqn (11.9)  
!
IMPLICIT NONE  
!  
REAL(KIND=8) :: PROB,ERM,XMA,XMB,RANF  
INTEGER :: I,N,IDT !--isebasti: included IDT  
!  
!--I is an indicator  
!--PROB is a probability  
!--ERM ratio of rotational to collision energy  
!--XMA degrees of freedom under selection-1  
!--XMB remaining degrees of freedom-1  
!  
I=0  
DO WHILE (I == 0)  
  CALL ZGF(RANF,IDT)  
  ERM=RANF  
  IF ((XMA < 1.D-6).OR.(XMB < 1.D-6)) THEN  
!    IF (XMA < 1.E-6.AND.XMB < 1.E-6) RETURN  
!--above can never occur if one mode is translational  
    IF (XMA < 1.D-6) PROB=(1.D00-ERM)**XMB  
    IF (XMB < 1.D-6) PROB=(1.D00-ERM)**XMA  
  ELSE  
    PROB=(((XMA+XMB)*ERM/XMA)**XMA)*(((XMA+XMB)*(1.D00-ERM)/XMB)**XMB)  
  END IF  
  CALL ZGF(RANF,IDT)  
  IF (PROB > RANF) I=1  
END DO  
!  
RETURN   
!  
END SUBROUTINE LBS  
!  
!*****************************************************************************  
!  
SUBROUTINE INITIALISE_SAMPLES  
!  
!--start a new sample  
!  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT  
USE MOLECS  
!  
IMPLICIT NONE  
!  
INTEGER :: N  
!  
NSAMP=0.  
TISAMP=FTIME  
NMISAMP=NM
ENERS=0.d0
COLLS=0.D00 ; WCOLLS=0.D00 ; CLSEP=0.D00  
TCOL=0.  
TDISS=0.D00   !--isebasti: uncommented
TRECOMB=0.D00 !--isebasti: uncommented
TFOREX=0.D00  !--isebasti: uncommented
TREVEX=0.D00  !--isebasti: uncommented
TREACG=0  
TREACL=0  
TNEX=0.D00    !--isebasti: uncommented
!
 CS=0. ; CSS=0. ; CSSS=0. ; CST=0.d0; BINS=0.d0; BIN=0.d0 !--isebasti: CST,BINS,BIN included
 CST(0,:)=1.d0; BINS(0,:,:)=1.d0; BIN(0,:)=1.d0           !--isebasti: to avoid dividing by zero
REAC=0.       !--isebasti: uncommented
SREAC=0.      !--isebasti: uncommented
!
NDISSOC=0   !used in qk
NRECOMB=0   !used in qk 
NDISSL=0    !used in qk 
NDROT=0     !bin counter
NDVIB=0     !bin
!
!IF (MNRE > 0) THEN
!IF (MNRE > 0 .AND. NOUT < 2) THEN  !CPER
!  NPVIB=0               !bin counter
!  NPVIB(:,:,:,:,:,0)=1  !initialization
!  TNPVIB=1.d0           !total counter 
!END IF
!  
END SUBROUTINE INITIALISE_SAMPLES  
!  
!*****************************************************************************  
SUBROUTINE SAMPLE_FLOW  
!  
!--sample the flow properties  
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT
USE OMP_LIB
!  
IMPLICIT NONE  
!  
INTEGER :: NC,NCC,LS,N,M,K,L,I,KV  
REAL(KIND=8) :: A,TE,TT,WF  
!  
!--NC the sampling cell number  
!--NCC the collision cell number  
!--LS the species code  
!--N,M,K working integers  
!--TE total translational energy  
!  
NSAMP=NSAMP+1
TSAMP=FTIME
!  
!$omp parallel &
!$omp private(n,ncc,nc,wf,ls,m,k) &
!$omp reduction(+:cs)
!$omp do
DO N=1,NM  
  NCC=IPCELL(N)   
  NC=ICCELL(3,NCC)  
  WF=1.D00
  IF (IWF == 1) WF=1.D00+WFM*PX(N)**IFX  
  IF ((NC > 0).AND.(NC <= NCELLS)) THEN  
    IF (MSP > 1) THEN  
      LS=ABS(IPSP(N))  
    ELSE  
      LS=1  
    END IF  
    CS(0,NC,LS)=CS(0,NC,LS)+1.D00  
    CS(1,NC,LS)=CS(1,NC,LS)+WF
    DO M=1,3  
      CS(M+1,NC,LS)=CS(M+1,NC,LS)+WF*PV(M,N)  
      CS(M+4,NC,LS)=CS(M+4,NC,LS)+WF*PV(M,N)**2  
    END DO  
    IF (MMRM > 0) CS(8,NC,LS)=CS(8,NC,LS)+WF*PROT(N)  
    IF (MMVM > 0) THEN  
      IF (ISPV(LS) > 0) THEN  
        DO K=1,ISPV(LS)  
          CS(K+8,NC,LS)=CS(K+8,NC,LS)+WF*DFLOAT(IPVIB(K,N))*BOLTZ*SPVM(1,K,LS)
        END DO  
      END IF  
    END IF  
  ELSE
    WRITE (*,*) 'Illegal sampling cell',NC,NCC,' for MOL',N,' at',PX(N)  !;STOP
  END IF
END DO  
!$omp end do
!$omp end parallel 
!
A=0.d0
IF ((IENERS > 0).AND.(GASCODE == 8)) CALL ENERGY(GASCODE,0,A)
ENERS=ENERS+A 
!  
RETURN  
END SUBROUTINE SAMPLE_FLOW  
!  
!*****************************************************************************  
SUBROUTINE OUTPUT_RESULTS  
!  
!--calculate the surface and flowfield properties  
!--generate TECPLOT files for displaying these properties  
!--calculate collisiion rates and flow transit times and reset time intervals  
!--add molecules to any flow plane molecule output files  
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT  
!   
IMPLICIT NONE  
!  
INTEGER :: I,IJ,J,JJ,K,KK,KR,L,M,N,NN,NMCR,CTIME,NMS(MSP),ISNAP(1,NSNAP),ZCHECK,NSS,IDT=0 !--isebasti: included ZCHECK,NSS,IDT  
INTEGER(KIND=8) :: NNN   
REAL :: AA,BB,CC,AS,AT,GAM  !--isebasti: included RANF,GAM
REAL(KIND=8) :: A,B,C,SDTM,SMCR,DOF,AVW,UU,VDOFM,TVIBM,DTMI,TT,EVIBM,RANF,F(MMVM),TVPDF(MMVM,MSP)  !--isebasti: included RANF,F,TVPDF  
REAL(KIND=8) :: VPF(MMVM,MSP),EQROT(MSP,100),EQVIBVT(MMVM,MSP,0:100),EQVIBOT(MMVM,MSP,0:100)  !--isebsati: included
REAL(KIND=8) :: SUM(0:12),SUMS(0:8,2),TV(MMVM,MSP),TVIB(MSP),DF(NCELLS,MMVM,MSP),VDOF(MSP),PPA(MSP),&
                THCOL(MSP,MSP),PSNAP(1,NSNAP),VSNAP(3,NSNAP),SDOF(MSP),SRR(MNRE)  
CHARACTER (LEN=16) :: FILENAME  
CHARACTER (LEN=4) :: E   
!  
!--CTIME  computer time (microseconds)  
!--SUMS(N,L) sum over species of CSS(N,J,L,M) for surface properties  
!   
!--For flowfield properties,where <> indicates sampled sum  
!--SUM(0) the molecular number sum over all species  
!--SUM(1) the weighted number sum over all species  
!--SUM(2) the weighted sum of molecular masses   
!--SUM(3),(4),(5) the weighted sum over species of m*<u>,<v>,<w>  
!--SUM(6) the weighted sum over species of m*(<u**2>+<v**2>+<w**2>)  
!--SUM(7) the weighted sum over species of <u**2>+<v**2>+<w**2>  
!--SUM(8) the weighted sum of rotational energy  
!--SUM(9) the weighted sum of rotational degrees of freedom  
!--SUM(10) the weighted sum over species of m*<u**2>  
!--SUM(11) the weighted sum over species of m*<v**2>  
!--SUM(12) sum over species of m*<w**2>  
!--UU velocity squared  
!--DOF degrees of freedom  
!--AVW the average value of the viscosity-temperature exponent  
!--DVEL velocity difference  
!--TVEL thermal speed  
!--SMCR sum of mcs/mfp over cells  
!--NMCR number in the sum  
!--VDOFM effective vibrational degrees of freedom of mixture  
!--TVIB(L)  
!--VDOF(L)  
!--TV(K,L) the temperature of vibrational mode K of species L  
!--SDOF(L) total degrees of freedom for species L
!--PPA particles per atom  
!--NMS number per species  
!--NSNAP number of particles considered in SNAPSHOT files
!--SRR sampled reaction rate
!  
!---------------------------------------------------------------------------- 
!--set variables  
!  
NOUT=NOUT+1
IF (NOUT > 9999) NOUT=NOUT-9999
CALL NUMCHAR4 (NOUT,E)    
WRITE (*,*) 'Generating files for output interval',NOUT
WRITE (*,*) 'Number of samples',NSAMP
!
!----------------------------------------------------------------------------
!--compute surface properties
!
VARSP=0.D00    
IF (IFX == 0) A=FNUM/(FTIME-TISAMP)   !--flow X-section area = unity for 1-D flow  
DO JJ=1,2  
IF (IFX == 1) A=FNUM/(2.D00*PI*XB(JJ)*(FTIME-TISAMP))  
IF (IFX == 2) A=FNUM/(4.D00*PI*XB(JJ)*XB(JJ)*(FTIME-TISAMP))  
!--JJ=1 for surface at XB(1), JJ=2 for surface at XB(2)  
  IF (ITYPE(JJ) == 2) THEN  
    SUMS=0.D00  
    DO L=1,MSP  
      DO J=0,8  
        DO IJ=1,2  
          SUMS(J,IJ)=SUMS(J,IJ)+CSS(J,JJ,L,IJ)  
        END DO  
      END DO  
    END DO    
!  
    VARS(0,JJ)=SUMS(0,1)  
    VARS(1,JJ)=SUMS(1,1)  
    VARS(2,JJ)=SUMS(1,2)  
    VARS(3,JJ)=SUMS(1,1)*A  
    VARS(4,JJ)=SUMS(1,2)*A  
    VARS(5,JJ)=SUMS(2,1)*A  
    VARS(6,JJ)=SUMS(2,2)*A   
    VARS(7,JJ)=SUMS(3,1)*A  
    VARS(8,JJ)=SUMS(3,2)*A  
    VARS(9,JJ)=SUMS(4,1)*A  
    VARS(10,JJ)=SUMS(4,2)*A  
    VARS(11,JJ)=SUMS(5,1)*A  
    VARS(12,JJ)=SUMS(5,2)*A  
    VARS(13,JJ)=SUMS(6,1)*A  
    VARS(14,JJ)=SUMS(6,2)*A  
    VARS(15,JJ)=SUMS(7,1)*A  
    VARS(16,JJ)=SUMS(7,2)*A   
    VARS(17,JJ)=SUMS(8,1)*A  
    VARS(18,JJ)=SUMS(8,2)*A  
    IF (CSSS(1,JJ) > 1.D-6) THEN  
      VARS(19,JJ)=CSSS(3,JJ)/CSSS(2,JJ)  
      VARS(20,JJ)=(CSSS(4,JJ)-CSSS(2,JJ)*VARS(19,JJ)*VARS(19,JJ))/(CSSS(1,JJ)*3.D00*BOLTZ)-TSURF(JJ)  
      VARS(19,JJ)=VARS(19,JJ)-VSURF(JJ)  
      IF (CSSS(6,JJ) > 0.D00) THEN  
        VARS(21,JJ)=(2.D000/BOLTZ)*(CSSS(5,JJ)/CSSS(6,JJ))-TSURF(JJ)  
      ELSE  
        VARS(21,JJ)=0.D00  
      END IF      
    ELSE  
      VARS(19,JJ)=0.D00  
      VARS(20,JJ)=0.D00  
      VARS(21,JJ)=0.D00  
    END IF  
    VARS(22,JJ)=(SUMS(2,1)+SUMS(2,2))*A  
    VARS(23,JJ)=(SUMS(3,1)+SUMS(3,2))*A  
    VARS(24,JJ)=(SUMS(4,1)+SUMS(4,2))*A  
    VARS(25,JJ)=(SUMS(5,1)+SUMS(5,2))*A  
    VARS(26,JJ)=(SUMS(6,1)+SUMS(6,2))*A  
    VARS(27,JJ)=(SUMS(7,1)+SUMS(7,2))*A  
    VARS(28,JJ)=(SUMS(8,1)+SUMS(8,2))*A  
    VARS(29,JJ)=VARS(11,JJ)+VARS(13,JJ)+VARS(15,JJ)  
    VARS(30,JJ)=VARS(12,JJ)+VARS(14,JJ)+VARS(16,JJ)  
    VARS(31,JJ)=VARS(29,JJ)+VARS(30,JJ)  
    DO L=1,MSP  
      IF (SUMS(1,1) > 0) THEN  
        VARS(32+L,JJ)=100.*CSS(1,JJ,L,1)/SUMS(1,1)  
      ELSE  
        VARS(32+L,JJ)=0.  
      END IF  
    END DO  
  END IF  
END DO   
!
!----------------------------------------------------------------------------
!--compute flowfield properties
VAR=0.D00   
VARSP=0.  
SMCR=0  
NMCR=0  
VDOFM=0.
DO N=1,NCELLS  
  A=FNUM/(CELL(4,N)*NSAMP)  
  IF (IVB == 1) A=A*((XB(2)-XB(1))/(XB(2)+VELOB*0.5D00*(FTIME+TISAMP)-XB(1)))**(IFX+1)  
!--check the above for non-zero XB(1)   
  SUM=0.  
  NMCR=NMCR+1  
  DO L=1,MSP  
    SUM(0)=SUM(0)+CS(0,N,L)  
    SUM(1)=SUM(1)+CS(1,N,L)  
    SUM(2)=SUM(2)+SP(5,L)*CS(1,N,L)  
    DO K=1,3  
      SUM(K+2)=SUM(K+2)+SP(5,L)*CS(K+1,N,L)  
      IF (CS(1,N,L) > 0.1D00) THEN  
        VARSP(K+1,N,L)=CS(K+4,N,L)/CS(1,N,L)  
!--VARSP(2,3,4 are temporarily the mean of the squares of the velocities  
        VARSP(K+8,N,L)=CS(K+1,N,L)/CS(1,N,L)  
!--VARSP(9,10,11 are temporarily the mean of the velocities  
      END IF  
    END DO  
    SUM(6)=SUM(6)+SP(5,L)*(CS(5,N,L)+CS(6,N,L)+CS(7,N,L))  
    SUM(10)=SUM(10)+SP(5,L)*CS(5,N,L)  
    SUM(11)=SUM(11)+SP(5,L)*CS(6,N,L)  
    SUM(12)=SUM(12)+SP(5,L)*CS(7,N,L)  
    IF (CS(1,N,L) > 0.5D00) THEN  
      SUM(7)=SUM(7)+CS(5,N,L)+CS(6,N,L)+CS(7,N,L)  
    END IF  
    IF (ISPR(1,L) > 0) THEN  
      SUM(8)=SUM(8)+CS(8,N,L)  
      SUM(9)=SUM(9)+CS(1,N,L)*ISPR(1,L)  
    END IF  
  END DO  
  AVW=0.  
  DO L=1,MSP  
    VARSP(0,N,L)=CS(1,N,L)  
    VARSP(1,N,L)=0.D00  
    VARSP(6,N,L)=0.  
    VARSP(7,N,L)=0.  
    VARSP(8,N,L)=0.  
    IF (SUM(1) > 0.1) THEN  
      VARSP(1,N,L)=CS(1,N,L)/SUM(1)  !isebasti: deleted 100* factor
      AVW=AVW+SP(3,L)*CS(1,N,L)/SUM(1)  
      IF ((ISPR(1,L) > 0).AND.(CS(1,N,L) > 0.5)) VARSP(6,N,L)=(2.D00/BOLTZ)*CS(8,N,L)/(DFLOAT(ISPR(1,L))*CS(1,N,L))  
    END IF  
    VARSP(5,N,L)=0.  
    DO K=1,3  
      VARSP(K+1,N,L)=(SP(5,L)/BOLTZ)*(VARSP(K+1,N,L)-VARSP(K+8,N,L)**2)  
      VARSP(5,N,L)=VARSP(5,N,L)+VARSP(K+1,N,L)  
    END DO  
    VARSP(5,N,L)=VARSP(5,N,L)/3.D00
    VARSP(8,N,L)=(3.D00*VARSP(5,N,L)+DFLOAT(ISPR(1,L))*VARSP(6,N,L))/(3.D00+DFLOAT(ISPR(1,L))) !isebasti: included according to DSMC.f90       
  END DO  
!  
  IF (IVB == 0) VAR(1,N)=CELL(1,N)  
  IF (IVB == 1) THEN  
    C=(XB(2)+VELOB*FTIME-XB(1))/DFLOAT(NDIV)      !--new DDIV  
    VAR(1,N)=XB(1)+(DFLOAT(N-1)+0.5)*C  
  END IF  
  VAR(2,N)=SUM(0)  
  IF (SUM(1) > 0.5) THEN  
    VAR(3,N)=SUM(1)*A    !--number density Eqn. (4.28)   
    VAR(4,N)=VAR(3,N)*SUM(2)/SUM(1)   !--density Eqn. (4.29)  
    VAR(5,N)=SUM(3)/SUM(2)    !--u velocity component Eqn. (4.30)  
    VAR(6,N)=SUM(4)/SUM(2)    !--v velocity component  
    VAR(7,N)=SUM(5)/SUM(2)    !--w velocity component  
    UU= VAR(5,N)**2+VAR(6,N)**2+VAR(7,N)**2  
    IF (SUM(1) > 1) THEN  
      VAR(8,N)=(ABS(SUM(6)-SUM(2)*UU))/(3.D00*BOLTZ*SUM(1))  !Eqn. (4.39) 
!--translational temperature  
      VAR(19,N)=(ABS(SUM(10)-SUM(2)*VAR(5,N)**2))/(BOLTZ*SUM(1))  
      VAR(20,N)=(ABS(SUM(11)-SUM(2)*VAR(6,N)**2))/(BOLTZ*SUM(1))  
      VAR(21,N)=(ABS(SUM(12)-SUM(2)*VAR(7,N)**2))/(BOLTZ*SUM(1))  
    ELSE  
      VAR(8,N)=1.  
      VAR(19,N)=1.  
      VAR(20,N)=1.  
      VAR(21,N)=1.  
    END IF  
!--rotational temperature  
    IF (SUM(9) > 0.01D00) THEN  
      VAR(9,N)=(2.D00/BOLTZ)*SUM(8)/SUM(9)    !Eqn. (4.36)  
    ELSE  
      VAR(9,N)=0.  
    END IF
    DOF=(3.D00+SUM(9)/SUM(1))
!--vibration temperature default    
    VAR(10,N)=FTMP(1)  
!--overall temperature based on translation and rotation  
    VAR(11,N)=(3.*VAR(8,N)+(SUM(9)/SUM(1))*VAR(9,N))/DOF  
!--scalar pressure (now (from V3) based on the translational temperature)
    VAR(18,N)=VAR(3,N)*BOLTZ*VAR(8,N)  
!
!--Tvib calculations according to DSMC.f90
    IF (MMVM > 0) THEN
      DO L=1,MSP
        VDOF(L)=0.
        IF (ISPV(L) > 0) THEN
          DO K=1,ISPV(L)
            IF (CS(K+8,N,L) > 0.) THEN
              A=CS(K+8,N,L)/CS(1,N,L)
              TV(K,L)=SPVM(1,K,L)/DLOG(1.d0+BOLTZ*SPVM(1,K,L)/A)  !--Eqn.(4.45) - assuming SHO
              DF(N,K,L)=2.d0*A/(BOLTZ*TV(K,L)) !--Eqn. (11.28) Bird94 - general definition
            ELSE
              TV(K,L)=0.
              DF(N,K,L)=0.
            END IF
            VDOF(L)=VDOF(L)+DF(N,K,L)  !--Eqn.(4.49)
          END DO
          TVIB(L)=0.
          DO K=1,ISPV(L)
            IF (VDOF(L) > 1.D-6) THEN
              TVIB(L)=TVIB(L)+TV(K,L)*DF(N,K,L)/VDOF(L)  !--Eqn.(4.50)
            ELSE
              TVIB(L)=FVTMP(1)
            END IF  
          END DO  
        ELSE
          TVIB(L)=0. !TREF  !--isebasti: TREF is not defined
          VDOF(L)=0.
        END IF
        VARSP(7,N,L)=TVIB(L)
      END DO
      VDOFM=0.
      TVIBM=0.
      A=1.D00 !--isebasti: instead of 0
      DO L=1,MSP
        IF (ISPV(L) > 0) A=A+CS(1,N,L)
      END DO
      DO L=1,MSP
        IF (ISPV(L) > 0) THEN  
          VDOFM=VDOFM+VDOF(L)*CS(1,N,L)/A  !--Eqn.(4.51)
          TVIBM=TVIBM+TVIB(L)*CS(1,N,L)/A  !--Eqn.(4.52)
        END IF  
      END DO   
      VAR(10,N)=TVIBM
    END IF
!
!--convert the species velocity components to diffusion velocities
    DO L=1,MSP
      IF (VARSP(0,N,L) > 0.5) THEN  
        DO K=1,3 
          VARSP(K+8,N,L)=VARSP(K+8,N,L)-VAR(K+4,N) 
        END DO
      ELSE
        DO K=1,3
          VARSP(K+8,N,L)=0.D00  
        END DO
      END IF  
    END DO
!
!--reset the overall temperature and degrees of freedom (now including vibrational modes)
    IF (MMVM > 0) THEN
      DO L=1,MSP
        SDOF(L)=3.D00+ISPR(1,L)+VDOF(L)
        VARSP(8,N,L)=(3.*VARSP(5,N,L)+ISPR(1,L)*VARSP(6,N,L)+VDOF(L)*VARSP(7,N,L))/SDOF(L)  !species overall T                    
      END DO 
      A=0.D00
      B=0.D00
      DO L=1,MSP
        A=A+SDOF(L)*VARSP(8,N,L)*CS(1,N,L)
        B=B+SDOF(L)*CS(1,N,L)
      END DO  
      VAR(11,N)=A/B !mixture overall T
      DOF=DOF+VDOFM !--isebasti: included
    END IF 
!
!--Mach number   
    VAR(17,N)=DSQRT(VAR(5,N)**2+VAR(6,N)**2+VAR(7,N)**2)  
    VAR(12,N)=VAR(17,N)/SQRT((DOF+2.D00)*VAR(11,N)*(SUM(1)*BOLTZ/SUM(2))/DOF)  
!--average number of molecules in (collision) cell
    VAR(13,N)=SUM(0)/NSAMP/DFLOAT(NCIS)    
    IF (COLLS(N) > 2.) THEN  
!--mean collision time  
      VAR(14,N)=0.5D00*(FTIME-TISAMP)*(SUM(1)/NSAMP)/WCOLLS(N) 
!--mean free path (based on r.m.s speed with correction factor based on equilibrium)   
      VAR(15,N)=0.92132D00*DSQRT(DABS(SUM(7)/SUM(1)-UU))*VAR(14,N)  
      VAR(16,N)=CLSEP(N)/(COLLS(N)*VAR(15,N))  
    ELSE  
!--m.f.p set by nominal values          
      VAR(14,N)=1.D10  
      VAR(15,N)=1.D10/VAR(3,N)  
    END IF  
  ELSE  
    DO L=3,19  
      VAR(L,N)=0.  
    END DO  
  END IF  
!
  IF(N == NSPDF) THEN
    TVPDF(:,:)=TV(:,:)                        !store TV for the cell with pdf sampling
    OPEN (7,FILE='RELAX.DAT',ACCESS='APPEND') !--write temperature relaxation
    WRITE (7,993) FTIME,VAR(8:11,N),VAR(18,N),SUM(9)/SUM(1),VDOFM,NM,2.*PCOLLS*DFLOAT(NSAMP)/SUM(1),&
                  DABS(0.5*(VAR(8,N)+VAR(9,N))-VAR(10,N)),DABS(VAR(8,N)-VAR(9,N)),DABS(VAR(8,N)-VAR(10,N)),&
                  (TV(:,L),L=1,MSP)
    CLOSE (7)
  END IF
!
END DO 
!
!----------------------------------------------------------------------------
!--write general/surface properties
!  
OPEN (3,FILE='DS1GEN.DAT')
!
IF (IFX == 0) WRITE (3,*) 'DSMC program DS1 for a one-dimensional plane flow'  
IF (IFX == 1) WRITE (3,*) 'DSMC program DS1 for a cylindrical flow'  
IF (IFX == 2) WRITE (3,*) 'DSMC program DS1 for a spherical flow'
WRITE (3,*)  
!  
WRITE (3,*) 'Interval',NOUT,'Time ',FTIME, ' with',NSAMP,' samples from',TISAMP  
WRITE (*,*) 'TOTAL MOLECULES = ',NM  
!
NMS=0  
DO N=1,NM  
  M=IPSP(N)  
  NMS(M)=NMS(M)+1  
END DO  
WRITE (3,*) 'Total simulated molecules =',NM  
DO N=1,MSP  
  WRITE (*,*) 'SPECIES ',N,' TOTAL = ',NMS(N)  
  WRITE (3,*) 'Species ',N,' total = ',NMS(N)  
END DO  
!
NNN=DINT(TOTMOV)    
WRITE (3,*) 'Total molecule moves   =',NNN  
NNN=DINT(TOTCOL)  
WRITE (3,*) 'Total collision events =',NNN  
NNN=DINT(TOTDUP)  
WRITE (3,*) 'Total duplicate collisions =',NNN  
!  
WRITE (3,*) 'Species dependent collision numbers in current sample'  
DO N=1,MSP  
  WRITE (3,901) TCOL(N,1:MSP)
END DO   
901 FORMAT(20G13.5)   
CTIME=MCLOCK()  
WRITE (3,*) 'Computation time',FLOAT(CTIME)/1000.,'seconds'  
WRITE (3,*) 'Collision events per second',(TOTCOL-TOTCOLI)*1000.D00/DFLOAT(CTIME)  
WRITE (3,*) 'Molecule moves per second',(TOTMOV-TOTMOVI)*1000.D00/DFLOAT(CTIME)  
WRITE (3,*)  
!  
IF (IPDF > 0) THEN  
  WRITE (3,*) 'Distribution function sampling started at',TISAMP  
  WRITE (3,*) 'Total dissociations',NDISSOC  
  WRITE (3,*) 'Total recombinations',NRECOMB  
  WRITE (3,*) 'Gas temperature',VAR(11,1),' K'  
  WRITE (3,*) 'Gas translational temperature',VAR(8,1),' K'  
  WRITE (3,*) 'Gas rotational temperature',VAR(9,1),' K'  
  WRITE (3,*) 'Gas vibrational temperature',VAR(10,1),' K'  
  DO L=1,MSP  
    WRITE (3,*) 'Species',L,' overall temperature',VARSP(8,1,L),' K'     
    WRITE (3,*) 'Species',L,' translational temperature',VARSP(5,1,L),' K'         
    WRITE (3,*) 'Species',L,' rotational temperature',VARSP(6,1,L),' K'   
    WRITE (3,*) 'Species',L,' vibrational temperature',VARSP(7,1,L),' K'   
  END DO  
WRITE (3,*)      
END IF      
!  
IF ((ITYPE(1) == 2).OR.(ITYPE(2) == 2)) WRITE (3,*) 'Surface quantities'  
DO JJ=1,2  
  IF (ITYPE(JJ) == 2) THEN  
    WRITE (3,*)  
    WRITE (3,*) 'Surface at',XB(JJ)  
    WRITE (3,*) 'Incident sample',VARS(0,JJ)   
    WRITE (3,*) 'Number flux',VARS(3,JJ),' /sq m/s'  
    WRITE (3,*) 'Inc pressure',VARS(5,JJ),' Refl pressure',VARS(6,JJ)  
    WRITE (3,*) 'Pressure', VARS(5,JJ)+VARS(6,JJ),' N/sq m'  
    WRITE (3,*) 'Inc y shear',VARS(7,JJ),' Refl y shear',VARS(8,JJ)  
    WRITE (3,*) 'Net y shear',VARS(7,JJ)-VARS(8,JJ),' N/sq m'  
    WRITE (3,*) 'Net z shear',VARS(9,JJ)-VARS(10,JJ),' N/sq m'  
    WRITE (3,*) 'Incident translational heat flux',VARS(11,JJ),' W/sq m'  
    IF (MMRM > 0) WRITE (3,*) 'Incident rotational heat flux',VARS(13,JJ),' W/sq m'  
    IF (MMVM > 0) WRITE (3,*) 'Incident vibrational heat flux',VARS(15,JJ),' W/sq m'  
    WRITE (3,*) 'Total incident heat flux',VARS(29,JJ),' W/sq m'  
    WRITE (3,*) 'Reflected translational heat flux',VARS(12,JJ),' W/sq m'  
    IF (MMRM > 0) WRITE (3,*) 'Reflected rotational heat flux',VARS(14,JJ),' W/sq m'  
    IF (MMVM > 0) WRITE (3,*) 'Reflected vibrational heat flux',VARS(16,JJ),' W/sq m'  
    WRITE (3,*) 'Total reflected heat flux',VARS(30,JJ),' W/sq m'  
    WRITE (3,*) 'Net heat flux',VARS(31,JJ),' W/sq m'  
    WRITE (3,*) 'Slip velocity (y direction)',VARS(19,JJ),' m/s'  
    WRITE (3,*) 'Translational temperature slip',VARS(20,JJ),' K'  
    IF (MMRM > 0) WRITE (3,*) 'Rotational temperature slip',VARS(21,JJ),' K'  
    IF (MSP > 1) THEN  
      DO L=1,MSP  
        WRITE (3,*) 'Species',L,' percentage',VARS(L+32,JJ)  
      END DO  
    END IF
    WRITE (3,*) 
  END IF
END DO
!
WRITE (3,994) ' Macro or Coll Temps (ITCV,IEAA,IZV):',ITCV,IEAA,IZV    
!
PPA=0  
DO N=1,NCELLS  
  DO M=1,MSP  
    PPA(M)=PPA(M)+VARSP(0,N,M)  
  END DO  
END DO
CLOSE(3)
!
!----------------------------------------------------------------------------
!--write number of reactions
!  
IF (MNRE > 0) THEN  
  OPEN (3,FILE='DS1REAC.DAT',ACCESS='APPEND')
!
  IF (JCD == 1) THEN  
    WRITE (3,*) 'New integrated DSMC chemistry model with no experimentally based rates'  
    WRITE (3,*)  
    WRITE(3,*) 'GAINS FROM REACTIONS'  
    WRITE(3,*) '                          Dissoc.     Recomb. Endo. Exch.  Exo. Exch.'  
    DO M=1,MSP  
      WRITE (3,*) ' SPECIES',M,TREACG(1,M),TREACG(2,M),TREACG(3,M),TREACG(4,M)     
    END DO  
    WRITE (3,*)   
    WRITE(3,*) 'LOSSES FROM REACTIONS'  
    WRITE(3,*) '                          Dissoc.     Recomb. Endo. Exch.  Exo. Exch.'  
    DO M=1,MSP  
      WRITE (3,*) ' SPECIES',M,TREACL(1,M),TREACL(2,M),TREACL(3,M),TREACL(4,M)  
    END DO  
    WRITE (3,*)  
    WRITE (3,*) 'TOTALS'  
    DO M=1,MSP  
      WRITE (3,*) ' SPECIES',M,' GAINS',TREACG(1,M)+TREACG(2,M)+TREACG(3,M)+TREACG(4,M),&  
                  ' LOSSES',TREACL(1,M)+TREACL(2,M)+TREACL(3,M)+TREACL(4,M)    
    END DO  
  END IF
!  
  IF ((JCD == 0).AND.(MSP > 1)) THEN  
    AS=0.
    SRR=0.
    DO M=1,MNRE
      AS=AS+REAC(M)
    END DO  
    IF (AS == 0.) AS=1.d0
    IF (IREAC == 0) THEN
      WRITE (3,993) FTIME,AS,REAC(1:MNRE)/AS  !no reaction rate sampling
    ELSE
      DO K=1,MNRE
        A=REAC(K)*(FNUM/CELL(4,NSPDF))/(FTIME-TISAMP)
        IF(KP(K) >= 0) THEN
          B=(VARSP(1,NSPDF,LE(K))*VARSP(1,NSPDF,ME(K))*VAR(3,NSPDF)**2.d0)/(AVOG*1.d3)                             !exchange/dissociation
        ELSE
          B=(VARSP(1,NSPDF,LE(K))*VARSP(1,NSPDF,ME(K))*VARSP(1,NSPDF,MP(K))*VAR(3,NSPDF)**3.d0)/(AVOG*1.d3)**2.d0  !recombination
        END IF
        SRR(K)=A/B !sampled reaction rate (cm3/mol/s)
      END DO
      WRITE (3,993) FTIME,VAR(8:11,NSPDF),VAR(18,NSPDF),AS,REAC(1:MNRE)/AS,SRR(1:MNRE)
    END IF
  END IF
!
  CLOSE(3) 
END IF
!
!----------------------------------------------------------------------------
!--write flowfield overall properties
!  
OPEN (3,FILE='DS1FP00.DAT')
!
WRITE (3,994) '# Flowfield Overall Properties '
WRITE (3,994) '# NCELLS:', NCELLS  
WRITE (3,994) '# NSAMP: ', NSAMP
WRITE (3,994) '# X-coord.     Cell   Sample     Number Dens.   Density   u velocity &  
            &  v velocity   w velocity   Trans. Temp.   Rot. Temp.   Vib. Temp. &  
            &  Temperature  Mach no.     Mols/cell    m.c.t        m.f.p     &  
            &  mcs/mfp        speed      Pressure        TTX          TTY          TTZ     &
            &  dtm/mct     <dx/mfp>      Fx          Fy            Fz         Qtransfer &
            &  Species Fractions'  
DO N=1,NCELLS  
  WRITE (3,996) VAR(1,N),N,VAR(2:21,N),DTM/VAR(14,N),(CELL(3,N)-CELL(2,N))/DFLOAT(NCIS)/VAR(15,N),&
                CST(1:4,N)/CST(0,N),0.01*VARSP(1,N,1:MSP)
END DO
CLOSE(3)
!
!----------------------------------------------------------------------------
!--write flowfield properties per species
!
DO L=1,MSP  
    WRITE(FILENAME,777) L
777 FORMAT('DS1FP',i2.2)
    OPEN (3,FILE=TRIM(FILENAME)//'.DAT')
!
  WRITE (3,994) '# Flowfield Properties Species:',L  
  WRITE (3,994) '# NCELLS:', NCELLS  
  WRITE (3,994) '# NSAMP: ', NSAMP
  WRITE (3,994) '# X-coord.     Cell   Sample    Fraction     Species TTx   Species TTy  Species TTz &  
             & Trans. Temp.  Rot. Temp.   Vib. Temp.   Spec. Temp  u Diff Vel   v Diff Vel   w Diff Vel.'   
  DO N=1,NCELLS  
    WRITE (3,996) VAR(1,N),N,VARSP(0,N,L),0.01*VARSP(1,N,L),VARSP(2:11,N,L)
  END DO
CLOSE(3)  
END DO
!
!----------------------------------------------------------------------------    
!--write composition of a reacting gas as a function of time  
!
OPEN (10,FILE='COMPOSITION.DAT',ACCESS='APPEND')
A=0.d0
AS=NM
IF (IENERS > 0) THEN
  IF (NOUT <= 1) ENERS0=ENERS
  A=ENERS/ENERS0-1.d0
  ENERS0=ENERS
END IF
WRITE (10,993) FTIME,VAR(8:11,NSPDF),VAR(18,NSPDF),NMS(1:MSP)/AS,A,NM,VAR(5,1),VAR(5,NCELLS) 
CLOSE (10)  
!
!----------------------------------------------------------------------------
!--write pdf files
! 
IF(IPDF > 0) THEN  
!
!--compute normalized velocity and speed pdfs
!
  PDF=0.d0
  PDFS=0.d0
  DO N=1,NBINS
    DO L=1,MSP
      DO K=1,3
        PDF(N,K)=PDF(N,K)+BINS(N,K,L)/DBINV(3)/BIN(0,K)
        PDFS(N,K,L)=BINS(N,K,L)/DBINV(3)/BINS(0,K,L)
      END DO
      PDF(N,4)=PDF(N,4)+BINS(N,4,L)/DBINC(3)/BIN(0,4)
      PDFS(N,4,L)=BINS(N,4,L)/DBINC(3)/BINS(0,4,L)    
    END DO
  END DO
!  
!--velocity components  
  OPEN (7,FILE='DS1DVEL.DAT',FORM='FORMATTED')
  WRITE (7,994) '# NSPDF, X-coord, Tt, Tr, Tv, To: ', NSPDF,VAR(1,NSPDF),VAR(8:11,NSPDF)  
  WRITE (7,994) '# NSamples total, spec1, spec2 ...:     ', BIN(0,1),BINS(0,1,1:MSP)  !summation values
  WRITE (7,994) '# Interval, Equilibrium pdf, U V W pdfs total, U V W pdfs spec1, U V W pdfs spec2 ... '
  DO N=1,NBINS
    A=DBINV(1)+DBINV(3)*(DFLOAT(N)-0.5d0)                !normalized U/VMP
    B=(1.d0/SPI)/DEXP(A*A)                               !Boltzmann distribution
    WRITE (7,993) A,B,PDF(N,1:3),(PDFS(N,1:3,L),L=1,MSP) !pdfs
  END DO  
  CLOSE(7) 
!  
!--speed
  OPEN (7,FILE='DS1DSPD.DAT',FORM='FORMATTED')
  WRITE (7,994) '# NSPDF, X-coord, Tt, Tr, Tv, To: ', NSPDF,VAR(1,NSPDF),VAR(8:11,NSPDF)    
  WRITE (7,994) '# NSamples total, spec1, spec2 ...:     ', BIN(0,1),BINS(0,1,1:MSP)  !summation values
  WRITE (7,994) '# Interval, Equilibrium pdf, C pdf total, C pdf spec1, C pdf spec2 ... '
  DO N=1,NBINS
    A=DBINC(1)+DBINC(3)*(DFLOAT(N)-0.5d0)                !normalized C/VMP
    B=((4.d0/SPI)*A*A)/DEXP(A*A)                         !Boltzmann distribution; ftr- Eqn 15.25 or N.5 from Laurendeau 
    WRITE (7,993) A,B,PDF(N,4),(PDFS(N,4,L),L=1,MSP)     !pdfs
  END DO  
  CLOSE(7)
!
!--set auxiliar variables
  EQROT=1.d0
  EQVIBVT=1.d0
  EQVIBOT=1.d0
  DO L=1,MSP 
    IF (MMRM > 0) THEN
      IF (ISPR(1,L) > 0) THEN  
        DO M=1,100  
          A=(DFLOAT(M)-0.5D00)*0.1D00   !--rotational values  
          BB=.5*ISPR(1,L)
          !Boltzmann distribution; frot - Eqn 11.21 from Bird94
          EQROT(L,M)=(1.d0/GAM(BB))*(A**(BB-1.d0))*DEXP(-A)*0.1D00 
        END DO
      END IF
    END IF
    IF (MMVM > 0) THEN 
      IF (ISPV(L) > 0) THEN 
        DO K=1,ISPV(L)
          VPF(K,L)=1.D00-DEXP(-SPVM(1,K,L)/TVPDF(K,L)) !--reciprocal of vib partition function   
          DO M=0,100      
            !Boltzmann distribution; fvib - Eqns pg 109,135,199 from Vicenti-Kruger (assuming SHO)
            EQVIBVT(K,L,M)=VPF(K,L)*DEXP(-DFLOAT(M)*SPVM(1,K,L)/TVPDF(K,L)) !--vibrational values based on Tv_mode
            EQVIBOT(K,L,M)=VPF(K,L)*DEXP(-DFLOAT(M)*SPVM(1,K,L)/VARSP(8,NSPDF,L)) !--vibrational values based on Tov     
          END DO
        END DO
      END IF
    END IF
  END DO
!  
!--rotational energy
  IF (MMRM > 0) THEN
    DO L=1,MSP 
      IF (ISPR(1,L) > 0) THEN 
        WRITE(FILENAME,778) L
        778 FORMAT('DS1DROT',i2.2)
        OPEN (7,FILE=TRIM(FILENAME)//'.DAT') 
        WRITE (7,994) '# NSPDF, X-coord, Tt, Tr, Tv, To: ', NSPDF,VAR(1,NSPDF),VAR(8:11,NSPDF)   
        WRITE (7,994) '# Equilibrium based on the rotational temperature'
        WRITE (7,994) '# Species and number of modes:',L,ISPR(1,L)  
        WRITE (7,994) '# erot/kT       sample      f_rot    f_rot_equilib    ratio'  
        DO M=1,100  
          A=(DFLOAT(M)-0.5D00)*0.1D00 
          B=DFLOAT(NDROT(L,M))/BINS(0,1,L) !NDROT and EQROT are fractions in interval 0.1, so multiply by 10 to obtain pdfs
          WRITE (7,993) A,DFLOAT(NDROT(L,M)),B*10.D00,EQROT(L,M)*10.D00,B/EQROT(L,M)    
        END DO      
        CLOSE (7)
      END IF
    END DO  
  END IF
!
!--vibrational levels
  F=0.d0
  IF (MMVM > 0) THEN 
    DO L=1,MSP 
      IF (ISPV(L) > 0) THEN 
        WRITE(FILENAME,779) L
        779 FORMAT('DS1DVIB',i2.2)
        OPEN (7,FILE=TRIM(FILENAME)//'.DAT')
        WRITE (7,994) '# NSPDF, X-coord, Tt, Tr, Tv, To: ', NSPDF,VAR(1,NSPDF),VAR(8:11,NSPDF)    
        WRITE (7,994) '# Equilibrium based on both vibrational temperature and overall gas temperature'
        WRITE (7,994) '# Species and number of modes:',L,ISPV(L)
        WRITE (7,994) '# level         sample      f_vib       f_equil(Tvib) ratio Eq.   f_equil(Tov)  ratio'  
        DO M=0,100   
          F(1:ISPV(L))=DFLOAT(NDVIB(1:ISPV(L),L,M))/BINS(0,1,L)      
          WRITE (7,993) DFLOAT(M),DFLOAT(NDVIB(1:ISPV(L),L,M)),F(1:ISPV(L)),&
                        EQVIBVT(1:ISPV(L),L,M),F(1:ISPV(L))/EQVIBVT(1:ISPV(L),L,M),&
                        EQVIBOT(1:ISPV(L),L,M),F(1:ISPV(L))/EQVIBOT(1:ISPV(L),L,M)  
        END DO      
        CLOSE (7)
      END IF
    END DO
  END IF
!
END IF
!
!--sampled pre- and post-reaction vibrational distributions !CPER
!IF (MNRE < 0) THEN  !turned off
!  DO KK=1,MNRE
!    I=IREA(1,KK)
!    J=IREA(2,KK)
!    K=JREA(2,KK,1)
!    KR=IREV(KK) 
!    WRITE(FILENAME,780) KK
!    780 FORMAT('DS1DVIBR',i2.2)
!    OPEN (7,FILE=TRIM(FILENAME)//'.DAT')  
!    WRITE (7,994) '# Sampled pre- and post-reaction vibrational distributions; IPRS:',IPRS
!    WRITE (7,994) '# Depleting and replenishing reactions:',KK,KR
!    WRITE (7,994) '# Initial species:',I,J,K
!    WRITE (7,994) '# level         sample       f_vib     depleting reac  --  sample       f_vib     replenishing reac'  
!    DO M=0,50   
!      WRITE (7,993) DFLOAT(M),&
!                    !--levels been depleted
!!                   DFLOAT(NPVIB(1,NSPDF,KK,I,1:MAX(1,ISPV(I)),M)),&
!                    DFLOAT(NPVIB(1,NSPDF,KK,I,1:MMVM,M)),&
!                    DFLOAT(NPVIB(1,NSPDF,KK,I,1:MMVM,M))/TNPVIB(1,NSPDF,KK,I),&
!                    DFLOAT(NPVIB(1,NSPDF,KK,J,1:MMVM,M)),&
!                    DFLOAT(NPVIB(1,NSPDF,KK,J,1:MMVM,M))/TNPVIB(1,NSPDF,KK,J),&
!                    DFLOAT(NPVIB(1,NSPDF,KK,K,1:MMVM,M)),&
!                    DFLOAT(NPVIB(1,NSPDF,KK,K,1:MMVM,M))/TNPVIB(1,NSPDF,KK,K),&
!                    !--levels been replenished
!                    DFLOAT(NPVIB(2,NSPDF,KR,I,1:MMVM,M)),&
!                    DFLOAT(NPVIB(2,NSPDF,KR,I,1:MMVM,M))/TNPVIB(2,NSPDF,KR,I),&
!                    DFLOAT(NPVIB(2,NSPDF,KR,J,1:MMVM,M)),&
!                    DFLOAT(NPVIB(2,NSPDF,KR,J,1:MMVM,M))/TNPVIB(2,NSPDF,KR,J),&
!                    DFLOAT(NPVIB(2,NSPDF,KR,K,1:MMVM,M)),&
!                    DFLOAT(NPVIB(2,NSPDF,KR,K,1:MMVM,M))/TNPVIB(2,NSPDF,KR,K)
!    END DO      
!    CLOSE (7)
!  END DO
!END IF
!
!----------------------------------------------------------------------------
!--sample molecule properties for SNAPSHOT files 
!  
NSS=0
DO K=1,NSNAP  
  CALL ZGF(RANF,IDT)  
  N=INT(RANF*DFLOAT(NM))+1
  NSS=NSS+1
  ISNAP(1,NSS)=IPSP(N)
  PSNAP(1,NSS)=PX(N)
  VSNAP(:,NSS)=PV(:,N)
END DO  
!
!----------------------------------------------------------------------------  
!--write binary files for post processing of multiple run cases and snapshot 
!
ZCHECK=1234567
!
102 CONTINUE  
  OPEN (7,FILE='DS1OUT_'//E//'.BIN',FORM='UNFORMATTED',ERR=102)  
  WRITE (7)IFX,NOUT,FTIME,NSAMP,TISAMP,NM,TOTMOV,TOTCOL,PCOLLS,TOTDUP,TCOL,&
           CTIME,TOTCOLI,TOTMOVI,NDISSOC,NRECOMB,&
           ITYPE,XB,VARS,NCELLS,VARSP,&  !end of general properties
           JCD,TREACG,TREACL,ITCV,IEAA,IZV,REAC,VAR,DTM,CELL,NCIS,CST,UVFX,NMS,& !end of reactions,flow properties, and composition
           IPDF,IPRS,BIN,BINS,DBINV,SPI,PDF,PDFS,NSPDF,NDROT,NDVIB,DBINC,ISNAP,PSNAP,VSNAP,SP,ZCHECK    !end of sample pdf and snapshot
  CLOSE(7) 
!
WRITE (*,*) 'Output and binary files are written'
!
!----------------------------------------------------------------------------
!--I/O format
!
999 FORMAT (I5,50G13.5)  
998 FORMAT (G270.0)  
997 FORMAT (G175.0)
996 FORMAT (G13.5,I5,50G13.5)  
995 FORMAT (22G13.5)
994 FORMAT (A,22G13.5)
993 FORMAT (99G13.5)
!
!----------------------------------------------------------------------------
!--reset collision and transit times etc.  
!  
DTMI=DTM  
DTM=DTM*2.  
!--this makes it possible for DTM to increase, it will be reduced as necessary  
DO N=1,NCCELLS  
!  
  NN=ICCELL(3,N)  
  B=(CELL(3,NN)-CELL(2,NN))/DFLOAT(NCIS)     !--collision cell width  
!  
  IF ((NN > 0).AND.(NN <= NCELLS)) THEN  
!  
    IF (VAR(13,NN) > 20.D00) THEN  
!--consider the local collision rate      
      CCELL(3,N)=0.5D00*VAR(14,NN)*CPDTM  
!--look also at collision cell transit time based on the local flow speed  
      A=0.5D00*(B/(ABS(VAR(5,NN))))*TPDTM  
      IF (A < CCELL(3,N)) CCELL(3,N)=A  
      IF (2.D00*CCELL(3,N) < DTM) DTM=2.D00*CCELL(3,N)  
    ELSE  
!-- base the time step on a collision cell transit time at the refence vmp  
      A=TPDTM*B/VMPM   
      IF (A < CCELL(3,N)) CCELL(3,N)=A  
    END IF   
    IF (1.9D00*CCELL(3,N) < DTM) DTM=1.9D00*CCELL(3,N)  
!   
  END IF  
END DO  
!  
WRITE (9,*) 'DTM changes  from',DTMI,' to',DTM  
WRITE (*,*) 'DTM changes  from',DTMI,' to',DTM
!  
!----------------------------------------------------------------------------
!--update output interval 
! 
TPOUT=OUTRAT
IF (ISF > 0) THEN
  IF ((NOUT >= 0  ).AND.(NOUT < 100)) TPOUT=OUTRAT*.1d0
  IF ((NOUT >= 100).AND.(NOUT < 150)) TPOUT=OUTRAT*.2d0
  IF ((NOUT >= 150).AND.(NOUT < 170)) TPOUT=OUTRAT*.5d0
  IF ((NOUT >= 200)) TPOUT=OUTRAT*INT(.9999999+(NOUT-190)/10) !ISF=0 !steady state is achieved
!
  IF ((CPDTM < 0.02).AND.(NOUT >= 200)) CPDTM=0.02
  IF ((CPDTM < 0.2 ).AND.(NOUT >= 250)) CPDTM=0.2
END IF
!
DTSAMP=DTSAMP*DTM/DTMI  
DTOUT=DTSAMP*TPOUT
TOUT=FTIME  
!
WRITE (9,*) 'NOUT:',NOUT,' OUTRAT:',TPOUT
WRITE (*,*) 'NOUT:',NOUT,' OUTRAT:',TPOUT
!
!----------------------------------------------------------------------------
!  
RETURN  
!  
END SUBROUTINE OUTPUT_RESULTS   
!   
!**************************************************************************************  
!  
SUBROUTINE NUMCHAR4(NNN,E)  
!  
!--produces the character equivalent E of a 4 digit integer NNN   
!  
CHARACTER(LEN=1) :: A  
CHARACTER(LEN=1) :: B  
CHARACTER(LEN=1) :: C  
CHARACTER(LEN=1) :: D  
CHARACTER(LEN=4) :: E  
A='0' ; B='0' ; C='0' ; D='0'  
N=NNN  
IF (N.GT.999) THEN  
  L=N/1000  
  A=CHAR(48+L)  
  N=N-1000*L  
END IF  
IF (N.GT.99) THEN  
  L=N/100  
  B=CHAR(48+L)  
  N=N-100*L  
END IF  
IF (N.GT.9) THEN  
  L=N/10  
  C=CHAR(48+L)  
  N=N-10*L  
END IF  
D=CHAR(48+N)  
E=A//B//C//D  
!  
RETURN  
!  
END SUBROUTINE NUMCHAR4  
!   
!*****************************************************************************  
!  
SUBROUTINE DISSOCIATION  
!  
!--dissociate molecules that have been marked with IPVIB(0,:) < 0  
!  
USE MOLECS  
USE GAS  
USE CALC
USE OUTPUT  
USE GEOM  
!  
IMPLICIT NONE  
!  
INTEGER ::K,KK,L,M,LS,MS,KS,JS,KV,IKA,NC,NS,NSP,MAXLEV,II,IV,IDT=0 !--isebasti: included IDT   
REAL(KIND=8) :: A,B,C,ECT,ECC,EVIB,VRR,VR,RMM,RML,RANF,SVDOF(2),PROB,ERM !--isebasti: included RANF  
REAL(KIND=8), DIMENSION(3) :: VRC,VCM,VRCP  
!  
L=0  
DO WHILE (L < NM)  
  L=L+1  
  LS=IPSP(L)  
  IF (ISPV(LS) > 0) THEN  
    IF (IPVIB(0,L) < 0) THEN  !dissociate through reaction IKA
      IKA=-IPVIB(0,L)  
      ECT=PROT(L)  !--ECT is the energy available for post-reaction states  
      DO KV=1,ISPV(LS)
        ECT=ECT+IPVIB(KV,L)*BOLTZ*SPVM(1,KV,LS)
      END DO        
      IF (NM >= MNM) CALL EXTEND_MNM(1.1)  
      NM=NM+1
      M=NM
!--update species
      IPSP(L)=JREA(1,IKA,1)    
      IPSP(M)=JREA(1,IKA,2)
      LS=IPSP(L)
      MS=IPSP(M)
!--set center of mass velocity as that of molecule          
      VCM(1:3)=PV(1:3,L)  
!--new born molecule
      PX(M)=PX(L)  
      IPCELL(M)=IPCELL(L)  
      PTIM(M)=PTIM(L)
!--set any internal mode to the ground state and reset IPVIB(0,L) and IPVIB(0,M) to 0          
      PROT(L)=0.d0  
      PROT(M)=0.d0
      IPVIB(:,L)=0  
      IPVIB(:,M)=0
!--effective vibrational degrees of freedom
      NC=IPCELL(L)        !collision cell  
      NS=ICCELL(3,NC)     !sampling cell  
      A=VAR(10,NS)        !sampling cell vibrational temperature
      SVDOF(:)=0.d0 
      IF (ISPV(LS) > 0) THEN
        DO KV=1,ISPV(LS)
          SVDOF(1)=SVDOF(1)+2.d0*(SPVM(1,KV,LS)/A)/(DEXP(SPVM(1,KV,LS)/A)-1.d0)
        END DO
      END IF
      IF (ISPV(MS) > 0) THEN
        DO KV=1,ISPV(MS)
          SVDOF(2)=SVDOF(2)+2.d0*(SPVM(1,KV,MS)/A)/(DEXP(SPVM(1,KV,MS)/A)-1.d0)
        END DO
      END IF  
!--Larsen-Borgnakke serial vibrational energy redistribution  
      DO NSP=1,2  
        IF (NSP == 1) THEN  
          K=L ; KS=LS ; JS=MS
        ELSE  
          K=M ; KS=MS ; JS=LS  
        END IF
        IF (ISPV(KS) > 0) THEN
          DO KV=1,ISPV(KS)  
            EVIB=DFLOAT(IPVIB(KV,K))*BOLTZ*SPVM(1,KV,KS)                        
            ECC=ECT+EVIB  
            MAXLEV=ECC/(BOLTZ*SPVM(1,KV,KS))
            CALL ZGF(RANF,IDT)
            II=0  
            DO WHILE (II == 0)  
              CALL ZGF(RANF,IDT)  
              IV=RANF*(MAXLEV+0.99999999D00)  
              IPVIB(KV,K)=IV  
              EVIB=DFLOAT(IV)*BOLTZ*SPVM(1,KV,KS)  
              IF (EVIB < ECC) THEN
                IF (NSP == 1) A=(5.d0-2.d0*SPM(3,KS,JS))+ISPR(1,KS)+ISPR(1,JS)+SVDOF(2)+SVDOF(1)
                IF (NSP == 2) A=(5.d0-2.d0*SPM(3,KS,JS))+ISPR(1,KS)+ISPR(1,JS)+SVDOF(2)  
                PROB=(1.D00-EVIB/ECC)**(A*0.5d0-1.d0)   !--see eqn (5.61) and (5.45)  
                CALL ZGF(RANF,IDT)  
                IF (PROB > RANF) II=1  
              END IF  
            END DO  
            ECT=ECC-EVIB  
          END DO  
        END IF
      END DO 
!-------------------------------------------------
!--Larsen-Borgnakke serial rotational energy redistribution  
      DO NSP=1,2  
        IF (NSP == 1) THEN  
          K=L ; KS=LS ; JS=MS   
        ELSE  
          K=M ; KS=MS ; JS=LS  
        END IF   
        IF (ISPR(1,KS) > 0) THEN  
          ECC=ECT+PROT(K)
          IF (NSP == 1) A=(5.d0-2.d0*SPM(3,KS,JS))+ISPR(1,JS)
          IF (NSP == 2) A=(5.d0-2.d0*SPM(3,KS,JS))
          CALL LBS(ISPR(1,KS)*0.5d0-1.d0,A*0.5d0-1.d0,ERM,IDT)
          PROT(K)=ERM*ECC  
          ECT=ECC-PROT(K)  
        END IF  
      END DO
!-------------------------------------------------
!--calculate new velocities
      VRR=2.d0*ECT/SPM(1,LS,MS)  
      VR=DSQRT(VRR) 
      VRC=0.d0                  !initial relative velocity
      RML=SPM(1,LS,MS)/SP(5,MS)  
      RMM=SPM(1,LS,MS)/SP(5,LS)
      CALL VHSS(LS,MS,VR,VRC,VRCP,IDT)
      PV(1:3,L)=VCM(1:3)+RMM*VRCP(1:3)  
      PV(1:3,M)=VCM(1:3)-RML*VRCP(1:3) 
      IPCP(L)=M
      IPCP(M)=L
    END IF  
  END IF  
END DO  
!  
RETURN  
!  
END SUBROUTINE DISSOCIATION  
!  
!************************************************************************************  
!  
SUBROUTINE CHECK_REACTION(IKA,N,L,M,LS,MS,VRR,VR,VCM,RML,RMM,IDT)  
!  
!--Implement the old collision model (JCD = 0)  
!  
!  
USE MOLECS  
USE GAS  
USE CALC  
USE OUTPUT  
USE GEOM
USE OMP_LIB  
!  
IMPLICIT NONE  
!  
!  
INTEGER :: J,K,L,M,N,LS,MS,IKA,NRE,KK,KS,MK,KA,KAA,MKK,JR,KR,JS,KV,IA,ISTE(MNRE),&
           IDT,NS,NPM,I,IVDC,KM,IV,IS,IVAR(MNRE),II,JJ,NSP,nstep !--isebasti: included IDT,NS
REAL :: CC,DD,GAM  
REAL(KIND=8) :: A,B,ECT,ECR,ECV,EC,STERT,THBCELL,WF,PSTERT,VR,VRR,RML,RMM,ECM,ECN,RANF !--isebasti: included RANF  
REAL(KIND=8) :: VDOF1(MMVM),VDOF2(MMVM),EV1(MMVM),EV2(MMVM),EV,ECV1,ECV2,ECV3,SVDOF1,SVDOF2,SVDOF3,ECR1,ECR2,ECR3,AL,&
                ATDOF,AIDOF,ANDOF,AVDOF,X,C,D,AA,BB,XI,PSI,PHI,CV,EE,SEV(2),EP,EFAC,EVIB,TCOLT,TEMP,ECT2,ERD
REAL(KIND=8) :: VRC(3),VCM(3),STER(MNRE),ECA(MNRE),CF,VRCP(3),PVD(3),TVAR(7,MNRE),PROB
REAL(KIND=8) :: VIBGEN   !add by Han
!  
!--A,B,C working variables  
!--J,K,KK,MK,KA,KAA,MKK,JR,KR,JS,KV,IA working integers  
!--N the cell  
!--L,S  the molecule numbers  
!--LS,MS,KS species   
!--IKA reaction indicator  
!      0 for no reaction  
!      1 for reaction   
!--ECR rotational energy  
!--ECV vibrational energy  
!--ECA available energy  
!--STER steric factor  
!--ISTE third body species  
!--NRE number of reactions that may occur for this pair  
!--STERT cumulative steric factor  
!--THBCELL number of third body molecules in cell  
!--WF weighting factor  
!--PSTERT probability  
!--VRC relative velocity components  
!--VCM center-of-mass velocity components  
!--VR relative speed  
!--VRR square of rel speed  
!--RML,RMM molecule mass parameters  
!
NS=ICCELL(3,N)     !sampling cell 
TEMP=VAR(10,NS)    !sampling cell vibrational temperature
IKA=0  
!--IKA becomes >0 if a reaction occurs  
!--consider reactions based on the continuum rate equations  
!  
NRE=NRSP(LS,MS)
IF (NRE > 0) THEN  
  ECT=0.5D00*SPM(1,LS,MS)*VRR  
  ECR=0.D00; ECV=0.D00
  EV1=0.D00; EV2=0.D00
  ECR1=0.D00; ECR2=0.D00
  ECV1=0.D00; ECV2=0.D00
  VDOF1=0.D00; VDOF2=0.D00
  SVDOF1=0.D00; SVDOF2=0.D00
  IF (ISPR(1,LS) > 0) ECR1=PROT(L)  
  IF (ISPR(1,MS) > 0) ECR2=PROT(M)
  ECR=ECR1+ECR2 
  IF (MMVM > 0) THEN  
    IF (ISPV(LS) > 0) THEN  
      DO KV=1,ISPV(LS)
        EV1(KV)=VIBGEN(IPVIB(KV,L),KV,LS)   !DFLOAT(IPVIB(KV,L))*BOLTZ*SPVM(1,KV,LS)
        VDOF1(KV)=2.d0*(SPVM(1,KV,LS)/TEMP)/(DEXP(SPVM(1,KV,LS)/TEMP)-1.d0)
        ECV1=ECV1+EV1(KV)
        SVDOF1=SVDOF1+VDOF1(KV)
      END DO
    END IF              
    IF (ISPV(MS) > 0) THEN  
      DO KV=1,ISPV(MS)  
        EV2(KV)=VIBGEN(IPVIB(KV,M),KV,MS)   !DFLOAT(IPVIB(KV,M))*BOLTZ*SPVM(1,KV,MS)
        VDOF2(KV)=2.d0*(SPVM(1,KV,MS)/TEMP)/(DEXP(SPVM(1,KV,MS)/TEMP)-1.d0)
        ECV2=ECV2+EV2(KV)
        SVDOF2=SVDOF2+VDOF2(KV)
      END DO
    END IF
    ECV=ECV1+ECV2
  END IF
  EC=ECT+ECR+ECV
  AL=SPM(3,LS,MS)-0.5d0
  ATDOF=(ISPR(1,LS)+ISPR(1,MS)+SVDOF1+SVDOF2+(4.d0-2.d0*AL))*0.5d0 !total number of dofs  
  AIDOF=(ISPR(1,LS)+ISPR(1,MS)+SVDOF1+SVDOF2)*0.5d0                !total internal dofs
  STERT=0.
!  
  DO J=1,NRE  
    K=IRCD(J,LS,MS)
    NPM=NREA(1,K)+NREA(2,K)
    X=REA(4,K)-0.5d0+AL
    ECA(J)=EC
    STER(J)=0.
    CF=1.d0
!
    IF ((EC >= REA(2,K)).AND.(EC+REA(5,K) > 0)) THEN !2nd condition prevents a negative post-reaction total energy !CPER
!
      IF (NPM == 1) THEN  
!--possible recombination reaction
        MKK=0
        CALL ZGF(RANF,IDT)  
        IF (IRECOM == 1 .AND. RANF < 0.001d0) THEN  
!----only one in 1000 possible recombinations are considered and   
!----the probability is increased by a factor of 1000 
!  
          IF (JREA(2,K,1) < 0) THEN  
!--third-body molecule may be any species in cell     
            IF (ICCELL(2,N) >= 3) THEN  
              MK=L  
              DO WHILE ((MK == L).OR.(MK == M))  
                CALL ZGF(RANF,IDT)  
                KK=INT(RANF*DFLOAT(ICCELL(2,N)))+ICCELL(1,N)+1  
                MK=ICREF(KK)  
              END DO  
              KS=IPSP(MK)  
              ISTE(J)=MK  
              A=REA(6,K)  
              IA=-JREA(2,K,1)  
              A=A*THBP(IA,KS)  
            ELSE  
              A=0.D00  
            END IF  
            THBCELL=ICCELL(2,N)             
          ELSE  
            KS=JREA(2,K,1)  
!--the third-body molecule must be species KS  
            THBCELL=0.  
            MKK=0  
            DO KAA=1,ICCELL(2,N)                       
              KA=ICCELL(1,N)+KAA  
              MK=ICREF(KA)  
              IF ((IPSP(MK) == KS).AND.(IPCELL(MK) > 0).AND.(IPVIB(0,MK) >= 0)) THEN
                THBCELL=THBCELL+1.  
                IF ((MK /= L).AND.(MK /= M)) MKK=MK  
              END IF  
            END DO  
            IF (MKK > 0) THEN                                
              MK=MKK  
              ISTE(J)=MK  
              A=REA(6,K)
            ELSE  
              A=0.
            END IF  
          END IF  
          B=THBCELL*FNUM/CCELL(1,N)  !number density of third body species in collision cell
          CC=3.5d0-AL !3.5d0-AIDOF-AL
          DD=3.5d0-AL+X !3.5d0-AIDOF-AL+X
          C=GAM(CC)
          D=GAM(DD)
          !IF (IREAC /= 2 .AND. EC > REA(5,K)) THEN !molecule should dissociate
          !  CF=0.d0
          !  WRITE(*,*) 'RECOMB-CF=0' 
          !END IF
          STER(J)=CF*B*A*(C/D)*ECA(J)**X*1000.d0  !note the factor
          STERT=STERT+STER(J)  
        END IF  
      END IF
!
      IF (NPM == 2) THEN
!--possible exchange reaction
        CC=ATDOF
        DD=AIDOF+1.5d0+REA(4,K)
        C=GAM(CC)
        D=GAM(DD)
        AA=X+ATDOF-1.d0
        BB=ATDOF-1.d0
        !--note measures to prevent underflows
        STER(J)=CF*(REA(6,K)*(C/D))*(((ECA(J)-REA(2,K))*1.d10)**AA/(ECA(J)*1.d10)**BB)*1.d10**(BB-AA)  
        STERT=STERT+STER(J)  
      END IF  
!
      IF (NPM == 3) THEN   
!--possible dissociation reaction
        IF (LS /= MS) THEN
          IF (LS == IREA(1,K)) IVDC=1  !molecule L dissociates
          IF (MS == IREA(1,K)) IVDC=2  !molecule M dissociates 
        ELSE
          IF (ECV1 >= ECV2) IVDC=1
          IF (ECV2 >  ECV1) IVDC=2
        END IF
        IF(REA(1,K) < 0.) THEN
          !TCE model (same as in exchange reactions)  
          CC=ATDOF
          DD=AIDOF+1.5d0+REA(4,K)
          C=GAM(CC)
          D=GAM(DD)
          AA=X+ATDOF-1.d0
          BB=ATDOF-1.d0
          !--note measures to prevent underflows
          STER(J)=CF*(REA(6,K)*(C/D))*(((ECA(J)-REA(2,K))*1.d10)**AA/(ECA(J)*1.d10)**BB)*1.d10**(BB-AA)  
          STERT=STERT+STER(J) 
        ELSE  
          !VDC model
          IF (IVDC == 1) AVDOF=SVDOF1*0.5d0    !internal dofs contributing to vibrational favoring                
          IF (IVDC == 2) AVDOF=SVDOF2*0.5d0
          ANDOF=ATDOF-AVDOF                    !internal dofs not contributing to vibrational favoring
          XI=ANDOF-1.d0
          PSI=REA(4,K)+ATDOF+0.5d0-(4.d0-2.d0*AL)*0.5d0
          PHI=REA(1,K)  
          CC=ANDOF
          DD=PSI+1.d0
          C=GAM(CC)
          D=GAM(DD)
          CV=1.d0
          BB=1.d0
          EE=1.d0
          EV=0.d0
          IF (MMVM > 0) THEN
            IF (IVDC == 1) THEN
              DO KV=1,ISPV(LS)
                AA=(TEMP/SPVM(1,KV,LS))**(VDOF1(KV)*0.5d0)
                SEV=0.d0
                DO I=1,IDL(KV,LS)
                  SEV(1)=SEV(1)+(DFLOAT(I)*SPVM(1,KV,LS))**PHI
                  SEV(2)=SEV(2)+DEXP(-DFLOAT(I)*SPVM(1,KV,LS)/TEMP)
                END DO 
                CV=CV*AA*SEV(1)/SEV(2)
                BB=BB*SPVM(1,KV,LS)**(VDOF1(KV)*0.5d0)
                EE=EE*(EV1(KV)/BOLTZ)**PHI  
              END DO
              EV=ECV1; ERD=ECR1; PVD(1:3)=PV(1:3,L)
            ELSE
              DO KV=1,ISPV(MS)
                AA=(TEMP/SPVM(1,KV,MS))**(VDOF2(KV)*0.5d0)
                SEV=0.d0
                DO I=1,IDL(KV,MS)
                 SEV(1)=SEV(1)+(DFLOAT(I)*SPVM(1,KV,MS))**PHI
                 SEV(2)=SEV(2)+DEXP(-DFLOAT(I)*SPVM(1,KV,MS)/TEMP)
                END DO 
                CV=CV*AA*SEV(1)/SEV(2)
                BB=BB*SPVM(1,KV,MS)**(VDOF2(KV)*0.5d0)
                EE=EE*(EV2(KV)/BOLTZ)**PHI  
              END DO
              EV=ECV2; ERD=ECR2; PVD(1:3)=PV(1:3,M)
            END IF
          END IF
          STER(J)=CF*(REA(6,K)*(C/D)/(CV*BB))*(((ECA(J)-REA(2,K))/BOLTZ)**PSI/((ECA(J)-EV)/BOLTZ)**XI)*EE
          STERT=STERT+STER(J)
        END IF  
      END IF 
!
!--sample pre-collisional vibrational levels  !--isebasti: included
!      DO I=1,3
!        KS=0
!        IF (I == 1) THEN
!          KM=L; KS=LS
!        ELSE IF (I == 2) THEN
!          KM=M; KS=MS
!        ELSE IF ((I == 3).AND.(NPM == 1).AND.(MKK > 0)) THEN
!          KM=ISTE(J); KS=IPSP(KM)
!        END IF
!        IF ((KS > 0).AND.(ISPV(KS) > 0)) THEN
!          DO KV=1,ISPV(KS)
!            NPVIB(1,NS,K,KS,KV,IPVIB(KV,KM))=NPVIB(1,NS,K,KS,KV,IPVIB(KV,KM))+1  !bin accumulation per species 
!          END DO
!          TNPVIB(1,NS,K,KS)=TNPVIB(1,NS,K,KS)+1.d0  !total counter
!        END IF
!      END DO
!
!--store collision data in temporary variables
      IVAR(K)=IVDC
      TVAR(1,K)=EC
      TVAR(2,K)=ECT
      TVAR(3,K)=ERD
      TVAR(4,K)=EV
      TVAR(5:7,K)=PVD(1:3)
    END IF
  END DO 
!
  JR=0
!
  CALL ZGF(RANF,IDT)
  IF (STERT > RANF) THEN
!--a reaction occurs     
    CALL ZGF(RANF,IDT)  
    PSTERT=0.  
    DO WHILE (PSTERT < RANF)
      JR=JR+1  
      PSTERT=PSTERT+STER(JR)/STERT  
    END DO
  END IF
!
  IF (JR /= 0) THEN
!--reaction K occurs
    K=IRCD(JR,LS,MS)
    IKA=K
    IVDC=IVAR(K)    
    EC=TVAR(1,K)  
    ECT=TVAR(2,K)  
    ERD=TVAR(3,K)  
    EV=TVAR(4,K)  
    PVD(1:3)= TVAR(5:7,K)
    NPM=NREA(1,K)+NREA(2,K) 
! 
!   ----------------------------------------------------------- 
!--modify molecule states according to post-reaction conditions 
    IF (IREAC <= 1) THEN   
      EP=EC+REA(5,K)           !post-reaction total energy (not accounting for third body yet)
!  
      IF (NPM == 1) THEN  
!--one post-collision molecule (recombination)
        IPSP(L)=JREA(1,K,1)    !molecule L becomes the recombined molecule
        LS=IPSP(L)             
        PV(1:3,L)=VCM(1:3)
        IPCELL(M)=-IPCELL(M)   !molecule M is marked for removal    
        M=ISTE(JR)             !calculate collision of molecule L with third body  
        MS=IPSP(M)
        VRC(1:3)=PV(1:3,L)-PV(1:3,M)  
        VRR=VRC(1)**2+VRC(2)**2+VRC(3)**2  
        ECT2=0.5D00*SPM(1,LS,MS)*VRR
        ECR2=0.d0; ECV2=0.d0            
        IF (ISPR(1,MS) > 0) ECR2=PROT(M)
        IF (ISPV(MS) > 0) THEN
            DO KV=1,ISPV(MS)  
              ECV2=ECV2+DFLOAT(IPVIB(KV,M))*BOLTZ*SPVM(1,KV,MS)  
            END DO  
        END IF
        EP=EP+ECT2+ECR2+ECV2   !add third body Etra, Erot, and Evib    
      END IF
!
      IF (NPM == 2) THEN
!--two post-collision molecules (exchange)  
        IPSP(L)=JREA(1,K,1)  
        IPSP(M)=JREA(2,K,1)  
        LS=IPSP(L)         
        MS=IPSP(M)
      END IF
!
      IF (NPM == 3) THEN  
!--three post-collision molecules (dissociation)
        IPVIB(0,L)=0  
        IPVIB(0,M)=0
        IF (IVDC == 1) IPVIB(0,L)=-K   !molecule is marked for dissociation through reaction K
        IF (IVDC == 2) IPVIB(0,M)=-K   
      END IF
!
!--set all internal energies to zero
      PROT(L)=0.d0
      PROT(M)=0.d0
      IPVIB(1:MMVM,L)=0  
      IPVIB(1:MMVM,M)=0
!
!-- populate vibrational levels (this part will be used for DSMC15 work)  
!      II=0                     
!      nstep=0
!      DO WHILE (II == 0)
!        EVIB=0.d0
!        DO NSP=1,2  
!          IF (NSP == 1) THEN  
!            KM=L ; KS=LS   
!          ELSE  
!            KM=M ; KS=MS  
!          END IF
!          IF (ISPV(KS) > 0) THEN   
!            DO KV=1,ISPV(KS)
!              JJ=0
!              DO WHILE (JJ == 0)
!                !CALL ZGF(RANF,IDT)  
!                !IV=RANF*(IDL(KV,KS)+0.99999999D00) 
!                CALL SVIB(KS,TEMP,IV,KV,IDT)  
!                !levels sampled from pre-collisional distribution of the respective depleting reaction
!                PROB=1.d0 !DFLOAT(NPVIB(1,NN,IREV(IKA),KS,KV,IV))/DFLOAT(MAXVAL(NPVIB(1,NN,IREV(IKA),KS,KV,:)))
!                CALL ZGF(RANF,IDT)
!                IF (PROB > RANF) JJ=1  
!              END DO
!              IPVIB(KV,KM)=IV
!              EVIB=EVIB+DFLOAT(IPVIB(KV,KM))*BOLTZ*SPVM(1,KV,KS)
!            END DO
!          END IF
!        END DO
!        nstep=nstep+1  
!        if (nstep > 1000000) then  
!          write (*,*) nstep,ika,ks,ispv(ks),evib,boltz*spvm(4,1,ks),ect  
!          IPVIB(1:MMVM,L)=0
!          IPVIB(1:MMVM,M)=0
!          EVIB=0.d0
!          II=1
!        end if
!        IF (EVIB < EP) II=1
!      END DO
!      EP=EP-EVIB             !vibrational energy not available for redistribution anymore
!
!--LB-RT redistribution is redone after reaction  (this part might be used for DSMC15 work)
!      AL=SPM(3,LS,MS)-0.5d0
!      ATDOF=ISPR(1,LS)+ISPR(1,MS)+(4.d0-2.d0*AL) !remaining dofs in L-M pair
!      PROT(L)=EP*ISPR(1,LS)/ATDOF  
!      PROT(M)=EP*ISPR(1,MS)/ATDOF  
!      EP=EP-PROT(L)-PROT(M)
!
!--update collision pair data
      VRR=2.D00*EP/SPM(1,LS,MS)  
      VR=DSQRT(VRR)  
      RML=SPM(1,LS,MS)/SP(5,MS)  
      RMM=SPM(1,LS,MS)/SP(5,LS)
      IF (NPM == 1) THEN       !update VCM for recombination reactions
        VCM(1:3)=RML*PV(1:3,L)+RMM*PV(1:3,M)  
      END IF
    END IF
!   ----------------------------------------------------------- 
  END IF 
END IF  
!  
RETURN  
END SUBROUTINE CHECK_REACTION  
!  
!************************************************************************************  
!  
SUBROUTINE ADAPT_CELLS  
!  
!--adapt the sampling cells through the splitting of the divisions into successive levels  
!--the collision cells are divisions of the sampling cells  
!  
USE MOLECS  
USE GAS  
USE GEOM  
USE CALC  
USE OUTPUT  
!  
IMPLICIT NONE  
!  
INTEGER :: M,N,L,K,KK,I,J,JJ,MSEG,NSEG,NSEG1,NSEG2,IDT=0 !included IDT  
REAL(KIND=8) :: A,B,DDE,DCRIT,DENNR,RANF !--isebasti: included RANF,DENNR
INTEGER, ALLOCATABLE, DIMENSION(:) :: KDIV,NC  
INTEGER, ALLOCATABLE, DIMENSION(:,:) :: ISD  
REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: XMIN,XMAX,DRAT  
!  
!--DCRIT  the number density ratio that causes a cell to be subdivided  
!--KDIV(N) the number of divisions/subdivisions (cells or further subdivisions) at level N  
!--DRAT(N) the contriburion to the density ratio of element N  
!--NC(I) the number of sampling cells at level I  
!--DDE the width of an element   
!--MSEG the maximum number of segments (a segment is the size of the smallest subdivision  
!--NSEG1 the (first segment-1) in the subdivision  
!--NSEG2 the final segment in the subdivision  
!--ISD(N,M) 0,1 for cell,subdivided for level N subdivision  
!--DENNR the refence number density
!  
DCRIT=4.0d0    !--may be altered !--isebasti: original value is 1.5d0
!  
!--determine the level to which the divisions are to be subdivided  
!  
A=1.D00  
IF (IADAPT == 1) DENNR=FND(1)               !--isebasti: included
IF (IADAPT == 2) DENNR=MINVAL(VAR(3,:))     !--isebasti: included
DO N=1,NCELLS  
  IF (VAR(3,N)/DENNR > A) A=VAR(3,N)/DENNR  !--isebasti: FND(1) replaced by DENNR 
END DO  
ILEVEL=0  
DO WHILE (A > DCRIT)  
  ILEVEL=ILEVEL+1  
  A=A/2.D00  
END DO  
WRITE (*,*) 'ILEVEL =',ILEVEL   !--isebasti: included
WRITE (9,*) 'ILEVEL =',ILEVEL   
NSEG=2**ILEVEL   
MSEG=NDIV*NSEG  
!  
ALLOCATE (KDIV(0:ILEVEL),DRAT(MSEG),NC(0:ILEVEL),ISD(0:ILEVEL,MSEG),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR KDIV ARRAY',ERROR  
ENDIF  
!  
DDE=(XB(2)-XB(1))/DFLOAT(MSEG)  
DO N=1,MSEG  
  A=XB(1)+(DFLOAT(N)-0.5D00)*DDE  
  CALL FIND_CELL(A,M,L)  
  DRAT(N)=VAR(3,L)/(DENNR*DFLOAT(NSEG)) !--isebasti: FND(1) replaced by DENNR
END DO    
!  
!--calculate the number of subdivisions at the various levels of subdivision  
KDIV=0  
!--also the number of sampling cells at each level  
NC=0   
!  
DO N=1,NDIV    !--divisions  
  ISD=0  
  ISD(0,1)=1  
  KDIV(0)=KDIV(0)+1  
!  WRITE (9,*) 'DIVISION',N  
  DO I=0,ILEVEL  !--level of subdivision  
!    WRITE (9,*) 'LEVEL',I  
    J=2**I  !--number of possible subdivisions at this level  
    JJ=NSEG/J  !--number of segments in a subdivision  
    DO M=1,J  
!      WRITE (9,*) 'SUBDIVISION',M  
      IF (ISD(I,M) == 1) THEN   
        NSEG1=(N-1)*NSEG+(M-1)*JJ+1   
        NSEG2=NSEG1+JJ-1  
        A=0.D00  
!        WRITE (9,*) 'NSEG RANGE',NSEG1,NSEG2  
        DO L=NSEG1,NSEG2  
          A=A+DRAT(L)  
        END DO  
!        WRITE (9,*) 'DENS CONTRIB',A  
        IF (A < DCRIT) THEN  
          NC(I)=NC(I)+1  
!          WRITE (9,*) 'LEVEL',I,' CELLS TO', NC(I)  
        ELSE  
          KDIV(I+1)=KDIV(I+1)+2  
!          WRITE (9,*) 'LEVEL',I+1,' SUBDIVISIONS TO',KDIV(I+1)  
          DO L=NSEG1-(N-1)*NSEG,NSEG2-(N-1)*NSEG  
            ISD(I+1,L)=1  
          END DO    
        END IF  
      END IF  
    END DO  
  END DO  
END DO   
!  
WRITE (9,*) 'KDIV',KDIV  
!  
WRITE (9,*) 'NC',NC  
WRITE (*,*)  
WRITE (9,*) 'Number of divisions',NDIV  
A=0  
NCELLS=0  
DO N=0,ILEVEL  
  A=A+DFLOAT(NC(N))/(2.D00**N)  
  NCELLS=NCELLS+NC(N)  
END DO    
WRITE (9,*) 'Total divisions from sampling cells',A  
WRITE (9,*) 'Adapted sampling cells',NCELLS  
NCCELLS=NCELLS*NCIS  
WRITE (9,*) 'Adapted collision cells',NCCELLS  
!  
DEALLOCATE (JDIV,CELL,ICELL,CCELL,ICCELL,COLLS,WCOLLS,CLSEP,VAR,VARSP,CS,CST,STAT=ERROR) !--isebasti: CST included  
!DEALLOCATE (NPVIB,TNPVIB,STAT=ERROR)
IF (ERROR /= 0) THEN  
  WRITE (*,*)'PROGRAM COULD NOT DEALLOCATE ARRAYS IN ADAPT',ERROR  
END IF  
!  
DO N=0,ILEVEL  
  IF (KDIV(N) > MDIV) MDIV=KDIV(N)  
END DO   
!    
ALLOCATE (JDIV(0:ILEVEL,MDIV),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR JDIV ARRAY IN ADAPT',ERROR  
ENDIF  
!  
ALLOCATE (CELL(4,NCELLS),ICELL(NCELLS),CCELL(5,NCCELLS),ICCELL(3,NCCELLS),XMIN(NCCELLS),XMAX(NCCELLS),STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR CELL ARRAYS IN ADAPT',ERROR  
ENDIF  
!--isebasti: CST included bellow
ALLOCATE (COLLS(NCELLS),WCOLLS(NCELLS),CLSEP(NCELLS),VAR(21,NCELLS),VARSP(0:11,NCELLS,MSP),&
          CS(0:8+MMVM,NCELLS,MSP),CST(0:4,NCELLS),STAT=ERROR)  !--isebasti: correcting CS allocation
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR SAMPLING ARRAYS IN ADAPT',ERROR  
ENDIF
!
!IF (MNRE > 0) ALLOCATE (NPVIB(2,NCELLS,MNRE,MSP,MMVM,0:50),TNPVIB(2,NCELLS,MNRE,MSP),STAT=ERROR)
IF (ERROR /= 0) THEN  
  WRITE (*,*) 'PROGRAM COULD NOT ALLOCATE SPACE FOR NPVIB ARRAYS IN ADAPT',ERROR  
ENDIF
!  
NCCELLS=0  
NCELLS=0  
!  
!--set the JDIV arrays and the sampling cells at the various levels of subdivision  
KDIV=0  
JDIV=0  
!  
DO N=1,NDIV    !--divisions  
  ISD=0  
  ISD(0,1)=1  
  KDIV(0)=KDIV(0)+1  
  DO I=0,ILEVEL  !--level of subdivision  
    J=2**I  !--number of possible subdivisions at this level  
    JJ=NSEG/J  !--number of segments in a subdivision  
    DO M=1,J  
      IF (ISD(I,M) == 1) THEN   
        NSEG1=(N-1)*NSEG+(M-1)*JJ+1   
        NSEG2=NSEG1+JJ-1  
        A=0.D00  
        DO L=NSEG1,NSEG2  
          A=A+DRAT(L)  
        END DO  
        IF (A < DCRIT) THEN  
          NCELLS=NCELLS+1  
          XMIN(NCELLS)=XB(1)+DFLOAT(NSEG1-1)*DDE  
          XMAX(NCELLS)=XMIN(NCELLS)+DFLOAT(NSEG2-NSEG1+1)*DDE  
          WRITE (9,*) NCELLS,I,' XMIN,XMAX',XMIN(NCELLS),XMAX(NCELLS)  
          JDIV(I,KDIV(I)-(J-M))=-NCELLS            
!          WRITE (9,*) 'JDIV(',I,',',KDIV(I)-(J-M),')=',-NCELLS  
        ELSE  
          JDIV(I,KDIV(I)-(J-M))=KDIV(I+1)  
!          WRITE (9,*) 'JDIV(',I,',',KDIV(I)-(J-M),')=',KDIV(I+1)           
          KDIV(I+1)=KDIV(I+1)+2  
          DO L=NSEG1-(N-1)*NSEG,NSEG2-(N-1)*NSEG  
            ISD(I+1,L)=1  
          END DO    
        END IF  
      END IF  
    END DO  
  END DO  
END DO   
!  
!--set the other quantities associated with the sampling cells and the collision cells  
!  
NCCELLS=0  
DO N=1,NCELLS  
  CELL(1,N)=(XMIN(N)+XMAX(N))/2.D00  
  CELL(2,N)=XMIN(N)  
  CELL(3,N)=XMAX(N)  
  IF (IFX == 0) CELL(4,N)=XMAX(N)-XMIN(N)    !--calculation assumes unit cross-section  
  IF (IFX == 1) CELL(4,N)=PI*(XMAX(N)**2-XMIN(N)**2)  
  IF (IFX == 2) CELL(4,N)=1.33333333333333333333D00*PI*(XMAX(N)**3-XMIN(N)**3)  
  ICELL(N)=NCCELLS  
  DO M=1,NCIS  
    NCCELLS=NCCELLS+1  
    ICCELL(3,NCCELLS)=N  
    CCELL(1,NCCELLS)=CELL(4,N)/DFLOAT(NCIS)  
    CCELL(3,NCCELLS)=DTM/2.D00  
    CCELL(4,NCCELLS)=2.D00*VMPM*SPM(2,1,1)  
    CALL ZGF(RANF,IDT)  
    CCELL(2,NCCELLS)=RANF  
    CCELL(5,NCCELLS)=FTIME  
  END DO  
END DO  
!  
!--assign the molecules to the cells  
!  
DO N=1,NM  
  CALL FIND_CELL(PX(N),IPCELL(N),JJ)  
  M=IPCELL(N)  
END DO  
!  
!--deallocate the local variables  
DEALLOCATE (KDIV,NC,ISD,XMIN,XMAX,DRAT,STAT=ERROR)  
IF (ERROR /= 0) THEN  
  WRITE (*,*)'PROGRAM COULD NOT DEALLOCATE LOCAL ARRAYS IN ADAPT',ERROR  
END IF  
!  
RETURN  
!  
END SUBROUTINE ADAPT_CELLS  
!  
!*****************************************************************************  
!  
SUBROUTINE ENERGY(M,I,TOTEN)  
!  
!--calculate the total energy (all molecules if I=0, otherwise molecule I)  
!--used for diagnostic purposes only  
!  
USE MOLECS  
USE GAS  
USE CALC  
!  
IMPLICIT NONE  
!  
INTEGER :: K,L,N,I,II,M,IV,KV,J  
REAL(KIND=8) :: TOTEN,TOTENI,HF(MSP)  
!  
TOTEN=0.  
!  
IF ((M == 6).OR.(M == 4)) THEN  
  IF (I == 0) THEN  
    DO N=1,NM  
      IF (IPCELL(N) > 0) THEN  
        L=IPSP(N)          
        TOTENI=TOTEN  
        IF (M == 6) THEN  
          IF (L==3) TOTEN=TOTEN+0.5D00*BOLTZ*SPVM(4,1,1)  
          IF (L==4) TOTEN=TOTEN+0.5D00*BOLTZ*SPVM(4,1,2)  
          IF (L==5) TOTEN=TOTEN+1.49D-19  
        END IF    
        IF (M == 4) THEN  
          IF ((L==2).OR.(L == 3)) TOTEN=TOTEN+0.5D00*BOLTZ*SPVM(4,1,1)  
        END IF  
        TOTEN=TOTEN+0.5D00*SP(5,L)*(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
        IF (ISPR(1,L) > 0) TOTEN=TOTEN+PROT(N)  
        IF (ISPV(L) > 0) THEN  
          DO KV=1,ISPV(L)  
          J=IPVIB(KV,N)  
          IF (J <0) THEN  
            J=-J  
            IF (J == 99999) J=0  
          END IF    
            TOTEN=TOTEN+DFLOAT(J)*BOLTZ*SPVM(1,KV,L)  
          END DO  
        END IF      
      END IF  
      IF ((TOTEN-TOTENI) > 1.D-16) WRITE (*,*) 'MOL',N,' ENERGY',TOTEN-TOTENI  
    END DO  
!  
!    WRITE (9,*) 'Total Energy =',TOTEN,NM  
!    WRITE (*,*) 'Total Energy =',TOTEN,NM  
  ELSE   
    N=I  
    IF (IPCELL(N) > 0) THEN  
      L=IPSP(N)  
      IF (M == 6) THEN  
        IF (L==3) TOTEN=TOTEN+0.5D00*BOLTZ*SPVM(4,1,1)  
        IF (L==4) TOTEN=TOTEN+0.5D00*BOLTZ*SPVM(4,1,2)  
        IF (L==5) TOTEN=TOTEN+1.49D-19   
      END IF   
      IF ((M == 4).OR.(M > 8)) THEN  
!        IF ((L==2).OR.(L == 3)) TOTEN=TOTEN+0.5D00*BOLTZ*SPVM(4,1,1)  
      END IF  
      TOTEN=TOTEN+0.5D00*SP(5,L)*(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
      IF (ISPR(1,L) > 0) TOTEN=TOTEN+PROT(N)  
      IF (ISPV(L) > 0) THEN  
        DO KV=1,ISPV(L)  
          J=IPVIB(KV,N)  
          IF (J <0) THEN  
            J=-J  
            IF (J == 99999) J=0  
          END IF    
          TOTEN=TOTEN+DFLOAT(J)*BOLTZ*SPVM(1,KV,L)  
        END DO  
      END IF   
    END IF  
  END IF  
END IF   
IF (M == 8) THEN
  HF(1)=0.d0         !H2 entalpy of formation [kcal/mol]; from O Conaire et al (2004)
  HF(2)=52.098d0     !H
  HF(3)=0.d0         !O2
  HF(4)=59.56d0      !O
  HF(5)=8.91d0       !OH
  HF(6)=-57.77d0     !H2O
  HF(7)=3.d0         !HO2
  HF(8)=-94.05d0     !CO2   !from wikipedia = 393509 J/mol
!  HF(8)=0.d0         !Ar
  HF(:)=HF(:)*4184.d3/AVOG  !converting to [J/molec]
  IF (I == 0) THEN   
    DO N=1,NM  
      IF (IPCELL(N) > 0) THEN  !energies are normalized by BOLTZ
        TOTENI=TOTEN  
        L=IPSP(N) 
        TOTEN=TOTEN+HF(L)/BOLTZ  
        TOTEN=TOTEN+0.5D00*(SP(5,L)/BOLTZ)*(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
        IF (ISPR(1,L) > 0) TOTEN=TOTEN+PROT(N)/BOLTZ  
        IF (ISPV(L) > 0) THEN  
          DO K=1,ISPV(L)  
            IF (IPVIB(K,N) < 0) THEN  
              WRITE (*,*) 'Dissociation marked molecule still in flow',N,IPVIB(K,N)  
!              STOP  
            END IF     
            TOTEN=TOTEN+IPVIB(K,N)*SPVM(1,K,L)  
         END DO  
        END IF  
        IF ((TOTEN-TOTENI) > 1.d-16/BOLTZ) WRITE (*,*) 'MOL',N,' SPECIES',L,' ENERGY/BOLTZ',TOTEN-TOTENI  
      END IF  
    END DO  
!  
!    WRITE (9,*) 'Total Energy =',TOTEN,NM  
!    WRITE (*,*) 'Total Energy =',TOTEN,NM  
  ELSE    
    N=I  
    IF (IPCELL(N) > 0) THEN  
      L=IPSP(N)  
      IF (IPCELL(N) > 0) THEN  
        L=IPSP(N)  
        IF (L==2) TOTEN=TOTEN+3.62D-19  
        IF (L==4) TOTEN=TOTEN+4.14D-19  
        IF (L==5) TOTEN=TOTEN+0.65D-19  
        IF (L==6) TOTEN=TOTEN-4.02D-19  
        IF (L==7) TOTEN=TOTEN+0.17D-19  
        TOTEN=TOTEN+0.5D00*SP(5,L)*(PV(1,N)**2+PV(2,N)**2+PV(3,N)**2)  
        IF (ISPR(1,L) > 0) TOTEN=TOTEN+PROT(N)  
        IF (ISPV(L) > 0) THEN  
          DO K=1,ISPV(L)  
            IV=IPVIB(K,N)  
            IF (IV < 0) THEN  
              IF (IV == -99999) THEN  
                IV=0  
              ELSE  
                IV=-IV  
              END IF   
              IF (L == 1) TOTEN=TOTEN+7.24E-19  
              IF (L == 3) TOTEN=TOTEN+8.28E-19  
              IF (L == 5) TOTEN=TOTEN+7.76E-19  
              IF (L == 6) TOTEN=TOTEN+8.29E-19  
              IF (L == 7) TOTEN=TOTEN+3.45E-19     
            END IF  
            TOTEN=TOTEN+IV*BOLTZ*SPVM(1,K,L)  
          END DO  
        END IF   
        IF (ABS(TOTEN) > 1.D-16) THEN  
          CONTINUE  
        END IF  
      END IF  
    END IF  
  END IF  
END IF  
!  
RETURN  
!  
END SUBROUTINE ENERGY  
!  
!************************************************************************************  
!  
SUBROUTINE COLLISIONS  
!  
!--calculate the collisions  
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT  
USE OMP_LIB
!  
IMPLICIT NONE  
! 
INTEGER :: N,NN,M,MM,L,LL,K,KK,KT,J,I,II,III,NSP,MAXLEV,IV,NSEL,KV,LS,MS,KS,JS,IKA,IIII,LZ,KL,IS,IREC,&
           NLOOP,IA,IDISS,IE,IEX,JJ,NPRI,LIMLEV,KVV,KW,ISP1,ISP2,ISP3,INIL,INIM,JI,LV,IVM,NMC,NVM,LSI,&
           JX,MOLA,KR,JKV,NSC,KKV,NUMEXR,IAX,nstep,ISWITCH,IDT=0 !included IDT  
REAL(KIND=8) :: A,AA,AAA,AB,B,BB,BBB,ASEL,DTC,SEP,VR,VRR,ECT,EVIB,ECC,ZV,COLT,ERM,C,OC,SD,D,CVR,PROB,&
                RML,RMM,ECTOT,ETI,EREC,ET2,XMIN,XMAX,WFC,CENI,CENF,VRRT,TCOLT,EA,DEN,SVDOF(2),RANF !--isebasti: included RANF  
REAL(KIND=8),DIMENSION(3) :: VRC,VCM,VRCP,VRCT
INTEGER :: EDVIBCON !--counter for EDVIB added by Han
REAL :: GAM  !GAMMA function added by Han (used in QK collision volume)
REAL(KIND=8) :: VIBGEN
! 
!--N,M,K working integer  
!--LS,MS,KS,JS molecular species   
!--VRC components of the relative velocity  
!--RML,RMM molecule mass parameters  
!--VCM components of the center of mass velocity  
!--VRCP post-collision components of the relative velocity  
!--SEP the collision partner separation  
!--VRR the square of the relative speed  
!--VR the relative speed  
!--ECT relative translational energy  
!--EVIB vibrational energy  
!--ECC collision energy (rel trans +vib)  
!--MAXLEV maximum vibrational level  
!--ZV vibration collision number  
!--COLT collision temperature (rel trans + vib)  
!--TCOLT collision temperature based rel trans  
!--SDF the number of degrees of freedom associated with the collision  
!--ERM rotational energy  
!--NSEL integer number of selections  
!--CVR product of collision cross-section and relative speed  
!--PROB a probability  
!--IKA reaction indicator  ( 0 for no reaction, 1 for reaction )  
!--KT third body molecule code  
!--ECTOT energy added at recmbination  
!--IREC initially 0, becomes 1 if a recombination occurs  
!--NPRI the priority collision molecule (the last one rejected as a previous collision molecule)  
!--WFC weighting factor in the cell  
!--IEX is the reaction that occurs (1 if only one is possible)  
!--EA activation energy  
!  
NUMEXR=0  
!
!$omp parallel &
!$omp private(idt) & !always =0 outside parallel regions
!$omp private(n,dtc,nn,wfc,aaa,c,xmin,xmax,asel,nsel,i,ie,kl,iiii,npri,iii,k,l,m,ranf,ll) &
!$omp private(sep,j,mm,a,kk,vrc,vrr,vr,cvr,ect,nsp,kv,evib,ecc,maxlev,colt,b,zv,ii,iv,prob,svdof) &
!$omp private(erm,vcm,vrcp,oc,sd,d,ls,ms,rml,rmm,iswitch,idiss,ks,js,limlev,lz,iex,irec) &   
!$omp private(tcolt,kt,aa,bb,kvv,ji,lsi,ivm,nmc,nvm,ectot,psf,jj,ea,den,iax,jx,ika,nstep) &
!$omp reduction(+:ndissoc,ndissl,trecomb,nrecomb,treacl,treacg,tnex,tforex,trevex) & !Q-K
!$omp reduction(+:totdup,totcol,pcolls,tcol,colls,wcolls,clsep,reac) 
!$    idt=omp_get_thread_num()  !thread id
!$omp do schedule (static)   
DO N=1,NCCELLS  
!
!WRITE (*,*) 'COLL CELL',N,' MOLS',ICCELL(2,N),' STEP',CCELL(3,N)  
!  
  IF (FTIME-CCELL(5,N) > CCELL(3,N)) THEN  
!  
    DTC=2.D00*CCELL(3,N)  
!--calculate collisions appropriate to  time DTC  
    IF (ICCELL(2,N) > 1) THEN      !--no collisions calculated if there are less than two molecules in collision cell  
      NN=ICCELL(3,N)  
      WFC=1.D00  
      IF ((IWF == 1).AND.(IVB == 0)) WFC=1.D00+WFM*CELL(1,NN)**IFX  
      CCELL(5,N)=CCELL(5,N)+DTC  
      IF (IVB == 0) AAA=CCELL(1,N)    
      IF (IVB == 1) THEN  
        C=(XB(2)+VELOB*FTIME-XB(1))/DFLOAT(NDIV*NCIS)  
        XMIN=XB(1)+DFLOAT(N-1)*C  
        XMAX=XMIN+C  
        WFC=1.D00+WFM*(0.5D00*(XMIN+XMAX))**IFX  
        IF (IFX == 0) AAA=XMAX-XMIN  
        IF (IFX == 1) AAA=PI*(XMAX**2-XMIN**2)  !--assumes unit length of full cylinder  
        IF (IFX == 2) AAA=1.33333333333333333333D00*PI*(XMAX**3-XMIN**3)    !--flow is in the full sphere  
      END IF  
!--these statements implement the N(N-1) scheme  
!  
      ASEL=0.5D00*ICCELL(2,N)*(ICCELL(2,N)-1)*WFC*FNUM*CCELL(4,N)*DTC/AAA+CCELL(2,N)  
!  
      NSEL=ASEL  
      CCELL(2,N)=ASEL-DFLOAT(NSEL)  
      IF (NSEL > 0) THEN  
        I=0   !--counts the number of selections  
        IE=0  !--becomes 1 if the collision is inelastic  
        KL=0  !--becomes 1 if it is the last selection  
  
        IIII=0 !--becomes 1 if there is a recombination  
        NPRI=0  
        DO KL=1,NSEL  
          I=I+1  
          III=0  
!            
          IF (ICCELL(2,N) == 2) THEN  
            K=1+ICCELL(1,N)  
            L=ICREF(K)  
            K=2+ICCELL(1,N)  
            M=ICREF(K)  
            IF (M == IPCP(L)) THEN  
              III = 1    
              CCELL(5,N)=CCELL(5,N)-DTC  
            END IF   
          ELSE  
            K=NPRI  
            IF (K > 0) THEN  
              L=K  
              NPRI=0  
            ELSE  
              CALL ZGF(RANF,IDT)  
              K=INT(RANF*DFLOAT(ICCELL(2,N)))+ICCELL(1,N)+1  
              L=ICREF(K)  
            END IF     
!--one molecule has been selected at random  
            IF (NNC == 0) THEN  
!--select the collision partner at random              
              M=L  
              DO WHILE (M == L)  
                CALL ZGF(RANF,IDT)  
                K=INT(RANF*DFLOAT(ICCELL(2,N)))+ICCELL(1,N)+1  
                M=ICREF(K)  
              END DO  
            ELSE  
!--select the nearest from the total number (< 30) or a random 30           
              IF (ICCELL(2,N) < 30) THEN  
                LL=ICCELL(2,N)  
              ELSE  
                LL=30  
              END IF  
              SEP=1.D10  
              M=0      
              DO J=1,LL    
                IF (LL < 30) THEN  
                  K=J+ICCELL(1,N)  
                ELSE    
                  CALL ZGF(RANF,IDT)  
                  K=INT(RANF*DFLOAT(ICCELL(2,N)))+ICCELL(1,N)+1  
                END IF    
                MM=ICREF(K)  
                IF (MM /= L) THEN   !--exclude the already selected molecule  
                  IF (MM /= IPCP(L)) THEN   !  exclude the previous collision partner  
                    A=DABS(PX(L)-PX(MM))   
                    IF ((A < SEP).AND.(A >1.D-8*DDIV)) THEN  
                      M=MM  
                      SEP=A  
                    END IF  
                  ELSE  
                    NPRI=MM    !MM is made the priority molecule  
                  END IF  
                END IF    
              END DO  
            END IF            
          END IF  
          IF (III == 0) THEN  
            DO KK=1,3  
              VRC(KK)=PV(KK,L)-PV(KK,M)  
            END DO  
            VRR=VRC(1)*VRC(1)+VRC(2)*VRC(2)+VRC(3)*VRC(3)  
            VR=SQRT(VRR)  
!--Simple gas              
            IF (MSP == 1) THEN     
              CVR=VR*CXSS*((2.D00*BOLTZ*SP(2,1)/(RMAS*VRR))**(SP(3,1)-0.5D00))*RGFS  
              IF (CVR > CCELL(4,N)) CCELL(4,N)=CVR  
              CALL ZGF(RANF,IDT)  
              IF (RANF < CVR/CCELL(4,N)) THEN                 
!--the collision occurs  
                IF ((M == IPCP(L)).AND.(L == IPCP(M))) TOTDUP=TOTDUP+1.D00     
                TOTCOL=TOTCOL+1.D00
                TCOL(1,1)=TCOL(1,1)+2.D00  
                COLLS(NN)=COLLS(NN)+1.D000  
                WCOLLS(NN)=WCOLLS(NN)+WFC
                IF(NN == NSPDF) PCOLLS=PCOLLS+WFC    
                SEP=DABS(PX(L)-PX(M))   
                CLSEP(NN)=CLSEP(NN)+SEP         
                IF ((ISPR(1,1) > 0)) THEN  
!--Larsen-Borgnakke serial redistribution   
                  ECT=0.5D00*RMAS*VRR  
                  DO NSP=1,2  
!--consider the molecules in turn  
                    IF (NSP == 1) THEN  
                      K=L   
                    ELSE  
                      K=M   
                    END IF  
                    IF (MMVM > 0) THEN  
                      IF (ISPV(1) > 0) THEN
                        DO KV=1,ISPV(1)
                          EVIB=IPVIB(KV,K)*SPVM(1,KV,1)*BOLTZ
                          ECC=ECT+EVIB  
                          MAXLEV=ECC/SPVM(1,KV,1)
                          IF (SPVM(3,KV,1) > 0.) THEN  
!--note quantizing of COLT in the following statements  
                            IF (IZV == 1) THEN 
                              COLT=(DFLOAT(MAXLEV)*SPVM(1,KV,1))/(3.5D00-SPM(3,1,1))        !--quantized collision temperature  
                            ELSE  
                              COLT=VAR(8,NN)        !--isebasti: Ttrans, according to DSMC.f90 (2013)   
                            END IF    
                            B=SPVM(4,KV,1)/SPVM(3,KV,1)    !Tdiss/Tref            
                            A=SPVM(4,KV,1)/COLT            !Tdiss/T       
       ZV=(A**SPM(3,1,1))*(SPVM(2,KV,1)*(B**(-SPM(3,1,1))))**(((A**0.3333333D00)-1.D00)/((B**0.33333D00)-1.D00))
                          ELSE
                            ZV=SPVM(2,KV,1) !not SPVM(2,KV,1)  
                          END IF  
                          CALL ZGF(RANF,IDT)  
                          IF (1.D00/ZV > RANF) THEN   
                            IE=1  
                            II=0 
                            DO WHILE (II == 0)  
                              CALL ZGF(RANF,IDT)  
                              IV=RANF*(MAXLEV+0.99999999D00)                             
                              IPVIB(KV,K)=IV 
                !Comment by Han: This part of code considers low temperature simple gas without the possibility of high Evib >
                !Edissociation, which means Evib is below Edissociation. 
                              EVIB=IV*BOLTZ*SPVM(3,KV,1)
                              IF (EVIB < ECC) THEN  
                                PROB=(1.D00-EVIB/ECC)**(1.5D00-SPM(3,1,1))  !--isebasti: SPM(3,KS,1) is corrected to SPM(3,1,1)
!--PROB is the probability ratio of eqn (5.61)  
                                CALL ZGF(RANF,IDT)  
                                IF (PROB > RANF) II=1  
                              END IF  
                            END DO  
                            ECT=ECC-EVIB  
                          END IF  
                        END DO  
                      END IF  
                    END IF  
!--now rotation of this molecule  
                    IF (ISPR(1,1) > 0) THEN  
                      IF (ISPR(2,1) == 0) THEN  
                        B=1.D00/SPR(1,1)  
                      ELSE    !--use molecule rather than mean value  
                        COLT=ECC/((2.5D00-SP(3,1))*BOLTZ)  
                        B=1.D00/(SPR(1,1)+SPR(2,1)*COLT+SPR(3,1)*COLT*COLT)  
                      END IF  
                      CALL ZGF(RANF,IDT)  
                      IF (B > RANF) THEN  
                        IE=1  
                        ECC=ECT+PROT(K)  
                        IF (ISPR(1,1) == 2) THEN  
                          CALL ZGF(RANF,IDT)  
                          ERM=1.D00-RANF**(1.D00/(2.5D00-SP(3,1)))  !eqn(5.46)  
                        ELSE  
                          CALL LBS(0.5D00*ISPR(1,1)-1.D00,1.5D00-SP(3,1),ERM,IDT)  
                        END IF  
                        PROT(K)=ERM*ECC  
                        ECT=ECC-PROT(K)  
                      END IF  
                    END IF  
                  END DO  
!--adjust VR for the change in energy  
                  VR=SQRT(2.D00*ECT/SPM(1,1,1))  
                END IF  
!--end of L-B redistribution  
!  
                DO KK=1,3  
                  VCM(KK)=0.5d0*(PV(KK,L)+PV(KK,M))  
                END DO  
!
!--calculate new velocities  
                CALL VHSS(1,1,VR,VRC,VRCP,IDT)
                PV(1:3,L)=VCM(1:3)+0.5d0*VRCP(1:3)  
                PV(1:3,M)=VCM(1:3)-0.5d0*VRCP(1:3)  
                IPCP(L)=M  
                IPCP(M)=L  
              END IF   !--collision occurrence  
            ELSE  
!--Gas Mixture  
              LS=ABS(IPSP(L))  
              MS=ABS(IPSP(M))   
              CVR=VR*SPM(2,LS,MS)*((2.D00*BOLTZ*SPM(5,LS,MS)/(SPM(1,LS,MS)*VRR))**(SPM(3,LS,MS)-0.5D00))*SPM(6,LS,MS)  
              IF (CVR > CCELL(4,N)) CCELL(4,N)=CVR  
              CALL ZGF(RANF,IDT)  
              IF ((CVR/CCELL(4,N)>RANF).AND.(IPCELL(L)>0).AND.(IPCELL(M)>0).AND.(IPVIB(0,L)>=0).AND.(IPVIB(0,M)>=0)) THEN      
!--the collision occurs (IPCELL<0 for recombined molecule marked for removal; IPVIB(0,L)<0 for molecule marked for dissociation)  
                IF ((M == IPCP(L)).AND.(L ==IPCP(M))) TOTDUP=TOTDUP+1.D00     
                TOTCOL=TOTCOL+1.D00
                TCOL(LS,MS)=TCOL(LS,MS)+1.D00  
                TCOL(MS,LS)=TCOL(MS,LS)+1.D00  
                COLLS(NN)=COLLS(NN)+1.D00  
                WCOLLS(NN)=WCOLLS(NN)+WFC
                IF(NN == NSPDF) PCOLLS=PCOLLS+WFC     
                SEP=DABS(PX(L)-PX(M))   
                CLSEP(NN)=CLSEP(NN)+SEP                             
                RML=SPM(1,LS,MS)/SP(5,MS)  
                RMM=SPM(1,LS,MS)/SP(5,LS)             
                DO KK=1,3  
                  VCM(KK)=RML*PV(KK,L)+RMM*PV(KK,M)  
                END DO  
                ISWITCH=0                  !--must be 0 if reactions are to occur  
                IF (ISWITCH == 0) THEN                  
                  !--check for dissociation  
                  IDISS=0
                  !--QK starts--------------------------------------------------------------------------  
                  IF (((ISPR(1,LS) > 0).OR.(ISPR(1,MS) > 0)).AND.(JCD == 1)) THEN  
                    ECT=0.5D00*SPM(1,LS,MS)*VRR  
                    DO NSP=1,2  
                      IF (NSP == 1) THEN  
                        K=L ; KS=LS ; JS=MS   
                      ELSE  
                        K=M ; KS=MS ; JS=LS  
                      END IF                                          
                      IF (MMVM > 0) THEN  
                        IF (ISPV(KS) > 0) THEN  
                          DO KV=1,ISPV(KS)  
                            IF ((IPVIB(KV,K) >= 0).AND.(IDISS == 0)) THEN   !--do not redistribute to a dissociating molecule marked for removal   
                              EVIB=VIBGEN(IPVIB(KV,K),KV,KS)                        
                              ECC=ECT+EVIB  
                              !--note quantizing of COLT in the following statements
                              IF (AHVIB .EQ. 0)THEN
                                MAXLEV=ECC/(BOLTZ*SPVM(1,KV,KS))  
                                LIMLEV=SPVM(4,KV,KS)/SPVM(1,KV,KS)  
                              ELSE IF (IDVIB(KS) .EQ. 0)THEN
                                MAXLEV=ECC/(BOLTZ*SPVM(1,KV,KS))  
                                LIMLEV=SPVM(4,KV,KS)/SPVM(1,KV,KS)  
                              ELSE
                                IF (ECC .LE. EDVIB(MDVIB(KV,KS)))THEN
                                  MAXLEV=IPVIB(KV,K)                   !truncate to vibrational level
                                  EDVIBCON=0
                                  DO WHILE ( ECC .GE. EDVIB(EDVIBCON,KV,KS))
                                    EDVIBCON=EDVIBCON+1
                                  END DO
                                  EDVIBCON=EDVIBCON-1
                                  MAXLEV=EDVIBCON
                                ELSE
                                  MAXLEV=MDVIB(KV,KS)+1
                                  LIMLEV=MAXLEV-1
                                  !these two values are set arbitrarily as long as MAXLEV > LIMLEV
                                END IF
                              END IF
                              IF ((MAXLEV > LIMLEV)) THEN  
                                !--dissociation occurs  -  reflects the infinity of levels past the dissociation limit                         
                                IDISS=1  
                                IF (IPDF > 0) NDISSOC=NDISSOC+1  
                                LZ=IPVIB(KV,K)  
                                NDISSL(LZ)=NDISSL(LZ)+1  
                                ECT=ECT-BOLTZ*SPVM(4,KV,KS)+EVIB  
                                !--adjust VR for the change in energy  
                                VRR=2.D00*ECT/SPM(1,LS,MS)                                 
                                VR=SQRT(VRR)  
                                IPVIB(KV,K)=-1    
                                !--a negative IPVIB marks a molecule for dissociation                                                               
                              END IF  
                            END IF  
                          END DO  
                        END IF  
                      END IF  
                    END DO  
                  END IF 
                  !--QK ends----------------------------------------------------------------------------  
                  !  
                  IEX=0    !--becomes the collision number if a reaction occurs   
                  IREC=0   !--becomes 1 if a recombination occurs
                  !  
                  !--calculate the collision temperature based on on relative translational energy  
                  IF ((ITCV == 1).OR.(IEAA == 1)) TCOLT=0.5D00*SPM(1,LS,MS)*VRR/(BOLTZ*(2.5-SPM(3,LS,MS))) ! bird 2011 
                  !                  
                  !--QK starts-------------------------------------------------------------------------- 
                  IF ((JCD == 1).AND.(IDISS == 0).AND.(IRECOM == 1)) THEN    !--use the Q-K reaction model, and dissociation has not occurred
                    !--consider possible recombinations  !--isebasti: included IRECOM == 1 condition
                    IF ((ISPRC(LS,MS) > 0).AND.(ICCELL(2,N) > 2)) THEN           
                      !--possible recombination using model based on collision volume for equilibrium  
                      !                    CALL ZGF(RANF,IDT)  
                      !                    IF (RANF < 0.2) THEN   !--third body probability will be increased by a factor of 5  
                      !--this is to save computational time, the factor can be much higher at very low densities                       
                      KT=L  
                      DO WHILE ((KT == L).OR.(KT == M))   
                        CALL ZGF(RANF,IDT)  
                        K=INT(RANF*DFLOAT(ICCELL(2,N)))+ICCELL(1,N)+1  
                        KT=ICREF(K)                     
                      END DO  
                      KS=IPSP(KT)   
                      !--the potential third body is KT OF species KS   
                      !                  
                      AA=(PI/6.D00)*(SP(1,LS)+SP(1,MS)+SP(1,KS))**3   !--reference volume  
                      IF (ITCV == 1) THEN  
                        ! BB=AA*SPRC(1,LS,MS,KS)*(TCOLT/SPM(5,LS,MS))**SPRC(2,LS,MS,KS)  !--collision volume 
                        B=2.5D00-SPM(3,LS,MS)
                        B=B**SPRC(2,LS,MS,KS)*GAM(B)/GAM(B+SPRC(2,LS,MS,KS))
                        BB=AA*B*SPRC(1,LS,MS,KS)*(TCOL/SPM(5,LS,MS))**SPRC(2,LS,MS,KS)  !-add a factor in Bird 2011
                      ELSE    
                        BB=AA*SPRC(1,LS,MS,KS)*(VAR(11,NN)/SPM(5,LS,MS))**SPRC(2,LS,MS,KS)  !--collision volume   
                      END IF                         
                      !  
                      B=1.*BB*ICCELL(2,N)*FNUM/AAA     !-- speed-up factor of 1  
                      IF (B > 1.D00) THEN  
                        WRITE (*,*) 'THREE BODY PROBABILITY',B     !note factor of 2 may have to be reduced  
                      END IF    
                      CALL ZGF(RANF,IDT)  
                      IF (RANF < B) THEN                         
                        IREC=1  
                        TRECOMB=TRECOMB+1.D00  
                        IF (IPDF > 0) NRECOMB=NRECOMB+1  
                        !--the collision now becomes a collision between these with L having the center of mass velocity  
                        A=0.5D00*SPM(1,LS,MS)*VRR   !--the relative energy of the recombining molecules  
                        IF (ISPR(1,LS) > 0) A=A+PROT(L)  
                        IF (ISPV(LS) > 0) THEN  
                          DO KVV=1,ISPV(LS)  
                            JI=IPVIB(KVV,L)  
                            IF (JI < 0) JI=-JI  
                            IF (JI == 99999) JI=0  
                            A=A+VIBGEN(JI,KVV,LS)  
                            !A=A+DFLOAT(JI)*BOLTZ*SPVM(1,KVV,LS)  
                          END DO  
                        END IF  
                        IF (ISPR(1,MS) > 0) A=A+PROT(M)  
                        IF (ISPV(MS) > 0) THEN  
                          DO KVV=1,ISPV(MS)  
                            JI=IPVIB(KVV,M)  
                            IF (JI < 0) JI=-JI  
                            IF (JI == 99999) JI=0                            
                            A=A+VIBGEN(JI,KVV,MS)
                            !A=A+DFLOAT(JI)*BOLTZ*SPVM(1,KVV,MS)  
                          END DO  
                        END IF   
                        TREACL(2,LS)=TREACL(2,LS)-1  
                        TREACL(2,MS)=TREACL(2,MS)-1  
                        LSI=LS  
                        LS=ISPRC(LS,MS)  
                        IPSP(L)=LS  
                        !--any additional vibrational modes must be set to zero                        
                        IVM=ISPV(LSI)  
                        NMC=IPSP(L)  
                        NVM=ISPV(NMC)  
                        IF (NVM > IVM) THEN  
                          DO KV=IVM+1,NVM  
                            IPVIB(KV,L)=0  
                          END DO  
                        END IF   
                        !                          
                        IPCELL(M)=-IPCELL(M)  !--recombining molecule M marked for removal  
                        M=KT                  !--third body molecule is set as molecule M  
                        MS=KS   
                        TREACG(2,LS)=TREACG(2,LS)+1                             
                        IF (ISPR(1,LS) > 0) PROT(L)=0.D00  
                        IF (ISPV(LS) > 0) THEN  
                          DO KVV=1,ISPV(LS)  
                            IF (IPVIB(KVV,L) < 0) THEN  
                              IPVIB(KVV,L)=-99999  
                            ELSE    
                              IPVIB(KVV,L)=0  
                            END IF    
                          END DO  
                        END IF  
                        IF (ISPR(1,MS) > 0) PROT(M)=PROT(KT)  
                        IF (ISPV(MS) > 0) THEN  
                          DO KVV=1,ISPV(MS)                                                  
                            IPVIB(KVV,M)=IPVIB(KVV,KT)  
                          END DO  
                        END IF  
                        ECTOT=A+SPVM(4,1,LS)*BOLTZ    !--the energy added to this collision   
                        DO KK=1,3  
                          PV(KK,L)=VCM(KK)  
                        END DO     
                        DO KK=1,3  
                          VRC(KK)=PV(KK,L)-PV(KK,M)  
                        END DO  
                        VRR=VRC(1)*VRC(1)+VRC(2)*VRC(2)+VRC(3)*VRC(3)  
                        ECT=0.5D00*SPM(1,LS,MS)*VRR+ECTOT  
                        VRR=2.D00*ECT/SPM(1,LS,MS)  
                        VR=SQRT(VRR)                      
                        RML=SPM(1,LS,MS)/SP(5,MS)  
                        RMM=SPM(1,LS,MS)/SP(5,LS)             
                        DO KK=1,3  
                          VCM(KK)=RML*PV(KK,L)+RMM*PV(KK,M)  
                        END DO  
                      END IF  
                      !                    END IF                              
                    END IF                            
                    !   
                    !--consider exchange and chain reactions   
                    !--this part is not changed for anharmolic vibrational model 
                    IF ((NSPEX(LS,MS) > 0).AND.(IREC == 0).AND.(IDISS == 0)) THEN  !--possible exchange reaction  
                      PSF=0.D00    !PSF(MMEX) PSF is the probability that this reaction will occur in this collision  
                      !                 
                      DO JJ=1,NSPEX(LS,MS)  
                        IF (LS == ISPEX(JJ,0,LS,MS)) THEN  
                          K=L  ;   KS=LS   ;   JS=MS  
                        ELSE  
                          K=M  ;   KS=MS   ;   JS=LS  
                        END IF                           
                        !--the pre-collision molecule that splits is K of species KS  
                        KV=ISPEX(JJ,3,LS,MS)   
                        JI=IPVIB(KV,K)  
                        IF (JI < 0) JI=-JI  
                        IF (JI == 99999) JI=0  
                        ECC=0.5D00*SPM(1,LS,MS)*VRR+DFLOAT(JI)*BOLTZ*SPVM(1,KV,KS)     
                        MAXLEV=ECC/(BOLTZ*SPVM(1,KV,KS))  
                        IF (IEAA == 1) THEN  
                          EA=SPEX(1,JJ,KS,JS)*((TCOLT/273.D00)**SPEX(2,JJ,KS,JS))*DABS(SPEX(3,JJ,KS,JS))  
                        ELSE  
                          EA=SPEX(1,JJ,KS,JS)*((VAR(11,NN)/273.D00)**SPEX(2,JJ,KS,JS))*DABS(SPEX(3,JJ,KS,JS))  
                        END IF    
                        IF (SPEX(3,JJ,KS,JS) < 0.D00) EA=EA-SPEX(3,JJ,KS,JS)  
                        IF (ECC > EA) THEN  
                          DEN=0.D00  
                          DO IAX=0,MAXLEV  
                            DEN=DEN+(1.D00-DFLOAT(IAX)*BOLTZ*SPVM(1,KV,KS)/ECC)**(1.5D00-SPM(3,KS,JS))  
                          END DO     
                          PSF(JJ)=ISPEX(JJ,4,LS,MS)*((1.D00-EA/ECC)**(1.5D00-SPM(3,KS,JS)))/DEN  !--probability of reaction  
                        END IF  
                      END DO  
                      !                    
                      IF (NSPEX(LS,MS) > 1) THEN   
                        BB=0.D00  
                        DO JJ=1,NSPEX(LS,MS)  
                          BB=BB+PSF(JJ)  
                        END DO  
                        !--BB is the sum of the probabilities   
                        CALL ZGF(RANF,IDT)  
                        IEX=0  
                        JJ=0  
                        A=0.D00  
                        DO WHILE ((IEX == 0).AND.(JJ < NSPEX(LS,MS)))  
                          JJ=JJ+1  
                          A=A+PSF(JJ)/B  
                          IF (A > RANF) IEX=JJ     
                        END DO  
                      ELSE  
                        CALL ZGF(RANF,IDT)  
                        IEX=0  
                        IF (PSF(1) > RANF) IEX=1  
                      END IF  
                      !  
                      IF (IEX > 0) THEN      
                        !-- exchange or chain reaction occurs  
                        JX=NEX(IEX,LS,MS)  
                        TNEX(JX)=TNEX(JX)+1.D00                     
                        IF (SPEX(3,IEX,LS,MS) < 0.D00) THEN  
                          TFOREX=TFOREX+1.D00  
                        ELSE  
                          TREVEX=TREVEX+1.D00  
                        END IF  
                        !                      WRITE (*,*) 'EXCHANGE',TFOREX,TREVEX  
                        !                      WRITE (*,*)  IEX,L,M,LS,MS                                      
                        IPSP(L)=ISPEX(IEX,1,LS,MS)   !--L is now the new molecule that splits  
                        IPSP(M)=ISPEX(IEX,2,LS,MS)  
                        !--any additional vibrational modes must be set to zero                        
                        IVM=ISPV(LS)  
                        NMC=IPSP(L)  
                        NVM=ISPV(NMC)  
                        IF (NVM > IVM) THEN  
                          DO KV=IVM+1,NVM  
                            IPVIB(KV,L)=0  
                          END DO  
                        END IF   
                        IVM=ISPV(MS)  
                        NMC=IPSP(M)  
                        NVM=ISPV(NMC)                          
                        IF (NVM > IVM) THEN  
                          DO KV=IVM+1,NVM  
                            IPVIB(KV,M)=0  
                          END DO  
                        END IF                          
                        !--put all pre-collision energies into the relative translational energy and adjust for the reaction energy                         
                        ECT=0.5D00*SPM(1,LS,MS)*VRR  
                        IF (ISPR(1,LS) > 0) ECT=ECT+PROT(L)  
                        IF (ISPV(LS) > 0) THEN  
                          DO KV=1,ISPV(LS)  
                            JI=IPVIB(KV,L)  
                            IF (JI < 0) JI=-JI  
                            IF (JI == 99999) JI=0                         
                            ECT=ECT+DFLOAT(JI)*BOLTZ*SPVM(1,KV,LS)  
                          END DO  
                        END IF  
                        IF (ISPR(1,MS) > 0) ECT=ECT+PROT(M)  
                        IF (ISPV(MS) > 0) THEN  
                          DO KV=1,ISPV(MS)  
                            JI=IPVIB(KV,M)  
                            IF (JI < 0) JI=-JI  
                            IF (JI == 99999) JI=0  
                            ECT=ECT+DFLOAT(JI)*BOLTZ*SPVM(1,KV,MS)  
                          END DO  
                        END IF  
                        ECT=ECT+SPEX(3,IEX,LS,MS)  
                        IF (ECT < 0.) THEN  
                          WRITE (*,*) '-VE ECT',ECT  
                          WRITE (*,*) 'REACTION',JJ-1,' BETWEEN',LS,MS  
                          STOP  
                        END IF  
                        IF (SPEX(3,IEX,LS,MS) < 0.D00) THEN    
                          TREACL(3,LS)=TREACL(3,LS)-1  
                          TREACL(3,MS)=TREACL(3,MS)-1                       
                          LS=IPSP(L)  
                          MS=IPSP(M)  
                          TREACG(3,LS)=TREACG(3,LS)+1  
                          TREACG(3,MS)=TREACG(3,MS)+1  
                        ELSE  
                          TREACL(4,LS)=TREACL(4,LS)-1  
                          TREACL(4,MS)=TREACL(4,MS)-1                       
                          LS=IPSP(L)  
                          MS=IPSP(M)  
                          TREACG(4,LS)=TREACG(4,LS)+1  
                          TREACG(4,MS)=TREACG(4,MS)+1  
                        END IF    
                        RML=SPM(1,LS,MS)/SP(5,MS)  
                        RMM=SPM(1,LS,MS)/SP(5,LS)     
                        !--calculate the new VRR to match ECT using the new molecular masses  
                        VRR=2.D00*ECT/SPM(1,LS,MS)  
                        IF (ISPV(LS) > 0) THEN  
                          DO KV=1,ISPV(LS)  
                            IF (IPVIB(KV,L) < 0) THEN  
                              IPVIB(KV,L)=-99999  
                            ELSE    
                              IPVIB(KV,L)=0  
                            END IF    
                          END DO  
                        END IF  
                        IF (ISPR(1,LS) > 0) PROT(L)=0.  
                        IF (ISPV(MS) > 0) THEN  
                          DO KV=1,ISPV(MS)  
                            IF (IPVIB(KV,M) < 0) THEN  
                              IPVIB(KV,M)=-99999  
                            ELSE                          
                              IPVIB(KV,M)=0  
                            END IF    
                          END DO  
                        END IF  
                        IF (ISPR(1,MS) > 0) PROT(M)=0.        
                      END IF  
                    END IF   
                  END IF   !--end of integrated reactions other than the deferred dissociation action in the DISSOCIATION subroutine
                  !--QK ends----------------------------------------------------------------------------   
                END IF                  
                !         
                !--consider any reactions that are specified by rate equations                  
                IKA=0      
                IF (MNRE > 0) THEN
                  CALL CHECK_REACTION(IKA,N,L,M,LS,MS,VRR,VR,VCM,RML,RMM,IDT)  
                END IF
                !
                IF (IKA /= 0) THEN
                  IF (IREAC == 0) THEN
                    REAC(IKA)=REAC(IKA)+1.D00 !count reactions in entire domain
                  ELSE
                    IF (NN == NSPDF) REAC(IKA)=REAC(IKA)+1.D00 !count reactions only in NSPDF cell
                  END IF
                  IF (IREAC == 2) IKA=0
                END IF
                !
                A=VAR(10,NN)  !sampling cell vibrational temperature
                SVDOF(:)=0.d0 
                IF (ISPV(LS) > 0) THEN
                  DO KV=1,ISPV(LS)
                    SVDOF(1)=SVDOF(1)+2.d0*(SPVM(1,KV,LS)/A)/(DEXP(SPVM(1,KV,LS)/A)-1.d0)
                  END DO
                END IF
                IF (ISPV(MS) > 0) THEN
                  DO KV=1,ISPV(MS)
                    SVDOF(2)=SVDOF(2)+2.d0*(SPVM(1,KV,MS)/A)/(DEXP(SPVM(1,KV,MS)/A)-1.d0)
                  END DO
                END IF  
!
                IF ((IREC == 0).AND.(IDISS == 0)) THEN   !--recombined redistribution already made in QK procedures 
!
                  ECT=0.5D00*SPM(1,LS,MS)*VRR 
                  TCOLT=ECT/(BOLTZ*(2.5d0-SPM(3,LS,MS))) 
!
                  IF ((IKA /= 0).AND.(IPRS == 1)) THEN 
!-------------------------------------------------
                  ELSE IF ((IKA /= 0).AND.(IPRS == 2)) THEN
!-------------------------------------------------
                  ELSE IF ((IKA /= 0).AND.(IPRS == 3)) THEN
!-------------------------------------------------
                  ELSE
!--Larsen-Borgnakke serial vibrational energy redistribution  
                    DO NSP=1,2  
                      IF (NSP == 1) THEN  
                        K=L ; KS=LS ; JS=MS
                      ELSE  
                        K=M ; KS=MS ; JS=LS  
                      END IF
!
                      IF (MMVM > 0) THEN  
                        IF (ISPV(KS) > 0) THEN
                          DO KV=1,ISPV(KS)  
                            IF ((IPVIB(KV,K) >= 0).AND.(IDISS == 0)) THEN   
!--do not redistribute to a dissociating molecule marked for removal or for a recombined molecule
                              EVIB=DFLOAT(IPVIB(KV,K))*BOLTZ*SPVM(1,KV,KS)                        
                              ECC=ECT+EVIB  
                              MAXLEV=ECC/(BOLTZ*SPVM(1,KV,KS))
                              IF (SPVM(3,KV,KS) > 0.) THEN  
                                IF (IZV == 1) THEN  
                                  COLT=(DFLOAT(MAXLEV)*SPVM(1,KV,KS))/(3.5D00-SPM(3,KS,JS))  !--quantized collision temperature  
                                ELSE  
                                  COLT=VAR(8,NN)   !--isebasti: Ttrans, according to DSMC.f90 (2013)    
                                END IF    
                                B=SPVM(4,KV,KS)/SPVM(3,KV,KS)    !Tdiss/Tref            
                                A=SPVM(4,KV,KS)/COLT             !Tdiss/T  
       ZV=(A**SPM(3,KS,JS))*(SPVM(2,KV,KS)*(B**(-SPM(3,KS,JS))))**(((A**0.3333333D00)-1.D00)/((B**0.33333D00)-1.D00))
                              ELSE
                                IF (SPVM(3,1,KS) == -1.) THEN
                                  ZV=SPVM(2,KV,KS)
                                ELSE IF (SPVM(3,1,KS) == -2.) THEN    
                                  IF (KS /= JS) THEN
                                    ZV=SPVM(2,1,KS)
                                  ELSE
                                    ZV=SPVM(2,2,KS)  
                                  END IF
                                END IF
                              END IF  
                              CALL ZGF(RANF,IDT)
                              IF ((1.D00/ZV > RANF).OR.(IKA /= 0).OR.(IREC == 1).OR.(IEX > 0)) THEN 
!-- a vibrational redistribution is always made after a chemical reaction     
                                IE=1            
                                II=0  
                                nstep=0  
                                 DO WHILE ((II == 0).and.(nstep < 100000))  
                                  nstep=nstep+1  
                                  if (nstep > 99000) then  
                                    write (*,*) nstep,ecc,maxlev  
                                    stop  
                                  end if    
                                  CALL ZGF(RANF,IDT)  
                                  IV=RANF*(MAXLEV+0.99999999D00)  
                                  IPVIB(KV,K)=IV  
                                  EVIB=DFLOAT(IV)*BOLTZ*SPVM(1,KV,KS)  
                                  IF (EVIB < ECC) THEN
                                    A=5.d0-2.d0*SPM(3,KS,JS)  !nonreacting collisions  
                                    IF (IKA /= 0) THEN        !reacting collisions
                                      !SVDOF(NSP)=SVDOF(NSP)-2.d0*(SPVM(1,KV,KS)/VAR(10,NN))/(DEXP(SPVM(1,KV,KS)/VAR(10,NN))-1.d0)
                                      IF (NSP == 1) A=(5.d0-2.d0*SPM(3,KS,JS))+ISPR(1,KS)+ISPR(1,JS)+SVDOF(2)+SVDOF(1)
                                      IF (NSP == 2) A=(5.d0-2.d0*SPM(3,KS,JS))+ISPR(1,KS)+ISPR(1,JS)+SVDOF(2)  
                                    END IF
                                    PROB=(1.D00-EVIB/ECC)**(A*0.5d0-1.d0)  
 !--PROB is the probability ratio of eqn (5.61) and (5.45)  
                                    CALL ZGF(RANF,IDT)  
                                  IF (PROB > RANF) II=1  
                                  END IF  
                                END DO  
                                ECT=ECC-EVIB  
                              END IF  
                            END IF  
                          END DO  
                        END IF
                      END IF
                    END DO 
                  END IF
!-------------------------------------------------
!                  IF (IKA /= 0) THEN  
!--sample post-collisional vibrational levels
!                    DO NSP=1,2  
!                      IF (NSP == 1) THEN  
!                        K=L ; KS=LS
!                      ELSE  
!                        K=M ; KS=MS  
!                      END IF
!                      IF (ISPV(KS) > 0) THEN 
!                        DO KV=1,ISPV(KS)
!                          NPVIB(2,NN,IKA,KS,KV,IPVIB(KV,K))=NPVIB(2,NN,IKA,KS,KV,IPVIB(KV,K))+1
!                        END DO
!                        TNPVIB(2,NN,IKA,KS)=TNPVIB(2,NN,IKA,KS)+1.d0
!                      END IF
!                    END DO
!                  END IF
!-------------------------------------------------
!--Larsen-Borgnakke serial rotational energy redistribution  
DO NSP=1,2  
  IF (NSP == 1) THEN  
    K=L ; KS=LS ; JS=MS   
  ELSE  
    K=M ; KS=MS ; JS=LS  
  END IF   
  !--now rotation of this molecule  
  IF (ISPR(1,KS) > 0) THEN  
    IF ((ISPR(2,KS) == 0).AND.(ISPR(2,JS) == 0)) THEN  
      B=1.D00/SPM(7,KS,JS)  
    ELSE  
      COLT=ECC/((2.5D00-SP(3,KS))*BOLTZ)  
      B=1.D00/(SPR(1,KS)+SPR(2,KS)*COLT+SPR(3,KS)*COLT*COLT)  
    END IF  
    CALL ZGF(RANF,IDT)  
    IF ((B > RANF).OR.(IKA /= 0).OR.(IREC == 1).OR.(IEX > 0)) THEN  
      IE=1  
      ECC=ECT+PROT(K)
      IF (IKA == 0) THEN  !nonreacting collisions
        IF (ISPR(1,KS) == 2) THEN  
          CALL ZGF(RANF,IDT)  
          ERM=1.D00-RANF**(1.D00/(2.5D00-SPM(3,KS,JS)))  !eqn(5.46)  
        ELSE  
          CALL LBS(ISPR(1,KS)*0.5d0-1.d0,1.5d0-SPM(3,KS,JS),ERM,IDT)  
        END IF
      ELSE    !reacting collisions 
        IF (NSP == 1) A=(5.d0-2.d0*SPM(3,KS,JS))+ISPR(1,JS)
        IF (NSP == 2) A=(5.d0-2.d0*SPM(3,KS,JS))
        CALL LBS(ISPR(1,KS)*0.5d0-1.d0,A*0.5d0-1.d0,ERM,IDT)
      END IF  
      PROT(K)=ERM*ECC  
      ECT=ECC-PROT(K)  
    END IF  
  END IF  
END DO
!-------------------------------------------------
VR=DSQRT(2.D00*ECT/SPM(1,LS,MS)) !--adjust VR after energy redistribution
                END IF
                !--end of internal energy redistribution    
                !-------------------------------------------------  
                !--calculate new velocities
                VRC(1:3)=PV(1:3,L)-PV(1:3,M)  
                CALL VHSS(LS,MS,VR,VRC,VRCP,IDT)
                PV(1:3,L)=VCM(1:3)+RMM*VRCP(1:3)  
                PV(1:3,M)=VCM(1:3)-RML*VRCP(1:3)  
                IPCP(L)=M  
                IPCP(M)=L  
              END IF   !--collision occurrence  
            END IF  
          END IF       !--separate simple gas / mixture coding   
        END DO    
      END IF      
    END IF        
  END IF
END DO  
!$omp end do
!$omp end parallel
!
!--remove any recombined atoms  
N=0  
DO WHILE (N < NM)  
  N=N+1
  IF (IPCELL(N) < 0) THEN  
    CALL REMOVE_MOL(N)
    N=N-1 
  END IF
END DO 
!  
!--dissociate marked molecules 
IF (MNRE > 0) CALL DISSOCIATION
!
RETURN  
END SUBROUTINE COLLISIONS  
!  
!*****************************************************************************  
!
SUBROUTINE VHSS(LS,MS,VR,VRC,VRCP,IDT)
!
!--calculate new scattering angles based on VHS/VSS model
!  
USE GAS
USE CALC    
USE OMP_LIB
!  
IMPLICIT NONE  
! 
INTEGER :: LS,MS,IDT 
REAL(KIND=8) :: A,B,C,D,OC,SD,VR,VRC(3),VRCP(3),RANF  
!
IF (ABS(SPM(8,LS,MS)-1.) < 1.d-3) THEN  
!--use the VHS logic  
  CALL ZGF(RANF,IDT)  
  B=2.D00*RANF-1.D00  
!--B is the cosine of a random elevation angle               
  A=DSQRT(1.D00-B*B)  
  VRCP(1)=B*VR  
  CALL ZGF(RANF,IDT)  
  C=2.D00*PI*RANF  
!--C is a random azimuth angle  
  VRCP(2)=A*DCOS(C)*VR  
  VRCP(3)=A*DSIN(C)*VR  
ELSE  
!--use the VSS logic  
  CALL ZGF(RANF,IDT)  
  B=2.D00*(RANF**SPM(8,LS,MS))-1.D00  !isebasti: SP(4,1) was used instead of SPM(8,LS,MS)
!--B is the cosine of the deflection angle for the VSS model (eqn (11.8)  
  A=DSQRT(1.D00-B*B)  
  CALL ZGF(RANF,IDT)  
  C=2.D00*PI*RANF  
  OC=DCOS(C)  
  SD=DSIN(C)  
  D=SQRT(VRC(2)**2+VRC(3)**2)  
  VRCP(1)=B*VRC(1)+A*SD*D  
  VRCP(2)=B*VRC(2)+A*(VR*VRC(3)*OC-VRC(1)*VRC(2)*SD)/D  
  VRCP(3)=B*VRC(3)-A*(VR*VRC(2)*OC+VRC(1)*VRC(3)*SD)/D  
!--the post-collision rel. velocity components are based on eqn (2.22)  
END IF
RETURN
END SUBROUTINE VHSS
!
!*****************************************************************************  
!  
SUBROUTINE THERMOPHORETIC  
!  
!--author: isebasti  
!--calculate thermophoretic force based on Gimelshein et al, AIAA 2005-766;  
!--assume one montionless particle of constant temperature per sampling cell;  
!--weighting factors are not considered in the current version;  
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT
USE OMP_LIB
!  
IMPLICIT NONE  
!  
INTEGER :: L,K
INTEGER(KIND=8) :: N,NC,NS  
REAL(KIND=8) :: ALPHAM,ALPHAT,ALPHAR,ALPHAV,A,B,D,E,F,G,EVIB,FVIB,DEGENV,TH  
REAL(KIND=8),DIMENSION(3) :: VR,TF  
REAL(KIND=8),DIMENSION(NCELLS) :: PTEMP  
REAL(KIND=8),DIMENSION(3,NCELLS) :: PPV  
!   
!--N molecule number  
!--NC,NS,L,K working integer   
!--A,B,E,F,FVIB auxiliar variable  
!--ALPHAM accommodation coefficient for momentum  
!--ALPHAT accommodation coefficient for translational energy  
!--ALPHAR accommodation coefficient for rotational energy  
!--ALPHAV accommodation coefficient for vibrational energy  
!--PPV(1:3,N) velocity components of the macroscopic particle (up)  
!--PTEMP(:) temperature of the macroscopic particle (Tp)  
!--VR(1:3) components of relative velocity btw particle and molecule  
!--G magnitude of the relative velocity  
!--EVIB molecule vibrational energy  
!--DEGENV degeneracy of a vibrational mode  
!--TF(1:3,N) components of thermophoretic force per unit cross section area (pi*Rp^2)  
!--TH(N) thermophoretic heat transfer per unit cross section area (pi*Rp^2)  
!--CST(0:4,:) sum of thermoforetic properties in a sampling cell  
!             0 number sum, 1:3 thermophoretic force compoments, 4 heat transfer  
!  
ALPHAM=1.d0  
ALPHAT=1.d0  
ALPHAR=1.d0  
ALPHAV=1.d0  
DEGENV=1.d0 !assuming all vibrational modes are non-degenerated  
!  
PPV(1:3,:)=0.d0  
PTEMP(:)=273.d0  
!  
!------------------------------------------------------------------------------  
!--loop over molecules (maybe it would be better a loop only over the macroscopic particles!)
!$omp parallel &
!$omp private(n,nc,ns,l,vr,g,a,b,d,e,tf,evib,fvib,th) &
!$omp reduction(+:cst)
!$omp do    
DO N=1,NM  
  NC=IPCELL(N)        !collision cell  
  NS=ICCELL(3,NC)     !sampling cell  
  L=IPSP(N)           !species of molecule n  
!  
  VR(1:3)=PV(1:3,N)-PPV(1:3,NS)  
  G=SQRT(VR(1)*VR(1)+VR(2)*VR(2)+VR(3)*VR(3))  
!  
  A=SP(5,L)*FNUM/CELL(4,NS)  
  B=1.d0+4.d0*ALPHAM*(1.d0-ALPHAT)/9.d0  
  D=ALPHAM*ALPHAT*SPI*SQRT(2.d0*BOLTZ*PTEMP(NS)/SP(5,L))/3.d0  
  TF(1:3)=A*VR(1:3)*(B*G+D) !thermophoretic force  
!  
  A=ALPHAM*FNUM*G/CELL(4,NS)  
  B=ALPHAT*(0.5d0*SP(5,L)*G*G-2.d0*BOLTZ*PTEMP(NS))  
!  
  D=0.d0  
  IF ((MMRM>0).AND.(ISPR(1,L)>0)) D=ALPHAR*(PROT(N)-0.5d0*ISPR(1,L)*BOLTZ*PTEMP(NS)) !to be validated  
!  
  E=0.d0  
  IF ((MMVM>0).AND.(ISPV(L)>0)) THEN !to be validated  
    EVIB=0.d0
    FVIB=0.d0  
    DO K=1,ISPV(L)  
      EVIB=EVIB+DFLOAT(IPVIB(K,N))*BOLTZ*SPVM(1,K,L)  
      FVIB=FVIB+DEGENV*BOLTZ*SPVM(1,K,L)/(DEXP(SPVM(1,K,L)/PTEMP(NS))-1.d0)  
    END DO  
    E=ALPHAV*(EVIB-FVIB)  
  END IF  
  TH=A*(B+D+E)  
!
!$omp critical  
  CST(1:3,NS)=CST(1:3,NS)+TF(1:3)  
  CST(4,NS)=CST(4,NS)+TH  
!$omp end critical
END DO
!$omp end do
!$omp end parallel
!------------------------------------------------------------------------------  
 CST(0,:)=CST(0,:)+1.d0
!Now I need to create new thermophoretic variables, e.g., VARTF so that I can reset CST samples
!  
!--loop over collision cells (this an alternative approach) 
!DO 500 NC=1,NCCELLS  
!NM=ICCELL(2,NC)      !#of molecules in collision cell nc  
!  
!DO 400 J=1,NM  
!K=J+ICCELL(1,NC)+1   !adress of molecule j in ICREF  !MUST BE DONE AFTER INDEX TO GET UPDATED ICREF
!N=ICREF(K)           !molecule number  
!L=IPSP(N)            !species of l  
!  
!DO EVERYTHING...  
!  
!400 CONTINUE  
!500 CONTINUE  
!  
RETURN  
END SUBROUTINE THERMOPHORETIC   
!  
!*****************************************************************************
!
SUBROUTINE SET_ZIGGURAT
!
!--author: isebasti
!--calculations required by Ziggurate Method
!--call this subroutine with a different jsrl value for a different set of seeds
!
USE CALC
USE OMP_LIB
!
IMPLICIT NONE
!
INTEGER :: nth,idtl,ishr3  !local variables
!
nth=1                      !for serial compilation
!$omp parallel
!$omp master
!$    nth=omp_get_num_threads( )
!$    write(*,*) 'Available threads     ', omp_get_max_threads( )
!$    write(*,*) 'Threads in use        ', nth
!$omp end master
!$omp end parallel
!
allocate(iseed(0:nth-1))
!
do idtl=0,nth-1
call ishr(ishr3)
iseed(idtl)=ishr3         !set a seed for each thread
!$    write(*,*) 'Thread',idtl,'Seed',iseed(idtl)
end do
!
RETURN
END SUBROUTINE SET_ZIGGURAT
!
!*****************************************************************************
!
SUBROUTINE ISHR(ISHR3L)
!
!--author: isebasti
!--calculations required by Ziggurate Method; initialize different seeds
!
USE CALC
!
IMPLICIT NONE
!
INTEGER :: jsrl,jsr_inputl,ishr3l  !local variables
!
jsrl=ijsr
jsr_inputl = jsrl
jsrl = ieor(jsrl,ishft(jsrl,13))
jsrl = ieor(jsrl,ishft(jsrl,-17))
jsrl = ieor(jsrl,ishft(jsrl,  5))
ishr3l = jsr_inputl+jsrl
ijsr=jsrl
!
RETURN
END SUBROUTINE ISHR
!
!*****************************************************************************
!
SUBROUTINE ZGF(RANFL,IDTL)
!
!--author: isebasti
!--generate a random fraction ]0,1[ using Ziggurate Method (Marsaglia & Tsang)
!--this openmp implemation is based on code available at
!--people.sc.fsu.edu/~jburkardt/cpp_src/ziggurat_openmp/ziggurat_openmp.html
!
USE CALC
!
IMPLICIT NONE
!
REAL(KIND=8) :: ranfl
INTEGER :: jsrl,idtl,jsr_inputl   !local variables
!
jsrl=iseed(idtl)
jsr_inputl = jsrl
jsrl = ieor(jsrl,ishft(jsrl, 13))
jsrl = ieor(jsrl,ishft(jsrl,-17))
jsrl = ieor(jsrl,ishft(jsrl,  5))
ranfl = 0.5e0+0.2328306e-9*real(jsr_inputl+jsrl)
iseed(idtl) = jsrl
!
RETURN
END SUBROUTINE ZGF
!
!*****************************************************************************
!
SUBROUTINE SAMPLE_PDF
!
!--author: isebasti  
!--sample pdfs for an single sampling cell
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT
USE OMP_LIB
!  
IMPLICIT NONE
!
REAL(KIND=8) :: VMPL,SC(4)
INTEGER(KIND=8) :: NS,NCI,NCF,NC,NMC,J,K,N,L,M,I,JB,KV
!
!--NS cell to be sampled  
!--NBINS number of bins
!--BINS(0,I,L) sampled sum for property I and species L   
!--BIN(0,I) total sampled sum for property I
!
!--set auxiliar variables
!
DBINV(1)=-3.d0 ; DBINV(2)=3.d0  !normalized velocity bin lower/upper boundaries
DBINC(1)= 0.d0 ; DBINC(2)=3.d0  !normalized speed
!	  	       
DBINV(3)=(DBINV(2)-DBINV(1))/DFLOAT(NBINS) !bin intervals
DBINC(3)=(DBINC(2)-DBINC(1))/DFLOAT(NBINS) 
!
!--sum to corresponding bin
!
NSPDF=NCELLS/2.+0.9999999d0         !user defined; must be consistent with grid
NS=NSPDF                            !sampling cell
  NCI=ICELL(NS)+1                   !initial collision cell
  IF(NS < NCELLS)  NCF=ICELL(NS+1)  !final collision cell
  IF(NS == NCELLS) NCF=NCCELLS
  IF(NCELLS == 1) NCI=1             !debuging, it may be improved
!
  DO NC=NCI,NCF          !do over collision cells
    NMC=ICCELL(2,NC)     !#of molecules in collision cell NC
!
    DO J=1,NMC           !do over molecules in collision cell NC
      K=J+ICCELL(1,NC)   !adress of molecule j in ICREF  
      N=ICREF(K)         !molecule number  
      L=IPSP(N)          !species of molecule n
!
      VMPL=SQRT(2.D00*BOLTZ*VARSP(5,NS,L)/SP(5,L)) !most probable velocity species L
!
      DO I=1,3
        SC(I)=(PV(I,N)-VAR(I+4,NS))/VMPL                      !normalized thermal velocity
        JB=1+(SC(I)-DBINV(1))/DBINV(3)                        !bin index
        IF (JB>0.AND.JB<NBINS) BINS(JB,I,L)=BINS(JB,I,L)+1.d0 !bin accumulation per species
      END DO
!
      SC(4)=DSQRT(SC(1)*SC(1)+SC(2)*SC(2)+SC(3)*SC(3))        !normalized thermal speed
      JB=1+(SC(4)-DBINC(1))/DBINC(3)                          !bin index
      IF (JB>0.AND.JB<NBINS) BINS(JB,4,L)=BINS(JB,4,L)+1.d0   !bin accumulation per species
!
      IF (ISPR(1,L) > 0) THEN                        !rotational energy
        M=1+(PROT(N)/(BOLTZ*VARSP(6,NS,L)))/0.1D00   !bin index
        IF (M < 101) NDROT(L,M)=NDROT(L,M)+1         !bin accumulation per species
      END IF
!
      IF (ISPV(L) > 0) THEN 
        DO KV=1,ISPV(L)                                     !vibrational mode
          NDVIB(KV,L,IPVIB(KV,N))=1+NDVIB(KV,L,IPVIB(KV,N)) !bin accumulation per species 
        END DO
      END IF
!
      BIN(0,:)=BIN(0,:)+1.d0          !total counter
      BINS(0,:,L)=BINS(0,:,L)+1.d0    !species counter
    END DO
  END DO
!
RETURN
END SUBROUTINE SAMPLE_PDF
!
!*****************************************************************************
!
SUBROUTINE UPDATE_MP
!
!--author: isebasti  
!--update macroscopic properties
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT
!  
IMPLICIT NONE
!
INTEGER :: NS,K,KK,L,N,NMCR,NBC,KCELLS(2,10)     !need to set NBC and KCELLS in some commom block
REAL(KIND=8) :: A,B,C,SMCR,DOF,AVW,UU,SVDF,VDOFM,TVIBM,EVIBM,SUM(0:12),AA,BB,SN,UBMEAN(2)  
REAL(KIND=8), DIMENSION(MSP) :: TVIB,VDOF,SDOF  
REAL(KIND=8), DIMENSION(MMVM,MSP) :: TV,THCOL  
REAL(KIND=8), DIMENSION(NCELLS,MMVM,MSP) :: DF  
!
!--set cells close to boundaries and some inner cells
!
!IF (NBC < NCELLS) NBC=NCELLS
!DO N=1,NBC
!  KCELLS(1,N)=N
!  KCELLS(2,N)=NCELLS-(N-1)
!END DO
!
!----------------------------------------------------------------------------
!
VAR=0.D00   
VARSP=0.  
SMCR=0  
NMCR=0  
VDOFM=0.
!
!--can use the lines below to consider only cells close to boundaries
!NBC=1  !consider a maximum of NBC sampling cells from each boundary  
!DO NS=1,2  
! DO KK=1,NBC  
!  N=KCELLS(NS,KK)
!
!--consider all NCELLS sampling cells  
DO N=1,NCELLS  !loop is the same as in OUTPUT subroutine
!  
  A=FNUM/(CELL(4,N)*NSAMP)  
  IF (IVB == 1) A=A*((XB(2)-XB(1))/(XB(2)+VELOB*0.5D00*(FTIME+TISAMP)-XB(1)))**(IFX+1)  
!--check the above for non-zero XB(1)   
  SUM=0.  
  NMCR=NMCR+1  
  DO L=1,MSP  
    SUM(0)=SUM(0)+CS(0,N,L)  
    SUM(1)=SUM(1)+CS(1,N,L)  
    SUM(2)=SUM(2)+SP(5,L)*CS(1,N,L)  
    DO K=1,3  
      SUM(K+2)=SUM(K+2)+SP(5,L)*CS(K+1,N,L)  
      IF (CS(1,N,L) > 0.1D00) THEN  
        VARSP(K+1,N,L)=CS(K+4,N,L)/CS(1,N,L)  !--VARSP(2,3,4 are temporarily the mean of the squares of the velocities  
        VARSP(K+8,N,L)=CS(K+1,N,L)/CS(1,N,L)  !--VARSP(9,10,11 are temporarily the mean of the velocities  
      END IF  
    END DO  
    SUM(6)=SUM(6)+SP(5,L)*(CS(5,N,L)+CS(6,N,L)+CS(7,N,L))  
    SUM(10)=SUM(10)+SP(5,L)*CS(5,N,L)  
    SUM(11)=SUM(11)+SP(5,L)*CS(6,N,L)  
    SUM(12)=SUM(12)+SP(5,L)*CS(7,N,L)  
    IF (CS(1,N,L) > 0.5D00) THEN  
      SUM(7)=SUM(7)+CS(5,N,L)+CS(6,N,L)+CS(7,N,L)  
    END IF  
    IF (ISPR(1,L) > 0) THEN  
      SUM(8)=SUM(8)+CS(8,N,L)  
      SUM(9)=SUM(9)+CS(1,N,L)*ISPR(1,L)  
    END IF  
  END DO  
  AVW=0.  
  DO L=1,MSP  
    VARSP(0,N,L)=CS(1,N,L)  
    VARSP(1,N,L)=0.D00  
    VARSP(6,N,L)=0.  
    VARSP(7,N,L)=0.  
    VARSP(8,N,L)=0.  
    IF (SUM(1) > 0.1) THEN  
      VARSP(1,N,L)=CS(1,N,L)/SUM(1)  !isebasti: deleted 100* factor
      AVW=AVW+SP(3,L)*CS(1,N,L)/SUM(1)  
      IF ((ISPR(1,L) > 0).AND.(CS(1,N,L) > 0.5)) VARSP(6,N,L)=(2.D00/BOLTZ)*CS(8,N,L)/(DFLOAT(ISPR(1,L))*CS(1,N,L))  
    END IF  
    VARSP(5,N,L)=0.  
    DO K=1,3  
      VARSP(K+1,N,L)=(SP(5,L)/BOLTZ)*(VARSP(K+1,N,L)-VARSP(K+8,N,L)**2)  
      VARSP(5,N,L)=VARSP(5,N,L)+VARSP(K+1,N,L)  
    END DO  
    VARSP(5,N,L)=VARSP(5,N,L)/3.D00
    VARSP(8,N,L)=(3.D00*VARSP(5,N,L)+DFLOAT(ISPR(1,L))*VARSP(6,N,L))/(3.D00+DFLOAT(ISPR(1,L))) !isebasti: included according to DSMC.f90       
  END DO  
!  
  IF (IVB == 0) VAR(1,N)=CELL(1,N)  
  IF (IVB == 1) THEN  
    C=(XB(2)+VELOB*FTIME-XB(1))/DFLOAT(NDIV)   !--new DDIV  
    VAR(1,N)=XB(1)+(DFLOAT(N-1)+0.5)*C  
  END IF  
  VAR(2,N)=SUM(0)  
  IF (SUM(1) > 0.5) THEN  
    VAR(3,N)=SUM(1)*A               !--number density Eqn. (4.28)   
    VAR(4,N)=VAR(3,N)*SUM(2)/SUM(1) !--density Eqn. (4.29)  
    VAR(5,N)=SUM(3)/SUM(2)          !--u velocity component Eqn. (4.30)  
    VAR(6,N)=SUM(4)/SUM(2)          !--v velocity component  
    VAR(7,N)=SUM(5)/SUM(2)          !--w velocity component  
    UU= VAR(5,N)**2+VAR(6,N)**2+VAR(7,N)**2  
    IF (SUM(1) > 1) THEN  
      VAR(8,N)=(ABS(SUM(6)-SUM(2)*UU))/(3.D00*BOLTZ*SUM(1))  !--translational temperature Eqn. (4.39) 
      VAR(19,N)=(ABS(SUM(10)-SUM(2)*VAR(5,N)**2))/(BOLTZ*SUM(1))  
      VAR(20,N)=(ABS(SUM(11)-SUM(2)*VAR(6,N)**2))/(BOLTZ*SUM(1))  
      VAR(21,N)=(ABS(SUM(12)-SUM(2)*VAR(7,N)**2))/(BOLTZ*SUM(1))  
    ELSE  
      VAR(8,N)=1.  
      VAR(19,N)=1.  
      VAR(20,N)=1.  
      VAR(21,N)=1.  
    END IF  
!--rotational temperature  
    IF (SUM(9) > 0.01D00) THEN  
      VAR(9,N)=(2.D00/BOLTZ)*SUM(8)/SUM(9)    !Eqn. (4.36)  
    ELSE  
      VAR(9,N)=0.  
    END IF
    DOF=(3.D00+SUM(9)/SUM(1))
!--vibration temperature default    
    VAR(10,N)=FTMP(1)  
!--overall temperature based on translation and rotation  
    VAR(11,N)=(3.*VAR(8,N)+(SUM(9)/SUM(1))*VAR(9,N))/DOF  
!--scalar pressure (now (from V3) based on the translational temperature)
    VAR(18,N)=VAR(3,N)*BOLTZ*VAR(8,N)  
!
!--Tvib calculations according to DSMC.f90
    IF (MMVM > 0) THEN
      DO L=1,MSP
        VDOF(L)=0.
        IF (ISPV(L) > 0) THEN
          DO K=1,ISPV(L)
            IF (CS(K+8,N,L) > 0.) THEN
              A=CS(K+8,N,L)/CS(1,N,L)
              TV(K,L)=SPVM(1,K,L)/DLOG(1.d0+BOLTZ*SPVM(1,K,L)/A)  !--Eqn.(4.45) - assuming SHO
              DF(N,K,L)=2.d0*A/(BOLTZ*TV(K,L)) !--Eqn. (11.28) Bird94 - general definition
            ELSE
              TV(K,L)=0.
              DF(N,K,L)=0.
            END IF
            VDOF(L)=VDOF(L)+DF(N,K,L)  !--Eqn.(4.49)
          END DO
          TVIB(L)=0.
          DO K=1,ISPV(L)
            IF (VDOF(L) > 1.D-6) THEN
              TVIB(L)=TVIB(L)+TV(K,L)*DF(N,K,L)/VDOF(L)  !--Eqn.(4.50)
            ELSE
              TVIB(L)=FVTMP(1)
            END IF  
          END DO  
        ELSE
          TVIB(L)=0. !TREF  !--isebasti: TREF is not defined
          VDOF(L)=0.
        END IF
        VARSP(7,N,L)=TVIB(L)
      END DO
      VDOFM=0.
      TVIBM=0.
      A=1.D00 !--isebasti: instead of 0
      DO L=1,MSP
        IF (ISPV(L) > 0) A=A+CS(1,N,L)
      END DO
      DO L=1,MSP
        IF (ISPV(L) > 0) THEN  
          VDOFM=VDOFM+VDOF(L)*CS(1,N,L)/A  !--Eqn.(4.51)
          TVIBM=TVIBM+TVIB(L)*CS(1,N,L)/A  !--Eqn.(4.52)
        END IF  
      END DO   
      VAR(10,N)=TVIBM
    END IF
!
!--convert the species velocity components to diffusion velocities
    DO L=1,MSP
      IF (VARSP(0,N,L) > 0.5) THEN  
        DO K=1,3 
          VARSP(K+8,N,L)=VARSP(K+8,N,L)-VAR(K+4,N) 
        END DO
      ELSE
        DO K=1,3
          VARSP(K+8,N,L)=0.D00  
        END DO
      END IF  
    END DO
!
!--reset the overall temperature and degrees of freedom (now including vibrational modes)
    IF (MMVM > 0) THEN
      DO L=1,MSP
        SDOF(L)=3.D00+ISPR(1,L)+VDOF(L)
        VARSP(8,N,L)=(3.*VARSP(5,N,L)+ISPR(1,L)*VARSP(6,N,L)+VDOF(L)*VARSP(7,N,L))/SDOF(L)  !species overall T                    
      END DO 
      A=0.D00
      B=0.D00
      DO L=1,MSP
        A=A+SDOF(L)*VARSP(8,N,L)*CS(1,N,L)
        B=B+SDOF(L)*CS(1,N,L)
      END DO  
      VAR(11,N)=A/B !mixture overall T
      DOF=DOF+VDOFM !--isebasti: included
    END IF 
!
!--Mach number   
    VAR(17,N)=DSQRT(VAR(5,N)**2+VAR(6,N)**2+VAR(7,N)**2)  
    VAR(12,N)=VAR(17,N)/SQRT((DOF+2.D00)*VAR(11,N)*(SUM(1)*BOLTZ/SUM(2))/DOF)  
!--average number of molecules in (collision) cell
    VAR(13,N)=SUM(0)/NSAMP/DFLOAT(NCIS)    
    IF (COLLS(N) > 2.) THEN  
!--mean collision time  
      VAR(14,N)=0.5D00*(FTIME-TISAMP)*(SUM(1)/NSAMP)/WCOLLS(N) 
!--mean free path (based on r.m.s speed with correction factor based on equilibrium)   
      VAR(15,N)=0.92132D00*DSQRT(DABS(SUM(7)/SUM(1)-UU))*VAR(14,N)  
      VAR(16,N)=CLSEP(N)/(COLLS(N)*VAR(15,N))  
    ELSE  
!--m.f.p set by nominal values          
      VAR(14,N)=1.D10  
      VAR(15,N)=1.D10/VAR(3,N)  
    END IF  
  ELSE  
    DO L=3,19  
      VAR(L,N)=0.  
    END DO  
  END IF  
! END DO  !loop is the same as in OUTPUT
END DO 
!
RETURN
END SUBROUTINE UPDATE_MP
!
!*****************************************************************************
!
SUBROUTINE UPDATE_BC
!
!--author: isebasti  
!--update inflow boundary conditions
!  
USE MOLECS  
USE CALC  
USE GEOM  
USE GAS  
USE OUTPUT
!  
IMPLICIT NONE
!
INTEGER :: NS,K,KK,L,N,NBC,KCELLS(2,10)      !need to set NBC and KCELLS in some commom block
REAL(KIND=8) :: A,B,AA,BB,SN,UBMEAN(2)  
!
!--set cells close to boundaries
!
NBC=1  !consider a maximum of NBC sampling cells from each boundary
IF (NCELLS < NBC) NBC=NCELLS
DO N=1,NBC
  KCELLS(1,N)=N
  KCELLS(2,N)=NCELLS-(N-1)
END DO
!
!--take mean velocities weighted by density
!
UBMEAN=0.
!
DO NS=1,2
  A=0.d0
  DO KK=1,NBC
    N=KCELLS(NS,KK) 
    UBMEAN(NS)=UBMEAN(NS)+VAR(4,N)*VAR(5,N)
    A=A+VAR(4,N)
  END DO
  UBMEAN(NS)=UBMEAN(NS)/A !=sum(rho*u)/sum(rho)
END DO
!
!--update inlet macroscopic properties velocities  
!
IF (IUBC == 1) THEN !trying to ensure boundary velocities near to VFX (assumed UFND and UFTMP are equal to freestream values)
  A=UBMEAN(1)-VFX(1)
  B=UBMEAN(2)-VFX(2)
!
  IF (DABS(A) >= 500.d0) UVFX(1)=UVFX(1)-1.0d0*A
  IF (DABS(B) >= 500.d0) UVFX(2)=UVFX(2)-1.0d0*B
!
  IF (DABS(A) >= 100.d0 .AND. DABS(A) < 500.d0) UVFX(1)=UVFX(1)-0.5d0*A
  IF (DABS(B) >= 100.d0 .AND. DABS(B) < 500.d0) UVFX(2)=UVFX(2)-0.5d0*B
!
  IF (DABS(A) >= 50.d0 .AND. DABS(A) < 100.d0) UVFX(1)=UVFX(1)-0.2d0*A
  IF (DABS(B) >= 50.d0 .AND. DABS(B) < 100.d0) UVFX(2)=UVFX(2)-0.2d0*B
!
  IF (DABS(A) >= 10.d0 .AND. DABS(A) < 50.d0) UVFX(1)=UVFX(1)-0.1d0*A
  IF (DABS(B) >= 10.d0 .AND. DABS(B) < 50.d0) UVFX(2)=UVFX(2)-0.1d0*B
!
  IF (DABS(A) >= 00.d0 .AND. DABS(A) < 10.d0) UVFX(1)=UVFX(1)-0.05d0*A
  IF (DABS(B) >= 00.d0 .AND. DABS(B) < 10.d0) UVFX(2)=UVFX(2)-0.05d0*B
END IF
!  
IF (IUBC == 2) THEN !pressure based boundary conditions (p and T are known/fixed at boundaries); see pg 66 in my Master's thesis
  DO K=1,2
    IF(K==1) N=1
    IF(K==2) N=NCELLS 
!
    A=VAR(17,N)/VAR(12,N)     !interior sound speed
    UFND(K)=FND(K)            !inlet number density
    UFTMP(K)=FTMP(K)          !inlet temperature
    B=UFND(K)*BOLTZ*UFTMP(K)  !inlet pressure
!
    IF (K == 1) UVFX(K)=VAR(5,N)+(B-VAR(18,N))/(VAR(4,N)*A)   !upstream velocity
    IF (K == 2) UVFX(K)=VAR(5,N)-(B-VAR(18,N))/(VAR(4,N)*A)   !downstream velocity
  END DO   
END IF
!
!--update inlet most probable speeds  
!
DO K=1,2  
  DO L=1,MSP
    UVMP(L,K)=SQRT(2.D0*BOLTZ*UFTMP(K)/SP(5,L)) !most probable speed
  END DO
END DO
!
!--update the entry quantities  
!  
DO K=1,2  
  IF (ITYPE(K) == 0) THEN  
    DO L=1,MSP      
      IF (K == 1) SN=UVFX(1)/UVMP(L,1)  
      IF (K == 2) SN=-UVFX(2)/UVMP(L,2)  
      AA=SN  
      A=1.D00+ERF(AA)  
      BB=DEXP(-SN**2)  
      ENTR(3,L,K)=SN  
      ENTR(4,L,K)=SN+SQRT(SN**2+2.D00)  
      ENTR(5,L,K)=0.5D00*(1.D00+SN*(2.D00*SN-ENTR(4,L,K)))  
      ENTR(6,L,K)=3.D00*UVMP(L,K)  
      B=BB+SPI*SN*A  
      ENTR(1,L,K)=(UFND(K)*FSP(L,K)*UVMP(L,K))*B/(FNUM*2.D00*SPI)  
      ENTR(2,L,K)=0.D00  
    END DO  
  END IF  
END DO  
!
RETURN
END SUBROUTINE UPDATE_BC
!
!*****************************************************************************
!
