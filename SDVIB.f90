SUBROUTINE SDVIB(L,TEMP,IVIB,K,IDT)
  USE GAS
  IMPLICIT NONE
  !-----------------------------------------------------
  !--set vibrational energy level based on Varand's O2 PES
  !--K mode of vibration
  !--L specie
  !--TEMP vibrational temperature
  !--IDT random number count
  !--IVIB vibrational energy level
  INTEGER :: L,IVIB,K,IDT
  REAL(KIND=8)  :: TEMP

  REAL(KIND=8),ALLOCATABLE :: P
  REAL(KIND=8) :: ZPAR=0.D00

  ALLOCATE(P(0:MDVIB(K,L)))
  DO I=0,MDVIB(K,L)
    P(I)=DEXP(-EDVIB(I,K,L)/TEMP)
    ZPAR=ZPAR+P(I)
  END DO

  CALL ZGF(RANF,IDT)  
  RANF=RANF*ZPAR

  ZPAR=0.D00
  DO IVIB=0,MDVIB(K,L)
    IF (RANF .LE. ZPAR2)THEN
      EXIT
    ELSE
      ZPAR=ZPAR+P(IVIB)  !accumulative probability
    END IF
  END DO
  IVIB=IVIB-1
    
END PROGRAM SDVIB

FUCTION VIBGEN(IVIB,KV,K)
USE GAS
USE CALC
IMPLICIT NONE
REAL(KIND=8) :: VIBGEN
INTEGER :: IVIB,KV,K
IF (AHVIB .EQ. 0)THEN
  VIBGEN=DFLOAT(IVIB)*BOLTZ*SPVM(1,KV,K)                        
ELSE IF (IDVIB(1) .EQ. 0)THEN
  VIBGEN=DFLOAT(IVIB)*BOLTZ*SPVM(1,KV,K)                        
ELSE
!  IF (IVIB .LE. MDVIB(KV,K))THEN
    VIBGEN=EDVIB(IVIB,KV,K)
 ! ELSE
  !  VIBGEN=EDVIB(MDVIB(KV,K),KV,K)
 ! END IF
  ! for safe reason
END IF
END FUNCTION VIBGEN
